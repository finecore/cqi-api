import SocketClient from "../socket/client/index.js";
import User from "../model/user.js";
import { verify, sign } from "../utils/jwt-util.js";

import AppInfo from "../model/app-info.js";

import _ from "lodash";
import moment from "moment";

import chalk from "chalk";
import PushHelper from "../helper/push-helper.js";

// Socket Server ips
const host = process.env.SERVER_SOC_HOST; // IP 는 구분자 ',' 로 서버별 로 설정.
const port = process.env.SERVER_SOC_PORT; // 포트는 동일 하게.

// Device 에서 수신된 데이터를 각 웹단말 (Web, Mobile 등)에 실시간 소켓 전송 한다.
// 분산 서버 환경에서는 Client 를 각 서버별로 생성 해서 동시에 전송 한다.
// Redis 사용을 안하는 이유는 Redis 서버를 따로 둬야 하며 소스가 복잡해지고 분산서버가 2 대 이상일 때 적용 할 예정.
class Dispatcher {
  constructor() {
    console.log(chalk.yellow("----------- Web Dispatcher Create ----------"));
    // 분산 서버 소켓 목록.
    this.clients = [];
    this.connect();

    this.pushHelper = new PushHelper();
    console.log(chalk.yellow("--------------------------------------------\n"));
  }

  connect() {
    // 분산 서버별 접속.
    if (host && port) {
      try {
        const client = new SocketClient(host, port);
        client.init();
        this.clients.push(client);
        console.log("-> Dispatcher append SocketClient ", client.host, client.port);
      } catch (err) {
        console.error("- Dispatcher ERROR ", err);
      }
    }
  }

  token(place_id, token, callback) {
    if (!token) {
      // 토큰 생성.
      sign({ channel: "api", place_id }, (_err, _token) => {
        if (!_err) {
          callback(_token);
        }
      });
    } else {
      callback(token);
    }
  }

  dispatch(json) {
    let {
      type = "",
      sendType = "ALL",
      headers: { url = "", token, method = "put", place_id },
      body,
    } = json;

    // 전송 타입 설정
    const result = this.getClientType(url);

    json.type = type = result.type;
    json.sendType = sendType = result.sendType;

    /*
    웹소켓 클라이언트 전송 시 사용 할 place_id
    API -> SOCKET -> Client 전송 시 API < -> SOCKET 서버 간에는 socket.place_id 가 최초 접속한 클라이언트만 저장 되므로 API 에서 내려주는 정보가 브라우저에 도달 하지 않는 문제 발생
    */
    if (!place_id && body && body.place_id) place_id = body.place_id;

    json.headers.place_id = place_id;

    console.log("\n");
    console.log("--------------------------------------------");
    console.log("------------- Dispatcher to socket clients", this.clients.length);
    console.log("--------------------------------------------");
    console.log("-> json ", json);

    if (url.indexOf("sync/") === 0) {
      console.log("---> sync 정보는 전파 하지 않습니다");
    } else {
      this.token(place_id, token, (_token) => {
        if (_token) {
          json.headers.token = _token;

          if (!place_id) {
            verify(_token, (err, auth) => {
              if (!err && auth.place_id) {
                json.headers.place_id = auth.place_id;
                this.send(url, json);
              }
            });
          } else {
            this.send(url, json);
          }
        }
      });
    }
  }

  send(url, json) {
    console.log("-> Dispatcher  client send ", { url, json });

    // 분산 Web Server list
    _.each(this.clients, (socket) => {
      socket.send(json);
      console.log("===> Dispatcher socket send ", socket.hostName);
    });

    this.push(json);
  }

  getClientType(url) {
    let type = "";
    let sendType = "";

    if (url) {
      // Vue 에서 사용하는 타입으로 변환한다.
      if (url.indexOf("isc/state") === 0) {
        type = "SET_ISC_STATE";
        sendType = "ALL";
      } else if (url.indexOf("isc/state/log") === 0) {
        type = "SET_ISC_STATE_LOG";
        sendType = "ALL";
      } else if (url.indexOf("isc/key/box") === 0) {
        type = "SET_ISC_KEY_BOX";
        sendType = "ALL";
      } else if (url.indexOf("isc/set") === 0) {
        type = "SET_ISC_SET";
        sendType = "ALL";
      } else if (url.indexOf("room/state") === 0) {
        type = "SET_ROOM_STATE";
        sendType = "ALL";
      } else if (url.indexOf("room/roomStateLog") === 0) {
        type = "SET_ROOM_STATE_LOG";
        sendType = "ALL";
      } else if (url.indexOf("room/sale") === 0) {
        type = "SET_ROOM_SALE";
        sendType = "ALL";
      } else if (url.indexOf("room/reserv") === 0) {
        type = "SET_ROOM_RESERV";
        sendType = "ALL";
      } else if (url.indexOf("device/item") === 0) {
        type = "SET_DEVICE";
        sendType = "ALL";
      } else if (url.indexOf("room/interrupt") === 0) {
        type = "SET_ROOM_INTERRUPT";
        sendType = "WEB|FRONT";
      } else {
        type = "";
        sendType = "ALL";
      }
    }

    const result = { type, sendType };

    console.log("-> Dispatcher getClientType : ", { sendType, type, url });

    return result;
  }

  push(json) {
    let {
      type,
      headers: { url = "", place_id },
      body,
    } = json;

    if (place_id) {
      AppInfo.selectPlaceAppInfos(place_id, (err, app_infos = []) => {
        if (!err) {
          let appPushs = [];

          let { room_state, room_sale, room_reserv } = body;

          let title = "이모나이코 알림";
          let message = "객실정보가 변경 되었습니다.";
          let room_id = 0;
          let data = body;

          console.log("- push body", { room_state, room_sale, room_reserv });

          if (room_state) {
            message = "객실 상태 정보 입니다.";
            room_id = room_state.room_id;
          } else if (room_sale) {
            message = "객실 판매 정보 입니다.";
            room_id = room_sale.room_id;
          } else if (room_reserv) {
            message = "객실 예약 정보 입니다.";
            room_id = room_reserv.room_id;
          }

          data.time = moment().format("HH:mm:ss");

          if (room_id) {
            _.map(app_infos, (info) => {
              console.log("- info", info);

              let { token, user_id, channel, os, version, badge } = info;

              let app_push = { token, title, message, place_id, room_id, data };

              // 마스터 앱
              if (channel === 1) {
                appPushs.push(app_push);
              }
              // 게스트 앱
              else if (channel === 2) {
                appPushs.push(app_push);
              }
              // 메이드 앱
              else if (channel === 3) {
                if (type === "SET_ROOM_STATE") {
                  // 청소 만.
                  if (room_state.clean !== undefined) {
                    app_push.message = room_state.clean ? "객실 청소요청 입니다." : "객실 청소가 완료 되었습니다.";

                    appPushs.push(app_push);
                  }
                }
              }
            });

            if (appPushs.length) {
              // app_push 테이블 등록.
              _.map(appPushs, (app_push, key) => {
                app_push.data = JSON.stringify(app_push.data);

                this.pushHelper.add(app_push, (res) => {
                  if (key === 0) {
                    // 각 채널 소켓으로 전파(메세지당 1번만 전송 한다.)
                    let app_push = res;
                    app_push.inserted = true;

                    this.pushHelper.sendSocket(app_push);
                  }
                });
              });

              // 바로 전송.
              this.pushHelper.sendAppPush();
            }
          }
        }
      });
    }
  }
}

export default Dispatcher;
