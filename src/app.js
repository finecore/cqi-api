import createError from 'http-errors';
import express from 'express';
import expressReactViews from 'express-react-views';
import path from 'path';
import { fileURLToPath } from 'url';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import cors from 'cors';
import _ from 'lodash';
import fs from 'fs'; // 파일 존재 여부를 확인하기 위해 fs 모듈 추가

// app routes..
import appRouters from './routes/index.js';
import {
  jsonRes,
  sendRes,
  isValidToken,
  checkPermission,
} from './utils/api-util.js';
import pkg from 'lodash';
import { HOST } from './api/config.js';

const { cloneDeep } = pkg;
var app = express();

// ES 모듈에서는 __dirname을 직접 사용할 수 없으므로, import.meta.url을 사용하여 대체
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

console.log('📂 Current __dirname:', __dirname);

// 정적 파일 서빙 경로 설정 (public 디렉토리에서 파일 제공)
app.use('/public', express.static(path.join(__dirname, 'public')));
app.use('/upload', express.static(path.join(__dirname, 'public/upload')));
app.use(express.static('public')); // public 폴더 내 정적 파일 제공

// 정적 파일 경로 디버깅 로그 추가
app.use((req, res, next) => {
  console.log(`Request URL: ${req.url}`);
  next();
});

// 인증서 발급을 위한 challenge 경로 설정
app.use(
  '/.well-known/acme-challenge',
  express.static(path.join(__dirname, '.well-known/acme-challenge')),
);

// Express에서 인증을 처리할 실제 서버 설정
app.get('/', (req, res) => {
  res.send('Hello, Express with SSL!');
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('view engine', 'jsx');
app.engine('jsx', expressReactViews.createEngine());

app.use(morgan('dev'));
app.use(express.json()); // JSON 데이터 파싱
app.use(express.urlencoded({ extended: true })); // URL encoded 데이터 파싱
app.use(cookieParser());
// public 디렉토리를 정적 파일 제공 경로로 설정
app.use(express.static(path.join(__dirname, 'public')));

const corsOptions = {
  origin: '*', // 모든 도메인 허용
};

app.use(cors(corsOptions));
app.options('*', cors(corsOptions)); // OPTIONS 요청에 대한 처리

// Middlewares
app.use(bodyParser.json({ limit: '100mb' }));

// JWT add.
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Headers', 'content-type, x-access-token');

  // 프록시 설정 시 /api가 붙어오면 삭제 처리한다.
  let { url, method, headers, body, auth } = cloneDeep(req);
  const channel = headers.channel;

  if (url) {
    url = url.replace(/^\/api/, '');
    req.url = url;
  }

  console.log('');
  console.log(
    '===================================================================',
  );
  console.log('=======> app req Sync Check', { url, method }, channel);
  console.log(
    '===================================================================',
  );

  next();
});

// 404 에러 핸들러 (파일 경로 점검 및 디버깅 로그)
app.use(function (err, req, res, next) {
  console.error(`❌ 404 Not Found: ${req.originalUrl}`);

  // 요청된 경로에 대한 실제 파일 경로를 확인
  const filePath = path.join(
    __dirname,
    'public/static',
    req.originalUrl.replace('/static', ''),
  );
  console.log('Checking file at path:', filePath); // 실제 파일 경로 출력

  // 파일이 존재하는지 확인
  if (!fs.existsSync(filePath)) {
    console.log(`File does not exist: ${filePath}`);
    // 404 오류와 함께 파일이 없음을 알리는 응답
    return jsonRes(req, res, {
      code: 404,
      message: `File not found: ${filePath}`,
      detail: `Requested file does not exist at the path: ${filePath}`,
    });
  }

  // 공통 포맷으로 오류 반환.
  jsonRes(req, res, {
    code: err.status || 500,
    message: err.message || err,
    detail:
      req.baseUrl +
      req.originalUrl +
      (err.stack && req.app.get('env') === 'development')
        ? ' at' + err.stack.split('at').join('\n ')
        : '',
  });
});

// App Routers.
appRouters(app);

export default app;
