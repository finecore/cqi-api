import _ from "lodash";

import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR, PREFERENCES } from "../constants/constants.js";

import qrcode from "qrcode";
import sharp from "sharp";
import jsqr from "jsqr";

// QR 코드 생성 API
const make_qr_code = (req, res) => {
  let { HOST_URL, deviceInfo, place_id, room_id } = req.body;

  const link = HOST_URL + "/" + place_id + "/" + room_id;

  QRCode.toDataURL(link, (err, url) => {
    if (err) {
      jsonRes(req, res, ERROR.MAKE_QR_ERROR, err.message);
    }
    var qr_data = {
      HOST_URL,
      deviceInfo,
      place_id,
      room_id,
      url,
    };

    console.log(">>> qr_data ", qr_data);

    jsonRes(req, res, err, { qr_data });
  });
};

// QR 코드 디코딩 함수
const decodeQRCode = (buffer, width, height) => {
  const { data } = jsQR(buffer, width, height);
  return data ? data : null;
};

// 이미지 업로드 및 QR 코드 읽기 API
const read_qr_code = async (req, res) => {
  let { place_id, room_id } = req.body;

  try {
    console.log("qr-img ", req.file);

    // 이미지 버퍼를 raw 데이터로 변환
    const { destination: path, filename: name, originalname, size, mimetype } = req.file;
    const { data, info } = await req.file.raw().toBuffer({ resolveWithObject: true });

    // QR 코드 디코딩
    const qr_data = decodeQRCode(data, info.width, info.height);

    let link = qr_data.split("/");

    if (place_id[link.length - 2] === place_id && room_id[link.length - 1] === room_id) {
      qr_data.read = true;
      jsonRes(req, res, err, { qr_data });
    } else {
      jsonRes(req, res, ERROR.READ_QR_ERROR, err.message);
    }
  } catch (error) {
    jsonRes(req, res, ERROR.READ_QR_ERROR, error.message);
  }
};

export default {
  make_qr_code,
  read_qr_code,
};
