import IscState from "../model/isc-state.js";
import IscStateLog from "../model/isc-state-log.js";
import IscStateLogController from "./isc-state-log.js";
import Device from "../model/device.js";

import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";

import _ from "lodash";

const list_a_isc_states = (req, res) => {
  let { place_id, filter = "1=1", limit = "10000", order = "a.reg_date", desc = "desc" } = req.params;

  if (place_id) filter = `b.place_id=${place_id}`;

  IscState.selectIscStateCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      IscState.selectIscStates(filter, order, desc, limit, (err, isc_states) => {
        jsonRes(req, res, err, { count, isc_states });
      });
  });
};

const list_place_isc_states = (req, res) => {
  let { place_id } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide list_place_isc_states place_id for select");
  } else {
    IscState.selectPlaceIscStates(place_id, (err, isc_states) => {
      jsonRes(req, res, err, { isc_states });
    });
  }
};

const read_a_isc_state = (req, res) => {
  const { serialno } = req.params;

  if (!serialno) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide isc_state serialno for select");
  } else {
    IscState.selectIscState(serialno, (err, isc_state) => {
      jsonRes(req, res, err, { isc_state });
    });
  }
};

const create_a_isc_state = (req, res) => {
  let { isc_state } = req.body; // post body.

  if (!isc_state.serialno) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide isc_state serialno  for insert");
  } else {
    IscState.insertIscState(isc_state, (err, info) => {
      if (info && info.insertId) req.body.isc_state.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info }, "IscState successfully inserted");
    });
  }
};

const update_a_isc_state = (req, res) => {
  const { serialno } = req.params;
  const { channel } = req.headers;
  const { isc_state } = req.body; // post body.

  if (!serialno) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide isc_state serialno for update");
  } else {
    Device.selectDeviceBySerialNo(serialno, (err, row) => {
      let { place_id, type } = row[0] || {};

      console.log("- update_a_isc_state", serialno, place_id, type, isc_state);

      if (!err) {
        if (type === "02") {
          isc_state.channel = channel;
          isc_state.serialno = serialno;

          // inert or update.
          IscState.updateIscState(isc_state, (err, info) => {
            jsonRes(req, res, err, { info }, "IscState successfully updated");

            isc_state.place_id = place_id;

            if (!err) {
              const {
                url,
                headers: { method, channel },
              } = req;

              // 단말/웹 으로 전파 한다.
              send_socket(req, isc_state); // 설정 정보 전파.
            }
          });
        } else {
          jsonRes(req, res, err, { info: null }, "IscState successfully updated");
        }
      } else {
        jsonRes(req, res, err, { info: null }, "IscState failure updated");
      }
    });
  }
};

const delete_a_isc_state = (req, res) => {
  const { serialno } = req.params;

  if (!serialno) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide isc_state serialno for delete");
  } else {
    // FOREIGN KEY Constraints table ON DELETE CASCADE ON UPDATE CASCADE
    IscState.deleteIscState(serialno, (err, info) => {
      jsonRes(req, res, err, { info }, "IscState successfully deleted");
    });
  }
};

// 소켓 서버로 전파.
const send_socket = (req, isc_state) => {
  const {
    url,
    headers: { method = "put", channel, "x-access-token": xtoken, token, uuid = "" },
  } = req;

  const json = {
    type: "SET_ISC_STATE", // 웹 reducer type, 어드민/모바일 store 타입은 socket server 에서 각각 변경.
    headers: {
      method,
      url,
      token: token || xtoken,
      channel: "api",
      req_channel: channel,
      uuid, // 웹 브라우저 고유 번호.
      place_id: isc_state.place_id, // 업소 정보.
      serialno: isc_state.serialno, // 장비 일련번호(소켓 전송 시 장비 구분용)
    },
    body: {
      isc_state,
    },
  };

  // console.log("---> api send_socket", json);

  // 소켓 서버로 전송.
  global.dispatcher.dispatch(json);
};

export default {
  list_a_isc_states,
  list_place_isc_states,
  read_a_isc_state,
  create_a_isc_state,
  update_a_isc_state,
  delete_a_isc_state,
};
