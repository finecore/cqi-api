import RoomSale from "../model/room-sale.js";
import RoomSalePay from "../model/room-sale-pay.js";
import RoomState from "../model/room-state.js";

import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { deepEqual } from "../utils/data-util.js";
import { ERROR, PREFERENCES } from "../constants/constants.js";
import moment from "moment";
import _ from "lodash";

import makeExcel from "../helper/excel-helper.js";

const list_all_room_sale = (req, res) => {
  const {
    place_id,
    filter = "1=1",
    between = "check_in",
    begin = moment().add(-1, "month").format("YYYY-MM-DD 00:00"),
    end = moment().format("YYYY-MM-DD HH:mm"),
    order = "a.id desc",
    excel = "",
  } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place_id for get room sale list");
  } else {
    RoomSale.selectAllRoomSales(place_id, filter, between, begin, end, order, (err, all_room_sales) => {
      if (!excel) jsonRes(req, res, err, { all_room_sales });
      else {
        let name = "매출 정보";
        makeExcel("SaleSearch", name, all_room_sales, res, (err, name) => {
          console.log("- download ", err, name);
        });
      }
    });
  }
};

const list_now_room_sale = (req, res) => {
  const { place_id, filter = "1=1", order = "a.id desc" } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room sales place_id for select");
  } else {
    RoomSale.selectNowRoomSales(place_id, filter, order, (err, all_room_sales) => {
      jsonRes(req, res, err, { all_room_sales });
    });
  }
};

const list_a_room_sale = (req, res) => {
  const {
    room_id,
    filter = "state = 'A'",
    between = "check_in",
    begin = moment().add(-1, "month").format("YYYY-MM-DD 00:00"),
    end = moment().format("YYYY-MM-DD HH:mm"),
    order = "a.id desc",
    limit = "10000",
    excel = false,
  } = req.params;

  if (!room_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room sales room_id for select");
  } else {
    RoomSale.selectRoomSales(room_id, filter, between, begin, end, order, limit, (err, room_sales) => {
      if (!excel) jsonRes(req, res, err, { room_sales });
      else {
        let name = "매출 정보";
        makeExcel("SaleSearch", name, room_sales, res, (err, name) => {
          console.log("- download ", err, name);
        });
      }
    });
  }
};

const read_a_room_sale = (req, res) => {
  if (!req.params.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room sale id for select");
  } else {
    RoomSale.selectRoomSale(req.params.id, (err, room_sale) => {
      jsonRes(req, res, err, { room_sale });
    });
  }
};

const read_a_room_sale_sum_place = (req, res) => {
  const { place_id, begin = moment().format("YYYY-MM-DD 00:00"), end = moment().format("YYYY-MM-DD 23:59") } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide all room sale place id for select");
  } else {
    RoomSale.selectPlaceRoomSale(place_id, begin, end, (err, room_sales) => {
      jsonRes(req, res, err, { room_sales });
    });
  }
};

const read_a_room_sale_sum_room = (req, res) => {
  const { place_id, begin = moment().format("YYYY-MM-DD 00:00"), end = moment().format("YYYY-MM-DD 23:59") } = req.params;

  RoomSale.selectSumRoomSale(place_id, begin, end, (err, room_sales) => {
    jsonRes(req, res, err, { room_sales });
  });
};

const read_a_room_sale_sum = (req, res) => {
  const { room_id, begin = moment().format("YYYY-MM-DD 00:00"), end = moment().format("YYYY-MM-DD 23:59") } = req.params;

  if (!room_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room sale sum room id for select");
  } else {
    RoomSale.selectRoomSaleSum(room_id, begin, end, (err, room_sales) => {
      jsonRes(req, res, err, { room_sales });
    });
  }
};

const create_a_room_sale = (req, res) => {
  let new_room_sale = req.body.room_sale; // post body.
  const { room_id, stay_type } = new_room_sale;

  console.log("-- create_a_room_sale", new_room_sale);

  if (!room_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room sale room_id for insert");
  } else {
    // 매출 중복 판매 여부 검사.
    RoomSale.selectIsRoomSaleNow(room_id, stay_type, (err, row) => {
      let { count = 0, sale } = row[0] || {};

      // 중복 시.
      if (count) {
        jsonRes(req, res, err, "No room sale error");
      } else {
        delete new_room_sale.check_out;

        RoomSale.insertRoomSale(new_room_sale, req, (err, info) => {
          if (info && info.insertId) req.body.room_sale.id = info.insertId; // 채널 전송 시 id 입력.

          jsonRes(req, res, err, { info }, "RoomSale successfully inserted");

          const {
            headers: { token, channel, uuid },
            auth: { place_id }, // token 에서 추출 한 정보.
          } = req;

          if (info && info.insertId) {
            new_room_sale.id = info.insertId;
            new_room_sale.reg_date = new Date();

            let {
              state = "A",
              channel: _channel,
              user_id = "",
              phone = "",
              stay_type,
              rollback = 0,
              serialno,
              default_fee = 0,
              add_fee = 0,
              pay_card_amt = 0,
              pay_cash_amt = 0,
              pay_point_amt = 0,
              prepay_ota_amt = 0,
              prepay_card_amt = 0,
              prepay_cash_amt = 0,
              prepay_point_amt = 0,
              save_point = 0,
              card_approval_num,
              card_merchant,
              card_no,
            } = new_room_sale;

            console.log("- new_room_sale", new_room_sale);

            let new_room_sale_pay = {
              state,
              channel: _channel || channel,
              first_pay: 1, // 매출의 첫 결재.
              sale_id: info.insertId,
              user_id,
              phone,
              stay_type,
              rollback,
              serialno,
              default_fee,
              add_fee,
              pay_card_amt,
              pay_cash_amt,
              pay_point_amt,
              prepay_ota_amt,
              prepay_card_amt,
              prepay_cash_amt,
              prepay_point_amt,
              save_point,
              card_approval_num,
              card_merchant,
              card_no,
              fee_total: Number(default_fee) + Number(add_fee),
            };

            console.log("- new_room_sale_pay", new_room_sale_pay);

            RoomSalePay.insertRoomSalePay(new_room_sale_pay, (err, info) => {
              console.log("- RoomSalePay successfully inserted");
            });

            const json = {
              type: "SET_ROOM_SALE", // 웹 reducer type, 어드민/모바일 store 타입은 socket server 에서 각각 변경.
              headers: {
                method: "post",
                url: "room/sale/" + info.insertId,
                token,
                channel: "api",
                uuid,
                req_channel: channel,
              },
              body: {
                room_sale: new_room_sale,
                place_id,
              },
            };

            // 소켓 서버로 전송.
            global.dispatcher.dispatch(json);
          }
        });
      }
    });
  }
};

const update_a_checkout_by_room_id = (req, res) => {
  const { room_sale } = req.body; // post body.
  const { room_id } = req.params;

  if (!room_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room sale data for update");
  } else {
    let clone_sale = _.cloneDeep(room_sale);

    delete clone_sale["keyless"];
    delete clone_sale["qr_key_yn"];
    // delete clone_sale["id"];

    console.log("- update_a_checkout_by_room_id", clone_sale);

    RoomSale.selectRoomSale(room_id, (err, result) => {
      if (!err && result[0]) {
        const row = result[0];

        RoomSale.updateCheckOut(room_id, (err, info) => {
          jsonRes(req, res, err, { info }, "RoomSale checkOut is successfully updated");

          const {
            headers: { token, channel, uuid },
            auth: { place_id }, // token 에서 추출 한 정보.
          } = req;

          if (!err) {
            // 매출 상태에서만 결제 정보 추가.
            if (row.stay_type) {
              let {
                state,
                channel: _channel,
                room_id,
                user_id,
                phone,
                stay_type,
                rollback,
                default_fee,
                add_fee,
                pay_card_amt,
                pay_cash_amt,
                pay_point_amt,
                prepay_ota_amt,
                prepay_card_amt,
                prepay_cash_amt,
                prepay_point_amt,
                save_point,
                card_approval_num,
                card_merchant,
                card_no,
                move_from,
                prev_stay_type,
              } = clone_sale;

              // 수정 유저 추가
              user_id = user_id || row.user_id;
              phone = phone || row.phone;
              stay_type = stay_type || row.stay_type;
              // rollback = rollback || row.rollback;

              if (rollback === 1) {
                default_fee = -row.default_fee || 0;
                add_fee = -row.add_fee || 0;
                pay_card_amt = -row.pay_card_amt || 0;
                pay_cash_amt = -row.pay_cash_amt || 0;
                pay_point_amt = -row.pay_point_amt || 0;
                prepay_ota_amt = -row.prepay_ota_amt || 0;
                prepay_card_amt = -row.prepay_card_amt || 0;
                prepay_cash_amt = -row.prepay_cash_amt || 0;
                prepay_point_amt = -row.prepay_point_amt || 0;
                save_point = -row.save_point || 0;
              } else {
                default_fee = default_fee - row.default_fee || 0;
                add_fee = add_fee - row.add_fee || 0;
                pay_card_amt = pay_card_amt - row.pay_card_amt || 0;
                pay_cash_amt = pay_cash_amt - row.pay_cash_amt || 0;
                pay_point_amt = pay_point_amt - row.pay_point_amt || 0;
                prepay_ota_amt = prepay_ota_amt - row.prepay_ota_amt || 0;
                prepay_card_amt = prepay_card_amt - row.prepay_card_amt || 0;
                prepay_cash_amt = prepay_cash_amt - row.prepay_cash_amt || 0;
                prepay_point_amt = prepay_point_amt - row.prepay_point_amt || 0;
                save_point = save_point - row.save_point || 0;
              }

              let fee_total = row.default_fee + row.add_fee + default_fee + add_fee || 0;

              // 요금 정보 있을경우만 저장.
              if (
                state ||
                phone ||
                default_fee ||
                add_fee ||
                rollback ||
                pay_card_amt ||
                pay_cash_amt ||
                pay_point_amt ||
                prepay_ota_amt ||
                prepay_card_amt ||
                prepay_cash_amt ||
                prepay_point_amt ||
                save_point ||
                move_from ||
                prev_stay_type
              ) {
                // 금액 변경 여부 체크.
                let new_room_sale_pay = {
                  state,
                  channel: _channel || channel,
                  first_pay: 0,
                  sale_id: id,
                  user_id,
                  phone,
                  stay_type,
                  rollback,
                  default_fee,
                  add_fee,
                  pay_card_amt,
                  pay_cash_amt,
                  pay_point_amt,
                  prepay_ota_amt,
                  prepay_card_amt,
                  prepay_cash_amt,
                  prepay_point_amt,
                  save_point,
                  card_approval_num,
                  card_merchant,
                  card_no,
                  fee_total,
                  move_from,
                  prev_stay_type,
                };

                console.log("- new_room_sale_pay", new_room_sale_pay);

                RoomSalePay.insertRoomSalePay(new_room_sale_pay, (err, info) => {
                  console.log("- RoomSalePay successfully inserted");
                });
              }

              const json = {
                type: "SET_ROOM_SALE", // 웹 reducer type, 어드민/모바일 store 타입은 socket server 에서 각각 변경.
                headers: {
                  method: "put",
                  url: "room/sale/roomid/" + room_id,
                  token,
                  channel: "api",
                  uuid,
                  req_channel: channel,
                },
                body: {
                  room_sale,
                  place_id,
                },
              };

              // 소켓 서버로 전송.
              global.dispatcher.dispatch(json);
            }

            PREFERENCES.Get(row.place_id, (preference) => {
              const { pms, user_key_in_check_out_power_off_time } = preference; // PMS 정보조회.

              console.log("---> PREFERENCES  ", clone_sale.room_id, clone_sale.state, { pms, user_key_in_check_out_power_off_time });

              if (clone_sale.room_id && clone_sale.state !== "A" && user_key_in_check_out_power_off_time === 0) {
                console.log("---> 고객카드 삽입 상태 퇴실 즉시 전원차단.");

                RoomState.selectRoomState(clone_sale.room_id, (err, room_state) => {
                  if (room_state && room_state[0]) {
                    const { room_id, use_auto_power_off, main_relay, state, key, name } = room_state[0];

                    console.log("- room_state ", { room_id, use_auto_power_off, main_relay, state, key, name });

                    // 전원 차단 사용 이고, 전원 공급 상태고 판매 상태가 아니고 고객키가 아직 있을때.
                    if (use_auto_power_off && main_relay === 1 && state !== "A" && key === 3) {
                      let new_state = {
                        channel: "api",
                        user_id: "auto",
                        main_relay: 0, // 메인 전원 차단
                      };

                      RoomState.updateRoomState(room_id, new_state, req, pms, (err, info) => {
                        console.log("---> 고객카드 삽입 상태 퇴실 시 전원 차단!");

                        const {
                          headers: { token, channel, uuid },
                          auth: { place_id }, // token 에서 추출 한 정보.
                        } = req;

                        const json = {
                          type: "SET_ROOM_STATE", // 웹 reducer type, 어드민/모바일 store 타입은 socket server 에서 각각 변경.
                          headers: {
                            method: "put",
                            url: "room/state/" + room_id,
                            token,
                            channel: "api",
                          },
                          body: {
                            room_state: {
                              room_id,
                              user_id: "auto",
                              main_relay: 0,
                              name,
                            },
                            place_id,
                          },
                        };

                        // 소켓 서버로 전송.
                        global.dispatcher.dispatch(json);
                      });
                    }
                  }
                });
              }
            });
          }
        });
      } else {
        jsonRes(req, res, err, "No room sale error");
      }
    });
  }
};

const update_a_room_sale = (req, res) => {
  const { room_sale } = req.body; // post body.
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room sale data for update");
  } else {
    let clone_sale = _.cloneDeep(room_sale);

    console.log("- update_a_room_sale", clone_sale);

    RoomSale.selectRoomSale(id, (err, result) => {
      if (!err && result[0]) {
        const row = result[0];

        RoomSale.updateRoomSale(id, room_sale, req, (err, info) => {
          jsonRes(req, res, err, { info }, "RoomSale successfully updated");

          const {
            headers: { token, channel, uuid },
            auth: { place_id }, // token 에서 추출 한 정보.
          } = req;

          if (!err) {
            // 매출 상태에서만 결제 정보 추가.
            if (row.stay_type) {
              let {
                state,
                channel: _channel,
                room_id,
                user_id,
                phone,
                stay_type,
                rollback,
                default_fee,
                add_fee,
                pay_card_amt,
                pay_cash_amt,
                pay_point_amt,
                prepay_ota_amt,
                prepay_card_amt,
                prepay_cash_amt,
                prepay_point_amt,
                save_point,
                card_approval_num,
                card_merchant,
                card_no,
                move_from,
                prev_stay_type,
              } = clone_sale;

              // 수정 유저 추가
              user_id = user_id || row.user_id;
              phone = phone || row.phone;
              stay_type = stay_type || row.stay_type;
              // rollback = rollback || row.rollback;

              if (rollback === 1) {
                default_fee = -row.default_fee || 0;
                add_fee = -row.add_fee || 0;
                pay_card_amt = -row.pay_card_amt || 0;
                pay_cash_amt = -row.pay_cash_amt || 0;
                pay_point_amt = -row.pay_point_amt || 0;
                prepay_ota_amt = -row.prepay_ota_amt || 0;
                prepay_card_amt = -row.prepay_card_amt || 0;
                prepay_cash_amt = -row.prepay_cash_amt || 0;
                prepay_point_amt = -row.prepay_point_amt || 0;
                save_point = -row.save_point || 0;
              } else {
                default_fee = default_fee - row.default_fee || 0;
                add_fee = add_fee - row.add_fee || 0;
                pay_card_amt = pay_card_amt - row.pay_card_amt || 0;
                pay_cash_amt = pay_cash_amt - row.pay_cash_amt || 0;
                pay_point_amt = pay_point_amt - row.pay_point_amt || 0;
                prepay_ota_amt = prepay_ota_amt - row.prepay_ota_amt || 0;
                prepay_card_amt = prepay_card_amt - row.prepay_card_amt || 0;
                prepay_cash_amt = prepay_cash_amt - row.prepay_cash_amt || 0;
                prepay_point_amt = prepay_point_amt - row.prepay_point_amt || 0;
                save_point = save_point - row.save_point || 0;
              }

              let fee_total = row.default_fee + row.add_fee + default_fee + add_fee || 0;

              // 요금 정보 있을경우만 저장.
              if (
                state ||
                phone ||
                default_fee ||
                add_fee ||
                rollback ||
                pay_card_amt ||
                pay_cash_amt ||
                pay_point_amt ||
                prepay_ota_amt ||
                prepay_card_amt ||
                prepay_cash_amt ||
                prepay_point_amt ||
                save_point ||
                move_from ||
                prev_stay_type
              ) {
                // 금액 변경 여부 체크.
                let new_room_sale_pay = {
                  state,
                  channel: _channel || channel,
                  first_pay: 0,
                  sale_id: id,
                  user_id,
                  phone,
                  stay_type,
                  rollback,
                  default_fee,
                  add_fee,
                  pay_card_amt,
                  pay_cash_amt,
                  pay_point_amt,
                  prepay_ota_amt,
                  prepay_card_amt,
                  prepay_cash_amt,
                  prepay_point_amt,
                  save_point,
                  card_approval_num,
                  card_merchant,
                  card_no,
                  fee_total,
                  move_from,
                  prev_stay_type,
                };

                console.log("- new_room_sale_pay", new_room_sale_pay);

                RoomSalePay.insertRoomSalePay(new_room_sale_pay, (err, info) => {
                  console.log("- RoomSalePay successfully inserted");
                });
              }

              const json = {
                type: "SET_ROOM_SALE", // 웹 reducer type, 어드민/모바일 store 타입은 socket server 에서 각각 변경.
                headers: {
                  method: "put",
                  url: "room/sale/" + id,
                  token,
                  channel: "api",
                  uuid,
                  req_channel: channel,
                },
                body: {
                  room_sale,
                  place_id,
                },
              };

              // 소켓 서버로 전송.
              global.dispatcher.dispatch(json);
            }

            PREFERENCES.Get(row.place_id, (preference) => {
              const { pms, user_key_in_check_out_power_off_time } = preference; // PMS 정보조회.

              console.log("---> PREFERENCES  ", clone_sale.room_id, clone_sale.state, { pms, user_key_in_check_out_power_off_time });

              if (clone_sale.room_id && clone_sale.state !== "A" && user_key_in_check_out_power_off_time === 0) {
                console.log("---> 고객카드 삽입 상태 퇴실 즉시 전원차단.");

                RoomState.selectRoomState(clone_sale.room_id, (err, room_state) => {
                  if (room_state && room_state[0]) {
                    const { room_id, use_auto_power_off, main_relay, state, key, name } = room_state[0];

                    console.log("- room_state ", { room_id, use_auto_power_off, main_relay, state, key, name });

                    // 전원 차단 사용 이고, 전원 공급 상태고 판매 상태가 아니고 고객키가 아직 있을때.
                    if (use_auto_power_off && main_relay === 1 && state !== "A" && key === 3) {
                      let new_state = {
                        channel: "api",
                        user_id: "auto",
                        main_relay: 0, // 메인 전원 차단
                      };

                      RoomState.updateRoomState(room_id, new_state, req, pms, (err, info) => {
                        console.log("---> 고객카드 삽입 상태 퇴실 시 전원 차단!");

                        const {
                          headers: { token, channel, uuid },
                          auth: { place_id }, // token 에서 추출 한 정보.
                        } = req;

                        const json = {
                          type: "SET_ROOM_STATE", // 웹 reducer type, 어드민/모바일 store 타입은 socket server 에서 각각 변경.
                          headers: {
                            method: "put",
                            url: "room/state/" + room_id,
                            token,
                            channel: "api",
                          },
                          body: {
                            room_state: {
                              room_id,
                              user_id: "auto",
                              main_relay: 0,
                              name,
                            },
                            place_id,
                          },
                        };

                        // 소켓 서버로 전송.
                        global.dispatcher.dispatch(json);
                      });
                    }
                  }
                });
              }
            });
          }
        });
      } else {
        jsonRes(req, res, err, "No room sale error");
      }
    });
  }
};

const delete_a_room_sale = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room sale id for delete");
  } else {
    RoomSale.selectRoomSale(id, (err, result) => {
      if (!err && result[0]) {
        const row = result[0];

        RoomSale.deleteRoomSale(id, (err, info) => {
          jsonRes(req, res, err, { info }, "RoomSale successfully deleted");
        });
      } else {
        jsonRes(req, res, err, "No room sale error");
      }
    });
  }
};

export default {
  list_all_room_sale,
  list_a_room_sale,
  list_now_room_sale,
  read_a_room_sale,
  read_a_room_sale_sum_place,
  read_a_room_sale_sum,
  read_a_room_sale_sum_room,
  create_a_room_sale,
  update_a_room_sale,
  update_a_checkout_by_room_id,
  delete_a_room_sale,
};
