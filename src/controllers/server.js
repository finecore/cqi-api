import Server from "../model/server.js";
import File from "../model/file.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";

const list_a_servers = (req, res) => {
  let { filter = "1=1" } = req.params;

  Server.selectServerCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      Server.selectServers(filter, (err, servers) => {
        jsonRes(req, res, err, { count, servers });
      });
  });
};

const read_a_server = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Pleservere provide server id for select");
  } else {
    Server.selectServer(id, (err, server) => {
      jsonRes(req, res, err, { server });
    });
  }
};

const create_a_server = (req, res) => {
  const { server } = req.body;

  Server.insertServer(server, (err, info) => {
    if (info && info.insertId) req.body.server.id = info.insertId; // 채널 전송 시 id 입력.

    jsonRes(req, res, err, { info }, "Server successfully inserted");
  });
};

const update_a_server = (req, res) => {
  const { id } = req.params;
  const { server } = req.body;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Pleservere provide server id for update");
  } else {
    Server.updateServer(id, server, (err, info) => {
      jsonRes(req, res, err, { info }, "Server successfully updated");
    });
  }
};

const delete_a_server = (req, res) => {
  const { id } = req.params;
  const { server } = req.body;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Pleservere provide server id for delete");
  } else {
    Server.deleteServer(id, (err, info) => {
      jsonRes(req, res, err, { info }, "Server successfully deleted");
    });
  }
};

export default {
  list_a_servers,
  create_a_server,
  read_a_server,
  update_a_server,
  delete_a_server,
};
