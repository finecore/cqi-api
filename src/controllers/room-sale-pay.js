import RoomSalePay from "../model/room-sale-pay.js";
import RoomState from "../model/room-state.js";

import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import moment from "moment";

import makeExcel from "../helper/excel-helper.js";

const list_all_room_sale_pay = (req, res) => {
  const {
    place_id,
    filter = "1=1",
    between = "a.reg_date",
    begin = moment().add(-1, "month").format("YYYY-MM-DD 00:00"),
    end = moment().format("YYYY-MM-DD HH:mm"),
    order = "a.id desc",
    excel = "",
  } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place_id for get room sale_pay list");
  } else {
    RoomSalePay.selectAllRoomSalePays(place_id, filter, between, begin, end, order, (err, all_room_sale_pays) => {
      if (!excel) jsonRes(req, res, err, { all_room_sale_pays });
      else {
        let name = "매출 정보";
        makeExcel("SaleSearch", name, all_room_sale_pays, res, (err, name) => {
          console.log("- download ", err, name);
        });
      }
    });
  }
};

const list_a_room_sale_pay = (req, res) => {
  const { room_id, filter = "1=1", between = "a.reg_date", begin = moment().add(-1, "month").format("YYYY-MM-DD 00:00"), end = moment().format("YYYY-MM-DD HH:mm"), order = "a.id desc" } = req.params;

  if (!room_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room sale_pays room_id for select");
  } else {
    RoomSalePay.selectRoomSalePaysByRoomId(room_id, filter, between, begin, end, order, (err, room_sale_pays) => {
      jsonRes(req, res, err, { room_sale_pays });
    });
  }
};

const list_a_room_sale_pay_by_member = (req, res) => {
  const {
    member_id,
    filter = "1=1",
    between = "a.reg_date",
    begin = moment().add(-1, "month").format("YYYY-MM-DD 00:00"),
    end = moment().format("YYYY-MM-DD HH:mm"),
    order = "a.id desc",
  } = req.params;

  if (!member_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room sale_pays member_id for select");
  } else {
    RoomSalePay.selectRoomSalePaysByMemberId(member_id, filter, between, begin, end, order, (err, room_sale_pays) => {
      jsonRes(req, res, err, { room_sale_pays });
    });
  }
};

const read_a_room_sale_pay = (req, res) => {
  if (!req.params.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room sale_pay id for select");
  } else {
    RoomSalePay.selectRoomSalePay(req.params.id, (err, room_sale_pay) => {
      jsonRes(req, res, err, { room_sale_pay });
    });
  }
};

const read_a_room_sale_pay_sum_place = (req, res) => {
  const { place_id, begin = moment().format("YYYY-MM-DD 00:00"), end = moment().format("YYYY-MM-DD 23:59") } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide all room sale_pay place id for select");
  } else {
    RoomSalePay.selectPlaceRoomSalePay(place_id, begin, end, (err, room_sale_pays) => {
      jsonRes(req, res, err, { room_sale_pays });
    });
  }
};

const read_a_room_sale_pay_sum = (req, res) => {
  const { room_id, begin = moment().format("YYYY-MM-DD 00:00"), end = moment().format("YYYY-MM-DD 23:59") } = req.params;

  if (!room_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room sale_pay sum room id for select");
  } else {
    RoomSalePay.selectRoomSalePaySum(room_id, begin, end, (err, room_sale_pays) => {
      jsonRes(req, res, err, { room_sale_pays });
    });
  }
};

const read_all_room_sale_pay_sum = (req, res) => {
  const { begin = moment().format("YYYY-MM-DD 00:00"), end = moment().format("YYYY-MM-DD 23:59") } = req.params;

  RoomSalePay.selectRoomSalePaySumAll(begin, end, (err, room_sale_pays) => {
    jsonRes(req, res, err, { room_sale_pays });
  });
};

const create_a_room_sale_pay = (req, res) => {
  const new_room_sale_pay = req.body.room_sale_pay; // post body.
  const { sale_id } = new_room_sale_pay;

  if (!sale_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room sale_pay data for insert");
  } else {
    // 중복 시.
    if (count) {
      jsonRes(req, res, ERROR.DUPLICATE_ROOM_SALE_PAY, { count }, "RoomSalePay failure inserted");
    } else {
      RoomSalePay.insertRoomSalePay(new_room_sale_pay, (err, info) => {
        if (info && info.insertId) req.body.room_sale_pay.id = info.insertId; // 채널 전송 시 id 입력.

        jsonRes(req, res, err, { info }, "RoomSalePay successfully inserted");
      });
    }
  }
};

const update_a_room_sale_pay = (req, res) => {
  const { id } = req.params;
  const { room_sale_pay } = req.body; // post body.

  if (!id || !room_sale_pay) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room sale_pay data for update");
  } else {
    RoomSalePay.updateRoomSalePay(id, room_sale_pay, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomSalePay successfully updated");
    });
  }
};

const update_a_room_sale_pay_by_member = (req, res) => {
  const { member_id } = req.params;
  const { room_sale_pay } = req.body; // post body.

  if (!member_id || !room_sale_pay) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room sale_pay data for update");
  } else {
    RoomSalePay.updateRoomSalePayByMemberId(member_id, room_sale_pay, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomSalePay successfully updated");
    });
  }
};

const delete_a_room_sale_pay = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room sale_pay id for delete");
  } else {
    RoomSalePay.deleteRoomSalePay(id, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomSalePay successfully deleted");
    });
  }
};

export default {
  list_all_room_sale_pay,
  list_a_room_sale_pay,
  list_a_room_sale_pay_by_member,
  read_a_room_sale_pay,
  read_a_room_sale_pay_sum_place,
  read_a_room_sale_pay_sum,
  read_all_room_sale_pay_sum,
  create_a_room_sale_pay,
  update_a_room_sale_pay,
  delete_a_room_sale_pay,
  update_a_room_sale_pay_by_member,
};
