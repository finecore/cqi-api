import jsonwebtoken from "jsonwebtoken";
import Member from "../model/member.js";
import { jsonRes } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import { verify, sign } from "../utils/jwt-util.js";
import crypto from "crypto";
import { encHash, decHash } from "../utils/crypto-util.js";

const member_login = (req, res) => {
  let {
    headers: { channel = "app" },
    body: { id, pwd, isEnc, carrier },
  } = req;

  // console.log("login request:", req.body);

  // 최소 6자리
  if (pwd?.length > 20) {
    // 암호화되었다면 복호화 한다.
    if (isEnc) {
      pwd = decHash(pwd);
      // console.log("====> login decHash pwd:", pwd);

      // 해시 생성
      let shasum = crypto.createHash("sha1");

      shasum.update(pwd);

      // 비밀번호 sha1 형식으로 암호화.
      pwd = shasum.digest("hex");

      console.log("- pwd sha1 ", pwd);
    }
  } else {
    return jsonRes(req, res, ERROR.INVALID_ARGUMENT, "비밀번호는 최소 5자리 입니다.");
  }

  // console.log("login request:", req.body.pwd.length, pwd);

  // query.
  Member.selectMemberById(id, (err, rows) => {
    let member = rows ? rows[0] : null;

    // console.log("login selectMemberById:", member);

    if (err) {
      return jsonRes(req, res, err);
    } else if (!member) {
      jsonRes(req, res, ERROR.NO_DATA, "아이디 와 비밀번호를 확인해 주세요.");
    } else if (member.pwd != pwd) {
      jsonRes(req, res, ERROR.INVALID_MEMBER_PARAMS);
    } else if (member.use_yn !== "Y") {
      jsonRes(req, res, ERROR.INVALID_MEMBER); // 사용자 사용 체크.
    } else {
      // JWT(Json Web Token) 생성.
      const { channel, id, birth, email, carrier, pms } = member;
      const {
        headers: { uuid = "" },
      } = req;

      Member.updateLastLoginDate(id, (err, info) => {
        console.log(`- updateLastLoginDate :: info=${info}, err=${err}`);
      }); // 마지막 로그인 시간 업데이트.

      const payload = { channel: "app", id, birth, email, carrier, uuid };

      sign(payload, function (err, token) {
        console.log(`-----------> member_login token  `, { token, payload });

        return jsonRes(req, res, err, { member, token });
      });
    }
  });
};

const sharePwd = "Imo!@Naico";

const member_share_login = (req, res) => {
  let {
    headers: { channel = "app" },
    body: { id, pwd, isEnc, carrier },
  } = req;

  // console.log("login request:", req.body);

  // 최소 6자리
  pwd = decHash(pwd);
  // console.log("====> login decHash pwd:", pwd);

  console.log("- pwd sha1 ", pwd);

  if (pwd !== sharePwd) {
    return jsonRes(req, res, ERROR.INVALID_ARGUMENT, "비밀번호가 올바르지 않습니다.");
  }

  // console.log("login request:", req.body.pwd.length, pwd);

  // query.
  Member.selectMemberById(id, (err, rows) => {
    let member = rows ? rows[0] : null;

    // console.log("login selectMemberById:", member);

    if (err) {
      return jsonRes(req, res, err);
    } else if (!member) {
      jsonRes(req, res, ERROR.NO_DATA, "아이디 와 비밀번호를 확인해 주세요.");
    } else if (member.pwd != pwd) {
      jsonRes(req, res, ERROR.INVALID_MEMBER_PARAMS);
    } else if (member.use_yn !== "Y") {
      jsonRes(req, res, ERROR.INVALID_MEMBER); // 사용자 사용 체크.
    } else {
      // JWT(Json Web Token) 생성.
      const { channel, id, birth, email, carrier, pms } = member;
      const {
        headers: { uuid = "" },
      } = req;

      Member.updateLastLoginDate(id, (err, info) => {
        console.log(`- updateLastLoginDate :: info=${info}, err=${err}`);
      }); // 마지막 로그인 시간 업데이트.

      const payload = { channel: "app", id, birth, email, carrier, uuid };

      sign(payload, function (err, token) {
        console.log(`-----------> member_login token  `, { token, payload });

        return jsonRes(req, res, err, { member, token });
      });
    }
  });
};

const create_a_member = (req, res) => {
  const { member } = req.body;

  // console.log("-------------> create_a_member", member);

  if (!member.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide member id for insert");
  } else {
    let pwd = member.pwd;

    // 암호화되었다면 복호화 한다.
    if (pwd.length >= 20) {
      pwd = decHash(pwd);
      console.log("====> login decHash pwd:", pwd);
    }

    // 해시 생성
    let shasum = crypto.createHash("sha1");

    shasum.update(pwd);

    // 비밀번호 sha1 형식으로 암호화.
    member.pwd = shasum.digest("hex");

    console.log("- pwd sha1 ", member.pwd);

    Member.insertMember(member, (err, info) => {
      if (err) {
        jsonRes(req, res, err);
      } else {
        if (info && info.insertId) member.id = info.insertId; // 채널 전송 시 id 입력.
        jsonRes(req, res, err, { info, member }, "Company successfully inserted");
      }
    });
  }
};

const read_a_member = (req, res) => {
  const { id } = req.params;

  console.log("====> read_a_member:", id);

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Plemaile provide member id for select");
  } else {
    Member.selectMember(id, (err, member) => {
      jsonRes(req, res, err, { member });
    });
  }
};

const list_a_members = (req, res) => {
  let { filter = "1=1", limit = "10000", order = "a.level", desc = "asc", place_id = 0 } = req.params;

  console.log("-> filter", filter);

  Member.selectMemberCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      Member.selectMembers(filter, order, desc, limit, (err, members) => {
        jsonRes(req, res, err, { count, members });
      });
  });
};

const update_a_member = (req, res) => {
  const { id } = req.params;
  const { member } = req.body;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Plemaile provide member id for update");
  } else {
    Member.selectMember(id, (err, result) => {
      result = result[0] || result;

      if (result) {
        if (member.pwd) {
          let pwd = member.pwd;

          // 암호화되었다면 복호화 한다.
          if (pwd.length >= 20) {
            pwd = decHash(pwd);
            console.log("====> login decHash pwd:", pwd);
          }

          // 해시 생성
          let shasum = crypto.createHash("sha1");

          shasum.update(pwd);

          // 비밀번호 sha1 형식으로 암호화.
          member.pwd = shasum.digest("hex");

          console.log("- pwd sha1 ", result.pwd, "===", member.pwd);

          if (result.pwd === member.pwd) {
            return jsonRes(req, res, ERROR.INVALID_SAME_PWD);
          }
        }
        Member.updateMember(id, member, (err, info) => {
          jsonRes(req, res, err, { info, member }, "Member successfully updated");
        });
      } else {
        jsonRes(req, res, ERROR.NO_DATA);
      }
    });
  }
};

const delete_a_member = (req, res) => {
  const { id } = req.params;
  const { member } = req.body;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Plemaile provide member id for delete");
  } else {
    Member.deleteMember(id, (err, info) => {
      jsonRes(req, res, err, { info }, "Member successfully deleted");
    });
  }
};

const member_sync_login = (req, res) => {
  let {
    headers: { channel = "front" },
    body: { id, carrier },
  } = req;

  console.log("member_sync_login request:", req.body);

  // query.
  Member.selectMemberById(id, (err, rows) => {
    let member = rows ? rows[0] : null;

    if (err) {
      return jsonRes(req, res, err);
    } else if (!member) {
      jsonRes(req, res, ERROR.NO_DATA, "아이디 와 비밀번호를 확인해 주세요.");
    } else if (member.use_yn !== "Y") {
      jsonRes(req, res, ERROR.INVALID_MEMBER); // 사용자 사용 체크.
    } else {
      // JWT(Json Web Token) 생성.
      const { id, birth, email, carrier } = member;
      const {
        headers: { channel = "app", uuid = "" },
      } = req;

      Member.updateLastLoginDate(id, (err, info) => {
        console.log(`- updateLastLoginDate :: info=${info}, err=${err}`);
      }); // 마지막 로그인 시간 업데이트.

      const payload = { channel, id, birth, email, carrier, uuid };

      sign(payload, function (err, token) {
        return jsonRes(req, res, err, { member, token });
      });
    }
  });
};

const create_sync_token = (req, res) => {
  const { id } = req.params;

  console.log("-----> create_sync_token id ", id);

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Member id 가 없습니다.");
  } else {
    // query.
    Member.selectMemberById(id, (err, rows) => {
      let member = rows ? rows[0] : null;

      console.log("- create_sync_token ", member);

      if (err) {
        return jsonRes(req, res, err);
      } else {
        // JWT(Json Web Token) 생성.
        const { channel = "sync", id, birth, email, carrier } = member;

        const payload = { channel, id, birth, email, carrier };

        sign(payload, function (err, token) {
          return jsonRes(req, res, err, { token, member });
        });
      }
    });
  }
};

const create_member_token = (req, res) => {
  const {
    headers: { id },
  } = req;

  console.log("-----> create_member_token id ", id);

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, " id 가 없습니다.");
  } else {
    const member = rows ? rows[0] : null;

    console.log("- create_member_token ", member);

    if (err) {
      return jsonRes(req, res, err);
    } else if (!member) {
      jsonRes(req, res, ERROR.NO_DATA);
    } else if (!member.expire) {
      // 장비 인증 만료 체크. carrier (01: IDM, 02: ISG, 10:RPT)
      jsonRes(req, res, member.carrier === "01" ? ERROR.IDM_EXPIRE_CERTIFICATION : member.carrier === "02" ? ERROR.ISG_EXPIRE_CERTIFICATION : ERROR.EXPIRE_CERTIFICATION);
    } else if (!member.birth || !member.place_expire) {
      // 업소 인증 만료 체크.
      jsonRes(req, res, ERROR.EXPIRE_CERTIFICATION);
    } else {
      // JWT(Json Web Token) 생성.
      const { channel = "member", id, birth, email, carrier } = member;

      const payload = { channel, id, birth, email, carrier };

      sign(payload, function (err, token) {
        return jsonRes(req, res, err, { token, member });
      });
    }
  }
};

const delete_member_token = (req, res) => {
  const { headers: id } = req;

  if (id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, " id 가 없습니다.");
  } else {
    // JWT(Json Web Token) 생성.
    const payload = ({ id } = req.params);

    sign(
      payload,
      function (err, token) {
        return jsonRes(req, res, err, { token });
      },
      -1 // 생성 후 만료 시킴.
    );
  }
};

const alive = (req, res) => {
  jsonRes(req, res, null, { alive: true });
};

export default {
  create_a_member,
  read_a_member,
  list_a_members,
  update_a_member,
  delete_a_member,
  member_login,
  member_share_login,
  member_sync_login,
  create_sync_token,
  create_member_token,
  delete_member_token,
  alive,
};
