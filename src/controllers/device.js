import Device from "../model/device.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";

const list_a_devices = (req, res) => {
  let { filter = "1=1", limit = "10000", order = "a.id", desc = "desc" } = req.params;

  Device.selectDeviceCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      Device.selectDevices(filter, order, desc, limit, (err, devices) => {
        jsonRes(req, res, err, { count, devices });
      });
  });
};

const list_place_devices = (req, res) => {
  const { place_id } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place_id for get device list");
  } else {
    Device.selectPlaceDevices(place_id, (err, devices) => {
      jsonRes(req, res, err, { devices });
    });
  }
};

const read_a_device = (req, res) => {
  const { id, serialno } = req.params;

  if (!id && !serialno) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide device id or serialno for select");
  } else {
    Device.selectDevice(id, serialno, (err, device) => {
      jsonRes(req, res, err, { device });
    });
  }
};

const create_a_device = (req, res) => {
  let new_device = req.body.device; // post body.

  if (!new_device.place_id || !new_device.name) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide device data for insert");
  } else {
    delete new_device["company_id"];
    delete new_device["company_name"];
    delete new_device["place_name"];

    Device.insertDevice(new_device, (err, info) => {
      if (info && info.insertId) req.body.device.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info }, "Device successfully inserted");
    });
  }
};

const update_a_device = (req, res) => {
  const { id } = req.params;
  let device = req.body.device; // post body.

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide device id for update");
  } else {
    delete device["company_id"];
    delete device["company_name"];
    delete device["place_name"];

    Device.updateDevice(id, device, (err, info) => {
      jsonRes(req, res, err, { info }, "Device successfully updated");
    });
  }
};

const update_a_device_serialno = (req, res) => {
  const { serialno } = req.params;
  let device = req.body.device; // post body.

  if (!serialno) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide device serialno for update");
  } else {
    delete device["company_id"];
    delete device["company_name"];
    delete device["place_name"];

    Device.updateDeviceSerialno(serialno, device, (err, info) => {
      jsonRes(req, res, err, { info }, "Device successfully updated");
    });
  }
};

const delete_a_device = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide device id for delete");
  } else {
    // FOREIGN KEY Constraints table ON DELETE CASCADE ON UPDATE CASCADE
    Device.deleteDevice(id, (err, info) => {
      jsonRes(req, res, err, { info }, "Device successfully deleted");
    });
  }
};

export default {
  list_a_devices,
  list_place_devices,
  create_a_device,
  read_a_device,
  update_a_device,
  update_a_device_serialno,
  delete_a_device,
};
