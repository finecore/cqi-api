import jsonwebtoken from "jsonwebtoken";
import { jsonRes } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import { verify, sign } from "../utils/jwt-util.js";
import crypto from "crypto";
import { encHash, decHash } from "../utils/crypto-util.js";
import MemberPreference from "../model/member-preference.js";

const list_a_member_preferences = (req, res) => {
  let { filter = "1=1", limit = "10000", order = "a.member_id", desc = "asc" } = req.params;

  MemberPreference.selectMemberPreferencesCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      MemberPreference.selectMemberPreferences(filter, order, desc, limit, (err, member_preferences) => {
        jsonRes(req, res, err, { count, member_preferences });
      });
  });
};

const create_a_member_preference = (req, res) => {
  const { member_preference } = req.body; // post body.

  if (!member_preference || !member_preference.member_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide member_preference member_id for insert");
  } else {
    MemberPreference.insertMemberPreference(member_preference, (err, info) => {
      jsonRes(req, res, err, { info, member_preference }, "MemberPreference successfully inserted");
    });
  }
};

const read_a_member_preference = (req, res) => {
  const { member_id } = req.params;

  if (!member_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "MemberPreference provide member_preference member_id for select");
  } else {
    MemberPreference.selectMemberPreference(member_id, (err, member_preference) => {
      jsonRes(req, res, err, { member_preference });
    });
  }
};

const update_a_member_preference = (req, res) => {
  const { member_id } = req.params;
  const { member_preference } = req.body;

  if (!member_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "MemberPreference provide member_preference member_id for update");
  } else {
    MemberPreference.updateMemberPreference(member_id, member_preference, (err, info) => {
      jsonRes(req, res, err, { info }, "MemberPreference successfully updated");
    });
  }
};

const delete_a_member_preference = (req, res) => {
  const { member_id } = req.params;
  const { member_preference } = req.body;

  if (!member_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "MemberPreference provide member_preference member_id for delete");
  } else {
    MemberPreference.deleteMemberPreference(member_id, (err, info) => {
      jsonRes(req, res, err, { info }, "MemberPreference successfully deleted");
    });
  }
};

export default {
  create_a_member_preference,
  read_a_member_preference,
  list_a_member_preferences,
  update_a_member_preference,
  delete_a_member_preference,
};
