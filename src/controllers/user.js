import User from "../model/user.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import crypto from "crypto";
import { encHash, decHash } from "../utils/crypto-util.js";
import { verify, sign } from "../utils/jwt-util.js";

const list_a_users = (req, res) => {
  let { filter = "1=1", limit = "10000", order = "a.level", desc = "asc", place_id = 0 } = req.params;

  if (place_id) filter = "a.place_id=" + place_id;

  console.log("-> filter", filter);

  User.selectUserCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      User.selectUsers(filter, order, desc, limit, (err, users) => {
        jsonRes(req, res, err, { count, users });
      });
  });
};

const read_a_user = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide user id for select");
  } else {
    User.selectUser(id, (err, user) => {
      // console.log("-------> user", user);
      jsonRes(req, res, err, { user });
    });
  }
};

const create_a_user = (req, res) => {
  const { user } = req.body; // post body.

  console.log("- create_a_user", user);

  if (!user.id || !user.pwd || !user.name) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide user place_id ,id ,pwd ,name for insert");
  } else {
    let pwd = user.pwd;

    // 최소 6자리
    if (pwd && pwd.length > 6) {
      // 암호화되었다면 복호화 한다.
      if (pwd.length >= 20) {
        pwd = decHash(pwd);
        console.log("====> login decHash pwd:", pwd);
      }

      // 해시 생성
      let shasum = crypto.createHash("sha1");

      shasum.update(pwd);

      // 비밀번호 sha1 형식으로 암호화.
      user.pwd = shasum.digest("hex");

      console.log("- pwd sha1 ", user.pwd);
    } else {
      return jsonRes(req, res, ERROR.INVALID_ARGUMENT, "비밀번호는 최소 6자리 입니다.");
    }

    // query.
    User.selectUserById(user.id, (err, rows) => {
      let rowUser = rows ? rows[0] : null;

      if (rowUser?.id) {
        return jsonRes(req, res, ERROR.ALEADY_USER); // 사용자 사용 체크.
      } else if (err) {
        return jsonRes(req, res, err);
      } else {
        // JWT(Json Web Token) 생성.
        const { channel, id, place_id, level, type, pms, imonaico_url } = user;
        const {
          headers: { uuid = "" },
        } = req;

        User.insertUser(user, (err, info) => {
          const payload = { channel, id, place_id, level, type, uuid, pms, imonaico_url };

          sign(payload, function (err, token) {
            return jsonRes(req, res, err, { user, token });
          });
        });
      }
    });
  }
};

const update_a_user = (req, res) => {
  const { id } = req.params;
  const { user } = req.body; // post body.

  if (!id || !user) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide user id or data for update");
  } else {
    if (user.pwd) {
      // 해시 생성
      var shasum = crypto.createHash("sha1");
      shasum.update(user.pwd);

      // 비밀번호 sha1 형식으로 암호화.
      user.pwd = shasum.digest("hex");
      console.log("- pwd sha1 ", user.pwd);
    } else {
      // 비밀번호 입력 안하면 해당 항목 제거.
      delete user.pwd;
    }

    delete user.pwd_confirm;

    if (user.id && user.id !== id) {
      User.selectUser(user.id, (err, sel_user) => {
        if (sel_user[0]) {
          jsonRes(req, res, ERROR.INVALID_USER_ID, user.id + "는 이미 사용 중인 아이디 입니다.");
        } else {
          User.updateUser(id, user, (err, info) => {
            jsonRes(req, res, err, { info }, "User successfully updated");
          });
        }
      });
    } else {
      User.updateUser(id, user, (err, info) => {
        jsonRes(req, res, err, { info }, "User successfully updated");
      });
    }
  }
};

const delete_a_user = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide user id for delete");
  } else {
    // FOREIGN KEY Constraints table ON DELETE CASCADE ON UPDATE CASCADE
    User.deleteUser(id, (err, info) => {
      jsonRes(req, res, err, { info }, "User successfully deleted");
    });
  }
};

export default {
  list_a_users,
  create_a_user,
  read_a_user,
  update_a_user,
  delete_a_user,
};
