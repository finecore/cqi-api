import MailReceiver from "../model/mail-receiver.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import crypto from "crypto";

const list_a_mail_receivers = (req, res) => {
  let { filter = "1=1", limit = "10000", order = "a.id", desc = "asc" } = req.params;

  MailReceiver.selectMailReceiverCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      MailReceiver.selectMailReceivers(filter, order, desc, limit, (err, mail_receivers) => {
        jsonRes(req, res, err, { count, mail_receivers });
      });
  });
};

const read_a_mail_receiver = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide mail_receiver id for select");
  } else {
    MailReceiver.selectMailReceiver(id, (err, mail_receiver) => {
      jsonRes(req, res, err, { mail_receiver });
    });
  }
};

const create_a_mail_receiver = (req, res) => {
  const { mail_receiver } = req.body; // post body.

  if (!mail_receiver || !mail_receiver.user_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide mail_receiver user_id for insert");
  } else {
    MailReceiver.insertMailReceiver(mail_receiver, (err, info) => {
      if (info && info.insertId) req.body.mail_receiver.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info }, "MailReceiver successfully inserted");
    });
  }
};

const update_a_mail_receiver = (req, res) => {
  const { id } = req.params;
  const { mail_receiver } = req.body; // post body.

  if (!id || !mail_receiver) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide mail_receiver id or data for update");
  } else {
    MailReceiver.updateMailReceiver(id, mail_receiver, (err, info) => {
      jsonRes(req, res, err, { info }, "MailReceiver successfully updated");
    });
  }
};

const delete_a_mail_receiver = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide mail_receiver id for delete");
  } else {
    MailReceiver.deleteMailReceiver(id, (err, info) => {
      jsonRes(req, res, err, { info }, "MailReceiver successfully deleted");
    });
  }
};

export default {
  list_a_mail_receivers,
  create_a_mail_receiver,
  read_a_mail_receiver,
  update_a_mail_receiver,
  delete_a_mail_receiver,
};
