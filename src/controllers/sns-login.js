import Member from '../model/member.js';
import { jsonRes } from '../utils/api-util.js';
import { ERROR } from '../constants/constants.js';
import { verify, sign } from '../utils/jwt-util.js';
import { encHash, decHash } from '../utils/crypto-util.js';

import passport from 'passport';
import GoogleStrategy from 'passport-google-oauth20';
import KakaoStrategy from 'passport-kakao';
import NaverStrategy from 'passport-naver';
import FacebookStrategy from 'passport-facebook';

// 사용자 인증 후 세션 처리
passport.serializeUser((user, done) => done(null, user));
passport.deserializeUser((obj, done) => done(null, obj));

const login = (id, req, res) => {
  Member.selectMemberById(id, (err, rows) => {
    const user = rows ? rows[0] : null;

    if (err) {
      return jsonRes(req, res, err);
    } else if (!user) {
      return jsonRes(
        req,
        res,
        ERROR.NO_DATA,
        '아이디와 비밀번호를 확인해 주세요.',
      );
    } else if (user.pwd !== req.body.pwd) {
      return jsonRes(req, res, ERROR.INVALID_USER_PWD);
    } else if (req.body.type && user.type !== req.body.type) {
      return jsonRes(req, res, ERROR.INVALID_USER_TYPE);
    } else if (user.use_yn !== 'Y') {
      return jsonRes(req, res, ERROR.INVALID_USER);
    } else if (!user.place_expire) {
      return jsonRes(req, res, ERROR.EXPIRE_CERTIFICATION);
    } else if (req.body.channel === 'admin' && user.level > 2) {
      return jsonRes(req, res, ERROR.INVALID_AUTHORITY);
    } else {
      const { channel, id, place_id, level, type, pms, imonaico_url } = user;
      const uuid = req.headers?.uuid || '';

      Member.updateLastLoginDate(id, (updateErr, info) => {
        if (updateErr) {
          console.error(`Failed to update last login: ${updateErr}`);
        }
        console.log(`Last login updated: ${info}`);
      });

      const payload = {
        channel,
        id,
        place_id,
        level,
        type,
        uuid,
        pms,
        imonaico_url,
      };

      sign(payload, (tokenErr, token) => {
        if (tokenErr) {
          return jsonRes(req, res, tokenErr);
        }
        return jsonRes(req, res, null, { user, token });
      });
    }
  });
};

// 구글 SNS 로그인
const google_sns_login = (req, res) => {
  console.log('==========> google_sns_login Start');

  // TODO 클라드 배포 후 checkqin.com 으로 변경함.
  passport.use(
    new GoogleStrategy(
      {
        clientID:
          '352607177116-3hkqfjb5cjqinuon4g4aei2krv0pgd5q.apps.googleusercontent.com',
        clientSecret: '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',
        callbackURL:
          (process.env.VITE_USE_HTTPS === 'Y'
            ? 'https://app.checkqin.com'
            : 'http://localhost:80') + '/auth/google/callback',
      },
      (accessToken, refreshToken, profile, done) => {
        if (profile) {
          console.log('==========> google_sns_login', { ...profile });
          login(profile.id, req, res);
        } else {
          return jsonRes(
            req,
            res,
            ERROR.INVALID_ARGUMENT,
            'Google profile not found',
          );
        }
      },
    ),
  );
};

// 카카오 SNS 로그인
const kakao_sns_login = (req, res) => {
  passport.use(
    new KakaoStrategy(
      {
        clientID: '0ce0de1542630ad75f25d54d92b4e2f9',
        callbackURL:
          (process.env.VITE_USE_HTTPS === 'Y'
            ? 'https://app.checkqin.com'
            : 'http://localhost:80') + '/auth/kakao/callback',
      },
      (accessToken, refreshToken, profile, done) => {
        if (profile) {
          login(profile.id, req, res);
        } else {
          return jsonRes(
            req,
            res,
            ERROR.INVALID_ARGUMENT,
            'Kakao profile not found',
          );
        }
      },
    ),
  );
};

// 네이버 SNS 로그인
const naver_sns_login = (req, res) => {
  passport.use(
    new NaverStrategy(
      {
        clientID: 'BhSBuX4dXnTJItOZCwBa',
        clientSecret: 'CzXM4y7S0_',
        callbackURL:
          (process.env.VITE_USE_HTTPS === 'Y'
            ? 'https://app.checkqin.com'
            : 'http://localhost:80') + '/auth/naver/callback',
      },
      (accessToken, refreshToken, profile, done) => {
        if (profile) {
          login(profile.id, req, res);
        } else {
          return jsonRes(
            req,
            res,
            ERROR.INVALID_ARGUMENT,
            'Naver profile not found',
          );
        }
      },
    ),
  );
};

// 페이스북 SNS 로그인
const facebook_sns_login = (req, res) => {
  passport.use(
    new FacebookStrategy(
      {
        clientID: '978621120829669',
        clientSecret: '00deda231cd64282992751c57e106cbb',
        callbackURL: 'https://localhost:3000/auth/facebook/callback',
      },
      (accessToken, refreshToken, profile, done) => {
        if (profile) {
          login(profile.id, req, res);
        } else {
          return jsonRes(
            req,
            res,
            ERROR.INVALID_ARGUMENT,
            'Facebook profile not found',
          );
        }
      },
    ),
  );
};

export default {
  google_sns_login,
  kakao_sns_login,
  facebook_sns_login,
  naver_sns_login,
};
