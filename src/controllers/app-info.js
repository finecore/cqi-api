import AppInfo from "../model/app-info.js";

import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";

import _ from "lodash";

const list_a_app_infos = (req, res) => {
  let { place_id, filter = "1=1", limit = "10000", order = "a.reg_date", desc = "desc" } = req.params;

  if (place_id) filter = `b.place_id=${place_id}`;

  AppInfo.selectAppInfoCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      AppInfo.selectAppInfos(filter, order, desc, limit, (err, app_infos) => {
        jsonRes(req, res, err, { count, app_infos });
      });
  });
};

const list_place_app_infos = (req, res) => {
  let { place_id } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide list_place_app_infos place_id for select");
  } else {
    AppInfo.selectPlaceAppInfos(place_id, (err, app_infos) => {
      jsonRes(req, res, err, { app_infos });
    });
  }
};

const read_a_app_info = (req, res) => {
  const { token } = req.params;

  if (!token) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide app_info token for select");
  } else {
    AppInfo.selectAppInfo(token, (err, app_info) => {
      jsonRes(req, res, err, { app_info });
    });
  }
};

const create_a_app_info = (req, res) => {
  let { app_info } = req.body; // post body.

  if (!app_info || !app_info.token) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide app_info token  for insert");
  } else {
    AppInfo.insertAppInfo(app_info, (err, info) => {
      jsonRes(req, res, err, { info }, "AppInfo successfully inserted");
    });
  }
};

const update_a_app_info = (req, res) => {
  const { token } = req.params;
  let { app_info } = req.body;

  if (!token) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide app_info token for update");
  } else {
    AppInfo.updateAppInfo(token, app_info, (err, info) => {
      jsonRes(req, res, err, { info }, "AppInfo successfully updated");
    });
  }
};

const delete_a_app_info = (req, res) => {
  const { token } = req.params;

  if (!token) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide app_info token for delete");
  } else {
    // FOREIGN KEY Constraints table ON DELETE CASCADE ON UPDATE CASCADE
    AppInfo.deleteAppInfo(token, (err, info) => {
      jsonRes(req, res, err, { info }, "AppInfo successfully deleted");
    });
  }
};

export default {
  list_a_app_infos,
  list_place_app_infos,
  read_a_app_info,
  create_a_app_info,
  update_a_app_info,
  delete_a_app_info,
};
