import RoomTheme from "../model/room-theme.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import moment from "moment";

const list_all_room_theme = (req, res) => {
  RoomTheme.selectAllRoomThemes((err, all_room_themes) => {
    jsonRes(req, res, err, { all_room_themes });
  });
};

const read_a_room_theme = (req, res) => {
  if (!req.params.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room theme id for select");
  } else {
    RoomTheme.selectRoomTheme(req.params.id, (err, room_theme) => {
      jsonRes(req, res, err, { room_theme });
    });
  }
};

const create_a_room_theme = (req, res) => {
  var new_room_theme = req.body.room_theme; // post body.

  if (!new_room_theme.room_type_id || !new_room_theme.day || !new_room_theme.hour) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room theme data for insert");
  } else {
    RoomTheme.insertRoomTheme(new_room_theme, (err, info) => {
      if (info && info.insertId) req.body.room_theme.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info }, "RoomTheme successfully inserted");
    });
  }
};

const update_a_room_theme = (req, res) => {
  let { room_theme } = req.body; // post body.

  if (!room_theme || !room_theme.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room theme data for update");
  } else {
    RoomTheme.updateRoomTheme(room_theme.id, room_theme, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomTheme successfully updated");
    });
  }
};

const delete_a_room_theme = (req, res) => {
  if (!req.params.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room theme id for delete");
  } else {
    RoomTheme.deleteRoomTheme(req.params.id, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomTheme successfully deleted");
    });
  }
};

export default {
  list_all_room_theme,
  read_a_room_theme,
  create_a_room_theme,
  update_a_room_theme,
  delete_a_room_theme,
};
