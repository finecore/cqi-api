import { jsonRes } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import Table from "../model/table.js";

const read_a_comment = (req, res) => {
  const { table_name } = req.params;
  if (!table_name) table_name = req.body.table_name;

  if (!table_name) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide com table for select");
  } else {
    Table.selectComment(table_name, (err, comment) => {
      jsonRes(req, res, err, { comment });
    });
  }
};

export default {
  read_a_comment,
};
