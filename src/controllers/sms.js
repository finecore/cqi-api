import Sms from "../model/sms.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import { TWILIO_PHONE_NUMBER, NICE_KEY, NICE_SEC } from "../api/config.js";
import { encHash, decHash } from "../utils/crypto-util.js";

const makeAuthNo = (n) => {
  var value = "";
  for (var i = 0; i < n; i++) {
    let n1 = 0;
    let n2 = 9;
    value += parseInt(Math.random() * (n2 - n1 + 1)) + n1;
  }
  setTimeout(() => {
    console.log("=====================================================================");
    console.log("===  make authNo >>>>>>>>>>>>>>>>> ", value, " <<<<<<<<<<<<<<<<<");
    console.log("=====================================================================");
  }, 3000);

  return value;
};

const list_a_smses = (req, res) => {
  let { filter = "1=1", limit = "10000", order = "a.tr_num", desc = "asc" } = req.params;

  Sms.selectSmsCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      Sms.selectSmses(filter, order, desc, limit, (err, smses) => {
        jsonRes(req, res, err, { count, smses });
      });
  });
};

const read_a_sms = (req, res) => {
  const { tr_num } = req.params;

  if (!tr_num) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide sms id or phone for select");
  } else {
    Sms.selectSms(tr_num, (err, sms) => {
      jsonRes(req, res, err, { count, sms });
    });
  }
};

const send_auth_no = (req, res) => {
  let { phone } = req.params;

  phone = "" + phone.replace(/-/gi, "");
  console.log("-- send_auth_no phone", phone);

  let authNo = makeAuthNo(6);

  let place_id = 0;

  if (phone) {
    Sms.selectSmsPlaceId(phone, (err, row) => {
      if (row && row[0] && row[0].place_id) {
        console.log("-- selectSmsPlaceId row", row);
        place_id = row[0].place_id;
        console.log("-- selectSmsPlaceId place_id", place_id);

        // let sms = {
        //   tr_senddate: new Date(),
        //   tr_sendstat: "0",
        //   tr_msgtype: "0",
        //   tr_callback: TWILIO_PHONE_NUMBER,
        //   tr_phone: phone,
        //   tr_msg: "[이모나이코 인증번호]\n" + authNo,
        //   tr_etc1: place_id,
        // };

        // Authorization: Basic ${Authorization}

        let sms = {
          tr_senddate: new Date(),
          tr_sendstat: "0",
          tr_msgtype: "0",
          tr_callback: TWILIO_PHONE_NUMBER,
          tr_phone: phone,
          tr_msg: "[이모나이코 인증번호]\n" + authNo,
          tr_etc1: place_id,
        };

        console.log("- authNo", authNo);

        if (!sms.tr_phone) {
          jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide sms data for insert");
        } else {
          Sms.insertSms(sms, (err, info) => {
            const encAuthNo = encHash(authNo);
            jsonRes(req, res, err, { info, encAuthNo }, "Sms successfully inserted");
          });
        }
      } else {
        jsonRes(req, res, ERROR.NO_DATA, "Has Not place_id data for selectSmsPlaceId");
      }
    });
  } else {
    jsonRes(req, res, ERROR.NO_DATA, "Please provide phone data for selectSmsPlaceId");
  }
};

const check_auth_no = (req, res) => {
  let { authNo } = req.params;

  console.log("-- send_auth_no authNo", authNo);

  // if (!authNo) {
  //   jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide phone data for selectSmsByPhone");
  // } else {
  //   // 암호화와 평문을 전송해야 해서 클라이언트단에서 비교 한다(사용안함)
  //   const encAuthNo = encHash(authNo);
  //   if(decAuthNo === authNo)
  //     jsonRes(req, res, err, { info, authNo }, "Sms successfully inserted");
  //   });
  // }
};

const create_a_sms = (req, res) => {
  let { sms } = req.body;

  console.log("- sms", sms);

  let place_id = 0;

  Sms.selectSmsPlaceId(sms.tr_phone, (err, place) => {
    if (place) {
      place_id = place.place_id;
    }
  });

  if (!sms.tr_senddate) sms.tr_senddate = new Date();
  if (!sms.tr_sendstat) sms.tr_sendstat = "0";
  if (!sms.tr_msgtype) sms.tr_msgtype = "0";
  if (!sms.tr_callback) sms.tr_callback = TWILIO_PHONE_NUMBER;
  if (!sms.tr_etc1) sms.tr_etc1 = place_id;

  if (!sms.tr_phone || !sms.tr_msg) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide sms data for insert");
  } else {
    Sms.insertSms(sms, (err, info) => {
      if (info && info.insertId) req.body.sms.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info }, "Sms successfully inserted");
    });
  }
};

const update_a_sms = (req, res) => {
  const { tr_num } = req.params;
  let { sms } = req.body;

  if (!tr_num || !sms.tr_msg) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide sms id for update");
  } else {
    Sms.updateSms(tr_num, sms, (err, info) => {
      jsonRes(req, res, err, { info }, "Sms successfully updated");
    });
  }
};

const delete_a_sms = (req, res) => {
  const { tr_num } = req.params;

  if (!tr_num) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide sms id for delete");
  } else {
    Sms.deleteSms(tr_num, (err, info) => {
      jsonRes(req, res, err, { info }, "Sms successfully deleted");
    });
  }
};

export default {
  list_a_smses,
  send_auth_no,
  check_auth_no,
  create_a_sms,
  read_a_sms,
  update_a_sms,
  delete_a_sms,
  makeAuthNo,
};
