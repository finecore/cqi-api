import File from "../model/file.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { upload, download, remove } from "../utils/file-util.js";
import { ERROR } from "../constants/constants.js";

const list_a_files = (req, res) => {
  let { filter = "1=1", limit = "10000", order = "a.id", desc = "desc" } = req.params;

  File.selectFileCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      File.selectFiles(filter, order, desc, limit, (err, files) => {
        jsonRes(req, res, err, { count, files });
      });
  });
};

const read_a_file = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide file id for select");
  } else {
    File.selectFile(id, (err, file) => {
      jsonRes(req, res, err, { file });
    });
  }
};

const create_a_file = (req, res) => {
  const { type, type_id } = req.body;
  const { destination: path, filename: name, originalname, size, mimetype } = req.file;

  if (!size) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide file for insert");
  } else {
    let file = { type, type_id, path, name, originalname, size, mimetype };

    File.insertFile(file, (err, info) => {
      if (info && info.insertId) file.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info, file }, "File successfully inserted");
    });
  }
};

const update_a_file = (req, res) => {
  const { id } = req.params;
  const { type, type_id } = req.body;
  const { destination: path, filename: name, originalname, size, mimetype } = req.file;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide file id for update");
  } else {
    let file = { type, type_id, path, name, originalname, size, mimetype };

    File.updateFile(id, file, (err, info) => {
      jsonRes(req, res, err, { info }, "File successfully updated");
    });
  }
};

const delete_a_file = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide file id for delete");
  } else {
    File.selectFile(id, (err, file) => {
      if (!err && file[0]) {
        File.deleteFile(id, (err, info) => {
          jsonRes(req, res, err, { info }, "File successfully deleted");

          if (!err) {
            const { path, name } = file[0];
            remove(path, name);
          }
        });
      } else {
        jsonRes(req, res, err, { info: {} }, "File successfully deleted");
      }
    });
  }
};

const upload_a_file = (req, res, next) => {
  console.log("----> file upload ", req.file);

  const { destination: path, filename: name, originalname, size, mimetype } = req.file;

  if (!size) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide file for insert");
  } else {
    let file = { path, name, originalname, size, mimetype };
    jsonRes(req, res, null, { info: {}, file }, "File successfully uploaded!");
  }
};

const remove_a_image = (req, res) => {
  const { path, name } = req.params;

  console.log("----> remove image ", { path, name });

  if (!path || !name) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide file path or name for delete");
  } else {
    remove(path, name, (err) => {
      jsonRes(req, res, err, { info: {} }, "File successfully deleted");
    });
  }
};

const download_a_file = (req, res, next) => {
  const { file_id, path, name } = req.params;

  if (!file_id && !path && !name) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide file id, path, name for select");
  } else {
    if (file_id) {
      File.selectFile(file_id, (err, file) => {
        if (!err && file[0]) {
          const { path, name } = file[0];
          const file_path = path + "/" + name;
          console.log("--- file download ", file_path);

          res.download(file_path); // Set disposition and send it.
        } else {
          jsonRes(req, res, err, { file });
        }
      });
    } else {
      download(req, res, next);
    }
  }
};

export default {
  list_a_files,
  create_a_file,
  read_a_file,
  update_a_file,
  delete_a_file,
  remove_a_image,
  upload_a_file,
  download_a_file,
};
