import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import axios from "axios";

const read_rss = (req, res) => {
  console.log(`*** read_rss - params ::`, req.params);

  const { url } = req.params;

  if (!url) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide encodedUrl for select");
  } else {
    const decodedUrl = decodeURIComponent(url);

    axios
      .get(decodedUrl)
      .then(function (response) {
        // handle success
        jsonRes(req, res, null, { data: response.data });
      })
      .catch(function (error) {
        // handle error
        jsonRes(req, res, error, { data: null });
      });
  }
};

export default {
  read_rss,
};
