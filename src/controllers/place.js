import Place from "../model/place.js";
import Preference from "../model/preference.js";
import RoomType from "../model/room-type.js";
import User from "../model/user.js";
import Room from "../model/room.js";
import RoomState from "../model/room-state.js";
import crypto from "crypto";

import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import moment from "moment";
import _ from "lodash";

import room from "./room.js";
import connection from "../db/connection.js";

const list_all_places = (req, res) => {
  let { filter = "1=1", limit = "10000", order = "reg_date", desc = "desc" } = req.params;

  Place.selectPlaceCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      Place.selectPlaces(filter, order, desc, limit, (err, places) => {
        jsonRes(req, res, err, { count, places });
      });
  });
};

const list_search_places = (req, res) => {
  let { text = "", sido = "", limit = "10", order = "a.rank", desc = "desc" } = req.params;

  let filter = text.trim() ? " (a.name LIKE '%" + text.trim() + "%' OR a.addr LIKE '%" + text.trim() + "%') " : "1=1";
  if (sido.trim()) filter += " AND a.sido = '" + sido.trim() + "' ";

  Place.selectPlaceCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      Place.selectPlacesSearch(filter, order, desc, limit, (err, places) => {
        jsonRes(req, res, err, { count, places });
      });
  });
};

const list_by_company = (req, res) => {
  let { company_id } = req.params;

  let filter = "a.company_id = " + company_id.trim();

  Place.selectPlaceCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      Place.selectPlacesSearch(filter, order, desc, limit, (err, places) => {
        jsonRes(req, res, err, { count, places });
      });
  });
};

const read_a_place = (req, res) => {
  if (!req.params.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place id for select");
  } else {
    Place.selectPlace(req.params.id, (err, place) => {
      jsonRes(req, res, err, { place });
    });
  }
};

const create_a_place = (req, res) => {
  let new_place = req.body.place; // post body.
  let { name, floor, rooms, room_cnt, user_id, user_pwd } = new_place;

  if (!name) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place data for insert");
  } else {
    new_place.reg_date = new Date();
    delete new_place.floor;
    delete new_place.rooms;
    delete new_place.room_cnt;
    delete new_place.user_id;
    delete new_place.user_pwd;

    Place.insertPlace(new_place, (err, info) => {
      if (info && info.insertId) req.body.place.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info }, "Place successfully inserted");

      if (!err) {
        const place_id = info.insertId;

        const voice_opt = {
          alarm: 1,
          key_1: 1,
          key_3: 1,
          key_4: 1,
          key_6: 1,
          door_0: 1,
          door_1: 1,
          clean_0: 1,
          clean_1: 1,
          outing_0: 1,
          outing_1: 1,
          stay_type: 0,
          delay_time: 2,
          queue_size: 5,
          auto_check_in: 1,
          auto_check_out: 1,
          main_relay_0: 0,
          main_relay_1: 0,
          car_call: 0,
        };

        const new_preferences = { place_id, voice_opt: JSON.stringify(voice_opt) };

        Preference.insertPreference(new_preferences, (err, info) => {
          console.log("- Preference successfully inserted");
        });

        if (user_pwd && user_pwd.length < 20) {
          // 해시 생성
          var shasum = crypto.createHash("sha1");
          shasum.update(user_pwd);

          // 비밀번호 sha1 형식으로 암호화.
          user_pwd = shasum.digest("hex");
          console.log("- pwd sha1 ", user_pwd);
        }

        const new_user = {
          place_id,
          id: user_id,
          pwd: user_pwd,
          name: "업주",
          level: 1,
          rank: "업주",
        };
        User.insertUser(new_user, (err, info) => {
          console.log("- User successfully inserted");
        });

        const new_room_type = { place_id, name: "일반실" };
        RoomType.insertRoomType(new_room_type, (err, info) => {
          console.log("- RoomType successfully inserted");

          if (!err) {
            const room_type_id = info.insertId;

            // 트랜젝션 !
            connection((_err, _connection) => {
              if (err) {
                _connection.release();
                throw err;
              }

              _connection.beginTransaction(function (err) {
                if (err) {
                  _connection.release();
                  throw err;
                }

                // 객실
                if (floor && rooms) {
                  for (let i = 0; i < floor; i++) {
                    for (let k = 0; k < rooms; k++) {
                      const new_room = {
                        place_id,
                        room_type_id,
                        name: i + 1 + String(k + 1).padStart(2, "0"),
                      };

                      Room.insertRoom(
                        new_room,
                        (err, info) => {
                          console.log("- Room successfully inserted");

                          if (!err) {
                            const new_room_state = { room_id: info.insertId };
                            // 객실 상태 등록.
                            RoomState.insertRoomState(
                              new_room_state,
                              (err, info) => {
                                console.log("- RoomState successfully inserted");
                              },
                              _connection
                            );
                          }
                        },
                        _connection
                      );
                    }
                  }

                  _connection.commit(function (err) {
                    if (err) {
                      _connection.rollback(function () {
                        console.error("rollback error");
                        _connection.release();
                        throw err;
                      });
                      console.error(err);
                    }

                    _connection.release();
                  });
                }
              });
            });
          }
        });
      }
    });
  }
};

const update_a_place = (req, res) => {
  let { place } = req.body; // post body.
  let { id } = place || {};

  if (!id || !place) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place data for update");
  } else {
    Place.updatePlace(id, place, (err, info) => {
      jsonRes(req, res, err, { info }, "Place successfully updated");
    });
  }
};

const delete_a_place = (req, res) => {
  if (!req.params.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place id for delete");
  } else {
    Place.deletePlace(req.params.id, (err, info) => {
      jsonRes(req, res, err, { info }, "Place successfully deleted");
    });
  }
};

export default {
  list_all_places,
  list_search_places,
  list_by_company,
  create_a_place,
  read_a_place,
  update_a_place,
  delete_a_place,
};
