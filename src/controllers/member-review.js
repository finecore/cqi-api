import jsonwebtoken from 'jsonwebtoken';
import { jsonRes } from '../utils/api-util.js';
import { ERROR } from '../constants/constants.js';
import { verify, sign } from '../utils/jwt-util.js';
import crypto from 'crypto';
import { encHash, decHash } from '../utils/crypto-util.js';
import MemberReview from '../model/member-review.js';

const list_a_member_reviews = (req, res) => {
  let {
    filter = '1=1',
    limit = '10000',
    order = 'a.member_id',
    desc = 'asc',
  } = req.params;

  MemberReview.selectMemberReviewsCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      MemberReview.selectMemberReviews(
        filter,
        order,
        desc,
        limit,
        (err, member_reviews) => {
          jsonRes(req, res, err, { count, member_reviews });
        },
      );
  });
};

const list_a_member_best_reviews = (req, res) => {
  let {
    filter = '1=1',
    limit = '5',
    order = 'a.star_point',
    desc = 'desc',
  } = req.params;

  MemberReview.selectMemberReviewsCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      MemberReview.selectMemberReviews(
        filter,
        order,
        desc,
        limit,
        (err, member_reviews) => {
          jsonRes(req, res, err, { count, member_reviews });
        },
      );
  });
};

const list_a_member_by_member_reviews = (req, res) => {
  const { member_id } = req.params;

  if (!member_id) {
    jsonRes(
      req,
      res,
      ERROR.INVALID_ARGUMENT,
      'Please provide list_a_member_by_member_reviews member_id for insert',
    );
  } else
    MemberReview.selectByMemberReviews(member_id, (err, member_reviews) => {
      jsonRes(req, res, err, { count: member_reviews?.length, member_reviews });
    });
};

const list_a_member_by_place_reviews = (req, res) => {
  const { place_id } = req.params;

  if (!place_id) {
    jsonRes(
      req,
      res,
      ERROR.INVALID_ARGUMENT,
      'Please provide list_a_member_by_place_reviews place_id for insert',
    );
  } else
    MemberReview.selectByPlaceReviews(place_id, (err, member_reviews) => {
      jsonRes(req, res, err, { count: member_reviews?.length, member_reviews });
    });
};

const create_a_member_review = (req, res) => {
  const { member_review } = req.body; // post body.

  if (!member_review) {
    jsonRes(
      req,
      res,
      ERROR.INVALID_ARGUMENT,
      'Please provide create_a_member_review member_review  for insert',
    );
  } else {
    MemberReview.insertMemberReview(member_review, (err, info) => {
      jsonRes(req, res, err, { info }, 'MemberReview successfully inserted');
    });
  }
};

const read_a_member_review = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(
      req,
      res,
      ERROR.INVALID_ARGUMENT,
      'MemberReview provide member_review id for select',
    );
  } else {
    MemberReview.selectMemberReview(id, (err, member_review) => {
      jsonRes(req, res, err, { member_review });
    });
  }
};

const update_a_member_review = (req, res) => {
  const { id } = req.params;
  const { member_review } = req.body;

  if (!id) {
    jsonRes(
      req,
      res,
      ERROR.INVALID_ARGUMENT,
      'MemberReview provide member_review id for update',
    );
  } else {
    MemberReview.updateMemberReview(id, member_review, (err, info) => {
      jsonRes(
        req,
        res,
        err,
        { info, member_review },
        'MemberReview successfully updated',
      );
    });
  }
};

const delete_a_member_review = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(
      req,
      res,
      ERROR.INVALID_ARGUMENT,
      'MemberReview provide member_review id for delete',
    );
  } else {
    MemberReview.deleteMemberReview(id, (err, info) => {
      jsonRes(req, res, err, { info }, 'MemberReview successfully deleted');
    });
  }
};

const delete_a_member_all_review = (req, res) => {
  const { member_id } = req.params;

  if (!member_id) {
    jsonRes(
      req,
      res,
      ERROR.INVALID_ARGUMENT,
      'MemberReview provide member_review member_id for all member delete',
    );
  } else {
    MemberReview.deleteAllMemberReview(member_id, (err, info) => {
      jsonRes(req, res, err, { info }, 'MemberReview successfully deleted');
    });
  }
};

export default {
  create_a_member_review,
  read_a_member_review,
  list_a_member_reviews,
  list_a_member_by_member_reviews,
  list_a_member_by_place_reviews,
  list_a_member_best_reviews,
  update_a_member_review,
  delete_a_member_review,
  delete_a_member_all_review,
};
