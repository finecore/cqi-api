import IscStateLog from "../model/isc-state-log.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";

import _ from "lodash";
import moment from "moment";

const list_a_isc_state_logs = (req, res) => {
  let { filter = "1=1", limit = "10000", order = "a.reg_date", desc = "desc" } = req.params; // paging.

  IscStateLog.selectIscStateLogCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      IscStateLog.selectIscStateLogs(filter, order, desc, limit, (err, isc_state_logs) => {
        jsonRes(req, res, err, { count, isc_state_logs });
      });
  });
};

const read_a_isc_state_day_logs = (req, res) => {
  let { place_id, day = -1 } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide isc_state_log place_id for select");
  } else {
    IscStateLog.selectIscStateDayLogs(place_id, day, (err, isc_state_logs) => {
      isc_state_logs.map((row) => (row.data = JSON.parse(row.data)));
      jsonRes(req, res, err, { isc_state_logs });
    });
  }
};

const read_a_isc_state_last_logs = (req, res) => {
  let { place_id, last_id = 0 } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide isc_state_log place_id for select");
  } else {
    IscStateLog.selectIscStateLastLogs(place_id, last_id, (err, isc_state_logs) => {
      isc_state_logs.map((row) => (row.data = JSON.parse(row.data)));
      jsonRes(req, res, err, { isc_state_logs });
    });
  }
};

const read_a_isc_state_log = (req, res) => {
  if (!req.params.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide isc_state_log id for select");
  } else {
    IscStateLog.selectIscStateLog(req.params.id, (err, isc_state_log) => {
      jsonRes(req, res, err, { isc_state_log });
    });
  }
};

const create_a_isc_state_log = (req, res) => {
  var new_isc_state_log = req.body.isc_state_log; // post body.

  if (!new_isc_state_log.room_id || !new_isc_state_log.data) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide isc_state_log data for insert");
  } else {
    new_isc_state_log["reg_date"] = new Date();
    delete new_isc_state_log["mod_date"];

    IscStateLog.insertIscStateLog(new_isc_state_log, (err, info) => {
      if (info && info.insertId) req.body.isc_state_log.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info }, "IscStateLog successfully inserted");
    });
  }
};

const update_a_isc_state_log = (req, res) => {
  let { id } = req.params;
  let { isc_state_log } = req.body; // post body.
  let { channel } = req.headers;

  console.log("- update_a_isc_state_log ", channel, isc_state_log);

  if (!id || !isc_state_log) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide isc_state_log id for update");
  } else {
    isc_state_log.channel = channel || "front";

    IscStateLog.updateIscStateLog(id, isc_state_log, (err, info) => {
      jsonRes(req, res, err, { info }, "IscStateLog successfully updated");
    });
  }
};

const delete_a_isc_state_log = (req, res) => {
  let { id } = req.params;
  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide isc_state_log id for delete");
  } else {
    // FOREIGN KEY Constraints ON DELETE CASCADE ON UPDATE CASCADE
    IscStateLog.deleteIscStateLog(id, (err, info) => {
      jsonRes(req, res, err, { info }, "IscStateLog successfully deleted");
    });
  }
};

// 소켓 서버로 전파.
const sendToSocket = (req, isc_state_log) => {
  const {
    url,
    headers: { method = "put", channel, "x-access-token": xtoken, token, uuid = "" },
  } = req;

  const json = {
    type: "SET_ISC_STATE_LOG", // 웹 reducer type, 어드민/모바일 store 타입은 socket server 에서 각각 변경.
    headers: {
      method,
      url,
      token: token || xtoken,
      channel: "api",
      req_channel: channel,
      uuid, // 웹 브라우저 고유 번호.
      place_id: isc_state_log.place_id, // 업소 정보.
      serialno: isc_state_log.serialno, // 장비 일련번호(소켓 전송 시 장비 구분용)
    },
    body: {
      isc_state_log,
    },
  };

  // console.log("---> isc_state_log sendToSocket", json);

  // 소켓 서버로 전송.
  global.dispatcher.dispatch(json);
};

export default {
  create_a_isc_state_log,
  list_a_isc_state_logs,
  read_a_isc_state_day_logs,
  read_a_isc_state_last_logs,
  read_a_isc_state_log,
  update_a_isc_state_log,
  delete_a_isc_state_log,
  sendToSocket,
};
