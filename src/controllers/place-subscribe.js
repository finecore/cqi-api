import PlaceSubscribe from "../model/place-subscribe.js";
import File from "../model/file.js";
import Mail from "../model/mail.js";
import _ from "lodash";

import moment from "moment";

import { keyToValue } from "../constants/key-map.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR, PREFERENCES } from "../constants/constants.js";

/** 업소에서 사용중인 구독서비스 목록 */
const list_place_used_subscribes = (req, res) => {
  const { place_id } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "PlaceSubscribe provide place_id for select");
  } else {
    PlaceSubscribe.selectPlaceUsedSubscribes(place_id, (err, place_subscribes) => {
      jsonRes(req, res, err, { place_subscribes });
    });
  }
};

const list_a_place_subscribes = (req, res) => {
  let { place_id, filter = "1=1", limit = "10000", order = "a.reg_date", desc = "desc" } = req.params;

  if (place_id) filter = `a.place_id=${place_id}`;

  PlaceSubscribe.selectPlaceSubscribeCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      PlaceSubscribe.selectPlaceSubscribes(filter, order, desc, limit, (err, place_subscribes) => {
        jsonRes(req, res, err, { count, place_subscribes });
      });
  });
};

const list_a_place_subscribes_prev_next = (req, res) => {
  const { id, place_id, filter = "1=1" } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "PlaceSubscribe provide place_subscribe id for select");
  } else {
    PlaceSubscribe.selectPlaceSubscribesPrevNext(id, place_id, filter, (err, place_subscribes) => {
      jsonRes(req, res, err, { place_subscribes });
    });
  }
};

const read_a_place_subscribe = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "PlaceSubscribe provide place_subscribe id for select");
  } else {
    PlaceSubscribe.selectPlaceSubscribe(id, (err, place_subscribe) => {
      jsonRes(req, res, err, { place_subscribe });
    });
  }
};

const create_a_place_subscribe = (req, res) => {
  const { place_subscribe } = req.body;

  PlaceSubscribe.insertPlaceSubscribe(place_subscribe, (err, info) => {
    if (info && info.insertId) req.body.place_subscribe.id = info.insertId; // 채널 전송 시 id 입력.

    jsonRes(req, res, err, { info }, "PlaceSubscribe successfully inserted");

    if (!err) {
      PlaceSubscribe.selectPlaceSubscribe(info.insertId, (err, place_subscribe) => {
        if (!err && place_subscribe[0]) {
          let { name, place_name, apply_begin_date, apply_end_date, apply_license_copy, license_copy, valid_yn } = place_subscribe[0];
          let content = (content = "구독 서비스 신규 신청 정보<br/>");
          content += `<br/>신청 기간 : ${moment(apply_begin_date).format("YYYY-MM-DD")} ~ ${moment(apply_end_date).format("YYYY-MM-DD")}`;
          if (apply_license_copy !== license_copy) content += `<br/>신청 라이선스 Copy : ${apply_license_copy}`;

          // 메일 타입(01: AS 메일, 02: 구독 메일,  03: OTA예약 연동 실패 메일, 09: 오류 메일)
          PREFERENCES.MailReceiverList("02", (list) => {
            _.each(list, (v) => {
              if (v.email && v.receive_yn === "Y") {
                let mail = {
                  type: 1, // 메일 타입 (0: 고객메일, 1: 관리자메일, 9: 서버메일)
                  to: v.email,
                  from: "subscribe@imonaico.kr",
                  subject: `[이모나이코 관리자] ${process.env.MODE !== "PROD" ? "[" + process.env.MODE + "]" : ""} [${place_name}] 구독 신규 요청 입니다.`,
                  content: `<strong>${name}</strong><br/></br> ${content}<br/><br/><br/><br/>주식회사 이모나이코컴퍼니<br/>1600-5356`,
                };
                Mail.insertMail(mail, (err, info) => {});
              }
            });
          });
        }
      });
    }
  });
};

const update_a_place_subscribe = (req, res) => {
  const { id } = req.params;
  const { place_subscribe } = req.body;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "PlaceSubscribe provide place_subscribe id for update");
  } else {
    PlaceSubscribe.updatePlaceSubscribe(id, place_subscribe, (err, info) => {
      jsonRes(req, res, err, { info }, "PlaceSubscribe successfully updated");

      if (!err) {
        PlaceSubscribe.selectPlaceSubscribe(id, (err, place_subscribe) => {
          if (!err && place_subscribe[0]) {
            let { name, place_name, apply_begin_date, begin_date, apply_end_date, end_date, apply_license_copy, license_copy, valid_yn, is_apply } = place_subscribe[0];

            // 고객 신청만 메일 발송.(0: 신규 신청, 1: 변경 신청, 2: 구독 완료)
            if (is_apply === 0 || is_apply === 1) {
              let content = "구독 서비스 변경 신청 정보<br/>";

              if (apply_begin_date !== begin_date || apply_end_date !== end_date)
                content += `<br/>변경 기간 : ${moment(apply_begin_date).format("YYYY-MM-DD")} ~ ${moment(apply_end_date).format("YYYY-MM-DD")}`;

              if (apply_license_copy !== license_copy) content += `<br/>변경 라이선스 Copy : ${apply_license_copy}`;
              content += `<br/>서비스 상태 : ${keyToValue("place_subscribe", "validYn", valid_yn)}`;

              // 메일 타입(01: AS 메일, 02: 구독 메일,  03: OTA예약 연동 실패 메일, 09: 오류 메일)
              PREFERENCES.MailReceiverList("02", (list) => {
                _.each(list, (v) => {
                  if (v.email && v.receive_yn === "Y") {
                    let mail = {
                      type: 1, // 메일 타입 (0: 고객메일, 1: 관리자메일, 9: 서버메일)
                      to: v.email,
                      from: "subscribe@imonaico.kr",
                      subject: `[이모나이코 관리자] ${process.env.MODE !== "PROD" ? "[" + process.env.MODE + "]" : ""} [${place_name}] 구독 변경 요청 입니다.`,
                      content: `<strong>${name}</strong><br/></br> ${content}<br/><br/><br/><br/>주식회사 이모나이코컴퍼니<br/>1600-5356`,
                    };
                    Mail.insertMail(mail, (err, info) => {});
                  }
                });
              });
            }
          }
        });
      }
    });
  }
};

const delete_a_place_subscribe = (req, res) => {
  const { id } = req.params;
  const { place_subscribe } = req.body;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "PlaceSubscribe provide place_subscribe id for delete");
  } else {
    let selSubscrib = null;

    PlaceSubscribe.selectPlaceSubscribe(id, (err, place_subscribe) => {
      if (!err && place_subscribe[0]) {
        selSubscrib = place_subscribe[0];
      }
    });

    PlaceSubscribe.deletePlaceSubscribe(id, (err, info) => {
      jsonRes(req, res, err, { info }, "PlaceSubscribe successfully deleted");

      if (!err && selSubscrib) {
        let { name, place_name, begin_date, end_date, license_copy, valid_yn } = selSubscrib;
        let content = "구독 서비스 삭제 정보<br/>";
        content += `<br/>구독 기간 : ${moment(begin_date).format("YYYY-MM-DD")} ~ ${moment(end_date).format("YYYY-MM-DD")}`;
        if (license_copy) content += `<br/>구독 라이선스 Copy : ${license_copy}`;
        content += `<br/>서비스 상태 : ${keyToValue("place_subscribe", "validYn", valid_yn)}`;

        // 메일 타입(01: AS 메일, 02: 구독 메일,  03: OTA예약 연동 실패 메일, 09: 오류 메일)
        PREFERENCES.MailReceiverList("02", (list) => {
          _.each(list, (v) => {
            if (v.email && v.receive_yn === "Y") {
              let mail = {
                type: 1, // 메일 타입 (0: 고객메일, 1: 관리자메일, 9: 서버메일)
                to: v.email,
                from: "subscribe@imonaico.kr",
                subject: `[이모나이코 관리자] ${process.env.MODE !== "PROD" ? "[" + process.env.MODE + "]" : ""} [${place_name}] 구독 삭제 입니다.`,
                content: `<strong>${name}</strong><br/></br> ${content}<br/><br/><br/><br/>주식회사 이모나이코컴퍼니<br/>1600-5356`,
              };
              Mail.insertMail(mail, (err, info) => {});
            }
          });
        });
      }
    });
  }
};

export default {
  list_place_used_subscribes,
  list_a_place_subscribes,
  list_a_place_subscribes_prev_next,
  create_a_place_subscribe,
  read_a_place_subscribe,
  update_a_place_subscribe,
  delete_a_place_subscribe,
};
