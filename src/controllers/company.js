import Company from "../model/company.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import moment from "moment";

const list_a_companys = (req, res) => {
  let { filter = "1=1", limit = "10000", order = "reg_date", desc = "desc" } = req.params; // paging.

  Company.selectCompanyCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      Company.selectCompanys(filter, order, desc, limit, (err, companys) => {
        jsonRes(req, res, err, { count, companys });
      });
  });
};

const read_a_company = (req, res) => {
  if (!req.params.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide company id for select");
  } else {
    Company.selectCompany(req.params.id, (err, company) => {
      jsonRes(req, res, err, { company });
    });
  }
};

const create_a_company = (req, res) => {
  let new_company = req.body.company; // post body.

  if (!new_company.name) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide company data for insert");
  } else {
    delete new_company.mod_date;
    new_company.reg_date = new Date();

    Company.insertCompany(new_company, (err, info) => {
      if (info && info.insertId) req.body.company.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info }, "Company successfully inserted");
    });
  }
};

const update_a_company = (req, res) => {
  const { id } = req.params;
  let { company } = req.body; // post body.

  if (!id || !company) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide company data for update");
  } else {
    Company.updateCompany(id, company, (err, info) => {
      jsonRes(req, res, err, { info }, "Company successfully updated");
    });
  }
};

const delete_a_company = (req, res) => {
  if (!req.params.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide company id for delete");
  } else {
    Company.deleteCompany(req.params.id, (err, info) => {
      jsonRes(req, res, err, { info }, "Company successfully deleted");
    });
  }
};

export default {
  list_a_companys,
  create_a_company,
  read_a_company,
  update_a_company,
  delete_a_company,
};
