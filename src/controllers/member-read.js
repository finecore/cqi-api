import jsonwebtoken from "jsonwebtoken";
import { jsonRes } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import { verify, sign } from "../utils/jwt-util.js";
import crypto from "crypto";
import { encHash, decHash } from "../utils/crypto-util.js";
import MemberRead from "../model/member-read.js";

const list_a_member_reads = (req, res) => {
  let { member_id = "", filter = "1=1", limit = "10000", order = "a.member_id", desc = "asc" } = req.params;

  if (filter === "1=1" && member_id) filter = "a.member_id ='" + member_id + "'";

  MemberRead.selectMemberReadsCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      MemberRead.selectMemberReads(filter, order, desc, limit, (err, member_reads) => {
        jsonRes(req, res, err, { count, member_reads });
      });
  });
};

const create_a_member_read = (req, res) => {
  const { member_read } = req.body; // post body.

  if (!member_read) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide create_a_member_read member_read  for insert");
  } else {
    MemberRead.insertMemberRead(member_read, (err, info) => {
      jsonRes(req, res, err, { info }, "MemberRead successfully inserted");
    });
  }
};

const read_a_member_read = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "MemberRead provide member_read id for select");
  } else {
    MemberRead.selectMemberRead(id, (err, member_read) => {
      jsonRes(req, res, err, { member_read });
    });
  }
};

const update_a_member_read = (req, res) => {
  const { id } = req.params;
  const { member_read } = req.body;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "MemberRead provide member_read id for update");
  } else {
    MemberRead.updateMemberRead(id, member_read, (err, info) => {
      jsonRes(req, res, err, { info, member_read }, "MemberRead successfully updated");
    });
  }
};

const delete_a_member_read = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "MemberRead provide member_read id for delete");
  } else {
    MemberRead.deleteMemberRead(id, (err, info) => {
      jsonRes(req, res, err, { info }, "MemberRead successfully deleted");
    });
  }
};

const delete_a_member_all_read = (req, res) => {
  const { member_id } = req.params;

  if (!member_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "MemberRead provide member_read member_id for all member delete");
  } else {
    MemberRead.deleteAllMemberRead(member_id, (err, info) => {
      jsonRes(req, res, err, { info }, "MemberRead successfully deleted");
    });
  }
};

export default {
  create_a_member_read,
  read_a_member_read,
  list_a_member_reads,
  update_a_member_read,
  delete_a_member_read,
  delete_a_member_all_read,
};
