import MemberPoint from '../model/member-point.js';
import MemberPointLog from '../model/member-point-log.js';
import {
  jsonRes,
  sendRes,
  isValidToken,
  checkPermission,
} from '../utils/api-util.js';
import { ERROR } from '../constants/constants.js';
import _ from 'lodash';

const list_a_member_points = (req, res) => {
  let {
    filter = '1=1',
    limit = '10000',
    order = 'a.id',
    desc = 'desc',
  } = req.params;

  MemberPoint.selectMemberPointCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      MemberPoint.selectMemberPoints(
        filter,
        order,
        desc,
        limit,
        (err, member_points) => {
          jsonRes(req, res, err, { count, member_points });
        },
      );
  });
};

const list_place_member_points = (req, res) => {
  const { place_id } = req.params;

  if (!place_id) {
    jsonRes(
      req,
      res,
      ERROR.INVALID_ARGUMENT,
      'Please provide place_id for get member_point list',
    );
  } else {
    MemberPoint.selectPlaceMemberPoints(place_id, (err, member_points) => {
      jsonRes(req, res, err, { member_points });
    });
  }
};

const read_a_member_point = (req, res) => {
  let { id, member_id } = req.params;

  if (!id && !member_id) {
    jsonRes(
      req,
      res,
      ERROR.INVALID_ARGUMENT,
      'Please provide member_point id or member_id for select',
    );
  } else {
    if (member_id) member_id = member_id.replace(/[^\d]/g, '');
    MemberPoint.selectMemberPoint(id, member_id, (err, member_point) => {
      jsonRes(req, res, err, { member_point });
    });
  }
};

const read_place_member_id_member_point = (req, res) => {
  let { place_id, member_id } = req.params;

  if (!place_id || !member_id) {
    jsonRes(
      req,
      res,
      ERROR.INVALID_ARGUMENT,
      'Please provide member_point place_id and member_id for select',
    );
  } else {
    member_id = member_id.replace(/[^\d]/g, '');
    MemberPoint.selectPlaceMemberPoints(
      place_id,
      member_id,
      (err, member_point) => {
        jsonRes(req, res, err, { member_point });
      },
    );
  }
};

const read_member_member_point = (req, res) => {
  let { member_id } = req.params;

  console.log('- read_member_member_point', member_id);

  if (!member_id) {
    jsonRes(
      req,
      res,
      ERROR.INVALID_ARGUMENT,
      'Please provide member_point member_id for select',
    );
  } else {
    MemberPoint.selectMemberMemberPoints(member_id, (err, member_points) => {
      jsonRes(req, res, err, { member_points });
    });
  }
};

const create_a_member_point = (req, res) => {
  let { member_point } = req.body;

  if (!member_point) {
    jsonRes(
      req,
      res,
      ERROR.INVALID_ARGUMENT,
      'Please provide member_point data for insert',
    );
  } else {
    let { place_id, member_id } = member_point;

    MemberPoint.selectPlaceMemberPoints(place_id, member_id, (err, result) => {
      if (result[0]) {
        jsonRes(req, res, ERROR.DUPLICATE_DATA, '');
      } else {
        MemberPoint.insertMemberPoint(member_point, (err, info) => {
          if (info && info.insertId) member_point.id = info.insertId; // 채널 전송 시 id 입력.
          jsonRes(
            req,
            res,
            err,
            { info, member_point },
            'MemberPoint successfully inserted',
          );

          if (!err) {
            let member_point_log = {
              place_id: member_point.place_id,
              member_point_id: info.insertId, // Insert 후 생성된 id  사용.
              member_id: member_point.member_id,
              change_point: member_point.point,
              point: member_point.point,
              save_type: member_point.save_type, // 포인트 적립 구분(1:숙박적립,2:대실적립,3:장기적립,4:이벤트적립,5:리뷰적립,6:기타적립)
              use_type: member_point.use_type, // 포포인트 사용 구분(1:숙박사용,2:대실사용,3:장기사용, 4:기타사용)
            };

            MemberPointLog.insertMemberPointLog(
              member_point_log,
              (err, info) => {},
            );
          }
        });
      }
    });
  }
};

const update_a_member_point = (req, res) => {
  const { id } = req.params;
  let { member_point } = req.body;

  if (!id) {
    jsonRes(
      req,
      res,
      ERROR.INVALID_ARGUMENT,
      'Please provide member_point id for update',
    );
  } else {
    MemberPoint.selectMemberPoint(id, (err, result) => {
      if (!err && result[0]) {
        // 포인트 정산 전에 로그 정보 기록.
        const change_point =
          member_point.point * (member_point.use_type ? -1 : 1);
        const point = Number(result[0].point) + change_point;

        console.log('- update_a_member_point', { change_point, point });

        member_point.point = point;

        MemberPoint.updateMemberPoint(id, member_point, (err, info) => {
          jsonRes(
            req,
            res,
            err,
            { info, member_point },
            'MemberPoint successfully updated',
          );

          member_point = _.merge({}, result[0], member_point);

          if (!err) {
            let member_point_log = {
              place_id: member_point.place_id,
              member_point_id: member_point.id,
              member_id: member_point.member_id,
              change_point,
              point,
              save_type: member_point.save_type, // 포인트 적립 구분(1:숙박적립,2:대실적립,3:장기적립,4:이벤트적립,5:리뷰적립,6:기타적립)
              use_type: member_point.use_type, // 포포인트 사용 구분(1:숙박사용,2:대실사용,3:장기사용, 4:기타사용)
            };

            // 적립 사용 로그 기록.
            MemberPointLog.insertMemberPointLog(
              member_point_log,
              (err, info) => {},
            );
          }
        });
      } else {
        jsonRes(req, res, ERROR.NO_DATA, '');
      }
    });
  }
};

const increase_place_member_point_member_id = (req, res) => {
  let { place_id, member_id, point } = req.params;

  if (!place_id || !member_id || !point) {
    return jsonRes(
      req,
      res,
      ERROR.INVALID_ARGUMENT,
      'Please provide place_id, member_id, and point for increase',
    );
  }

  MemberPoint.selectPlaceMemberPoints(place_id, member_id, (err, result) => {
    if (!err && result[0]) {
      let {
        id: member_point_id,
        point: current_point,
        save_type,
        use_type,
      } = result[0];
      const new_point = Number(current_point) + Number(point);

      MemberPoint.increaseMemberPoint(
        member_point_id,
        new_point,
        (err, info) => {
          if (!err) {
            let member_point_log = {
              place_id,
              member_point_id,
              member_id,
              change_point: point,
              point: new_point,
              use_type,
              save_type,
            };

            MemberPointLog.insertMemberPointLog(member_point_log, () => {});
          }

          jsonRes(
            req,
            res,
            err,
            { info, new_member_point: new_point },
            'MemberPoint successfully updated',
          );
        },
      );
    } else {
      jsonRes(req, res, ERROR.PASS_ERROR, { new_member_point: {} });
    }
  });
};

const decrease_place_member_point_member_id = (req, res) => {
  let { place_id, member_id, point } = req.params;

  if (!place_id || !member_id || !point) {
    return jsonRes(
      req,
      res,
      ERROR.INVALID_ARGUMENT,
      'Please provide place_id, member_id, and point for decrease',
    );
  }

  MemberPoint.selectPlaceMemberPoints(place_id, member_id, (err, result) => {
    if (!err && result[0]) {
      let {
        id: member_point_id,
        point: current_point,
        save_type,
        use_type,
      } = result[0];

      let new_point = Number(current_point) - Number(point);
      if (new_point < 0) new_point = 0;

      MemberPoint.decreaseMemberPoint(
        member_point_id,
        -new_point,
        (err, info) => {
          if (!err) {
            let member_point_log = {
              place_id,
              member_point_id,
              member_id,
              change_point: -point,
              point: new_point,
              use_type,
              save_type,
            };

            MemberPointLog.insertMemberPointLog(member_point_log, () => {});
          }

          jsonRes(
            req,
            res,
            err,
            { info, new_member_point: new_point },
            'MemberPoint successfully updated',
          );
        },
      );
    } else {
      jsonRes(req, res, ERROR.PASS_ERROR, { new_member_point: {} });
    }
  });
};

const delete_a_member_point = (req, res) => {
  const { id, user_id } = req.params;
  let { member_point } = req.body;

  console.log('- delete_a_member_point', id, user_id);

  if (!id) {
    jsonRes(
      req,
      res,
      ERROR.INVALID_ARGUMENT,
      'Please provide member_point id for delete',
    );
  } else {
    MemberPoint.selectMemberPoint(id, (err, result) => {
      if (!err && result[0]) {
        MemberPoint.deleteMemberPoint(id, (err, info) => {
          member_point = _.merge({}, result[0], member_point);

          if (!err) {
            let member_point_log = {
              place_id: member_point.place_id,
              member_point_id: member_point.id,
              member_id: member_point.member_id,
              change_point: -result[0].point,
              point: 0,
              // type: rollback === "1" ? 5 : 2, // 1: 관리자 변경, 2: 고객 적립, 3: 고객 사용, 4: 적립 취소, 5: 사용 취소
              use_type: member_point.use_type, // 포포인트 사용 구분(1:숙박사용,2:대실사용,3:장기사용, 4:기타사용)÷÷
              save_type: member_point.save_type, // 포인트 적립 구분(1:숙박적립,2:대실적립,3:장기적립,4:이벤트적립,5:리뷰적립,6:기타적립)
            };

            MemberPointLog.insertMemberPointLog(
              member_point_log,
              (err, info) => {},
            );
          }

          jsonRes(req, res, err, { info }, 'MemberPoint successfully deleted');
        });
      } else {
        jsonRes(req, res, ERROR.NO_DATA, '');
      }
    });
  }
};

export default {
  list_a_member_points,
  list_place_member_points,
  create_a_member_point,
  read_a_member_point,
  read_place_member_id_member_point,
  read_member_member_point,
  update_a_member_point,
  increase_place_member_point_member_id,
  decrease_place_member_point_member_id,
  delete_a_member_point,
};
