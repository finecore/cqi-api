import Mail from "../model/mail.js";
import File from "../model/file.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";

const list_a_mails = (req, res) => {
  let {
    filter = "1=1",
    between = "a.reg_date",
    begin = moment().add(-1, "month").format("YYYY-MM-DD 00:00"),
    end = moment().format("YYYY-MM-DD HH:mm"),
    limit = "10000",
    order = "a.reg_date",
    desc = "desc",
  } = req.params;

  Mail.selectMailCount(filter, between, begin, end, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      Mail.selectMails(filter, between, begin, end, order, desc, limit, (err, mails) => {
        jsonRes(req, res, err, { count, mails });
      });
  });
};

const read_a_mail = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Plemaile provide mail id for select");
  } else {
    Mail.selectMail(id, (err, mail) => {
      jsonRes(req, res, err, { mail });
    });
  }
};

const create_a_mail = (req, res) => {
  const { mail } = req.body;

  Mail.insertMail(mail, (err, info) => {
    if (info && info.insertId) req.body.mail.id = info.insertId; // 채널 전송 시 id 입력.

    jsonRes(req, res, err, { info }, "Mail successfully inserted");
  });
};

const update_a_mail = (req, res) => {
  const { id } = req.params;
  const { mail } = req.body;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Plemaile provide mail id for update");
  } else {
    Mail.updateMail(id, mail, (err, info) => {
      jsonRes(req, res, err, { info }, "Mail successfully updated");
    });
  }
};

const delete_a_mail = (req, res) => {
  const { id } = req.params;
  const { mail } = req.body;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Plemaile provide mail id for delete");
  } else {
    Mail.deleteMail(id, (err, info) => {
      jsonRes(req, res, err, { info }, "Mail successfully deleted");
    });
  }
};

export default {
  list_a_mails,
  create_a_mail,
  read_a_mail,
  update_a_mail,
  delete_a_mail,
};
