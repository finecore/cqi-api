import SeasonPremium from "../model/season-premium.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import moment from "moment";

const list_all_season_premium = (req, res) => {
  const { place_id } = req.params;

  if (!place_id) {
    jsonRes(res, ERROR.INVALID_ARGUMENT, "Please provide place_id for get season premium list");
  } else {
    SeasonPremium.selectAllSeasonPremiums(place_id, (err, all_season_premiums) => {
      jsonRes(req, res, err, { all_season_premiums });
    });
  }
};

const between_season_premium = (req, res) => {
  const { place_id, begin, end } = req.params;

  if (!place_id) {
    jsonRes(res, ERROR.INVALID_ARGUMENT, "Please provide season premium place_id for select");
  } else {
    SeasonPremium.selectBetweenSeasonPremiums(place_id, begin, end, (err, season_premiums) => {
      jsonRes(req, res, err, { season_premiums });
    });
  }
};

const read_a_season_premium = (req, res) => {
  if (!req.params.id) {
    jsonRes(res, ERROR.INVALID_ARGUMENT, "Please provide season premium id for select");
  } else {
    SeasonPremium.selectSeasonPremium(req.params.id, (err, season_premium) => {
      jsonRes(req, res, err, { season_premium });
    });
  }
};

const create_a_season_premium = (req, res) => {
  var new_season_premium = req.body.season_premium; // post body.

  if (!new_season_premium.place_id || !new_season_premium.begin || !new_season_premium.end) {
    jsonRes(res, ERROR.INVALID_ARGUMENT, "Please provide season premium data for insert");
  } else {
    SeasonPremium.insertSeasonPremium(new_season_premium, (err, info) => {
      if (info && info.insertId) req.body.season_premium.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info }, "SeasonPremium successfully inserted");
    });
  }
};

const update_a_season_premium = (req, res) => {
  var season_premium = req.body.season_premium; // post body.

  if (!new_season_premium.id) {
    jsonRes(res, ERROR.INVALID_ARGUMENT, "Please provide season premium data for update");
  } else {
    SeasonPremium.updateSeasonPremium(season_premium.id, season_premium, (err, info) => {
      jsonRes(req, res, err, { info }, "SeasonPremium successfully updated");
    });
  }
};

const delete_a_season_premium = (req, res) => {
  if (!req.params.id) {
    jsonRes(res, ERROR.INVALID_ARGUMENT, "Please provide season premium id for delete");
  } else {
    SeasonPremium.deleteSeasonPremium(req.params.id, (err, info) => {
      jsonRes(req, res, err, { info }, "SeasonPremium successfully deleted");
    });
  }
};

const delete_all_season_premium = (req, res) => {
  if (!req.params.place_id) {
    jsonRes(res, ERROR.INVALID_ARGUMENT, "Please provide season premium id for delete");
  } else {
    SeasonPremium.deleteAllSeasonPremiums(req.params.place_id, (err, info) => {
      jsonRes(req, res, err, { info }, "SeasonPremium successfully all deleted");
    });
  }
};

export default {
  list_all_season_premium,
  between_season_premium,
  read_a_season_premium,
  create_a_season_premium,
  update_a_season_premium,
  delete_a_season_premium,
  delete_all_season_premium,
};
