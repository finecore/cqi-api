import jsonwebtoken from "jsonwebtoken";
import User from "../model/user.js";
import { jsonRes } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import { verify, sign } from "../utils/jwt-util.js";
import Device from "../model/device.js";
import crypto from "crypto";
import { encHash, decHash } from "../utils/crypto-util.js";

const user_login = (req, res) => {
  let {
    headers: { channel = "front" },
    body: { id, pwd, type },
  } = req;

  console.log("login request:", req.body);

  // 최소 6자리
  if (pwd && pwd.length > 6) {
    // 암호화되었다면 복호화 한다.
    if (pwd.length >= 20) {
      pwd = decHash(pwd);
      console.log("====> login decHash pwd:", pwd);
    }

    // 해시 생성
    let shasum = crypto.createHash("sha1");

    shasum.update(pwd);

    // 비밀번호 sha1 형식으로 암호화.
    pwd = shasum.digest("hex");

    console.log("- pwd sha1 ", pwd);
  } else {
    return jsonRes(req, res, ERROR.INVALID_ARGUMENT, "비밀번호는 최소 5자리 입니다.");
  }

  // query.
  User.selectUserById(id, (err, rows) => {
    let user = rows ? rows[0] : null;

    if (err) {
      return jsonRes(req, res, err);
    } else if (!user) {
      jsonRes(req, res, ERROR.NO_DATA, "아이디 와 비밀번호를 확인해 주세요.");
    } else if (user.pwd != pwd) {
      jsonRes(req, res, ERROR.INVALID_USER_PWD);
    } else if (type && user.type != type) {
      // 타입 (1: 이모나이코, 2: 대리점, 3: 업소, 4: PMS, 5;member)
      jsonRes(req, res, ERROR.INVALID_USER_TYPE);
    } else if (user.use_yn !== "Y") {
      jsonRes(req, res, ERROR.INVALID_USER); // 사용자 사용 체크.
    } else if (!user.place_expire) {
      jsonRes(req, res, ERROR.EXPIRE_CERTIFICATION); // 업소 인증 만료 체크.
    } else if (channel === "admin" && user.level > 2) {
      jsonRes(req, res, ERROR.INVALID_AUTHORITY); // 매니저 권한 이상만 관리자 로그인 가능.
    } else {
      // JWT(Json Web Token) 생성.
      const { channel, id, place_id, level, type, pms, eagle_url } = user;
      const {
        headers: { uuid = "" },
      } = req;

      User.updateLastLoginDate(id, (err, info) => {
        console.log(`- updateLastLoginDate :: info=${info}, err=${err}`);
      }); // 마지막 로그인 시간 업데이트.

      const payload = { channel, id, place_id, level, type, uuid, pms, eagle_url };

      sign(payload, function (err, token) {
        return jsonRes(req, res, err, { user, token });
      });
    }
  });
};

const user_sync_login = (req, res) => {
  let {
    headers: { channel = "front" },
    body: { id, type },
  } = req;

  console.log("user_sync_login request:", req.body);

  // query.
  User.selectUserById(id, (err, rows) => {
    let user = rows ? rows[0] : null;

    if (err) {
      return jsonRes(req, res, err);
    } else if (!user) {
      jsonRes(req, res, ERROR.NO_DATA, "아이디 와 비밀번호를 확인해 주세요.");
    } else if (type && user.type != type) {
      // 타입 (1: 이모나이코, 2: 대리점, 3: 업소, 4: PMS)
      jsonRes(req, res, ERROR.INVALID_USER_TYPE);
    } else if (user.use_yn !== "Y") {
      jsonRes(req, res, ERROR.INVALID_USER); // 사용자 사용 체크.
    } else if (!user.place_expire) {
      jsonRes(req, res, ERROR.EXPIRE_CERTIFICATION); // 업소 인증 만료 체크.
    } else if (channel === "admin" && user.level > 2) {
      jsonRes(req, res, ERROR.INVALID_AUTHORITY); // 매니저 권한 이상만 관리자 로그인 가능.
    } else {
      // JWT(Json Web Token) 생성.
      const { channel, id, place_id, level, type, pms, eagle_url } = user;
      const {
        headers: { uuid = "" },
      } = req;

      User.updateLastLoginDate(id, (err, info) => {
        console.log(`- updateLastLoginDate :: info=${info}, err=${err}`);
      }); // 마지막 로그인 시간 업데이트.

      const payload = { channel, id, place_id, level, type, uuid, pms, eagle_url };

      sign(payload, function (err, token) {
        return jsonRes(req, res, err, { user, token });
      });
    }
  });
};

const create_sync_token = (req, res) => {
  const { id } = req.params;

  console.log("-----> create_sync_token serialno ", serialno);

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "User id 가 없습니다.");
  } else {
    // query.
    User.selectUserById(id, (err, rows) => {
      let user = rows ? rows[0] : null;

      console.log("- create_sync_token ", device);

      if (err) {
        return jsonRes(req, res, err);
      } else {
        // JWT(Json Web Token) 생성.
        const { channel = "sync", id, place_id, type, name, serialno, pms = "" } = user;

        const payload = { channel, id, place_id, type, name, serialno, pms };

        sign(payload, function (err, token) {
          return jsonRes(req, res, err, { token, device });
        });
      }
    });
  }
};

const create_device_token = (req, res) => {
  const {
    headers: { serialno },
  } = req;

  console.log("-----> create_device_token serialno ", serialno);

  if (!serialno) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Device serialno 가 없습니다.");
  } else {
    Device.selectDeviceBySerialNo(serialno, (err, rows) => {
      const device = rows ? rows[0] : null;

      console.log("- create_device_token ", device);

      if (err) {
        return jsonRes(req, res, err);
      } else if (!device) {
        jsonRes(req, res, ERROR.NO_DATA);
      } else if (!device.expire) {
        // 장비 인증 만료 체크. type (01: IDM, 02: ISG, 10:RPT)
        jsonRes(req, res, device.type === "01" ? ERROR.IDM_EXPIRE_CERTIFICATION : device.type === "02" ? ERROR.ISG_EXPIRE_CERTIFICATION : ERROR.EXPIRE_CERTIFICATION);
      } else if (!device.place_id || !device.place_expire) {
        // 업소 인증 만료 체크.
        jsonRes(req, res, ERROR.EXPIRE_CERTIFICATION);
      } else {
        // JWT(Json Web Token) 생성.
        const { channel = "device", id, place_id, type, name, serialno, pms } = device;

        const payload = { channel, id, place_id, type, name, serialno, pms };

        sign(payload, function (err, token) {
          return jsonRes(req, res, err, { token, device });
        });
      }
    });
  }
};

const delete_device_token = (req, res) => {
  const { headers: id } = req;

  if (serialno) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Device seriano 가 없습니다.");
  } else {
    // JWT(Json Web Token) 생성.
    const payload = ({ seriano } = req.params);

    sign(
      payload,
      function (err, token) {
        return jsonRes(req, res, err, { token });
      },
      -1 // 생성 후 만료 시킴.
    );
  }
};

const alive = (req, res) => {
  jsonRes(req, res, null, { alive: true });
};

export default {
  user_login,
  user_sync_login,
  create_sync_token,
  create_device_token,
  delete_device_token,
  alive,
};
