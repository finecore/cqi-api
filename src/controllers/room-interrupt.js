import RoomInterrupt from "../model/room-interrupt.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR, PREFERENCES } from "../constants/constants.js";
import _ from "lodash";
import moment from "moment";
import { sign } from "../utils/jwt-util.js";

const list_all_room_interrupts = (req, res) => {
  const { place_id } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place_id for get room interrupt list");
  } else {
    RoomInterrupt.selectPlaceRoomInterrupts(place_id, (err, all_room_interrupts) => {
      jsonRes(req, res, err, { all_room_interrupts });
    });
  }
};

const read_a_room_interrupt = (req, res) => {
  if (!req.params.room_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room interrupt room_id for select");
  } else {
    RoomInterrupt.selectRoomInterrupt(req.params.room_id, (err, room_interrupt) => {
      jsonRes(req, res, err, { room_interrupt });
    });
  }
};

const read_user_room_interrupt = (req, res) => {
  let { user_id, channel } = req.params;
  if (!user_id || !channel) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room interrupt user_id or channel for select");
  } else {
    RoomInterrupt.selectUserRoomInterrupt(user_id, channel, (err, room_interrupt) => {
      jsonRes(req, res, err, { room_interrupt });
    });
  }
};

const create_a_room_interrupt = (req, res) => {
  var { room_interrupt } = req.body; // post body.

  if (room_interrupt.uuid) delete room_interrupt.uuid;

  room_interrupt.mod_date = new Date().toISOString().slice(0, 19).replace("T", " ");

  if (!room_interrupt || !room_interrupt.room_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room interrupt data for insert", room_interrupt);
  } else {
    RoomInterrupt.selectRoomInterrupt(room_interrupt.room_id, (err, result) => {
      if (!err) {
        const { place_id, sale, state, token, mod_date } = result[0] || {};

        // 인터럽트 걸린게 있는지 검사.
        if (token && token !== room_interrupt.token) {
          jsonRes(req, res, ERROR.ALREADY_INTERRUPT, "잠시만 기다려주세요.");
        } else {
          RoomInterrupt.insertRoomInterrupt(room_interrupt, (err, info) => {
            if (info && info.insertId) req.body.room_interrupt.id = info.insertId; // 채널 전송 시 id 입력.
            jsonRes(req, res, err, { info }, "RoomInterrupt successfully inserted");
          });
        }
      } else {
        jsonRes(req, res, err, { room_interrupt });
      }
    });
  }
};

const update_a_room_interrupt = (req, res) => {
  var { room_interrupt } = req.body; // post body.

  // console.log("- room_interrupt", room_interrupt);

  if (!req.params.room_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room interrupt room_id or token for update");
  } else {
    RoomInterrupt.selectRoomInterrupt(req.params.room_id, (err, result) => {
      if (!err) {
        const { place_id, sale, state, token, mod_date } = result[0] || {};

        // 인터럽트 걸린게 있는지 검사.
        if (token && token !== room_interrupt.token && !room_interrupt.is_update) {
          jsonRes(req, res, ERROR.ALREADY_INTERRUPT, "잠시만 기다려주세요.");
        } else {
          RoomInterrupt.insertRoomInterrupt(room_interrupt, (err, info) => {
            jsonRes(req, res, err, { info }, "RoomInterrupt successfully inserted");
          });
        }
      } else {
        jsonRes(req, res, err, { room_interrupt });
      }
    });
  }
};

const delete_a_room_interrupt = (req, res) => {
  const { room_id } = req.params;

  if (!room_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room interrupt room_id for delete");
  } else {
    let room_interrupt = { room_id: Number(room_id), deleted: true };
    RoomInterrupt.deleteRoomInterrupt(room_id, (err, info) => {
      jsonRes(req, res, err, { info, room_interrupt }, "RoomInterrupt successfully deleted");
    });
  }
};

const delete_user_room_interrupt = (req, res) => {
  let { user_id, channel } = req.params;

  if (!user_id || !channel) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room interrupt user_id or channel for delete");
  } else {
    let room_interrupt = { user_id, channel, deleted: true };
    RoomInterrupt.deleteUserRoomInterrupt(user_id, channel, (err, info) => {
      jsonRes(req, res, err, { info, room_interrupt }, "RoomInterrupt successfully deleted");
    });
  }
};

const delete_all_room_interrupt = (req, res) => {
  const { room_id } = req.params;

  if (!room_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room_id for place all room interrupts delete");
  } else {
    let room_interrupt = { room_id: Number(room_id), deleted: true };
    RoomInterrupt.deleteAllRoomInterrupts(room_id, (err, info) => {
      jsonRes(req, res, err, { info, room_interrupt }, "RoomInterrupt successfully deleted");
    });
  }
};

// 오류로 인터럽트가 해제 되지 않은 객실을 해제 해준다.
const release_interrupt = () => {
  RoomInterrupt.selectAllRoomInterrupts((err, all_room_interrupts) => {
    if (!err) {
      _.map(all_room_interrupts, (room_interrupt) => {
        const { place_id, room_id, sale, state, token, mod_date } = room_interrupt;

        // 인터럽트 걸린게 있는지 검사.
        if (place_id) {
          PREFERENCES.Get(place_id, (preference) => {
            const diff = Math.abs(new Date() - new Date(moment(mod_date, "YYYY-MM-DD HH:mm:ss"))); // ms
            const min = Math.floor(diff / 1000 / 60);

            // console.info("-- release_interrupt min ", min, preference.interrupt_release);

            // 인터럽트 자동 취소 시간 지났다면 삭제
            if (min > preference.interrupt_release || (!sale && !state)) {
              RoomInterrupt.deleteRoomInterrupt(room_id, (err, info) => {
                if (!err) {
                  let room_interrupt = { room_id: Number(room_id) };

                  // 토큰 생성.
                  sign({ channel: "api", place_id: place_id }, (err, token) => {
                    if (token) {
                      const json = {
                        type: "SET_ROOM_INTERRUPT", // 웹 reducer type, 어드민/모바일 store 타입은 socket server 에서 각각 변경.
                        headers: {
                          method: "delete",
                          url: "room/interrupt/" + place_id,
                          token,
                          channel: "api",
                        },
                        body: {
                          room_interrupt,
                          place_id,
                        },
                      };

                      // 소켓 서버로 전송.
                      global.dispatcher.dispatch(json);
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
};

setInterval(() => {
  release_interrupt();
}, 1000 * 10);

export default {
  list_all_room_interrupts,
  create_a_room_interrupt,
  read_a_room_interrupt,
  read_user_room_interrupt,
  update_a_room_interrupt,
  delete_a_room_interrupt,
  delete_user_room_interrupt,
  delete_all_room_interrupt,
};
