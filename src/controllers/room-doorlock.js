import RoomDoorLock from "../model/room-doorlock.js";
import Sms from "../model/sms.js";

import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import moment from "moment";
import { sign } from "../utils/jwt-util.js";
import { encrypt, decrypt } from "../utils/aes256-util.js";

const list_all_room_doorlock = (req, res) => {
  let { place_id, filter = "1=1", order = "room_id asc" } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place_id for get room doorlock list");
  } else {
    RoomDoorLock.selectAllRoomDoorLocks(place_id, filter, order, (err, all_room_doorlocks) => {
      jsonRes(req, res, err, { all_room_doorlocks });
    });
  }
};

const list_a_room_doorlocks = (req, res) => {
  let { filter = "1=1", between = "start_date", begin = moment().format("YYYY-MM-DD 00:00:00"), end = moment().format("YYYY-MM-DD 23:59:59"), order = "start_date desc" } = req.params;

  if (!filter) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room doorlock filter for select");
  } else {
    RoomDoorLock.selectRoomDoorLocks(filter, between, begin, end, order, (err, room_doorlocks) => {
      jsonRes(req, res, err, { room_doorlocks });
    });
  }
};

const read_a_room_doorlock = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room doorlock id for select");
  } else {
    RoomDoorLock.selectRoomDoorLock(id, (err, room_doorlock) => {
      jsonRes(req, res, err, { room_doorlock });
    });
  }
};

const read_a_room_doorlock_roomid = (req, res) => {
  const { room_id } = req.params;

  if (!room_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room doorlock room_id for select");
  } else {
    RoomDoorLock.selectRoomDoorLockByRoomId(room_id, (err, room_doorlock) => {
      if (!err && !room_doorlock[0]) {
        // 도어락 정보가 없으면 등록.
        RoomDoorLock.insertRoomDoorLock({ room_id }, (err, info) => {
          let id = !err && info ? info.insertId : 0;
          room_doorlock.push({ id, room_id });
          jsonRes(req, res, err, { room_doorlock });
        });
      } else {
        jsonRes(req, res, err, { room_doorlock });
      }
    });
  }
};

const create_a_room_doorlock = (req, res) => {
  const { room_doorlock } = req.body; // post body.

  if (!room_doorlock.room_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room doorlock data for insert");
  } else {
    RoomDoorLock.insertRoomDoorLock(room_doorlock, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomDoorLock successfully inserted");
    });
  }
};

const update_a_room_doorlock = (req, res) => {
  const { id } = req.params;
  const { room_doorlock } = req.body;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room doorlock data for update");
  } else {
    RoomDoorLock.updateRoomDoorLock(id, room_doorlock, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomDoorLock successfully updated");
    });
  }
};

const delete_a_room_doorlock = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room doorlock id for delete");
  } else {
    RoomDoorLock.deleteRoomDoorLock(id, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomDoorLock successfully deleted");
    });
  }
};

const delete_a_room_doorlock_roomid = (req, res) => {
  const { room_id } = req.params;

  if (!room_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room doorlock id for delete");
  } else {
    RoomDoorLock.deleteRoomDoorLockByRoomId(room_id, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomDoorLock successfully all deleted");
    });
  }
};

const clear_doorlock_data = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room doorlock data for clear");
  } else {
    RoomDoorLock.updateRoomDoorLock(
      id,
      {
        qr_key_phone: null,
        qr_key_data: null,
        start_date: null,
        end_date: null,
        cancel: 0,
        send_sms: 0,
        reg_date: null,
        mod_date: null,
      },
      (err, info) => {
        jsonRes(req, res, err, { info }, "RoomDoorLock successfully clear");
      }
    );
  }
};

const login_yy_qrcode = (req, res) => {
  const { query } = req.body;

  console.log("- login_yy_qrcode", { query });
};

const send_yy_qrcode = (req, res) => {
  const { id } = req.params;
  const {
    data: { query, room_doorlock, place_name, pnone },
  } = req.body;

  console.log("- send_yy_qrcode", { id, query, room_doorlock, place_name, pnone });

  if (!id || !query) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room doorlock data for update");
  }
};

export default {
  list_all_room_doorlock,
  list_a_room_doorlocks,
  read_a_room_doorlock,
  read_a_room_doorlock_roomid,
  create_a_room_doorlock,
  update_a_room_doorlock,
  delete_a_room_doorlock,
  delete_a_room_doorlock_roomid,
  login_yy_qrcode,
  send_yy_qrcode,
  clear_doorlock_data,
};
