import Preference from "../model/preference.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR, PREFERENCES } from "../constants/constants.js";
import moment from "moment";

const list_all_preferences = (req, res) => {
  Preference.selectAllPreferences((err, all_preferences) => {
    jsonRes(req, res, err, { all_preferences });
  });
};

const read_a_preference = (req, res) => {
  if (!req.params.place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide preference id for select");
  } else {
    Preference.selectPreference(req.params.place_id, (err, preference) => {
      jsonRes(req, res, err, { preference });
    });
  }
};

const create_a_preferences = (req, res) => {
  var new_preferences = req.body.preference; // post body.

  if (!new_preferences.place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide preference place id for insert");
  } else {
    new_preferences.reg_date = new Date();

    Preference.insertPreference(new_preferences, (err, info) => {
      if (info && info.insertId) req.body.preference.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info }, "Preference successfully inserted");

      // 환경 설정값 캐싱.
      PREFERENCES.Set(new_preferences.place_id, new_preferences);
    });
  }
};

const update_a_preferences = (req, res) => {
  var preference = req.body.preference; // post body.

  // console.log("- update_a_preferences", preference);

  if (!req.params.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide preference id for update");
  } else {
    Preference.updatePreference(req.params.id, preference, (err, info) => {
      jsonRes(req, res, err, { info }, "Preference successfully updated");

      // 환경 설정값 캐싱.
      if (!err) PREFERENCES.Set(preference.place_id, preference);
    });
  }
};

const delete_a_preferences = (req, res) => {
  if (!req.params.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide preference id for delete");
  } else {
    Preference.deletePreference(req.params.id, (err, info) => {
      jsonRes(req, res, err, { info }, "Preference successfully deleted");
    });
  }
};

export default {
  list_all_preferences,
  create_a_preferences,
  read_a_preference,
  update_a_preferences,
  delete_a_preferences,
};
