import jsonwebtoken from "jsonwebtoken";
import { jsonRes } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import { verify, sign } from "../utils/jwt-util.js";
import crypto from "crypto";
import { encHash, decHash } from "../utils/crypto-util.js";
import MemberLike from "../model/member-like.js";

const list_a_member_likes = (req, res) => {
  // 등록일순으로 1년간의 데이터 조회.
  let {
    member_id,
    filter = "1=1",
    between = "a.reg_date",
    begin = moment().add(-1, "year").format("YYYY-MM-DD 00:00:00"),
    end = moment().format("YYYY-MM-DD 23:59:59"),
    order = "a.id desc",
  } = req.params;

  if (!member_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide member_id for get member_like list");
  } else {
    MemberLike.selectMemberLikes(member_id, filter, between, begin, end, order, (err, member_likes) => {
      jsonRes(req, res, err, { member_likes });
    });
  }
};

const create_a_member_like = (req, res) => {
  const { member_like } = req.body; // post body.

  if (!member_like) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide create_a_member_like member_like  for insert");
  } else {
    MemberLike.insertMemberLike(member_like, (err, info) => {
      jsonRes(req, res, err, { info }, "MemberLike successfully inserted");
    });
  }
};

const read_a_member_like = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "MemberLike provide member_like id for select");
  } else {
    MemberLike.selectMemberLike(id, (err, member_like) => {
      jsonRes(req, res, err, { member_like });
    });
  }
};

const read_a_member_like_by_place = (req, res) => {
  const { place_id, member_id } = req.params;

  if (!place_id || !member_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "MemberLike provide read_a_member_like_by_place place_id or member_id for select");
  } else {
    MemberLike.selectMemberLikeByPlaceId(place_id, member_id, (err, member_like) => {
      jsonRes(req, res, err, { member_like });
    });
  }
};

const update_a_member_like = (req, res) => {
  const { id } = req.params;
  const { member_like } = req.body;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "MemberLike provide member_like id for update");
  } else {
    MemberLike.updateMemberLike(id, member_like, (err, info) => {
      jsonRes(req, res, err, { info, member_like }, "MemberLike successfully updated");
    });
  }
};

const delete_a_member_like = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "MemberLike provide member_like id for delete");
  } else {
    MemberLike.deleteMemberLike(id, (err, info) => {
      jsonRes(req, res, err, { info }, "MemberLike successfully deleted");
    });
  }
};

const delete_a_member_all_like = (req, res) => {
  const { member_id } = req.params;

  if (!member_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "MemberLike provide member_like member_id for all member delete");
  } else {
    MemberLike.deleteAllMemberLike(member_id, (err, info) => {
      jsonRes(req, res, err, { info }, "MemberLike successfully deleted");
    });
  }
};

export default {
  create_a_member_like,
  read_a_member_like_by_place,
  read_a_member_like,
  list_a_member_likes,
  update_a_member_like,
  delete_a_member_like,
  delete_a_member_all_like,
};
