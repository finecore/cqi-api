import IscKeyBox from "../model/isc-key-box.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";

const list_all_isc_key_boxs = (req, res) => {
  const { serialno } = req.params;

  if (!serialno) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide serialno for get isc_key_box list");
  } else {
    IscKeyBox.selectAllIscKeyBoxs(serialno, (err, all_isc_key_boxs) => {
      jsonRes(req, res, err, { all_isc_key_boxs });
    });
  }
};

const list_a_isc_key_boxs = (req, res) => {
  let { filter = "1=1", limit = "10000", order = "gid,did", desc = "" } = req.params; // paging.

  IscKeyBox.selectIscKeyBoxCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      IscKeyBox.selectIscKeyBoxs(filter, order, desc, limit, (err, isc_key_boxs) => {
        jsonRes(req, res, err, { count, isc_key_boxs });
      });
  });
};

const list_a_isc_key_boxs_use = (req, res) => {
  IscKeyBox.selectIscKeyBoxsUse((err, isc_key_boxs) => {
    jsonRes(req, res, err, { isc_key_boxs });
  });
};

const read_a_isc_key_box = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide isc_key_box id for select");
  } else {
    IscKeyBox.selectIscKeyBox(id, (err, isc_key_box) => {
      jsonRes(req, res, err, { isc_key_box });
    });
  }
};

const create_a_isc_key_box = (req, res) => {
  let new_isc_key_box = req.body.isc_key_box; // post body.

  console.log("- create_a_isc_key_box", new_isc_key_box);

  if (!new_isc_key_box.serialno) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide isc_key_box data for insert");
  } else {
    delete new_isc_key_box["mod_date"];
    new_isc_key_box.reg_date = new Date();

    IscKeyBox.insertIscKeyBox(new_isc_key_box, (err, info) => {
      if (info && info.insertId) req.body.isc_key_box.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info }, "IscKeyBox successfully inserted");
    });
  }
};

const update_a_isc_key_box = (req, res) => {
  const { id } = req.params;
  let { isc_key_box } = req.body; // post body.

  if (!id || !isc_key_box) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide isc_key_box id for update");
  } else {
    isc_key_box.id = id;

    IscKeyBox.updateIscKeyBox(isc_key_box, (err, info) => {
      jsonRes(req, res, err, { info }, "IscKeyBox successfully updated");
    });
  }
};

const delete_a_isc_key_box = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide isc_key_box id for delete");
  } else {
    // FOREIGN KEY Constraints table ON DELETE CASCADE ON UPDATE CASCADE
    IscKeyBox.deleteIscKeyBox(id, (err, info) => {
      jsonRes(req, res, err, { info }, "IscKeyBox successfully deleted");
    });
  }
};

export default {
  list_all_isc_key_boxs,
  list_a_isc_key_boxs,
  list_a_isc_key_boxs_use,
  create_a_isc_key_box,
  read_a_isc_key_box,
  update_a_isc_key_box,
  delete_a_isc_key_box,
};
