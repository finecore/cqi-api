import Version from "../model/version.js";
import File from "../model/file.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";

const list_a_versions = (req, res) => {
  let { type, model, filter = "1=1", limit = "10000", order = "a.reg_date", desc = "desc" } = req.params;

  if (type) filter = `a.type=${type}`;
  if (type && model) filter += ` and a.model=${model}`;

  Version.selectVersionCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      Version.selectVersions(filter, order, desc, limit, (err, versions) => {
        jsonRes(req, res, err, { count, versions });
      });
  });
};

const read_a_version = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide version id for select");
  } else {
    Version.selectVersion(id, (err, version) => {
      jsonRes(req, res, err, { version });
    });
  }
};

const create_a_version = (req, res) => {
  const { version } = req.body;

  Version.insertVersion(version, (err, info) => {
    if (info && info.insertId) req.body.version.id = info.insertId; // 채널 전송 시 id 입력.

    jsonRes(req, res, err, { info }, "Version successfully inserted");
  });
};

const update_a_version = (req, res) => {
  const { id } = req.params;
  const { version } = req.body;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide version id for update");
  } else {
    Version.updateVersion(id, version, (err, info) => {
      jsonRes(req, res, err, { info }, "Version successfully updated");
    });
  }
};

const delete_a_version = (req, res) => {
  const { id } = req.params;
  const { version } = req.body;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide version id for delete");
  } else {
    Version.deleteVersion(id, (err, info) => {
      jsonRes(req, res, err, { info }, "Version successfully deleted");
    });
  }
};

export default {
  list_a_versions,
  create_a_version,
  read_a_version,
  update_a_version,
  delete_a_version,
};
