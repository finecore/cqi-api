import DeviceRc from "../model/device-rc.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";

const list_a_device_rcs = (req, res) => {
  let { filter = "1=1", limit = "10000", order = "a.id", desc = "desc" } = req.params;

  DeviceRc.selectDeviceRcCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      DeviceRc.selectDeviceRcs(filter, order, desc, limit, (err, device_rcs) => {
        jsonRes(req, res, err, { count, device_rcs });
      });
  });
};

const list_place_device_rcs = (req, res) => {
  const { place_id } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place_id for get device_rc list");
  } else {
    DeviceRc.selectPlaceDeviceRcs(place_id, (err, device_rcs) => {
      jsonRes(req, res, err, { device_rcs });
    });
  }
};

const read_a_device_rc = (req, res) => {
  const { id, uuid } = req.params;

  if (!id && !uuid) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide device_rc id or uuid for select");
  } else {
    DeviceRc.selectDeviceRc(id, uuid, (err, device_rc) => {
      jsonRes(req, res, err, { device_rc });
    });
  }
};

const create_a_device_rc = (req, res) => {
  let new_device_rc = req.body.device_rc; // post body.

  if (!new_device_rc.place_id || !new_device_rc.name) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide device_rc data for insert");
  } else {
    DeviceRc.insertDeviceRc(new_device_rc, (err, info) => {
      if (info && info.insertId) req.body.device_rc.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info }, "DeviceRc successfully inserted");
    });
  }
};

const update_a_device_rc = (req, res) => {
  const { id } = req.params;
  let device_rc = req.body.device_rc; // post body.

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide device_rc id for update");
  } else {
    DeviceRc.updateDeviceRc(id, device_rc, (err, info) => {
      jsonRes(req, res, err, { info }, "DeviceRc successfully updated");
    });
  }
};

const update_a_device_rc_by_uuid = (req, res) => {
  const { uuid } = req.params;
  let device_rc = req.body.device_rc; // post body.

  if (!uuid) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide device_rc uuid for update");
  } else {
    DeviceRc.updateDeviceRcUuid(uuid, device_rc, (err, info) => {
      jsonRes(req, res, err, { info }, "DeviceRc successfully updated");
    });
  }
};

const delete_a_device_rc = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide device_rc id for delete");
  } else {
    DeviceRc.deleteDeviceRc(id, (err, info) => {
      jsonRes(req, res, err, { info }, "DeviceRc successfully deleted");
    });
  }
};

export default {
  list_a_device_rcs,
  list_place_device_rcs,
  create_a_device_rc,
  read_a_device_rc,
  update_a_device_rc,
  update_a_device_rc_by_uuid,
  delete_a_device_rc,
};
