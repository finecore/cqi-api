import moment from "moment";
import QnaAnswer from "../model/qna-answer.js"; // QnaAnswer 모델
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";

const list_a_qna_answers = (req, res) => {
  const { qna_id } = req.params;

  if (!qna_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide qna_id for selecting qna_answers");
  } else {
    QnaAnswer.selectQnaAnswers(qna_id, (err, qna_answers) => {
      jsonRes(req, res, err, { qna_answers });
    });
  }
};

const read_a_qna_answer = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide answer id for select");
  } else {
    QnaAnswer.selectQnaAnswer(id, (err, answer) => {
      jsonRes(req, res, err, { answer });
    });
  }
};

const create_a_qna_answer = (req, res) => {
  const { qna_id, content } = req.body;

  if (!qna_id || !content) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide qna_id and answer content for insert");
  } else {
    QnaAnswer.insertQnaAnswer({ qna_id: qna_id, content }, (err, info) => {
      jsonRes(req, res, err, { info }, "Answer successfully inserted");
    });
  }
};

const update_a_qna_answer = (req, res) => {
  const { id } = req.params;
  const { content } = req.body;

  if (!id || !content) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide answer id or content for update");
  } else {
    QnaAnswer.updateQnaAnswer(id, { content }, (err, info) => {
      jsonRes(req, res, err, { info }, "Answer successfully updated");
    });
  }
};

const delete_a_qna_answer = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide answer id for delete");
  } else {
    QnaAnswer.deleteQnaAnswer(id, (err, info) => {
      jsonRes(req, res, err, { info }, "Answer successfully deleted");
    });
  }
};

export default {
  list_a_qna_answers,
  read_a_qna_answer,
  create_a_qna_answer,
  update_a_qna_answer,
  delete_a_qna_answer,
};
