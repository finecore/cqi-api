import Qna from "../model/qna.js";
import moment from "moment";
import { jsonRes } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";

// QNA 목록 조회
const list_a_qnas = (req, res) => {
  let {
    filter = "1=1",
    between = "a.reg_date",
    begin = moment().add(-1, "year").format("YYYY-MM-DD 00:00"),
    end = moment().format("YYYY-MM-DD HH:mm"),
    limit = "10000",
    order = "a.reg_date",
    desc = "desc",
    all = "0",
  } = req.query;

  // QNA 목록 카운트 조회
  Qna.selectQnaCount(filter, between, begin, end, all, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else {
      // QNA 목록 조회
      Qna.selectQnas(filter, between, begin, end, order, desc, limit, all, (err, qnas) => {
        jsonRes(req, res, err, { count, qnas });
      });
    }
  });
};

// QNA 이전/다음 항목 조회
const list_a_qnas_prev_next = (req, res) => {
  const { id, filter, between, begin, end } = req.query;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide qna id for select");
  } else {
    Qna.selectQnasPrevNext(id, filter, between, begin, end, (err, qnas) => {
      jsonRes(req, res, err, { qnas });
    });
  }
};

// 특정 QNA 조회
const read_a_qna = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide qna id for select");
  } else {
    Qna.selectQna(id, (err, qna) => {
      jsonRes(req, res, err, { qna });
    });
  }
};

const list_a_qna_with_answers = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide qna id for select");
  } else {
    Qna.selectQnaWithAnswers(id, (err, qnaWithAnswers) => {
      jsonRes(req, res, err, { qnaWithAnswers });
    });
  }
};

// Qna 컨트롤러
const list_a_qnas_with_answers_count = (req, res) => {
  let { filter = "1=1", limit = "10000", order = "q.reg_date", desc = "desc" } = req.params;

  limit = limit.replace(/[\"\']/gi, "");

  Qna.selectQnaWithAnswersCountCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else {
      // FAQ 목록 조회 (답변 개수 포함)
      Qna.selectQnaWithAnswersCount(filter, order, desc, limit, (err, qnas) => {
        jsonRes(req, res, err, { count, qnas });
      });
    }
  });
};

// 새로운 QNA 추가
const create_a_qna = (req, res) => {
  const { qna } = req.body;

  if (!qna) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide qna data for creation");
  } else {
    Qna.insertQna(qna, (err, info) => {
      if (info && info.insertId) req.body.qna.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info }, "Qna successfully inserted");
    });
  }
};

// 기존 QNA 수정
const update_a_qna = (req, res) => {
  const { id } = req.params;
  const { qna } = req.body;

  if (!id || !qna) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide qna id or data for update");
  } else {
    Qna.updateQna(id, qna, (err, info) => {
      jsonRes(req, res, err, { info }, "Qna successfully updated");
    });
  }
};

// 특정 QNA 삭제
const delete_a_qna = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide qna id for delete");
  } else {
    Qna.deleteQna(id, (err, info) => {
      jsonRes(req, res, err, { info }, "Qna successfully deleted");
    });
  }
};

export default {
  list_a_qnas,
  list_a_qnas_prev_next,
  list_a_qna_with_answers,
  list_a_qnas_with_answers_count,
  create_a_qna,
  read_a_qna,
  update_a_qna,
  delete_a_qna,
};
