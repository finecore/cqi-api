import NoticePlace from "../model/notice-place.js";
import File from "../model/file.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";

const list_a_notice_places = (req, res) => {
  let {
    filter = "1=1",
    between = "a.reg_date",
    begin = moment().add(-1, "month").format("YYYY-MM-DD 00:00"),
    end = moment().format("YYYY-MM-DD HH:mm"),
    limit = "10000",
    order = "a.reg_date",
    desc = "desc",
    all = "0",
  } = req.params;

  NoticePlace.selectNoticePlaceCount(filter, between, begin, end, all, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      NoticePlace.selectNoticePlaces(filter, between, begin, end, order, desc, limit, all, (err, notice_places) => {
        jsonRes(req, res, err, { count, notice_places });
      });
  });
};

const read_a_notice_place = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide notice_place id for select");
  } else {
    NoticePlace.selectNoticePlace(id, (err, notice_place) => {
      jsonRes(req, res, err, { notice_place });
    });
  }
};

const create_a_notice_place = (req, res) => {
  const { notice_place } = req.body;

  if (!notice_place) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide notice_place data for update");
  } else {
    notice_place.reg_date = new Date();

    NoticePlace.insertNoticePlace(notice_place, (err, info) => {
      if (info && info.insertId) req.body.notice_place.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info }, "NoticePlace successfully inserted");
    });
  }
};

const update_a_notice_place = (req, res) => {
  const { id } = req.params;
  const { notice_place } = req.body;

  if (!id || !notice_place) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide notice_place id or data for update");
  } else {
    NoticePlace.updateNoticePlace(id, notice_place, (err, info) => {
      jsonRes(req, res, err, { info }, "NoticePlace successfully updated");
    });
  }
};

const delete_a_notice_place = (req, res) => {
  const { id } = req.params;
  const { notice_place } = req.body;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide notice_place id for delete");
  } else {
    NoticePlace.deleteNoticePlace(id, (err, info) => {
      jsonRes(req, res, err, { info }, "NoticePlace successfully deleted");
    });
  }
};

export default {
  list_a_notice_places,
  create_a_notice_place,
  read_a_notice_place,
  update_a_notice_place,
  delete_a_notice_place,
};
