import RoomReserv from "../model/room-reserv.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import moment from "moment";
import { sign } from "../utils/jwt-util.js";

const list_all_room_reserv = (req, res) => {
  let {
    place_id,
    filter = "1=1",
    between = "check_in_exp",
    begin = moment().add(-1, "year").format("YYYY-MM-DD 00:00:00"),
    end = moment().format("YYYY-MM-DD 23:59:59"),
    order = "check_in_exp asc",
  } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place_id for get room reserv list");
  } else {
    RoomReserv.selectAllRoomReservs(place_id, filter, between, begin, end, order, (err, all_room_reservs) => {
      jsonRes(req, res, err, { all_room_reservs });
    });
  }
};

const list_member_room_reserv = (req, res) => {
  let {
    member_id,
    filter = "1=1",
    between = "check_in_exp",
    begin = moment().add(-1, "year").format("YYYY-MM-DD 00:00:00"),
    end = moment().format("YYYY-MM-DD 23:59:59"),
    order = "check_in_exp asc",
  } = req.params;

  if (!member_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide member_id for get room reserv list");
  } else {
    RoomReserv.selectMemberRoomReservs(member_id, filter, between, begin, end, order, (err, room_reservs) => {
      jsonRes(req, res, err, { room_reservs });
    });
  }
};

const list_a_room_reservs = (req, res) => {
  let {
    filter = "1=1",
    between = "check_in_exp",
    begin = moment().add(-1, "year").format("YYYY-MM-DD 00:00:00"),
    end = moment().format("YYYY-MM-DD 23:59:59"),
    order = "check_in_exp asc",
  } = req.params;

  if (!filter) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room reserv filter for select");
  } else {
    RoomReserv.selectRoomReservs(filter, between, begin, end, order, (err, room_reservs) => {
      jsonRes(req, res, err, { room_reservs });
    });
  }
};

const list_a_can_reserv_rooms = (req, res) => {
  let { filter = "1=1", between = "a.check_in_exp", begin = moment().add(-1, "year").format("YYYY-MM-DD 00:00:00"), end = moment().format("YYYY-MM-DD 23:59:59"), order = "b.name asc" } = req.params;

  if (!filter) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room reserv filter for select");
  } else {
    RoomReserv.selectRoomsCanReservByTypeId(filter, between, begin, end, order, (err, room_reservs) => {
      jsonRes(req, res, err, { room_reservs });
    });
  }
};

const list_a_can_not_reserv_rooms = (req, res) => {
  let { filter = "1=1", between = "a.check_in_exp", begin = moment().add(-1, "year").format("YYYY-MM-DD 00:00:00"), end = moment().format("YYYY-MM-DD 23:59:59"), order = "b.name asc" } = req.params;

  if (!filter) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room reserv filter for select");
  } else {
    RoomReserv.selectRoomsCanNotReservByTypeId(filter, between, begin, end, order, (err, room_reservs) => {
      jsonRes(req, res, err, { room_reservs });
    });
  }
};

const read_a_room_id_reserv = (req, res) => {
  let {
    room_id,
    filter = "1=1",
    between = "check_in_exp",
    begin = moment().add(-1, "year").format("YYYY-MM-DD 00:00:00"),
    end = moment().format("YYYY-MM-DD 23:59:59"),
    order = "check_in_exp asc",
  } = req.params;

  if (!room_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room_id for get room reserv list");
  } else {
    RoomReserv.selectRoomIdRoomReservs(room_id, filter, between, begin, end, order, (err, room_reserv) => {
      jsonRes(req, res, err, { room_reserv });
    });
  }
};

const read_a_item_id_reserv = (req, res) => {
  let {
    id,
    filter = "1=1",
    between = "check_in_exp",
    begin = moment().add(-1, "year").format("YYYY-MM-DD 00:00:00"),
    end = moment().format("YYYY-MM-DD 23:59:59"),
    order = "check_in_exp asc",
  } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide id for get room reserv list");
  } else {
    RoomReserv.selectItemIdReservs(id, filter, between, begin, end, order, (err, room_reserv) => {
      jsonRes(req, res, err, { room_reserv });
    });
  }
};

const read_a_place_id_reserv = (req, res) => {
  let {
    place_id,
    filter = "1=1",
    between = "check_in_exp",
    begin = moment().add(-1, "year").format("YYYY-MM-DD 00:00:00"),
    end = moment().format("YYYY-MM-DD 23:59:59"),
    order = "check_in_exp asc",
  } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place_id for get room reserv list");
  } else {
    RoomReserv.selectPlaceIdRoomReservs(place_id, filter, between, begin, end, order, (err, room_reservs) => {
      jsonRes(req, res, err, { room_reservs });
    });
  }
};

const list_a_place_enable_reservs = (req, res) => {
  let { place_id } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide enable room reserv place_id for select");
  } else {
    RoomReserv.selectPlaceEnableReservs(place_id, (err, room_reservs) => {
      jsonRes(req, res, err, { room_reservs });
    });
  }
};

const read_a_room_reserv = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room reserv id for select");
  } else {
    RoomReserv.selectRoomReserv(id, (err, room_reserv) => {
      jsonRes(req, res, err, { room_reserv });
    });
  }
};

const read_a_room_reserv_num = (req, res) => {
  const { num } = req.params;

  if (!num) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room reserv num for select");
  } else {
    RoomReserv.selectRoomReservNum(num, (err, room_reserv) => {
      jsonRes(req, res, err, { room_reserv });
    });
  }
};

const read_a_member_reserv = (req, res) => {
  const { num } = req.params;

  if (!num) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room reserv num for select");
  } else {
    RoomReserv.selectRoomReservNum(num, (err, room_reserv) => {
      jsonRes(req, res, err, { room_reserv });
    });
  }
};

const read_a_room_reserv_mms_mo_num = (req, res) => {
  const { member_id } = req.params;

  if (!member_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide member reserv member_id for select");
  } else {
    RoomReserv.selectMemberReserv(member_id, (err, room_reserv) => {
      jsonRes(req, res, err, { room_reserv });
    });
  }
};

const read_a_room_reserv_now = (req, res) => {
  const { room_id } = req.params;

  if (!room_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room reserv now room id for select");
  } else {
    RoomReserv.selectRoomReservNow(room_id, (err, room_reserv) => {
      jsonRes(req, res, err, { room_reserv });
    });
  }
};

const create_a_room_reserv = (req, res) => {
  const { room_reserv } = req.body; // post body.

  if (!room_reserv.state || !room_reserv.place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room_reserv data mission state or place_id for insert");
  } else {
    RoomReserv.insertRoomReserv(room_reserv, (err, info) => {
      if (info && info.insertId) req.body.room_reserv.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info }, "RoomReserv successfully inserted");

      const { place_id } = room_reserv;

      if (!err && place_id) {
        RoomReserv.selectPlaceEnableReservs(place_id, (err, room_reservs) => {
          if (!err && room_reservs) {
            // jsonRes 전송 후 이므로 명시적 전송.
            send_socket(place_id, room_reservs, req);
          }
        });
      }
    });
  }
};

const update_a_room_reserv = (req, res) => {
  const { id } = req.params;
  const { room_reserv } = req.body;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room reserv data for update");
  } else {
    RoomReserv.updateRoomReserv(id, room_reserv, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomReserv successfully updated");

      if (!err) {
        RoomReserv.selectRoomReserv(id, (err, room_reserv) => {
          if (!err && room_reserv[0]) {
            const { place_id } = room_reserv[0];

            if (place_id) {
              RoomReserv.selectPlaceEnableReservs(place_id, (err, room_reservs) => {
                if (!err && room_reservs) {
                  // jsonRes 전송 후 이므로 명시적 전송.
                  send_socket(place_id, room_reservs, req);
                }
              });
            }
          }
        });
      }
    });
  }
};

const delete_a_room_reserv = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room reserv id for delete");
  } else {
    RoomReserv.deleteRoomReserv(id, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomReserv successfully deleted");
    });
  }
};

const delete_all_room_reserv = (req, res) => {
  const { room_id } = req.params;

  if (!room_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room reserv id for delete");
  } else {
    RoomReserv.deleteRoomReservs(room_id, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomReserv successfully all deleted");
    });
  }
};

const delete_all_member_reserv = (req, res) => {
  const { member_id } = req.params;

  if (!member_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide member_id  for delete");
  } else {
    RoomReserv.deleteMemberReservs(member_id, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomReserv successfully all deleted");
    });
  }
};

const send_socket = (place_id, room_reservs, req) => {
  console.log("---> RoomReserv send_socket", place_id);

  // 토큰 생성.
  sign({ channel: "api", place_id: place_id }, (err, token) => {
    if (token) {
      const { channel, uuid } = req ? req.headers : {};

      const json = {
        type: "SET_PLACE_ENABLE_RESERV_LIST", // 웹 reducer type, 어드민/모바일 store 타입은 socket server 에서 각각 변경.
        headers: {
          method: "put",
          url: "room/reserv/enable/" + place_id,
          token,
          channel: "api",
          uuid,
          req_channel: channel,
        },
        body: {
          room_reservs,
          place_id,
        },
      };

      // 소켓 서버로 전송.
      global.dispatcher.dispatch(json);
    }
  });
};

export default {
  list_all_room_reserv,
  list_member_room_reserv,
  list_a_can_reserv_rooms,
  list_a_can_not_reserv_rooms,
  read_a_item_id_reserv,
  read_a_room_id_reserv,
  read_a_place_id_reserv,
  list_a_room_reservs,
  list_a_place_enable_reservs,
  read_a_room_reserv,
  read_a_room_reserv_num,
  read_a_room_reserv_mms_mo_num,
  read_a_member_reserv,
  create_a_room_reserv,
  update_a_room_reserv,
  delete_a_room_reserv,
  delete_all_room_reserv,
  delete_all_member_reserv,
  send_socket,
};
