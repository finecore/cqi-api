import PlaceImgs from "../model/place-imgs.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import moment from "moment";

const list_all_place_imgs = (req, res) => {
  const { place_id } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place_id for get place imgs list");
  } else {
    PlaceImgs.selectAllPlaceImgss(place_id, (err, all_place_imgss) => {
      jsonRes(req, res, err, { all_place_imgss });
    });
  }
};

const read_a_place_imgs = (req, res) => {
  if (!req.params.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place imgs id for select");
  } else {
    PlaceImgs.selectPlaceImgs(req.params.id, (err, place_imgs) => {
      jsonRes(req, res, err, { place_imgs });
    });
  }
};

const create_a_place_imgs = (req, res) => {
  var new_place_imgs = req.body.place_imgs; // post body.

  if (!new_place_imgs.place_id || !new_place_imgs.user_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place imgs data for insert");
  } else {
    PlaceImgs.insertPlaceImgs(new_place_imgs, (err, info) => {
      if (info && info.insertId) req.body.place_imgs.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info }, "PlaceImgs successfully inserted");
    });
  }
};

const update_a_place_imgs = (req, res) => {
  const { id } = req.params;
  let { place_imgs } = req.body; // post body.

  if (!id || !place_imgs) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place imgs data for update");
  } else {
    PlaceImgs.updatePlaceImgs(id, place_imgs, (err, info) => {
      jsonRes(req, res, err, { info }, "PlaceImgs successfully updated");
    });
  }
};

const delete_a_place_imgs = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place imgs id for delete");
  } else {
    PlaceImgs.deletePlaceImgs(id, (err, info) => {
      jsonRes(req, res, err, { info }, "PlaceImgs successfully deleted");
    });
  }
};

const delete_all_place_imgs = (req, res) => {
  if (!req.params.place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place_id for delete");
  } else {
    PlaceImgs.deleteAllPlaceImgs(req.params.place_id, (err, info) => {
      jsonRes(req, res, err, { info }, "PlaceImgs successfully all deleted");
    });
  }
};

export default {
  list_all_place_imgs,
  read_a_place_imgs,
  create_a_place_imgs,
  update_a_place_imgs,
  delete_a_place_imgs,
  delete_all_place_imgs,
};
