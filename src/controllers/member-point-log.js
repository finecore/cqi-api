import MemberPointLog from "../model/member-point-log.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import moment from "moment";

const list_all_member_point_log = (req, res) => {
  const {
    place_id,
    filter = "1=1",
    between = "a.reg_date",
    begin = moment().add(-1, "day").format("YYYY-MM-DD HH:mm"),
    end = moment().format("YYYY-MM-DD HH:mm"),
    order = "a.reg_date desc",
  } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place_id for get member_point_log log list");
  } else {
    MemberPointLog.selectAllMemberPointLogs(place_id, filter, between, begin, end, order, (err, all_member_point_logs) => {
      jsonRes(req, res, err, { all_member_point_logs });
    });
  }
};

const read_a_member_point_logs = (req, res) => {
  // 등록일순으로 1년간의 데이터 조회.
  let {
    member_id,
    filter = "1=1",
    between = "a.reg_date",
    begin = moment().add(-1, "year").format("YYYY-MM-DD 00:00:00"),
    end = moment().format("YYYY-MM-DD 23:59:59"),
    order = "a.reg_date desc",
  } = req.params;

  if (!member_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide member_id for get member_like list");
  } else {
    MemberPointLog.selectMemberPointLogs(member_id, filter, between, begin, end, order, (err, member_point_logs) => {
      jsonRes(req, res, err, { member_point_logs });
    });
  }
};

const read_a_member_point_logs_by_place = (req, res) => {
  let { place_id, limit = 500 } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide member_point_log log place_id for select");
  } else {
    MemberPointLog.selectMemberPointLogsByPlace(place_id, Number(limit), (err, _logs) => {
      jsonRes(req, res, err, { member_point_logs });
    });
  }
};

const read_a_member_point_day_logs = (req, res) => {
  let { place_id, day = -1 } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide member_point_log place_id for select");
  } else {
    MemberPointLog.selectMemberPointDayLogs(place_id, day, (err, member_point_logs) => {
      jsonRes(req, res, err, { member_point_logs });
    });
  }
};

const read_a_member_point_last_logs = (req, res) => {
  let { place_id, last_id = 0 } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide member_point_log place_id for select");
  } else {
    MemberPointLog.selectMemberPointLastLogs(place_id, last_id, (err, member_point_logs) => {
      jsonRes(req, res, err, { member_point_logs });
    });
  }
};

const read_a_member_point_log = (req, res) => {
  let { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide member_point_log id for select");
  } else {
    MemberPointLog.selectMemberPointLog(id, (err, member_point_log) => {
      jsonRes(req, res, err, { member_point_log });
    });
  }
};

const create_a_member_point_log = (req, res) => {
  var new_member_point_log = req.body.member_point_log; // post body.

  if (!new_member_point_log.room_id || !new_member_point_log.data) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide member_point_log data for insert");
  } else {
    if (new_member_point_log.change_point !== undefined && new_member_point_log.change_point !== 0)
      MemberPointLog.insertMemberPointLog(new_member_point_log, (err, info) => {
        if (info && info.insertId) req.body.member_point_log.id = info.insertId; // 채널 전송 시 id 입력.

        jsonRes(req, res, err, { info }, "MemberPointLog successfully inserted");
      });
  }
};

const update_a_member_point_log = (req, res) => {
  var member_point_log = req.body.member_point_log; // post body.

  if (!req.params.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide member_point_log id for update");
  } else {
    MemberPointLog.updateMemberPointLog(req.params.id, member_point_log, (err, info) => {
      jsonRes(req, res, err, { info }, "MemberPointLog successfully updated");
    });
  }
};

const delete_a_member_point_log = (req, res) => {
  if (!req.params.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide member_point_log id for delete");
  } else {
    MemberPointLog.deleteMemberPointLog(req.params.room_id, (err, info) => {
      jsonRes(req, res, err, { info }, "MemberPointLog successfully deleted");
    });
  }
};

export default {
  list_all_member_point_log,
  create_a_member_point_log,
  read_a_member_point_logs,
  read_a_member_point_day_logs,
  read_a_member_point_last_logs,
  read_a_member_point_log,
  update_a_member_point_log,
  delete_a_member_point_log,
};
