import SyncLog from "../model/sync-log.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import dayjs from "dayjs";

const list_all_sync_logs = (req, res) => {
  let { filter = "1=1", order = "reg_date", desc = "asc", limit = "100" } = req.params;

  SyncLog.selectSyncLogCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      SyncLog.selectSyncLogs(filter, order, desc, limit, (err, synclogs) => {
        jsonRes(req, res, err, { count, synclogs });
      });
  });
};

const read_a_sync_log = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Plesyncloge provide synclog id for select");
  } else {
    SyncLog.selectSyncLog(id, (err, synclog) => {
      jsonRes(req, res, err, { synclog });
    });
  }
};

const create_a_sync_log = (req, res) => {
  let { synclog } = req.body;

  SyncLog.insertSyncLog(synclog, (err, info) => {
    if (info && info.insertId) synclog.id = info.insertId; // 채널 전송 시 id 입력.

    jsonRes(req, res, err, { synclog, info }, "SyncLog successfully inserted");
  });
};

const update_a_sync_log = (req, res) => {
  const { id } = req.params;
  const { synclog } = req.body;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Plesyncloge provide synclog id for update");
  } else {
    SyncLog.updateSyncLog(id, synclog, (err, info) => {
      jsonRes(req, res, err, { info }, "SyncLog successfully updated");
    });
  }
};

const delete_a_sync_log = (req, res) => {
  const { id } = req.params;
  const { synclog } = req.body;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Plesyncloge provide synclog id for delete");
  } else {
    SyncLog.deleteSyncLog(id, (err, info) => {
      jsonRes(req, res, err, { info }, "SyncLog successfully deleted");
    });
  }
};

const delete_all_old_sync_log = (req, res) => {
  const yesterday = dayjs().add(-1, "day").format("YYYY-MM-DD");

  if (!yesterday) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Plesyncloge provide synclog id for delete");
  } else {
    SyncLog.deleteSyncLogAfter24(yesterday, (err, info) => {
      jsonRes(req, res, err, { info }, "SyncLog successfully deleted");
    });
  }
};

export default {
  list_all_sync_logs,
  create_a_sync_log,
  read_a_sync_log,
  update_a_sync_log,
  delete_a_sync_log,
  delete_all_old_sync_log,
};
