import MmsMo from "../model/mms-mo.js";
import File from "../model/file.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";

const list_a_mms_mos = (req, res) => {
  let { filter = "1=1", limit = "10000", order = "a.MO_KEY", desc = "desc" } = req.params;

  MmsMo.selectMmsMoCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      MmsMo.selectMmsMoes(filter, order, desc, limit, (err, mms_mos) => {
        jsonRes(req, res, err, { count, mms_mos });
      });
  });
};

const read_a_mms_mo = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide mms_mo id for select");
  } else {
    MmsMo.selectMmsMo(id, (err, mms_mo) => {
      jsonRes(req, res, err, { mms_mo });
    });
  }
};

const create_a_mms_mo = (req, res) => {
  const { mms_mo } = req.body;

  mms_mo.reg_date = new Date();

  MmsMo.insertMmsMo(mms_mo, (err, info) => {
    if (info && info.insertId) req.body.mms_mo.id = info.insertId; // 채널 전송 시 id 입력.

    jsonRes(req, res, err, { info }, "MmsMo successfully inserted");
  });
};

const update_a_mms_mo = (req, res) => {
  const { id } = req.params;
  const { mms_mo } = req.body;

  if (!id || !mms_mo) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide mms_mo id for update");
  } else {
    MmsMo.updateMmsMo(id, mms_mo, (err, info) => {
      jsonRes(req, res, err, { info }, "MmsMo successfully updated");
    });
  }
};

const delete_a_mms_mo = (req, res) => {
  const { id } = req.params;
  const { mms_mo } = req.body;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide mms_mo id for delete");
  } else {
    MmsMo.deleteMmsMo(id, (err, info) => {
      jsonRes(req, res, err, { info }, "MmsMo successfully deleted");
    });
  }
};

export default {
  list_a_mms_mos,
  read_a_mms_mo,
  create_a_mms_mo,
  update_a_mms_mo,
  delete_a_mms_mo,
};
