// .env config.
import dotenv from "dotenv";
dotenv.config();

// mysql db
const config = {
  connectionLimit: 500,
  connectTimeout: 10000,
  multipleStatements: true,
  host: process.env.DB_HOST_IP,
  port: process.env.DB_HOST_PORT,
  user: process.env.DB_HOST_USER,
  password: process.env.DB_HOST_PWD,
  database: "checkqin",
  charset: "utf8mb4",
  debug: false,
  authPlugins: {
    mysql_native_password: () => () => Buffer.from("Imonaico0!"),
  },
};

console.info("-------------------------------------------------");
console.info("- DB Config DB_HOST_LOCAL : " + process.env.DB_HOST_LOCAL);
console.table(config);
console.info("-------------------------------------------------\n");

export default config;
