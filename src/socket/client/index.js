import net from "net";
import chalk from "chalk";
import _ from "lodash";
import ip from "ip";

import { sign } from "../../utils/jwt-util.js";
import SocketParser from "../../helper/socket-parser.js";

// Socket Server <-> HOST_URL 통신용 소켓 클라이언트.
// 분산 서버 환경에서는 Client 를 각 서버별로 생성 해서 동시에 전송 한다.(dispatcher 에서 생성)
class SocketClient {
  constructor(host, port) {
    this.host = host;
    this.port = port;

    this.hostName = `${host}:${port}`;

    const ipaddr = ip.address(); // my ip address
    this.clientName = ipaddr;

    this.socket = null;
    this.interval = 10 * 1000; // 연결 체크 인터벌.
    this.parser = SocketParser;
  }

  init() {
    console.log(`-> Tcp Client init: ${this.host} :  ${this.port}`);

    if (!this.host || !this.port) {
      console.error("-> Socket Server init error, host or port empty!");
      return false;
    }

    this.socket = new net.Socket();

    this.socket.connect(this.port, this.host);

    // write 시 딜레이를 주지 않고 바로 전송(딜레이 시 다음 데이터가 붙어서 전송 되므로 JSON 파싱 오류 발생!)
    this.socket.setNoDelay(true);

    this.socket.on("connect", (sock) => {
      console.log(`-> Tcp Client connected to: ${this.host} :  ${this.port}`);

      // JWT(Json Web Token) 생성.
      sign({ channel: "api", place_id: 0, member_id: 0 }, (err, token) => {
        const json = {
          headers: { url: "", channel: "api", token },
          body: "",
        };

        this.send(json);
      });
    });

    this.socket.on("data", (buff) => {
      // buffer to json.
      const json = this.parser.decode(buff, "utf8");

      console.log(chalk.yellow(`--------> From Socket Server <-------`));
      console.log(this.hostName + " -> " + this.clientName);
      console.log(chalk.grey(JSON.stringify(json)));
      console.log(chalk.yellow("-------------------------------------\n"));

      const { headers } = json;

      // API -> WEB,DEVICE bordcast
      if (headers) {
        if (headers["x-access-token"])
          // x-access-token -> token 으로 변경.
          headers.token = headers["x-access-token"];

        // Api -> Web Respose 는 제외.
        if (headers.token) {
          // TODO 외부 장비로 전송
        } else {
          console.info("-> No Header Token!", headers);
        }
      }
    });

    this.socket.on("error", (err) => {
      console.info("-> Socket Server ", err);
    });

    this.socket.on("close", () => {
      console.info("-> Socket Server closed");
      // this.socket.destroy();
    });

    // 소켓 연결 체크 및 재시도.
    setInterval(() => {
      this.reconnect();
    }, this.interval);
  }

  reconnect() {
    if (this.socket) {
      if (!this.socket.writable) {
        this.socket.connect(this.port, this.host);
      }
    } else {
      this.init();
    }
  }

  send(json) {
    if (!this.socket || !this.socket.writable) {
      console.log("-> Socket un writable", json);
    } else {
      // json to buffer.
      const buff = this.parser.encode(json, "utf8");

      // send to client.
      this.socket.write(buff);

      console.log(chalk.yellow(`---------< To Socket Server >--------`));
      console.log(this.clientName + " -> " + this.hostName);
      let msg = JSON.stringify(json);
      msg = msg.length > 1000 ? msg.substring(0, 1000) + " ....more..." : msg;
      console.log(chalk.grey(msg));
      console.log(chalk.yellow("-------------------------------------\n"));
    }
  }
}

export default SocketClient;
