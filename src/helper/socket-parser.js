/**
 * Socket Parser Class.
 */
const SocketParser = {
  isJson(json) {
    return /^[\],:{}\s]*$/.test(json.toString());
  },

  /**
   * buffer to json.
   *
   * @param {*} buff
   * @param {string} [encoding="base64"] (ascii, utf8, base64)
   * @returns
   * @memberof SocketParser
   */
  decode(buff, encoding = "utf8") {
    var json = buff.toString(encoding);

    try {
      json = json.replace(/"\\/gi, "").trim();
      json = JSON.parse(json);
    } catch (e) {
      json = this.error(500, e.message, "전문 형식이 맞지 않습니다.", json);
    }
    return json;
  },

  /**
   * json to buffer.
   *
   * @param {*} json
   * @param {string} [encoding="base64"] (ascii, utf8, base64)
   * @returns
   * @memberof SocketParser
   */
  encode(json = "", encoding = "utf8") {
    if (typeof json === "object") json = JSON.stringify(json);

    try {
      json = json.replace(/"\\/gi, "");
    } catch (e) {
      console.log("-> encode error", e);
    }

    return new Buffer.from(json, encoding);
  },

  error(code = 500, message = "", detail = "", json = "") {
    return {
      common: {
        success: false,
        error: {
          code: code || 500,
          message: message,
          detail: detail || "소켓 오류 입니다.",
        },
      },
      body: json || "",
    };
  },
};

export default SocketParser;
