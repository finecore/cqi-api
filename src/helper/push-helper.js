import PushLog from '../model/socket-log.js';
import Room from '../model/room.js';
import AppInfo from '../model/app-info.js';
import AppPush from '../model/app-push.js';
import { verify, sign } from '../utils/jwt-util.js';

import request from 'request';
import { Expo } from 'expo-server-sdk';
import _ from 'lodash';
import moment from 'moment';

const expo = new Expo();

/**
 * Push Helper Class.
 */
class PushHelper {
  constructor(socket) {
    this.socket = socket;

    this.rooms = [];

    // 객실정보 조회.
    setInterval(() => {
      this.loadRooms();
    }, 1000 * 60);

    // 20초마다 주기적으로 푸시 전송.
    setInterval(() => {
      this.sendAppPush();
    }, 1000 * 20);

    this.loadRooms();
  }

  loadRooms() {
    Room.selectAllRooms((err, rooms) => {
      this.rooms = rooms;
    });
  }

  setAppPush(token, app_push) {
    AppPush.updateTokenAppPush(token, app_push, (err, info) => {});
  }

  sendAppPush() {
    let filter =
      'a.send = 0 AND a.read = 0 AND a.reg_date > DATE_ADD(NOW(), INTERVAL -1 MINUTE)'; // 1분 이내 데이터 전송.
    let limit = 10;

    // 7 일 지난 푸시 삭제.
    AppPush.deleteOldAppPush(-7, (err, info) => {});

    AppPush.selectAppPushs(filter, limit, (err, app_pushs) => {
      if (app_pushs) {
        this.push(app_pushs);
      }
    });
  }

  add(app_push, callback) {
    AppPush.insertAppPush(app_push, (err, info) => {
      if (info) {
        app_push.id = info.insertId;
        console.log('- PushHelper insertAppPush', info.insertId);
      }
      if (callback) callback(app_push);
    });
  }

  push(app_pushs) {
    console.log('- PushHelper push', app_pushs.length);

    if (app_pushs.length) {
      let notifications = [];

      for (let info of app_pushs) {
        // Each push token looks like ExponentPushToken[xxxxxxxxxxxxxxxxxxxxxx]

        let {
          id,
          token,
          title,
          message,
          room_id,
          data = {},
          send,
          read,
          user_id,
          type,
          os,
          version,
          badge,
        } = info;

        // console.log('- push info', info);

        data = JSON.parse(data);

        data.id = id;

        // 객실명 추가.
        if (room_id) {
          const room = _.find(this.rooms, { id: room_id });
          if (room) message = room.name + ' ' + message;
        }

        if (!Expo.isExpoPushToken(token)) {
          console.error(`Push token ${token} is not a valid Expo push token`);
          continue;
        }

        notifications.push({
          to: token,
          sound: 'default',
          badge: ++badge,
          title: title || '이모나이코 알림',
          body: message, // 잠금 화면 표시 정보.
          priority: 'high',
          data,
        });
      }

      if (notifications.length) {
        let chunks = expo.chunkPushNotifications(notifications);

        (async () => {
          for (let chunk of chunks) {
            try {
              let receipts = await expo.sendPushNotificationsAsync(chunk);

              _.map(receipts, (item, idx) => {
                let success = item.status === 'ok';
                let push = chunk[idx];

                // console.log("- receipt", item, idx, push);

                // 푸시 전송 여부 업데이트.
                if (success) {
                  //  badge count up.
                  let { to, badge } = push;

                  let app_push = { send: 1, badge };

                  this.setAppPush(to, app_push);
                }
              });
            } catch (error) {
              console.info('PushHelper push error', error);
            }
          }
        })();
      }
    }
  }

  sendSocket(app_push) {
    // console.log("---> sendSocket app_push", app_push);

    let { id, place_id } = app_push;

    if (place_id) {
      // 토큰 생성.
      sign({ channel: 'api', place_id }, (err, token) => {
        if (token) {
          const json = {
            type: 'SET_APP_PUSH', // 웹 reducer type, 어드민/모바일 store 타입은 socket server 에서 각각 변경.
            headers: {
              method: 'put',
              url: 'app/push/' + id,
              token,
              channel: 'api',
              place_id,
            },
            body: {
              app_push,
            },
          };

          // 소켓 서버로 전송.
          global.dispatcher.dispatch(json);
        }
      });
    }
  }
}

export default PushHelper;
