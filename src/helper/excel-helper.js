import excel4node from "excel4node";
import _ from "lodash";

function setSheet(wb, sectionTitle, rows) {
  var sheet = wb.addWorksheet(sectionTitle);
  var titleStyle = setStyle(wb, "title");
  var contentStyle = setStyle(wb, "contents");

  // console.log("- setSheet", sectionTitle, rows);

  if (!rows || !rows[0]) return;

  _.each(rows, (f, row) => {
    let col = 1;
    _.each(f, (v, k) => {
      // console.log({ row, col, v, k });
      if (row === 0) {
        let width = 12;
        if (k.indexOf("check_") > -1 || k.lastIndexOf("_date") > -1) width = 17;
        sheet.column(col).setWidth(width);
      }
      setInputText(sheet, row + 1, col, v, row === 0 ? titleStyle : contentStyle);
      col++;
    });
  });
}

function setStyle(wb, type) {
  var border = {
    top: { style: "thin" },
    bottom: { style: "thin" },
    left: { style: "thin" },
    right: { style: "thin" },
  };
  var alignment = { horizontal: "center" };
  var family = "decorative";
  var fillType = "none";
  var font = {};
  var fill = {};

  if (type == "title") {
    font = {
      bold: true,
      size: 10,
    };
    alignment = { horizontal: "center" };
    fill = {
      patternType: "solid",
      fgColor: "#F8F5EE",
    };
    fillType = "pattern";
  } else if (type == "contents") {
    font = {
      size: 9,
      wrapText: true,
    };
    alignment = { horizontal: "left" };
  } else if (type == "center") {
    font = { size: 9 };
    alignment = { horizontal: "center" };
  }

  font.family = family;
  fill.type = fillType;
  return wb.createStyle({
    border: border,
    font: font,
    alignment: alignment,
    fill: fill,
  });
}

function setInputText(sheet, row, col, text, style) {
  sheet.cell(row, col).style(style);

  //  console.log(row, " ", col);

  if (!text || text == "" || typeof text == "undefined" || text == undefined || text == "undefined") {
    return sheet.cell(row, col).string(" ");
  } else {
    text = text.toString().replace(/[\x00-\x09\x0B-\x1F]/gi, "");
    sheet.cell(row, col).string(text);
  }
}

const makeExcel = function (title, path, name, data, cb) {
  var wb = new excel.Workbook();

  setSheet(wb, title, data);

  var filename = path + "/" + name + ".xlsx";

  wb.write(filename, function (err, stats) {
    if (err) {
      console.error(err);
      return cb(err);
    } else {
      console.log(stats);
      cb(null, filename);
    }
  });
};

export default makeExcel;
