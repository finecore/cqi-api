const debug = process.env.MODE !== 'PROD';

const REMOTE_HOST = process.env.REMOTE_API_HOST;
const REMOTE_PORT = process.env.REMOTE_API_PORT;
const HOST = process.env.HOST;
const PORT = process.env.PORT;
const SMS_SEND_NO = process.env.SMS_SEND_NO;
const TWILIO_ACCOUNT_SID = process.env.TWILIO_ACCOUNT_SID;
const TWILIO_AUTH_TOKEN = process.env.TWILIO_AUTH_TOKEN;
const TWILIO_PHONE_NUMBER = process.env.TWILIO_PHONE_NUMBER;
const NICE_KEY = process.env.NICE_KEY;
const NICE_SEC = process.env.NICE_SEC;

const IS_SSL = Boolean(process.env.VITE_SSL);
const SSL_PATH = process.env.VITE_SSL_PATH;
const SSL_PWD = process.env.VITE_SSL_PWD;

const HOST_URL = HOST + ':' + PORT; // ngrok 는 port 럾다
const SOC_LOCAL_URL = HOST + ':' + PORT; // ngrok 는 port 럾다
const REMOTE_API_HOST_URL = REMOTE_HOST + ':' + REMOTE_PORT; // ngrok 는 port 럾다

console.log('->  HOST_URL ', { HOST_URL, REMOTE_API_HOST_URL });

export {
  HOST,
  PORT,
  HOST_URL,
  IS_SSL,
  SSL_PATH,
  SSL_PWD,
  SOC_LOCAL_URL,
  REMOTE_API_HOST_URL,
  SMS_SEND_NO,
  TWILIO_ACCOUNT_SID,
  TWILIO_AUTH_TOKEN,
  TWILIO_PHONE_NUMBER,
  NICE_KEY,
  NICE_SEC,
};
