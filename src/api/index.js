import { api, ajax } from "../utils/cloud-api-util.js";

// list
const list = async (url, data, config) => {
  try {
    const res = await ajax.get(`/${url}`, { ...data, ...config });
    return api.checkResponse(res);
  } catch (error) {
    return api.errorRequest(error);
  }
};

// item
const item = async (url, data, config) => {
  try {
    const res = await ajax.get(`/${url}`, { ...data, ...config });
    return api.checkResponse(res);
  } catch (error) {
    return api.errorRequest(error);
  }
};

// post
const post = async (url, data, config) => {
  try {
    const res = await ajax.post(`/${url}`, data, config);
    return api.checkResponse(res);
  } catch (error) {
    return api.errorRequest(error);
  }
};

// put
const put = async (url, data, config) => {
  try {
    console.log("- put req", { url, data, config });
    const response = await axios.put(url, data, config);
    // const response = await axios.put(baseURL, data, config);
    console.log("- put  Resource:", response.data);
  } catch (error) {
    return api.errorRequest(error);
  }
};

// del
const del = async (url, data, config) => {
  // data 를 넘기기 위해.
  try {
    const res = await ajax({
      url: `/${url}`,
      method: "delete",
      data: { ...data },
      ...config,
    });
    return api.checkResponse(res);
  } catch (error) {
    return api.errorRequest(error);
  }
};

export { list, item, post, put, del };
