import MmsMo from '../model/mms-mo.js';
import PlaceSubscribe from '../model/place-subscribe.js';
import User from '../model/user.js';
import RoomReserv from '../model/room-reserv.js';
import Mail from '../model/mail.js';
import Sms from '../model/sms.js';

import { ERROR, PREFERENCES } from '../constants/constants.js';
import { keyToValue } from '../constants/key-map.js';

import _ from 'lodash';
import moment from 'moment';
import { sign } from '../utils/jwt-util.js';
import format from '../utils/format-util.js';
import ota from '../utils/ota-util.js';
import { CheckSubscribe } from '../utils/subscribe-util.js';

import chalk from 'chalk';
import twilio from 'twilio';
import { encrypt, decrypt } from '../utils/aes256-util.js';
import {
  NICE_KEY,
  NICE_SEC,
  TWILIO_ACCOUNT_SID,
  TWILIO_AUTH_TOKEN,
  TWILIO_PHONE_NUMBER,
} from '../api/config.js';

import axios from 'axios';

// MMS MO(대표번호 문자수신) 관리자.
class MmsMoManager {
  constructor() {
    console.log(
      chalk.yellow('--------------------------------------------------------'),
    );
    console.log(
      chalk.yellow('----------- MmsMoManager Scheduler Create ----------'),
    );
    console.log(
      chalk.yellow('--------------------------------------------------------'),
    );

    this.timer = null;
    this.otaSubscribePlace = [];
    this.receiveMailUsers = [];
    this.placeSubscribes = [];

    // Ajax As Axios
    this.ajax = axios.create({
      baseURL: '',
    });

    // interceptor
    this.ajax.interceptors.request.use(
      (config) => {
        console.log(
          '>>>>>> axios request >>>>>>\n %c' + config.method.toUpperCase(),
          'background: #222; color: #bada55',
          moment(config.time).format('mm:ss:SSS'),
          config.url,
          '\ndata:',
          config.data,
        );
        return config;
      },
      (error) => {
        return Promise.reject(error);
      },
    );

    this.ajax.interceptors.response.use(
      function (response) {
        console.log(
          '<<<<<< axios response <<<<<<\n %c' +
            response.config.method.toUpperCase() +
            '%c  %c ',
          'background: #f66; color: #fff',
          'background: #fff; color: #fff',
          'background: #4b49d7; color: #fff',
          response.config.url,
          '\ndata:',
          response.data,
        );

        return response;
      },
      function (error) {
        return Promise.reject(error);
      },
    );

    this.token = '';

    // this.airbnb("login");

    setTimeout(() => {
      this.init();
    }, 10 * 1000);

    console.log('');
  }

  init() {
    if (this.timer) clearInterval(this.timer);

    this.users();
    this.subscribes();

    this.run();
  }

  run() {
    console.log('--------------------------------------------');
    console.log('---- MmsMoManager Scheduler Run!');
    console.log('--------------------------------------------');

    let isRunningSvr = process.env.SVR_NAME === 'RUNNING';

    // 시간 타이머.
    this.timer = setInterval(() => {
      // 1분 마다 실행.
      if (Number(moment().format('ss')) === 0) {
        this.users();
        this.subscribes();
      }
      // 10초 마다 실행.
      else if (Number(moment().format('ss')) % (isRunningSvr ? 10 : 15) === 0) {
        // if (this.otaSubscribePlace.length) {
        console.log('---- MmsMoManager Scheduler Run Interval!');
        this.parse();
        this.getSmsList();
        if (this.token) {
          // this.airbnb("reservations");
        } else {
          // console.error("- airbnb token is nothing...!");
          // TEST Token..
          // this.token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiTWFuamluIEJhZSIsImlzcyI6ImFpcmJuYiIsImlkIjoiMTY2OTQzNDYiLCJleHAiOjE2MjIyNjc0NTd9.koI07AVdd34bQ1pHpZ6QR9nj89Wmm_cGaa617UOHi1Y";
          // }/
        }
      }
      // 1일 마다 실행.
      else if (Number(moment().format('hhmmss')) === 0) {
        if (isRunningSvr) {
          this.delete();
        }
      }
    }, 1000);
  }

  subscribes() {
    // 구독 목록 조회.
    let filter = '1 = 1',
      limit = '100000',
      order = 'a.id',
      desc = 'asc';

    PlaceSubscribe.selectPlaceSubscribes(
      filter,
      order,
      desc,
      limit,
      (err, place_subscribes) => {
        if (!err) {
          this.placeSubscribes = place_subscribes;
          this.otaSubscribePlace = [];

          // 서비스 구독 유효 여부.
          _.each(place_subscribes, (subscribe) => {
            if (CheckSubscribe(subscribe) && subscribe.code === '11')
              this.otaSubscribePlace.push(subscribe);
          });

          console.log(
            '- MmsMoManager otaSubscribePlace',
            this.otaSubscribePlace.length,
          );
        }
      },
    );
  }

  users() {
    // 업소별 메일 수신 직원 목록
    let filter = "a.email != '' AND a.ota_mo_mail_yn != '0'",
      limit = '10000',
      order = 'a.id',
      desc = 'asc';

    User.selectUsers(filter, order, desc, limit, (err, users) => {
      this.receiveMailUsers = users;
    });
  }

  parse() {
    // ETC1 데이터 처리 상태  NULL: 파싱전, S: 파싱완료, I: 예약대기, N:해당없음, E: 파싱오류
    // let filter = "(ETC1 IS NULL OR ETC1 = 'E') AND DATE_SUB(NOW(), INTERVAL 3 DAY) < RESULT_DATE",
    let filter =
        'ETC1 IS NULL AND DATE_SUB(NOW(), INTERVAL 1 DAY) < RESULT_DATE',
      limit = '50',
      order = 'a.MO_KEY',
      desc = 'asc';

    console.log('- MmsMoManager parse', { filter });

    MmsMo.selectMmsMoes(filter, order, desc, limit, (err, mms_mos) => {
      if (mms_mos) {
        console.log('- MmsMoManager mms_mos', mms_mos.length);

        _.map(mms_mos, (row, idx) => {
          let { MO_KEY, ID, PHONE, SUBJECT, ETC1, RESULT_DATE, CONTENT } = row;

          ota.parse(this.otaSubscribePlace, row, (result) => {
            console.log(chalk.yellow('- parse res'), { result });

            _.each(result, ({ reserv, standby }, k) => {
              let {
                ota_code,
                reserv_num,
                state,
                place_id,
                room_type_id,
                check_in_exp,
                check_out_exp,
                room_fee,
                reserv_fee,
              } = reserv;

              console.log(chalk.yellow('- each reserv'), {
                ota_code,
                reserv_num,
                state,
                place_id,
                room_type_id,
                check_in_exp,
                check_out_exp,
                room_fee,
                reserv_fee,
              });

              let success = (
                state === 'B'
                  ? ota_code && reserv_num
                  : ota_code &&
                    reserv_num &&
                    place_id &&
                    check_in_exp &&
                    check_out_exp
              )
                ? 'S'
                : 'E';

              if (standby) success = standby; // 예약 대기:I, 기타: N

              console.log(
                success === 'S' && place_id
                  ? chalk.green('- parse success')
                  : chalk.red('- parse fail'),
                success,
              );

              // 완료 시 room_reserv 등록.
              if (success === 'S') {
                // 취소시
                if (state === 'B') {
                  delete reserv.check_in_exp;
                  delete reserv.check_out_exp;
                }

                // MO_KEY 설정.
                reserv.mms_mo_num = MO_KEY;

                this.insertReserv(reserv, row, (err, info) => {
                  success = !err ? 'S' : 'E';

                  console.log(
                    success === 'S'
                      ? chalk.green('- insertReserv success')
                      : chalk.red('- insertReserv fail'),
                    { success, err },
                  );

                  if (info && info.insertId) reserv.id = info.insertId;

                  let mms_mo = {
                    MO_KEY,
                    ETC1: success,
                  };

                  this.update(mms_mo);

                  // 자동 예약 알림 메일.
                  this.send_mail(reserv, row, success);
                });
              } else {
                let mms_mo = {
                  MO_KEY,
                  ETC1: success,
                };

                this.update(mms_mo);

                // 자동 예약 알림 메일. (예약 대기, 미리 예약은 메일 발송 안함.)
                if (success === 'E' && SUBJECT !== 'ICT모텔') {
                  this.send_mail(reserv, row, success);
                }
              }
            });
          });
        });
      }
    });
  }

  airbnb(type) {
    console.log('- MmsMoManager airbnb');

    if (type === 'login') {
      var config = {
        method: 'get',
        url: 'https://15.164.244.232/api/login/auth?client=web&code=008c19dc0badd69b49d3',
        headers: {},
      };

      this.ajax(config)
        .then(function (response) {
          let { data } = response;

          console.log('- airbnb res data', JSON.stringify(data));

          let { jwt } = data;

          this.token = jwt;
        })
        .catch(function (error) {
          console.log(error);
        });
    } else if (type === 'reservations') {
      var config = {
        method: 'get',
        url: 'https://15.164.244.232/api/reservations',
        headers: {
          Authorization: `Bearer ${this.token}`,
        },
      };

      this.ajax(config)
        .then(function (response) {
          let { data } = response;

          console.log('- airbnb res data', JSON.stringify(data));

          if (data) {
            let reservs = data.body;

            _.map(reservs, (row, idx) => {
              row.type = '예약알림';
              row.reserv_num =
                Math.floor(Math.random() * (90000000 - 80000000)) + 80000000; //min ~ max 사이의 임의의 정수 반환

              let { name, address, description, checkInDate, checkOutDate } =
                row;

              this.airbnbParse(row);
            });
          }
        })
        .catch(function (error) {
          console.log(error);
        });
    } else if (type === 'cancels') {
      var config = {
        method: 'get',
        url: 'https://15.164.244.232/api/cancels',
        headers: {
          Authorization: `Bearer ${this.token}`,
        },
      };

      this.ajax(config)
        .then(function (response) {
          let { data } = response;

          console.log('- airbnb res data', JSON.stringify(data));

          if (data) {
            let reservs = data.body;

            _.map(reservs, (row, idx) => {
              row.type = '예약취소알림';

              let {
                name,
                reserv_num,
                address,
                description,
                checkInDate,
                checkOutDate,
              } = row;

              this.airbnbParse(row);
            });
          }
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  }

  airbnbParse(row) {
    ota.airbnb(this.otaSubscribePlace, row, (result) => {
      console.log(chalk.yellow('- parse res'), result);

      _.each(reserv, ({ reserv, standby }, k) => {
        let {
          ota_code,
          reserv_num,
          state,
          place_id,
          room_type_id,
          check_in_exp,
          check_out_exp,
          room_fee,
          reserv_fee,
        } = reserv;

        console.log(chalk.yellow('- reserv'), {
          ota_code,
          reserv_num,
          state,
          place_id,
          room_type_id,
          check_in_exp,
          check_out_exp,
          room_fee,
          reserv_fee,
        });

        let success = (
          state === 'B'
            ? ota_code && reserv_num
            : ota_code &&
              reserv_num &&
              place_id &&
              check_in_exp &&
              check_out_exp
        )
          ? 'S'
          : 'E';

        if (standby) success = standby; // 예약 대기:I, 기타: N

        console.log(
          success === 'S'
            ? chalk.green('- parse success')
            : chalk.red('- parse fail'),
          success,
        );

        // 완료 시 room_reserv 등록.
        if (success === 'S') {
          // 취소시
          if (state === 'B') {
            delete reserv.check_in_exp;
            delete reserv.check_out_exp;
          }

          this.insertReserv(reserv, row, (err, info) => {
            success = !err ? 'S' : 'E';

            console.log(
              success === 'S'
                ? chalk.green('- insertReserv success')
                : chalk.red('- insertReserv fail'),
              { success, err },
            );

            if (info && info.insertId) reserv.id = info.insertId;

            // 자동 예약 알림 메일.
            this.send_mail(reserv, row, success);
          });
        } else {
          // 자동 예약 알림 메일. (예약 대기, 미리 예약은 메일 발송 안함.)
          if (success === 'E') {
            this.send_mail(reserv, row, success);
          }
        }
      });
    });
  }

  getSmsList() {
    console.log('- MmsMoManager Scheduler getSmsList');

    const filter = 'TR_SENDSTAT = 0';
    const order = 'TR_NUM';
    const desc = 'ASC';
    const limit = 10;

    Sms.selectSmses(filter, order, desc, limit, (err, smses) => {
      console.log('- MmsMoManager selectSmses smses', smses?.length);
      if (smses?.length) {
        _.each(smses, (row, k) => {
          this.send_auth_no(row);
        });
      }
    });
  }

  insertReserv(room_reserv, row, callback) {
    // 객실/객실타입이 맞는게 없으면 업데이트 안함.(기존 배정 상태 유지)
    if (!room_reserv.room_type_id) {
      delete room_reserv.room_type_id;
      delete room_reserv.room_id;
    }

    room_reserv.mod_date = new Date();

    // 예약 번호 중복 시 업데이트 함(변경/취소 등의 이유)
    RoomReserv.insertRoomReserv(room_reserv, (err, info) => {
      if (!err) {
        let { id, place_id, room_id } = room_reserv;

        id = id || info.insertId;

        room_reserv.id = id;

        //  client 예약 정보.
        const json = {
          type: 'SET_ROOM_RESERV',
          headers: {
            method: 'put',
            url: 'room/reserv/' + id,
            token: '',
            channel: 'api',
            uuid: '',
            req_channel: 'api',
          },
          body: {
            room_reserv,
            place_id,
          },
        };

        // jsonRes 호출 안하므로 명시적 전송.
        this.send_socket(place_id, json);

        // 예약번호 QR 전송 서비스.(예약 등록 시에만 전송)
        // if (info && info.insertId) {
        //   this.send_auth_no(room_reserv, row);
        // }

        // 객실 지정 시 전송.
        if (room_id) {
          RoomReserv.selectPlaceEnableReservs(place_id, (err, room_reservs) => {
            if (!err) {
              //  client 예약 가능 객실 업데이트
              const json = {
                type: 'SET_PLACE_ENABLE_RESERV_LIST', // 웹 reducer type, 어드민/모바일 store 타입은 socket server 에서 각각 변경.
                headers: {
                  method: 'put',
                  url: 'room/reserv/enable/' + place_id,
                  token: '',
                  channel: 'api',
                  uuid: '',
                  req_channel: 'api',
                },
                body: {
                  room_reservs,
                  place_id,
                },
              };

              // jsonRes 호출 안하므로 명시적 전송.
              this.send_socket(place_id, json);
            }
          });
        }
      }

      callback(err, info);
    });
  }

  update(mms_mo) {
    MmsMo.updateMmsMo(mms_mo.MO_KEY, mms_mo, (err, info) => {});
  }

  delete(days = -7) {
    MmsMo.deleteOldDays(days, (err, info) => {});
  }

  send_socket(place_id, json) {
    console.log('---> mmo send_socket', place_id);

    // 토큰 생성.
    sign({ channel: 'api', place_id }, (err, token) => {
      if (token) {
        json.headers.token = token;

        // 소켓 서버로 전송.
        global.dispatcher.dispatch(json);
      }
    });
  }

  send_auth_no2(row) {
    // console.log("---> mmo send_auth_no ", { row });

    let {
      TR_NUM,
      TR_SENDSTAT,
      TR_RSLTSTAT,
      TR_PHONE,
      TR_CALLBACK,
      TR_MSG,
      TR_ETC1,
    } = row;

    let subscribe = _.find(this.placeSubscribes, { code: '31' });

    // 서비스 구독 유효 여부.
    // if (!CheckSubscribe(subscribe)) {
    //   console.log("---> no subscribe ");
    // TODO 임시로 테스트 전화 번호 등록!
    // PHONE = "010-9844-0098";
    //   return false;
    // }

    try {
      const accountSid = TWILIO_ACCOUNT_SID;
      const authToken = TWILIO_AUTH_TOKEN;
      const client = twilio(accountSid, authToken);

      client.messages
        .create({
          body: TR_MSG,
          from: TWILIO_PHONE_NUMBER,
          to: TR_PHONE.replace(/010/gi, '+8210'),
        })
        .then((message) => {
          console.log('- sms send_auth_no OK', message.sid);
          if (message.sid) {
            const sms = {
              TR_SENDSTAT: 1,
              TR_ETC2: message.sid,
            };

            Sms.updateSms(TR_NUM, sms, (err, info) => {
              console.log('Sms successfully updated');
            });
          }
        });
    } catch (err) {
      console.error('- sms send_auth_no Fail!', err.message);
    }
  }

  send_auth_no(row) {
    // console.log("---> mmo send_auth_no ", { row });

    let {
      TR_NUM,
      TR_SENDSTAT,
      TR_RSLTSTAT,
      TR_PHONE,
      TR_CALLBACK,
      TR_MSG,
      TR_ETC1,
    } = row;

    // SMS 문자 서비스 구독여부.
    let subscribe = _.find(this.placeSubscribes, {
      place_id: TR_ETC1,
      code: '10',
    });

    // console.log("---> mmo send_auth_no subscribe", subscribe);

    // 서비스 구독 유효 여부.
    if (!CheckSubscribe(subscribe)) {
      console.log('---> no subscribe ');
      // TODO 임시로 테스트 전화 번호 등록!
      return false;
    } else {
      console.log('---> Subscription found:', subscribe);
    }

    try {
      var config = {
        method: 'post',
        url: 'https://svc.niceapi.co.kr:22001/digital/niceid/oauth/oauth/token HTTP/1.1',
        headers: {
          Authorization: `Basic ${Base64Encoding(NICE_KEY + ':' + NICE_SEC)}`,
        },
      };
      let data = 'grant_type=client_credentials&scope=default';

      this.ajax(config, data)
        .then(({ data }) => {
          console.log('- sms send_auth_no OK', data);

          const {
            dataHeader: { GW_RSLT_CD, GW_RSLT_MSG },
            dataBody: { access_token, token_type, expires_in, scope },
          } = data;

          if (GW_RSLT_CD === 1200) {
            const sms = {
              TR_SENDSTAT: 1,
              TR_ETC2: message.sid,
            };

            Sms.updateSms(TR_NUM, sms, (err, info) => {
              if (err) {
                console.error('Failed to update SMS:', err);
              } else {
                console.log('Sms successfully updated', info);
              }
            });
          }
        })
        .catch((error) => {
          console.error('Error occurred:', error);
        });
    } catch (err) {
      console.error('- sms send_auth_no Fail!', err.message);
    }
  }
  // 메일 발송.
  send_mail(reserv, row, success) {
    console.log('---> mmo send_mail ', { success });

    try {
      let { ota_code, state, reserv_num, place_id, room_type_name, type } =
        reserv;

      let {
        MO_KEY,
        ID,
        PHONE,
        CALLBACK,
        SUBJECT = '',
        ETC1,
        RESULT_DATE,
        CONTENT = '',
      } = row;

      CONTENT = CONTENT.replace(/\n/gi, '\r\n').replace(/\r/gi, '\r\n');

      let line = CONTENT.replace(/\r\n/gi, '<br/>');

      // ---------------------------------------------------------------------
      // [문자자동전달] 앱에서 발송하였습니다.
      // Send from [문자자동전달] App
      // -------------------------------------------- rain5neyo@sooft --------
      if (/(문자자동전달)/.test(line[line.length - 3])) {
        line.splice(line.length - 4, line.length - 1); // 문자자동전달 앱 문구 삭제.
      }

      let typeName = type === 0 ? '등록' : type === 1 ? '변경' : '취소';

      let subject = `${
        process.env.MODE !== 'PROD' ? '[' + process.env.MODE + ']' : ''
      } `;
      subject += ` ${keyToValue('room_reserv', 'ota_code', ota_code)} `;
      if (room_type_name) subject += ` [${room_type_name}] `;
      subject += ` 예약 [${typeName} ${
        success === 'S' ? '완료' : '실패'
      }] 알림 메일 입니다.`;

      let content = `<h4><font color=green>이모나이코 [OTA 예약 자동 등록 서비스] 알림 메일 입니다.</font></h4>OTA 예약문자 [${typeName}] 처리 ${
        success === 'S'
          ? "<font color='blue'>완료</font>"
          : "<font color='red'>실패</font>"
      } 입니다.`;
      content += `<br/><br/>------------ 예약 문자 ------------<br/>${line}<br/>--------------------------------------`;
      if (success === 'E')
        content += `<br/><br/><font color='red'>예약 ${typeName} 실패 시 [이글 > 예약관리] 메뉴에서 수동으로 진행 해주시기 바랍니다.</font>`;

      if (place_id) {
        let users = _.filter(
          this.receiveMailUsers,
          (user) => user.place_id === place_id,
        );

        _.each(users, (user) => {
          let { id, email, ota_mo_mail_yn } = user;
          console.log('---> user', { id, email, ota_mo_mail_yn, success });

          if (
            ota_mo_mail_yn === '1' ||
            (ota_mo_mail_yn === '2' && success === 'E')
          ) {
            // 알림 메일 모두 수신 또는 실패시 수신 시 발송.
            let mail = {
              type: 0, // 메일 타입 (0: 고객메일, 1: 관리자메일, 9: 서버메일)
              to: email,
              from: 'reserv@imonaico.kr',
              subject: `[이모나이코] ${subject}`,
              content: `${content}<br/><br/><br/><br/>주식회사 이모나이코컴퍼니<br/>1600-5356`,
            };

            Mail.insertMail(mail, (err, info) => {});
          }
        });
      }

      if (success === 'E') {
        let {
          ota_code,
          state,
          reserv_num,
          place_id,
          room_type_name,
          check_in_exp,
          check_out_exp,
        } = reserv;

        content += `<br/><br/>${JSON.stringify({
          ota_code,
          place_id,
          state,
          reserv_num,
          room_type_name,
          check_in_exp,
          check_out_exp,
        })}`;

        // 메일 타입(01: AS 메일, 02: 구독 메일,  03: OTA예약 연동 실패 메일, 09: 오류 메일)
        PREFERENCES.MailReceiverList('03', (list) => {
          _.each(list, (v) => {
            if (v.email && v.receive_yn === 'Y') {
              let mail = {
                type: 1, // 메일 타입 (0: 고객메일, 1: 관리자메일, 9: 서버메일)
                to: v.email,
                from: 'reserv@imonaico.kr',
                subject: `[이모나이코 관리자] ${subject}`,
                content: `${content}<br/><br/><br/><br/>주식회사 이모나이코컴퍼니<br/>1600-5356`,
              };
              Mail.insertMail(mail, (err, info) => {});
            }
          });
        });
      }
    } catch (e) {
      console.error('- sand_mail error', e);
    }
  }
}

export default MmsMoManager;
