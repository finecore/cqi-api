import sgMail from "@sendgrid/mail"; // default import 방식으로 수정
import Mail from "../model/mail.js";
import _ from "lodash";
import moment from "moment";
import chalk from "chalk";
import format from "../utils/format-util.js";

// 메일 발송 관리자.
class MailManager {
  constructor() {
    console.log(chalk.yellow("----------- MailManager Scheduler Create ----------"));
    this.timer = null;

    console.log("- EMAIL_SENDGRID_API_KEY" + process.env.EMAIL_SENDGRID_API_KEY);

    setTimeout(
      () => {
        this.init();
      },
      process.env.MODE === "DEV" ? 3000 : 30000
    );

    console.log("");
  }

  init() {
    sgMail.setApiKey(process.env.EMAIL_SENDGRID_API_KEY);
    this.run();
  }

  run() {
    console.log("--------------------------------------------");
    console.log("---- MailManager Run!");
    console.log("--------------------------------------------");

    if (this.timer) clearInterval(this.timer);

    // 10초 마다 실행.
    this.timer = setInterval(() => {
      this.sendMails();
    }, 10 * 1000);

    // 1일 마다 실행.
    this.timer = setInterval(() => {
      this.delMails();
    }, 24 * 60 * 60 * 1000);
  }

  send({ type = 0, to, from, subject, content }) {
    console.log("-> MailManager send", { to, from, subject });

    if (to && subject && content && content.length > 2) {
      const mail = {
        type,
        to,
        from: from || process.env.EMAIL_FROM_ADDR,
        subject,
        content,
      };

      Mail.insertMail(mail, (err, as) => {});
    }
  }

  sendMails() {
    let filter = "a.send_date IS NULL AND is_error = 0",
      between = "a.reserv_date",
      begin = moment().add(-1, "day").format("YYYY-MM-DD 00:00"),
      end = moment().add(1, "minute").format("YYYY-MM-DD HH:mm"),
      limit = "10000",
      order = "a.reg_date",
      desc = "asc";

    Mail.selectMails(filter, between, begin, end, order, desc, limit, (err, mails) => {
      _.each(mails, (mail) => {
        const { id, type, to, from, subject, content, attachments } = mail;

        if (to && subject && content) {
          const msg = {
            to,
            from: process.env.EMAIL_FROM_ADDR,
            subject: subject,
            text: content,
            html: content,
          };

          if (attachments) {
            msg.attachments = JSON.parse(attachments);
          }

          sgMail.send(msg).then(
            () => {
              Mail.updateMail(id, { send_date: new Date() }, (err, mails) => {
                console.log("- mail send ", to);
              });
            },
            (error) => {
              error.msg = msg;

              console.error(error);

              Mail.updateMail(id, { is_error: 1 }, (err, mails) => {});
            }
          );
        }
      });
    });
  }

  delMails() {
    let date = moment().add(-180, "day").format("YYYY-MM-DD HH:mm");

    Mail.deleteOldMail(date, (err, mails) => {
      console.log("- old mail delete ", date);
    });
  }
}

export default MailManager;
