import RoomStateLog from "../model/room-state-log.js";
import RoomSale from "../model/room-sale.js";
import RoomSalePay from "../model/room-sale-pay.js";
import IscStateLog from "../model/isc-state-log.js";

import { ERROR, PREFERENCES } from "../constants/constants.js";

import _ from "lodash";
import moment from "moment";

import chalk from "chalk";

// 데이터 관리자.
class DataManager {
  constructor() {
    console.log(chalk.yellow("----------- DataManager Scheduler Create ----------"));
    this.timer = null;

    setTimeout(
      () => {
        this.init();
      },
      process.env.MODE === "DEV" ? 3000 : 30000
    );

    console.log("");
  }

  init() {
    if (this.timer) clearInterval(this.timer);

    // 시간 타이머. (서버 시작시 바로 시작 금지!)
    this.timer = setInterval(() => {
      let hour = moment().hour();

      // 매일 새벽 6 시에 동작.
      // if (hour === 6) {
      //   this.run();
      // }

      // 1 시간 마다 삭제
      this.run();
    }, 60 * 60 * 1000);
  }

  run() {
    console.log("--------------------------------------------");
    console.log("---- DataManager Scheduler Run!");
    console.log("--------------------------------------------");

    // 무인 로그는 30일 저장.
    let keepIscLogDate = moment().add(-30, "day").format("YYYY-MM-DD 00:00");

    // 무인 판매기 로그 삭제
    IscStateLog.deleteIscStateLogAfterKeepDay(keepIscLogDate, (err, info) => {});

    PREFERENCES.List((preference) => {
      _.map(preference, (preference) => {
        let { place_id, sale_next_delete, state_next_delete, day_sale_end_time, data_keep_day_sale, data_keep_day_state } = preference;

        // 데이터 보관 일수 최대.
        if (!data_keep_day_sale) data_keep_day_sale = 180;
        if (!data_keep_day_state) data_keep_day_state = 60;

        // 데이터는 최소 보관 일
        if (data_keep_day_sale < 1) data_keep_day_sale = 1;
        if (data_keep_day_state < 1) data_keep_day_state = 1;

        // 매출 보관 일이 지났다면 삭제.
        if (!sale_next_delete || moment() >= moment(sale_next_delete)) {
          // 이전 매출 삭제 일(정산 시간 기준)
          let keepSaleDate = moment({ hour: day_sale_end_time, minute: 0 }).add(-data_keep_day_sale, "day").format("YYYY-MM-DD HH:mm");

          console.log("---> keepSaleDate");

          console.log({
            place_id,
            sale_next_delete,
            day_sale_end_time,
            data_keep_day_sale,
            keepSaleDate,
          });

          // 결제 이력 삭제(매출 이력 삭제 이전에 해야함)
          RoomSalePay.deleteRoomSalesPayAfterKeepDay(place_id, keepSaleDate, (err, info) => {
            if (!err) {
              preference.sale_next_delete = moment({ hour: day_sale_end_time, minute: 0 }).add(1, "day").format("YYYY-MM-DD HH:mm");
            }
          });

          // 매출 이력 삭제
          RoomSale.deleteRoomSalesAfterKeepDay(place_id, keepSaleDate, (err, info) => {
            if (!err) {
              preference.sale_next_delete = moment({ hour: day_sale_end_time, minute: 0 }).add(1, "day").format("YYYY-MM-DD HH:mm");
            }
          });
        }

        // 객실 이력 보관 일이 지났다면 삭제.
        if (!state_next_delete || moment() >= moment(state_next_delete)) {
          // 이전 이력 삭제 일(0 시기준)
          let keepStateDate = moment({ hour: 0, minute: 0 }).add(-data_keep_day_state, "day").format("YYYY-MM-DD HH:mm");

          console.log("---> keepStateDate");

          console.log({
            place_id,
            state_next_delete,
            data_keep_day_state,
            keepStateDate,
          });

          RoomStateLog.deleteRoomStateAfterKeepDay(place_id, keepStateDate, (err, info) => {
            if (!err) {
              preference.state_next_delete = moment({ hour: 0, minute: 0 }).add(1, "day").format("YYYY-MM-DD HH:mm");
            }
          });
        }
      });
    });
  }
}

export default DataManager;
