import _ from "lodash";

export const codes = {
  com: {
    useYn: [
      { text: "사용", value: "Y" },
      { text: "사용안함", value: "N" },
    ],
    acptYn: [
      { text: "허용", value: "Y" },
      { text: "허용안함", value: "N" },
    ],
    agreeYn: [
      { text: "동의", value: "Y" },
      { text: "동의안함", value: "N" },
    ],
    mailYn: [
      { text: "수신", value: "Y" },
      { text: "수신안함", value: "N" },
    ],
    otaMailYn: [
      { text: "수신", value: "1" },
      { text: "실패만 수신", value: "2" },
      { text: "수신안함", value: "0" },
    ],
    payYn: [
      { text: "유료", value: 1 },
      { text: "무료", value: 0 },
    ],
    pms: [
      { text: "산하정보기술(sanha)", value: "sanha" },
      { text: "신도이디에스(shindo)", value: "shindo" },
      { text: "마이크로닉(micronic)", value: "micronic" },
      { text: "루넷(roonets)", value: "roonets" },
      { text: "무노스(moonos)", value: "moonos" },
      { text: "호텔스토리(hotelstory)", value: "hotelstory" },
      { text: "KT기가지니(gigagenie)", value: "gigagenie" },
    ],
  },
  room: {
    state: [
      { value: 0, text: "공실" },
      { value: 1, text: "입실" },
      { value: 2, text: "외출" },
      { value: 3, text: "청소요청" },
      { value: 4, text: "청소중" },
      { value: 5, text: "청소완료" },
      { value: 6, text: "퇴실" },
      { value: 7, text: "입실취소" },
      { value: 8, text: "퇴실취소" },
    ],
  },
  room_state: {
    sale: [
      { value: 0, text: "공실" },
      { value: 1, text: "숙박" },
      { value: 2, text: "대실" },
      { value: 3, text: "장기" },
    ],
    key: [
      { value: 0, text: "NO KEY" },
      { value: 1, text: "고객키 ▼" },
      { value: 2, text: "마스터 ▼" },
      { value: 3, text: "청소키 ▼" },
      { value: 4, text: "고객키 △" },
      { value: 5, text: "마스터 △" },
      { value: 6, text: "청소키 △" },
    ],
    door: [
      { value: 0, text: "문 닫】【힘" },
      { value: 1, text: "문 【열림】" },
    ],
    isc_sale: [
      { value: 0, text: "숙박/대실" },
      { value: 1, text: "X" },
      { value: 2, text: "숙박" },
      { value: 3, text: "대실" },
    ],
    clean: [
      { value: 0, text: "청소 완료" },
      { value: 1, text: "청소 요청" },
    ],
    outing: [
      { value: 0, text: "외출 복귀" },
      { value: 1, text: "외출" },
    ],
    signal: [
      { value: 0, text: "통신 정상" },
      { value: 1, text: "통신 이상" },
    ],
  },
  room_sale: {
    state: [
      { value: "A", text: "매출 등록" },
      { value: "B", text: "입실 취소" },
      { value: "C", text: "정산 완료" },
    ],
    channel: [
      { value: "front", text: "프론트" },
      { value: "mobile", text: "모바일" },
      { value: "isc", text: "무인" },
      { value: "device", text: "IDM" },
      { value: "api", text: "자동" },
      { value: "pms", text: "PMS" },
      { value: "pad", text: "PAD" },
      { value: "watch", text: "WATCH" },
    ],
    stay_type: [
      { value: 0, text: "공실" },
      { value: 1, text: "숙박" },
      { value: 2, text: "대실" },
      { value: 3, text: "장기" },
    ],
  },
  room_interrupt: {
    channel: [
      { value: 1, text: "FRONT" },
      { value: 2, text: "ISG" },
      { value: 3, text: "MOBILE" },
      { value: 4, text: "PAD" },
      { value: 5, text: "WATCH" },
    ],
  },
  room_reserv: {
    ota_code: [
      { value: 1, text: "야놀자" },
      { value: 2, text: "여기어때" },
      { value: 3, text: "네이버" },
      { value: 4, text: "에어비앤비" },
      { value: 5, text: "호텔나우" },
      { value: 8, text: "기타" },
      { value: 9, text: "직접예약" },
    ],
    state: [
      { value: "A", text: "정상예약" },
      { value: "B", text: "취소예약" },
      { value: "C", text: "사용완료" },
    ],
    visit_type: [
      { value: 1, text: "도보방문" },
      { value: 2, text: "차량방문" },
      { value: 3, text: "대중교통" },
    ],
  },
  user: {
    type: [
      { value: 1, text: "이모나이코" },
      { value: 2, text: "대리점" },
      { value: 3, text: "업소" },
    ],
    level: [
      { value: 0, text: "슈퍼관리자" },
      { value: 1, text: "책임자(업주)" },
      { value: 2, text: "관리자(매니저)" },
      { value: 3, text: "근무자(카운터)" },
      { value: 4, text: "메이드(청소원)" },
      { value: 5, text: "PMS" },
      { value: 9, text: "뷰어" },
    ],
  },
  device: {
    type: [
      { value: "01", text: "IDM" },
      { value: "02", text: "ISG" },
      { value: "10", text: "RPT" },
    ],
  },
  notice: {
    type: [
      { value: "", text: "타입 선택" },
      { value: 1, text: "공지 알림" },
      { value: 2, text: "업데이트 알림" },
    ],
    important: [
      { value: "", text: "구분 선택" },
      { value: 1, text: "일반" },
      { value: 2, text: "중요" },
    ],
  },
  notice_place: {
    type: [
      { value: "", text: "타입 선택" },
      { value: 1, text: "공지" },
      { value: 2, text: "메모" },
    ],
    state: [
      { value: "", text: "진행 상태" },
      { value: 1, text: "등록" },
      { value: 2, text: "확인" },
      { value: 3, text: "처리중" },
      { value: 4, text: "취소" },
      { value: 9, text: "처리완료" },
    ],
  },
  as: {
    type: [
      { value: "", text: "타입 선택" },
      { value: 1, text: "AS 요청" },
      { value: 2, text: "건의 사항" },
    ],
    state: [
      { value: "", text: "진행 상태" },
      { value: 1, text: "등록" },
      { value: 2, text: "확인" },
      { value: 3, text: "처리중" },
      { value: 4, text: "취소" },
      { value: 9, text: "처리완료" },
    ],
  },
  model: {
    "01": [
      { value: "01", text: "A01" },
      { value: "02", text: "I01" },
    ],
    "02": [{ value: "01", text: "XA" }],
    10: [{ value: "01", text: "R01" }],
  },
  subscribe: {
    pay_type: [
      { value: "D", text: "일간 결제" },
      { value: "M", text: "월간 결제" },
      { value: "Y", text: "년간 결제" },
    ],
  },
  place_subscribe: {
    applyState: [
      { value: null, text: "신청 상태 선택" },
      { value: 0, text: "신규 신청" },
      { value: 1, text: "변경 신청" },
      { value: 2, text: "구독 완료" },
    ],
    validYn: [
      { text: "무료 서비스", value: 0 },
      { text: "정상 서비스", value: 1 },
      { text: "서비스 중지", value: 2 },
    ],
  },
};

export function keyCodes(talble, type) {
  // console.log("--- keyCodes", { talble, type }, codes[talble][type]);

  return codes[talble][type] || [];
}

export function keyToValue(talble, type, value) {
  if (value !== null && value !== undefined) value = isNaN(value) || (value.length > 1 && value.charAt(0) === "0") ? value : Number(value);

  let code = _.find(codes[talble][type] || [], { value });
  // console.log("--- keyToValue", { talble, type, value }, code, codes[talble][type]);

  if (!code) {
    // console.log("- keyToValue error", { talble, type, value });
    return value;
  }

  return code.text;
}

// 조건에 따른 객실 상태값 (단지 room_state 의 clean, sale,  key 만 보고 판단)
// 화면에 보이는 상태이다.
export function displayState(roomState) {
  /*
      clean = 0:없음, 1:있음 (청소요청)
      sale  = 0:업음 1:숙박 2:대실 3:장기
      key   = 0:없음, 1:고객키,2:마스터키 3:청소키

      state = 0:공실, 1:입실, 2:외출, 3:청소대기, 4:청소중, 5:청소완료, 6:퇴실 (상태 조건이 변경 될 때마다 체크 하여 반영 한다)
    */
  const { clean, sale, key, outing, check_out } = roomState;

  let code = 0;

  // 판매 상태 일때.
  if (sale > 0) code = 1; // 입실.
  if (sale > 0 && (key < 1 || key > 3) && outing === 1) code = 2; // 외출(key out 시)
  if (sale > 0 && check_out) code = 6; // 퇴실

  // 청소 요청 상태 일때.
  if (clean === 1) code = 3; // 청소요청
  if (key === 3) code = 4; // 청소중
  if (sale > 0 && key === 6) code = 5; // 청소완료

  console.log("----> displayState ", { clean, sale, key, outing, check_out });

  return code;
}

export function displayStateName(roomState, roomSale) {
  const code = displayState(roomState, roomSale);
  return keyToValue("room", "state", code);
}
