import _ from 'lodash';
import moment from 'moment';

import {
  keyCodes,
  keyToValue,
  displayState,
  displayStateName,
} from './key-map.js';

const PMS = {
  SANHA: {
    URL: 'https://wingsapi.sanhait.com/rm/v1',
    HEADERS: {
      VENDOR_ID: 'ICREW',
    },
    BODY: {
      companyId: 'SANHA',
      intfType: 'SINGLE',
      bsnsCode: '11',
      langTypeCode: 'KR',
      propertyNo: '11',
      recvXML: '',
      roomNo: '',
      userId: 'ICREW',
      userIp: '',
    },
    RES_CODE: [
      {
        code: 200,
        message: 'OK',
      },
      {
        code: 201,
        message: 'No Content',
      },
      {
        code: 202,
        message: 'Partial Content',
      },
      {
        code: 400,
        message: 'Internal Error',
      },
      {
        code: 401,
        message: 'Service Unablable',
      },
      {
        code: 404,
        message: 'Gateway Timeout',
      },
      {
        code: 405,
        message: 'Bed Request Type',
      },
      {
        code: 500,
        message: 'Service Unablable',
      },
    ],
    STATE: [
      { value: 0, code: '', text: '공실' },
      { value: 1, code: 'E', text: '재실' },
      { value: 2, code: 'T', text: '외출' },
      { value: 3, code: 'G', text: '청소지시' },
      { value: 3, code: 'W', text: '청소대기' },
      { value: 4, code: 'C', text: '청소중' },
      { value: 5, code: 'I', text: '청소완료' },
      { value: 6, code: '', text: '퇴실' },
    ],
    ROOM_STATE: [
      { sale: 0, outing: 0, code: '', text: '공실' },
      { sale: 1, outing: 0, code: 'E', text: '재실' },
      { sale: 1, outing: 1, code: 'T', text: '외출' },
    ],
    CLEAN_STATE: [
      { clean: 1, key: 0, code: 'G', text: '청소지시' },
      { clean: 2, key: 0, code: 'W', text: '청소대기' }, // 사용안함.
      { clean: 1, key: 3, code: 'C', text: '청소중' },
      { clean: 0, key: 0, code: 'I', text: '청소완료' },
    ],
  },
  ROONETS: {
    URL: 'https://www.rpms.kr',
    HEADERS: {
      VENDOR_ID: 'ICREW',
    },
    BODY: {
      data: '',
    },
    RES_CODE: [
      {
        code: 200,
        message: 'OK',
      },
      {
        code: 201,
        message: 'No Content',
      },
      {
        code: 202,
        message: 'Partial Content',
      },
      {
        code: 400,
        message: 'Internal Error',
      },
      {
        code: 401,
        message: 'Service Unablable',
      },
      {
        code: 404,
        message: 'Gateway Timeout',
      },
      {
        code: 405,
        message: 'Bed Request Type',
      },
      {
        code: 500,
        message: 'Service Unablable',
      },
    ],
    STATE: [
      { value: 0, code: '', text: '공실' },
      { value: 1, code: 'E', text: '재실' },
      { value: 2, code: 'T', text: '외출' },
      { value: 3, code: 'G', text: '청소지시' },
      { value: 3, code: 'W', text: '청소대기' },
      { value: 4, code: 'C', text: '청소중' },
      { value: 5, code: 'I', text: '청소완료' },
    ],
    ROOM_STATE: [
      { sale: 0, outing: 0, code: '', text: '공실' },
      { sale: 1, outing: 0, code: 'E', text: '재실' },
      { sale: 1, outing: 1, code: 'T', text: '외출' },
    ],
    CLEAN_STATE: [
      { clean: 1, key: 0, code: 'G', text: '청소지시' },
      { clean: 2, key: 0, code: 'W', text: '청소대기' }, // 사용안함.
      { clean: 1, key: 3, code: 'C', text: '청소중' },
      { clean: 0, key: 0, code: 'I', text: '청소완료' },
    ],
  },
  MOONOS: {
    URL: 'https://www.moonos.com',
    HEADERS: {
      VENDOR_ID: 'ICREW',
    },
    BODY: {
      data: '',
    },
    RES_CODE: [
      {
        code: 200,
        message: 'OK',
      },
      {
        code: 201,
        message: 'No Content',
      },
      {
        code: 202,
        message: 'Partial Content',
      },
      {
        code: 400,
        message: 'Internal Error',
      },
      {
        code: 401,
        message: 'Service Unablable',
      },
      {
        code: 404,
        message: 'Gateway Timeout',
      },
      {
        code: 405,
        message: 'Bed Request Type',
      },
      {
        code: 500,
        message: 'Service Unablable',
      },
    ],
    STATE: [
      { value: 0, code: '', text: '공실' },
      { value: 1, code: 'E', text: '재실' },
      { value: 2, code: 'T', text: '외출' },
      { value: 3, code: 'G', text: '청소지시' },
      { value: 3, code: 'W', text: '청소대기' },
      { value: 4, code: 'C', text: '청소중' },
      { value: 5, code: 'I', text: '청소완료' },
      { value: 6, code: 'N', text: '입실취소' },
      { value: 7, code: 'R', text: '퇴실취소' },
    ],
    ROOM_STATE: [
      { sale: 0, outing: 0, code: '', text: '공실' },
      { sale: 1, outing: 0, code: 'E', text: '재실' },
      { sale: 1, outing: 1, code: 'T', text: '외출' },
    ],
    CLEAN_STATE: [
      { clean: 1, key: 0, code: 'G', text: '청소지시' },
      { clean: 2, key: 0, code: 'W', text: '청소대기' }, // 사용안함.
      { clean: 1, key: 3, code: 'C', text: '청소중' },
      { clean: 0, key: 0, code: 'I', text: '청소완료' },
    ],
  },
  HOTELSTORY: {
    URL: 'https://www.hotelstory.com',
    HEADERS: {
      VENDOR_ID: 'ICREW',
    },
    BODY: {
      data: '',
    },
    RES_CODE: [
      {
        code: 200,
        message: 'OK',
      },
      {
        code: 201,
        message: 'No Content',
      },
      {
        code: 202,
        message: 'Partial Content',
      },
      {
        code: 400,
        message: 'Internal Error',
      },
      {
        code: 401,
        message: 'Service Unablable',
      },
      {
        code: 404,
        message: 'Gateway Timeout',
      },
      {
        code: 405,
        message: 'Bed Request Type',
      },
      {
        code: 500,
        message: 'Service Unablable',
      },
    ],
    STATE: [
      { value: 0, code: '', text: '공실' },
      { value: 1, code: 'E', text: '재실' },
      { value: 2, code: 'T', text: '외출' },
      { value: 3, code: 'G', text: '청소지시' },
      { value: 3, code: 'W', text: '청소대기' },
      { value: 4, code: 'C', text: '청소중' },
      { value: 5, code: 'I', text: '청소완료' },
    ],
    ROOM_STATE: [
      { sale: 0, outing: 0, code: '', text: '공실' },
      { sale: 1, outing: 0, code: 'E', text: '재실' },
      { sale: 1, outing: 1, code: 'T', text: '외출' },
    ],
    CLEAN_STATE: [
      { clean: 1, key: 0, code: 'G', text: '청소지시' },
      { clean: 2, key: 0, code: 'W', text: '청소대기' }, // 사용안함.
      { clean: 1, key: 3, code: 'C', text: '청소중' },
      { clean: 0, key: 0, code: 'I', text: '청소완료' },
    ],
  },
  GIGAGENIE: {
    URL: {
      DEV: ' https://125.159.61.195:50014',
      TEST: 'https://125.159.61.195:50014',
      PROD: 'https://api.gghotel.kt.com:9483',
    },
    HEADERS: {},
    BODY: {
      data: '',
    },
  },
};

const HAS_PMS = (pms) => {
  if (pms) pms = pms.toUpperCase();
  return PMS[pms] ? true : false;
};

const GET_PMS_DATA = (pms, name) => {
  if (pms) pms = pms.toUpperCase();
  return PMS[pms][name];
};

const STATE_CODE_TO_VLUE = (pms, code) => {
  if (pms) pms = pms.toUpperCase();

  let { value, text } = _.find(PMS[pms].STATE, { code });

  // console.log("-> STATE_VALUE_TO_TXT ", { pms, value, code, text });

  return value;
};

const STATE_VALUE_TO_TXT = (pms, state) => {
  if (pms) pms = pms.toUpperCase();

  const value = displayState(state);

  let { code, text } = _.find(PMS[pms].STATE, { value });

  // console.log("-> STATE_VALUE_TO_TXT ", { pms, value, code, text });

  return text;
};

const STATE_VALUE_TO_CODE = (pms, state) => {
  if (pms) pms = pms.toUpperCase();

  let { name, sale, clean, outing, key } = state;

  sale = sale > 0 ? 1 : 0;
  key = key === 3 ? 3 : 0;

  console.log('-> STATE_VALUE_TO_CODE ', {
    pms,
    name,
    sale,
    clean,
    outing,
    key,
  });

  let { code: code1 } = _.find(PMS[pms].ROOM_STATE, { sale, outing }) || {
    code: '',
  };
  let { code: code2 } = _.find(PMS[pms].CLEAN_STATE, { clean, key }) || {
    code: '',
  };

  const dilimeter = '^!';
  const code = name + dilimeter + code1 + dilimeter + code2;

  return code;
};

const RES_JSON = (pms, res, code, msg, data) => {
  if (pms) pms = pms.toUpperCase();

  console.log('-> RES_JSON ', { pms, code, data });

  if (code === 200) {
    data = data || { success: 'OK' };

    return res.json(data);
  } else {
    if (!msg) {
      let { message } = _.find(PMS[pms].RES_CODE, { code });

      msg = message;
    }

    if (process.env.MODE === 'PROD') {
      console.warn('-> pms RES_JSON error ', { pms, code, data, msg }); // 운영은 오류 발생 안시킴
    } else {
      console.error('-> pms RES_JSON error ', { pms, code, data, msg });
    }

    return res.status(code).json({ fail: msg });
  }
};

export {
  HAS_PMS,
  GET_PMS_DATA,
  STATE_CODE_TO_VLUE,
  STATE_VALUE_TO_TXT,
  STATE_VALUE_TO_CODE,
  RES_JSON,
};
