// message-types.js

export const USER_JOINED = "USER_JOINED";
export const USER_LEFT = "USER_LEFT";
export const JOIN_REQUESTED = "JOIN_REQUESTED";
export const USER_REQUESTED = "USER_REQUESTED";
export const USER_STARTED_TYPING = "USER_STARTED_TYPING";
export const USER_STOPPED_TYPING = "USER_STOPPED_TYPING";
export const MESSAGE_ADDED = "MESSAGE_ADDED";
export const PUSH_STORE = "PUSH_STORE";
