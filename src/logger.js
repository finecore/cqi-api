import winston, { transports, createLogger, format } from 'winston'; // Add 'transports' and 'createLogger'
import 'winston-daily-rotate-file'; // ES6 import 방식
import * as dateUtil from 'date-utils';

const logDir = 'log';

const dailyRotateFileTransport = new transports.DailyRotateFile({
  filename: `${logDir}/%DATE%-app.log`,
  datePattern: 'YYYY-MM-DD',
  zippedArchive: false,
  maxSize: '10m',
  maxFiles: '30d',
});

const dailyRotateFileTransportError = new transports.DailyRotateFile({
  filename: `${logDir}/%DATE%-err.log`,
  datePattern: 'YYYY-MM-DD',
  zippedArchive: false,
  maxSize: '10m',
  maxFiles: '30d',
});

const logger = createLogger({
  level: 'debug',
  format: format.combine(
    format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss',
    }),
    format.printf((info) => `${info.timestamp} ${info.level}: ${info.message}`),
  ),
  transports: [
    // write console log (stop)
    // new transports.Console({
    //   level: "debug",
    //   format: format.combine(
    //     format.colorize(),
    //     format.printf((info) => `${info.timestamp} ${info.level}: ${info.message}`)
    //   ),
    // }),
    dailyRotateFileTransport,
  ],
  exceptionHandlers: [
    // new transports.Console({
    //   level: "error",
    //   format: format.combine(
    //     format.colorize(),
    //     format.printf((info) => `${info.timestamp} ${info.level}: ${info.message}`)
    //   ),
    // }),
    dailyRotateFileTransportError,
  ],
});

// 추가한 메서드들
logger.br = (line) => {
  logger.debug('\r\n');
};

logger.substr = (data, len = 200) => {
  if (!data) return '';

  const logdate = typeof data === 'object' ? JSON.stringify(data) : data;

  return logdate.length > len
    ? logdate.substring(0, len / 2) +
        '\n    .............................. skip ...............................  \n' +
        logdate.substring(logdate.length - len / 2, logdate.length - 1)
    : logdate;
};

// ES6 export 방식으로 내보내기
export default logger;
