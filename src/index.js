import http from 'http';
import https from 'https';
import chalk from 'chalk';
import moment from 'moment';
import _ from 'lodash';
import ip from 'ip';
import util from 'util';
import { spawn } from 'child_process';
import stackTrace from 'stack-trace';
import dotenv from 'dotenv';
import { fileURLToPath } from 'url';
import path, { dirname } from 'path';
import fs from 'fs';

import logger from './logger.js';
import Dispatcher from './dispatcher/dispatcher.js';
import DataManager from './scheduler/data-manager.js';
import RoomSaleReport from './scheduler/room-sale-report.js';
import MmsMoManager from './scheduler/mms-mo-manager.js';
import MailManager from './scheduler/mail-manager.js';
import Server from './model/server.js';

dotenv.config();

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

// 환경 변수 설정
const HOST = process.env.HOST || ip.address();
const PORT = parseInt(process.env.PORT, 10) || 4000;
const MODE = process.env.MODE.toLowerCase();
const useHttps = process.env.VITE_USE_HTTPS === 'Y';

// config 객체 초기화
const config = {};
_.map(process.env, (v, k) => {
  if (k.charAt(0) === k.charAt(0).toUpperCase()) config[k] = v;
});

// Express 앱 불러오기
import app from './app.js';
app.set('HOST', HOST);

let server;

const sslCertPaths = {
  dev: {
    key: path.resolve(__dirname, '../key/key.pem'),
    cert: path.resolve(__dirname, '../key/cert.pem'),
  },
  test: {
    key: path.resolve('/etc/letsencrypt/live/api.checkqin.com/privkey.pem'),
    cert: path.resolve('/etc/letsencrypt/live/api.checkqin.com/fullchain.pem'),
  },
  prod: {
    key: path.resolve('/etc/letsencrypt/live/api.checkqin.com/privkey.pem'),
    cert: path.resolve('/etc/letsencrypt/live/api.checkqin.com/fullchain.pem'),
  },
};

// SSL 인증서 검증 함수
const validateSSLCerts = (MODE) => {
  const { key, cert } = sslCertPaths[MODE];
  if (!fs.existsSync(key) || !fs.existsSync(cert)) {
    console.error('SSL 인증서 파일을 찾을 수 없습니다.', { key, cert });
    process.exit(1);
  }
  return {
    key: fs.readFileSync(key, 'utf8'),
    cert: fs.readFileSync(cert, 'utf8'),
  };
};

// 서버 인스턴스 생성 함수
const createServer = (useHttps, credentials) => {
  if (useHttps) {
    console.info('HTTPS 서버를 시작합니다.');
    return https.createServer(credentials, app);
  } else {
    console.info('HTTP 서버를 시작합니다.');
    return http.createServer(app);
  }
};

// HTTPS 사용 시 인증서 파일 로드
if (useHttps) {
  const credentials = validateSSLCerts(MODE);
  server = createServer(useHttps, credentials);
} else {
  server = createServer(useHttps, null);
}

// 서버 시작
server.listen(PORT, HOST, () => {
  console.info('======== socket alive check server started =========');
  console.info('Listening on ' + MODE, HOST, HOST);
  console.info('====================================================\n');
  server.on('error', onError);
});

server.on('listening', onListening);

function onError(error) {
  console.error('==> server error ' + error);

  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof HOST === 'string' ? 'Pipe ' + HOST : 'Port ' + HOST;

  switch (error.code) {
    case 'EACCES':
      console.error(bind + '  elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

function onListening() {
  const addr = server.address();
  console.info(
    `========== [${process.env.MODE}] API server started =============`,
  );
  console.info('- Listening on ' + HOST + ':' + addr.port);
  console.info('- SVR_NAME', process.env.SVR_NAME);
  console.info('- HOST', process.env.HOST);
  console.info('- pm_id', process.env.pm_id);
  console.info(
    `- DB HOST ${process.env.DB_HOST_IP}:${process.env.DB_HOST_PORT}`,
  );
  console.info('============================================\n');

  console.info(`========== scheduler started =============`);

  if (process.env.HOST) {
    console.info('- cluster instance', process.env.HOST);
    console.info('- IS RUNNING Server');

    global.dataManager = new DataManager();
    global.roomSaleHelper = new RoomSaleReport();
    global.mmsMoManager = new MmsMoManager();
  }

  global.dispatcher = new Dispatcher();

  console.info('============================================\n');

  setTimeout(() => {
    global.mailManager = new MailManager();

    const content = `${moment().format('YYYY-MM-DD HH:mm:ss')}
      <br/><br/><strong>Api Server Started!</strong><br/><br/>
      SVR: ${process.env.SVR_NAME}<br/>
      PM2: ${process.env.pm_id}<br/>
      Ip: ${HOST}<br/>
      Port: ${addr.port}`;

    sendErrMail(
      'info@imonaico.kr',
      `Server ${process.env.pm_id} Started!`,
      content,
    );
  }, 5 * 1000 * Number(process.env.pm_id));

  setInterval(() => {
    getServerInfo(`name='${process.env.SVR_NAME}'`);
  }, 60 * 1000);

  getServerInfo(`name='${process.env.SVR_NAME}'`);
}

function getServerInfo(filter) {
  console.log('- getServerInfo ', filter);

  try {
    Server.selectServers(filter, (err, servers) => {
      if (!err && servers[0]) {
        const { write_log } = servers[0];
        global.isWriteLog = write_log;
      } else {
        console.error('- getServerInfo error', err);
      }
    });
  } catch (e) {
    console.error('- getServerInfo error catch', e);
  }
}

function sendErrMail(from, subject, content) {
  subject = `${
    process.env.MODE !== 'PROD' ? '[' + process.env.MODE + ']' : ''
  } Api ${subject || ''} (${process.env.SVR_NAME})`;
  from = from || process.env.EMAIL_FROM_ADDR;

  if (global.mailManager && process.env.MODE !== 'DEV' && content.length > 3) {
    content +=
      '<br/><br/><br/>------------ Stack ------------<br/>' +
      global.lastLogData?.join('<br/>') +
      '</font><br/>' +
      content;

    global.mailManager.send({
      type: 9,
      to: process.env.EMAIL_ERR_TO_ADDR,
      from,
      subject,
      content,
    });
  }
}

// uncaughtException 핸들링
process.on('uncaughtException', (err) => {
  console.error('===> uncaughtException', err);
});

// 로그 처리 개선
global.lastLogData = global.lastLogData || [];

console.logCopy = console.log.bind(console);

console.log = function (...args) {
  let data = args
    .map((arg) => (typeof arg === 'object' ? util.inspect(arg) : arg))
    .join(' ');
  data = moment().format('YYYY-MM-DD HH:mm:ss') + ' ' + data;

  this.logCopy(data);

  if (_.trim(data)) {
    data = data
      .replace(/.token":(["'].*?["'])/g, `"token":"........"`)
      .replace(/(\[\d{1,2}m)/gi, '');
    if (global.isWriteLog) {
      try {
        logger.debug(data);
      } catch (e) {}
    }

    if (!Array.isArray(global.lastLogData)) global.lastLogData = [];
    global.lastLogData.push(data);
    if (global.lastLogData.length > 5) global.lastLogData.shift();
  }
};
