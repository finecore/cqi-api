import roomStateLog from "../controllers/room-state-log.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";

export default (app) => {
  // room  state route.
  app.route(["/room/state/log/all/:place_id/:begin/:end", "/room/state/log/all/:place_id/:begin/:end/:limit"]).get(roomStateLog.list_all_room_state_log);
  app.route("/room/state/log/list/:place_id/:limit").get(roomStateLog.read_a_room_state_logs);
  app.route("/room/state/log/day/:place_id/:day").get(roomStateLog.read_a_room_state_day_logs);
  app.route("/room/state/log/last/:place_id/:last_id").get(roomStateLog.read_a_room_state_last_logs);

  app.route("/room/state/log/:id").get(roomStateLog.read_a_room_state_log).put(roomStateLog.update_a_room_state_log).delete(isValidToken, checkPermission, roomStateLog.delete_a_room_state_log);

  app.route("/room/state/log").post(roomStateLog.create_a_room_state_log);
};
