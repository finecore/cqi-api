import roomType from "../controllers/room-type.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";

export default (app) => {
  // room  type route.
  app.route("/room/type/all/:place_id").get(isValidToken, roomType.list_all_room_type).delete(isValidToken, checkPermission, roomType.delete_all_room_type);

  app
    .route("/room/type/:id")
    .get(isValidToken, roomType.read_a_room_type)
    .put(isValidToken, checkPermission, roomType.update_a_room_type)
    .delete(isValidToken, checkPermission, roomType.delete_a_room_type);

  app.route("/room/type").post(isValidToken, checkPermission, roomType.create_a_room_type);
};
