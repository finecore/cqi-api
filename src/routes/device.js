import device from "../controllers/device.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import _ from "lodash";

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { method } = req;
  const { id, type, name } = req.params;

  if (!id) validationError.detail.push("device id 를 입력해 주세요!");
  if (isNaN(id)) validationError.detail.push("device id 는 숫자를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

export default (app) => {
  app.route(["/device/all", "/device/list/:filter/:order/:desc/:limit"]).get(isValidToken, device.list_a_devices);
  app.route("/device/place/:place_id").get(isValidToken, device.list_place_devices);

  app.route("/device").post(device.create_a_device);

  app.route("/device/:id").get(valid, isValidToken, device.read_a_device).put(valid, isValidToken, device.update_a_device).delete(valid, isValidToken, checkPermission, device.delete_a_device);

  app.route("/device/serialno/:serialno").get(isValidToken, device.read_a_device);

  app.route("/device/serialno/:serialno").put(isValidToken, device.update_a_device_serialno);
};
