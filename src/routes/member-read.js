import MemberRead from "../controllers/member-read.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import _ from "lodash";

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { method } = req;
  const { member_id, id } = req.params;

  if (!member_id && !id) validationError.detail.push("member-read member_id or id 를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

export default (app) => {
  app.route("/member/read/list/:member_id/:filter/:order/:desc/:limit").get(isValidToken, MemberRead.list_a_member_reads);

  // 회원 가립시 등록하므로 토큰 없음.
  app.route("/member/read").post(MemberRead.create_a_member_read);

  app.route("/member/read/all/member/:member_id").delete(valid, isValidToken, checkPermission, MemberRead.delete_a_member_all_read);

  app
    .route("/member/read/:id")
    .get(valid, isValidToken, MemberRead.read_a_member_read)
    .put(valid, isValidToken, MemberRead.update_a_member_read)
    .delete(valid, isValidToken, checkPermission, MemberRead.delete_a_member_read);
};
