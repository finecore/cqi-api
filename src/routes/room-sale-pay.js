import roomSalePay from "../controllers/room-sale-pay.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import _ from "lodash";

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { id } = req.params;

  if (!id) validationError.detail.push("room_sale_pay id 를 입력해 주세요!");
  if (isNaN(id)) validationError.detail.push("room_sale_pay id 는 숫자를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

export default (app) => {
  // room  sale route.

  app.route(["/room/sale/pay/all/:place_id", "/room/sale/pay/all/:place_id/:begin/:end"]).get(isValidToken, roomSalePay.list_all_room_sale_pay);
  app.route(["/room/sale/pay/all/:place_id/:excel", "/room/sale/pay/all/:place_id/:begin/:end/:excel"]).get(roomSalePay.list_all_room_sale_pay);
  app.route(["/room/sale_pay/list/:room_id", "/room/sale/pay/list/:room_id/:begin/:end"]).get(isValidToken, roomSalePay.list_a_room_sale_pay);
  app.route(["/room/sale/pay/member/list/:member_id/:filter/:between/:begin/:end"]).get(isValidToken, roomSalePay.list_a_room_sale_pay_by_member);
  app.route(["/room/sale/pay/list/:room_id/:excel", "/room/sale/pay/list/:room_id/:begin/:end/:excel"]).get(roomSalePay.list_a_room_sale_pay);
  app.route("/room/sale/pay/sum/place/:place_id/:begin/:end").get(isValidToken, roomSalePay.read_a_room_sale_pay_sum_place);
  app.route("/room/sale/pay/sum/place/:place_id/:begin/:end/:excel").get(roomSalePay.read_a_room_sale_pay_sum_place);
  app.route(["/room/sale/pay/today/:room_id", "/room/sale/pay/sum/:room_id/:begin/:end"]).get(isValidToken, roomSalePay.read_a_room_sale_pay_sum);
  app.route(["/room/sale/pay/today/:room_id/:excel"]).get(roomSalePay.read_a_room_sale_pay_sum);
  app.route("/room/sale/pay/sum/all/:begin/:end").get(isValidToken, roomSalePay.read_all_room_sale_pay_sum);
  app.route("/room/sale/pay/sum/all/:begin/:end/:excel").get(roomSalePay.read_all_room_sale_pay_sum);
  app.route("/room/sale/pay").post(isValidToken, checkPermission, roomSalePay.create_a_room_sale_pay);

  app.route(["/room/sale/pay/member/del/:member_id"]).put(isValidToken, roomSalePay.update_a_room_sale_pay_by_member);

  app
    .route("/room/sale/pay/:id")
    .get(valid, isValidToken, roomSalePay.read_a_room_sale_pay)
    .put(valid, isValidToken, checkPermission, roomSalePay.update_a_room_sale_pay)
    .delete(valid, isValidToken, checkPermission, roomSalePay.delete_a_room_sale_pay);
};
