import Mail from "../controllers/mail.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";

export default (app) => {
  app.route(["/mail/list/:filter/:between/:begin/:end/:limit"]).get(isValidToken, Mail.list_a_mails);

  app.route("/mail").post(isValidToken, checkPermission, Mail.create_a_mail);

  app.route("/mail/:id").get(isValidToken, Mail.read_a_mail).put(isValidToken, checkPermission, Mail.update_a_mail).delete(isValidToken, checkPermission, Mail.delete_a_mail);
};
