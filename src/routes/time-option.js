import Service from "../controllers/time-option.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";

export default (app) => {
  app.route(["/time/option/place/:place_id", "/time/option/list/:filter/:order/:desc/:limit"]).get(isValidToken, Service.list_a_time_options);

  app.route("/time/option").post(isValidToken, checkPermission, Service.create_a_time_option);

  app
    .route("/time/option/:id")
    .get(isValidToken, Service.read_a_time_option)
    .put(isValidToken, checkPermission, Service.update_a_time_option)
    .delete(isValidToken, checkPermission, Service.delete_a_time_option);
};
