import PlaceSubscribe from "../controllers/place-subscribe.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";

export default (app) => {
  // "/place/subscribe/:place_id" 이 아래 get("/place/subscribe/:id")과 중복되어 제거함.
  //app.route(["/place/subscribe/:place_id", "/place/subscribe/list/:filter", "/place/subscribe/list/:filter/:order/:desc/:limit"]).get(isValidToken, PlaceSubscribe.list_a_place_subscribes);
  app.route(["/place/subscribe/list/:filter", "/place/subscribe/list/:filter/:order/:desc/:limit"]).get(isValidToken, PlaceSubscribe.list_a_place_subscribes);

  app.route("/place/subscribe/prevnext/:id/:place_id/:filter").get(isValidToken, PlaceSubscribe.list_a_place_subscribes_prev_next);

  app.route("/place/subscribe").post(isValidToken, checkPermission, PlaceSubscribe.create_a_place_subscribe);

  app
    .route("/place/subscribe/:id")
    .get(isValidToken, PlaceSubscribe.read_a_place_subscribe)
    .put(isValidToken, checkPermission, PlaceSubscribe.update_a_place_subscribe)
    .delete(isValidToken, checkPermission, PlaceSubscribe.delete_a_place_subscribe);

  app.route("/place/subscribe/:path/:name/:newname").put(isValidToken, checkPermission, PlaceSubscribe.update_a_place_subscribe);
};
