import Qna from "../controllers/qna.js"; // Qna 컨트롤러
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";

export default (app) => {
  // QNA 목록 조회 (필터링 및 페이지네이션 지원)
  app.route(["/qna/list/:filter/:between/:begin/:end", "/qna/list/:filter/:between/:begin/:end/:limit", "/qna/list/:filter/:between/:begin/:end/:limit/:all"]).get(isValidToken, Qna.list_a_qnas); // /qna/list/:filter/:between/:begin/:end

  // QNA 이전/다음 항목 조회
  app.route("/qna/prevnext/:id/:filter/:between/:begin/:end").get(isValidToken, Qna.list_a_qnas_prev_next); // /qna/prevnext/:id

  // QNA 생성
  app.route("/qna").post(isValidToken, checkPermission, Qna.create_a_qna); // /qna (POST)

  // QNA 조회
  app.route("/qna/:id").get(isValidToken, Qna.read_a_qna); // /qna/:id (GET)

  // QNA 수정
  app.route("/qna/:id").put(isValidToken, checkPermission, Qna.update_a_qna); // /qna/:id (PUT)

  // QNA 삭제
  app.route("/qna/:id").delete(isValidToken, checkPermission, Qna.delete_a_qna); // /qna/:id (DELETE)

  // QNA 목록 조회 (답변 개수 포함)
  app.route(["/qna/list/with-answers-count/:filter", "/qna/list/with-answers-count/:filter/:limit"]).get(isValidToken, Qna.list_a_qnas_with_answers_count); // 새로운 API 엔드포인트 추가

  // QNA 상세 조회 + 답변 목록 조회 (질문 작성자 및 답변 작성자 이름 포함)
  app.route("/qna/with-answers/:id").get(isValidToken, Qna.list_a_qnas_with_answers_count); // /qna/with-answers/:id
};
