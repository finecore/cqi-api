import Server from "../controllers/server.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import _ from "lodash";

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { id } = req.params;

  if (!id) validationError.detail.push("server id 를 입력해 주세요!");
  if (isNaN(id)) validationError.detail.push("server id 는 숫자를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

export default (app) => {
  app.route(["/server/list/:filter"]).get(isValidToken, Server.list_a_servers);
  app.route("/server").post(isValidToken, checkPermission, Server.create_a_server);
  app.route("/server/:id").get(valid, isValidToken, Server.read_a_server).put(isValidToken, checkPermission, Server.update_a_server).delete(isValidToken, checkPermission, Server.delete_a_server);
};
