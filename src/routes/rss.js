import Rss from "../controllers/rss.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";

export default (app) => {
  app.route("/rss/:url").get(isValidToken, Rss.read_rss);
};
