import auth from "../controllers/auth.js";
import { jsonRes } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import _ from "lodash";

const valid_login = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const {
    body: { id, pwd },
  } = req;

  if (!id) validationError.detail.push("아이디를 입력해 주세요!");
  if (!pwd) validationError.detail.push("비밀번호를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

const valid_sync_login = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const {
    body: { id },
  } = req;

  if (!id) validationError.detail.push("아이디를 입력해 주세요!");
  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

const valid_token = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { headers: id } = req;

  if (!id) validationError.detail.push("serialno 를 입력해 주세요!");
  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

export default (app) => {
  // front manager login.
  app.route("/login").post(valid_login, auth.user_login).get(auth.user_login);
  app.route("/login/sync").post(valid_sync_login, auth.user_sync_login).get(auth.user_sync_login);

  // device auth token.
  app
    .route("/auth/device/token")
    .get(valid_token, auth.create_device_token)
    .post(valid_token, auth.create_device_token)
    .put(valid_token, auth.create_device_token)
    .delete(valid_token, auth.delete_device_token);

  app.route("/auth/sync/token/:id").get(valid_token, auth.create_sync_token);

  app.route("/alive").get(auth.alive);
};
