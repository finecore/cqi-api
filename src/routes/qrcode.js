import qrcode from "../controllers/qr-code.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { upload, rename, remove, download } from "../utils/file-util.js";

export default (app) => {
  app.route("/make/qr").post(qrcode.make_qr_code);
  app.route("/read/qr").post(isValidToken, checkPermission, upload.single("file"), qrcode.read_qr_code);
};
