import MemberPreference from "../controllers/member-preference.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import _ from "lodash";

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { method } = req;
  const { member_id } = req.params;

  if (!member_id) validationError.detail.push("member-preference member_id 를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

export default (app) => {
  app.route("/member/preference/list/:filter/:order/:desc/:limit").get(isValidToken, MemberPreference.list_a_member_preferences);

  // 회원 가립시 등록하므로 토큰 없음.
  app.route("/member/preference").post(MemberPreference.create_a_member_preference);

  app
    .route("/member/preference/:member_id")
    .get(valid, isValidToken, MemberPreference.read_a_member_preference)
    .put(valid, isValidToken, MemberPreference.update_a_member_preference)
    .delete(valid, isValidToken, checkPermission, MemberPreference.delete_a_member_preference);
};
