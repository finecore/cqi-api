import QnaAnswer from "../controllers/qna-answer.js"; // QnaAnswer 컨트롤러
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";

export default (app) => {
  // FAQ 답변 목록 조회 (특정 FAQ에 대한 답변)
  app.route("/qna/answer/list/:qna_id").get(isValidToken, QnaAnswer.list_a_qna_answers); // /qna/answer/:qna_id

  // FAQ 답변 조회
  app.route("/qna/answer/answer/:id").get(isValidToken, QnaAnswer.read_a_qna_answer); // /qna/answer/answer/:id

  // FAQ 답변 생성
  app.route("/qna/answer").post(isValidToken, checkPermission, QnaAnswer.create_a_qna_answer); // /qna/answer

  // FAQ 답변 수정
  app.route("/qna/answer/:id").put(isValidToken, checkPermission, QnaAnswer.update_a_qna_answer); // /qna/answer/:id

  // FAQ 답변 삭제
  app.route("/qna/answer/:id").delete(isValidToken, checkPermission, QnaAnswer.delete_a_qna_answer); // /qna/answer/:id
};
