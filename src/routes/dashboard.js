import dashboard from "../controllers/dashboard.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import _ from "lodash";

export default (app) => {
  app.route("/dashboard/total/place/sido").get(isValidToken, dashboard.total_place_sido);
  app.route("/dashboard/total/now/ieg").get(isValidToken, dashboard.total_now_ieg);
  app.route("/dashboard/total/now/isg").get(isValidToken, dashboard.total_now_isg);

  app.route(["/dashboard/list/now/ieg/:place_id", "/dashboard/list/now/ieg/:filter/:order/:desc/:limit"]).get(isValidToken, dashboard.list_now_ieg);
  app.route(["/dashboard/list/now/isg/:place_id", "/dashboard/list/now/isg/:filter/:order/:desc/:limit"]).get(isValidToken, dashboard.list_now_isg);

  app.route("/dashboard/total/reserv/sum/:filter/:begin/:end/:type/:groups").get(isValidToken, dashboard.total_reserv_sum);

  app.route("/dashboard/total/room/status/sum/:filter/:groups").get(isValidToken, dashboard.total_room_status_sum);
  app.route("/dashboard/total/isg/status/sum/:filter/:groups").get(isValidToken, dashboard.total_isg_status_sum);

  app.route("/dashboard/list/now/event/:filter/:limit").get(isValidToken, dashboard.list_now_event);

  app.route("/dashboard/total/report/ieg/:filter/:begin/:end/:type/:groups").get(isValidToken, dashboard.total_report_ieg);
  app.route("/dashboard/total/report/isg/:filter/:begin/:end/:type/:groups").get(isValidToken, dashboard.total_report_isg);
};
