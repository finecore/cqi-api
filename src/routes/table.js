import table from "../controllers/table.js";
import { jsonRes } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import _ from "lodash";

const valid_token = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { headers: id } = req;

  if (!id) validationError.detail.push("serialno 를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

export default (app) => {
  app.route("/table/comment/:table_name").get(valid_token, table.read_a_comment);
};
