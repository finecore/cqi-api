import roomState from "../controllers/room-state.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import _ from "lodash";

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { room_id } = req.params;

  if (!room_id) validationError.detail.push("room_state id 를 입력해 주세요!");
  if (isNaN(room_id)) validationError.detail.push("room_state id 는 숫자를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

export default (app) => {
  // room  state route.
  app.route("/room/state/all/:place_id").get(isValidToken, roomState.list_all_room_state).put(isValidToken, checkPermission, roomState.update_all_room_state);
  app.route("/room/state/list/:filter/:order/:desc/:limit").get(isValidToken, roomState.list_a_room_states);

  app.route("/room/state/signal/:place_id").put(isValidToken, roomState.update_signal_room_state);
  app.route("/room/state/checkout/:room_id").put(isValidToken, roomState.checkout_room_state);

  app.route("/room/state/all/sale/:place_id").get(isValidToken, roomState.list_all_room_state_sale);
  app.route("/room/state/sale/:room_id").get(valid, isValidToken, roomState.read_a_room_state_sale);

  app
    .route("/room/state/:room_id")
    .get(valid, isValidToken, roomState.read_a_room_state)
    .put(valid, isValidToken, checkPermission, roomState.update_a_room_state)
    .delete(valid, isValidToken, checkPermission, roomState.delete_a_room_state);

  app.route("/room/state").post(isValidToken, checkPermission, roomState.create_a_room_state);

  app.route("/room/state/name/:place_id/:room_name").get(isValidToken, roomState.read_a_room_state_by_name);
};
