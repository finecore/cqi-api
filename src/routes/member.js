import member from "../controllers/member.js";
import { jsonRes } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import _ from "lodash";

const valid_login = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const {
    body: { id, pwd },
  } = req;

  if (!id) validationError.detail.push("아이디를 입력해 주세요!");
  if (!pwd) validationError.detail.push("비밀번호를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

const valid_sync_login = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const {
    body: { id },
  } = req;

  if (!id) validationError.detail.push("아이디를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

const valid_token = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { headers: id } = req;

  if (!id) validationError.detail.push("serialno 를 입력해 주세요!");
  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

export default (app) => {
  // front manager login.
  app.route("/member/login").post(valid_login, member.member_login).get(member.member_login);
  app.route("/member/share/login").post(valid_login, member.member_login).get(member.member_share_login);
  app.route("/member/login/sync").post(valid_sync_login, member.member_sync_login).get(member.member_sync_login);
  app.route(["/member/list/place/:place_id", "/member/list/:filter/:order/:desc/:limit"]).get(member.list_a_members);

  app.route("/member").post(member.create_a_member);
  app.route("/member/idcheck/:id").get(member.read_a_member);
  app.route("/member/crud/:id").get(valid_token, member.read_a_member).put(valid_token, member.update_a_member).delete(valid_token, member.delete_a_member);

  app.route("/member/alive").get(valid_token, member.alive);
};
