import SyncLog from "../controllers/sync-log.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import _ from "lodash";

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { id } = req.params;

  if (!id) validationError.detail.push("server id 를 입력해 주세요!");
  if (isNaN(id)) validationError.detail.push("server id 는 숫자를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

export default (app) => {
  app.route(["/sync/log/list/:filter/:order/:desc/:limit"]).get(isValidToken, SyncLog.list_all_sync_logs);
  app.route("/sync/log").post(SyncLog.create_a_sync_log);
  app
    .route("/sync/log/:id")
    .get(valid, isValidToken, SyncLog.read_a_sync_log)
    .put(isValidToken, checkPermission, SyncLog.update_a_sync_log)
    .delete(isValidToken, checkPermission, SyncLog.delete_a_sync_log);

  app.route("/sync/old/logs").delete(isValidToken, checkPermission, SyncLog.delete_all_old_sync_log);
};
