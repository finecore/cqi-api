import placeLock from "../controllers/place-lock.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";

export default (app) => {
  // place  lock route.
  app.route("/place/lock/all/:place_id").get(isValidToken, placeLock.list_all_place_lock).delete(isValidToken, checkPermission, placeLock.delete_all_place_lock);

  app
    .route("/place/lock/:id")
    .get(isValidToken, placeLock.read_a_place_lock)
    .put(isValidToken, checkPermission, placeLock.update_a_place_lock)
    .delete(isValidToken, checkPermission, placeLock.delete_a_place_lock);

  app.route("/place/lock").post(isValidToken, checkPermission, placeLock.create_a_place_lock);
};
