import iscState from "../controllers/isc-state.js";
import { jsonRes, sendRes, isValidToken, checkSerialno, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import _ from "lodash";

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { url, method } = req;
  const { serialno } = req.params;

  if (!id) validationError.detail.push("isc-state serialno 를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

export default (app) => {
  app.route("/isc/state/list/:filter/:order/:desc/:limit").get(isValidToken, iscState.list_a_isc_states);
  app.route("/isc/state/place/:place_id").get(isValidToken, iscState.list_place_isc_states);
  app.route("/isc/state").post(isValidToken, checkSerialno, iscState.create_a_isc_state);

  app
    .route("/isc/state/:serialno")
    .get(valid, isValidToken, checkSerialno, iscState.read_a_isc_state)
    .put(valid, isValidToken, checkSerialno, iscState.update_a_isc_state)
    .delete(valid, isValidToken, checkSerialno, checkPermission, iscState.delete_a_isc_state);
};
