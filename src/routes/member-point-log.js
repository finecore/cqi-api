import MemberPointLog from "../controllers/member-point-log.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";

export default (app) => {
  // room  state route.
  app.route("/member/point/log/all/:place_id/:begin/:end").get(MemberPointLog.list_all_member_point_log);
  app.route("/member/point/log/list/:member_id/:filter/:between/:begin/:end").get(isValidToken, MemberPointLog.read_a_member_point_logs);
  app.route("/member/point/log/list/place/:place_id").get(MemberPointLog.read_a_member_point_logs);
  app.route("/member/point/log/day/:place_id/:day").get(MemberPointLog.read_a_member_point_day_logs);
  app.route("/member/point/log/last/:place_id/:last_id").get(MemberPointLog.read_a_member_point_last_logs);

  app
    .route("/member/point/log/:id")
    .get(MemberPointLog.read_a_member_point_log)
    .put(MemberPointLog.update_a_member_point_log)
    .delete(isValidToken, checkPermission, MemberPointLog.delete_a_member_point_log);

  app.route("/member/point/log").post(MemberPointLog.create_a_member_point_log);
};
