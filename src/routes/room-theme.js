import roomTheme from "../controllers/room-theme.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";

export default (app) => {
  // room  theme route.
  app.route("/room/theme/all").get(isValidToken, roomTheme.list_all_room_theme);

  app
    .route("/room/theme/:id")
    .get(isValidToken, roomTheme.read_a_room_theme)
    .put(isValidToken, checkPermission, roomTheme.update_a_room_theme)
    .delete(isValidToken, checkPermission, roomTheme.delete_a_room_theme);

  app.route("/room/theme").post(isValidToken, checkPermission, roomTheme.create_a_room_theme);
};
