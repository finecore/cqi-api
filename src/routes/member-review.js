import MemberReview from "../controllers/member-review.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import _ from "lodash";

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { method } = req;
  const { member_id, id } = req.params;

  if (!member_id && !id) validationError.detail.push("member-review member_id or id 를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

export default (app) => {
  app.route("/member/review/list/:filter/:order/:desc/:limit").get(isValidToken, MemberReview.list_a_member_reviews);
  app.route("/member/review/main/best").get(isValidToken, MemberReview.list_a_member_best_reviews);
  app.route("/member/review/member/:member_id").get(isValidToken, MemberReview.list_a_member_by_member_reviews);
  app.route("/member/review/place/:place_id").get(isValidToken, MemberReview.list_a_member_by_place_reviews);

  // 회원 가립시 등록하므로 토큰 없음.
  app.route("/member/review").post(MemberReview.create_a_member_review);

  app.route("/member/review/all/member/:member_id").delete(valid, isValidToken, checkPermission, MemberReview.delete_a_member_all_review);

  app
    .route("/member/review/:id")
    .get(valid, isValidToken, MemberReview.read_a_member_review)
    .put(valid, isValidToken, MemberReview.update_a_member_review)
    .delete(valid, isValidToken, checkPermission, MemberReview.delete_a_member_review);
};
