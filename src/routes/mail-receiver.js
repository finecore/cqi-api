import MailReceiver from "../controllers/mail-receiver.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import _ from "lodash";

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { id } = req.params;

  if (!id) validationError.detail.push("mail_receiver id 를 입력해 주세요!");
  if (isNaN(id)) validationError.detail.push("mail_receiver id 는 숫자를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

export default (app) => {
  app.route(["/mail/receiver/list/:filter/:order/:desc/:limit"]).get(isValidToken, MailReceiver.list_a_mail_receivers);

  app.route("/mail_receiver").post(isValidToken, checkPermission, MailReceiver.create_a_mail_receiver);

  app
    .route("/mail/receiver/:id")
    .get(valid, isValidToken, MailReceiver.read_a_mail_receiver)
    .put(valid, isValidToken, checkPermission, MailReceiver.update_a_mail_receiver)
    .delete(valid, isValidToken, checkPermission, MailReceiver.delete_a_mail_receiver);
};
