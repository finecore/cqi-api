import NoticePlace from "../controllers/notice-place.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";

export default (app) => {
  app
    .route(["/notice/place/list/:filter/:begin/:end", "/notice/place/list/:filter/:begin/:end/:limit", "/notice/place/list/:filter/:begin/:end/:limit/:all"])
    .get(isValidToken, NoticePlace.list_a_notice_places);

  app.route("/notice/place").post(isValidToken, checkPermission, NoticePlace.create_a_notice_place);

  app
    .route("/notice/place/:id")
    .get(isValidToken, NoticePlace.read_a_notice_place)
    .put(isValidToken, checkPermission, NoticePlace.update_a_notice_place)
    .delete(isValidToken, checkPermission, NoticePlace.delete_a_notice_place);
};
