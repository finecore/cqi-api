import roomReserv from "../controllers/room-reserv.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import _ from "lodash";

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { id } = req.params;

  if (!id) validationError.detail.push("room_reserv id 를 입력해 주세요!");
  if (isNaN(id)) validationError.detail.push("room_reserv id 는 숫자를 입력해 주세요!");
  1;

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

export default (app) => {
  // room  reserv route.
  app
    .route(["/room/reserv/all/:place_id", "/room/reserv/all/:place_id/:begin/:end"])
    .get(isValidToken, roomReserv.list_all_room_reserv)
    .delete(isValidToken, checkPermission, roomReserv.delete_all_room_reserv);

  app.route(["/room/reserv/all/member/:member_id"]).delete(isValidToken, checkPermission, roomReserv.delete_all_member_reserv);

  app.route(["/room/reserv/member/:member_id/:between", "/room/reserv/member/:member_id/:filter/:between/:begin/:end"]).get(isValidToken, roomReserv.list_member_room_reserv);
  app.route(["/room/reserv/place/:place_id", "/room/reserv/place/:place_id/:between/:begin/:end"]).get(isValidToken, roomReserv.read_a_place_id_reserv);
  app.route(["/room/reserv/room/:room_id", "/room/reserv/room/:room_id/:between/:begin/:end"]).get(isValidToken, roomReserv.read_a_room_id_reserv);
  app.route(["/room/reserv/:id/:between/:begin/:end"]).get(isValidToken, roomReserv.read_a_item_id_reserv);

  app.route(["/room/reserv/list/:filter/:begin/:end"]).get(isValidToken, roomReserv.list_a_room_reservs);
  app.route(["/room/reserv/enable/:place_id"]).get(isValidToken, roomReserv.list_a_place_enable_reservs);

  // QR 코드 조회는 validateion 체크 안함.
  app.route("/room/reserv/qr/:id").get(roomReserv.read_a_room_reserv);

  app
    .route("/room/reserv/:id")
    .get(isValidToken, roomReserv.read_a_room_reserv)
    .put(isValidToken, roomReserv.update_a_room_reserv)
    .delete(isValidToken, checkPermission, roomReserv.delete_a_room_reserv);

  app.route(["/room/reserv/num/:num"]).get(isValidToken, roomReserv.read_a_room_reserv_num);
  app.route(["/room/reserv/mms/mo/:num"]).get(isValidToken, roomReserv.read_a_room_reserv_mms_mo_num);
  app.route(["/room/reserv/member/:member_id"]).get(isValidToken, roomReserv.read_a_member_reserv);

  app.route("/room/reserv").post(isValidToken, checkPermission, roomReserv.create_a_room_reserv);

  app.route("/room/reserv/canreserv/:filter/:between/:begin/:end/:order").get(isValidToken, checkPermission, roomReserv.list_a_can_reserv_rooms);
  app.route("/room/reserv/cannotreserv/:filter/:between/:begin/:end/:order").get(isValidToken, checkPermission, roomReserv.list_a_can_not_reserv_rooms);
};
