export default (app) => {
  /* GET home page. */
  app.get("/", (req, res, next) => {
    res.status(200).json({
      common: {
        success: true,
        message: "welcome api server!",
        error: null,
      },
    });
  });
};
