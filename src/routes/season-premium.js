import seasonPremium from "../controllers/season-premium.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";

export default (app) => {
  // season  premium route.
  app.route("/season/premium/all/:place_id").get(isValidToken, seasonPremium.list_all_season_premium).delete(isValidToken, checkPermission, seasonPremium.delete_all_season_premium);

  app.route("/season/premium/:place_id/:begin/:end").get(isValidToken, seasonPremium.between_season_premium);

  app
    .route("/season/premium/:id")
    .get(isValidToken, seasonPremium.read_a_season_premium)
    .put(isValidToken, checkPermission, seasonPremium.update_a_season_premium)
    .delete(isValidToken, checkPermission, seasonPremium.delete_a_season_premium);

  app.route("/season/premium").post(isValidToken, checkPermission, seasonPremium.create_a_season_premium);
};
