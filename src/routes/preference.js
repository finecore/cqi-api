import preference from "../controllers/preference.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import _ from "lodash";

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { id } = req.params;

  if (!id) validationError.detail.push("preference id 를 입력해 주세요!");
  if (isNaN(id)) validationError.detail.push("preference id 는 숫자를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

export default (app) => {
  // preference route.
  app.route("/preference/all").get(isValidToken, preference.list_all_preferences);
  app.route("/preference/:place_id").get(isValidToken, preference.read_a_preference);

  app.route("/preference/:id").put(valid, isValidToken, checkPermission, preference.update_a_preferences).delete(valid, isValidToken, checkPermission, preference.delete_a_preferences);

  app.route("/preference").post(isValidToken, checkPermission, preference.create_a_preferences);
};
