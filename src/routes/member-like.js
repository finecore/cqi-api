import MemberLike from "../controllers/member-like.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import _ from "lodash";

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { method } = req;
  const { member_id, id } = req.params;

  if (!member_id && !id) validationError.detail.push("member-like member_id or id 를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

export default (app) => {
  app.route("/member/like/list/:member_id/:filter/:between/:begin/:end").get(isValidToken, MemberLike.list_a_member_likes);
  app.route("/member/like/all/:member_id").delete(valid, isValidToken, checkPermission, MemberLike.delete_a_member_all_like);

  app.route("/member/like/place/:place_id/:member_id").get(isValidToken, MemberLike.read_a_member_like_by_place);
  app.route("/member/like").post(isValidToken, checkPermission, MemberLike.create_a_member_like);

  app
    .route("/member/like/:id")
    .get(valid, isValidToken, MemberLike.read_a_member_like)
    .put(valid, isValidToken, MemberLike.update_a_member_like)
    .delete(valid, isValidToken, checkPermission, MemberLike.delete_a_member_like);
};
