import UserPlace from "../controllers/user-place.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import _ from "lodash";

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { id } = req.params;

  if (!id) validationError.detail.push("user_place id 를 입력해 주세요!");
  if (isNaN(id)) validationError.detail.push("user_place id 는 숫자를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

export default (app) => {
  app.route(["/user/place/all", "/user/place/:user_id", "/user/place/list/:filter/:order/:desc/:limit"]).get(isValidToken, UserPlace.list_a_user_places);

  app.route("/user/place").post(isValidToken, checkPermission, UserPlace.create_a_user_place);

  app
    .route("/user/place/:id")
    .get(valid, isValidToken, UserPlace.read_a_user_place)
    .put(valid, isValidToken, checkPermission, UserPlace.update_a_user_place)
    .delete(valid, isValidToken, checkPermission, UserPlace.delete_a_user_place);
};
