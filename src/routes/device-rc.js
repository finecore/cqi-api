import deviceRc from "../controllers/device-rc.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import _ from "lodash";

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { method } = req;
  const { id, type, name } = req.params;

  if (!id) validationError.detail.push("device-rc id 를 입력해 주세요!");
  if (isNaN(id)) validationError.detail.push("device-rc id 는 숫자를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

export default (app) => {
  app.route(["/device/rc/all", "/device/rc/list/:filter/:order/:desc/:limit"]).get(isValidToken, deviceRc.list_a_device_rcs);
  app.route("/device/rc/place/:place_id").get(isValidToken, deviceRc.list_place_device_rcs);

  app.route("/device-rc").post(deviceRc.create_a_device_rc);

  app
    .route("/device/rc/:id")
    .get(valid, isValidToken, deviceRc.read_a_device_rc)
    .put(valid, isValidToken, deviceRc.update_a_device_rc)
    .delete(valid, isValidToken, checkPermission, deviceRc.delete_a_device_rc);

  app.route("/device/rc/uuid/:uuid").get(isValidToken, deviceRc.read_a_device_rc);

  app.route("/device/rc/uuid/:uuid").put(isValidToken, deviceRc.update_a_device_rc_by_uuid);
};
