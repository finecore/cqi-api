import MemberPoint from '../controllers/member-point.js';
import {
  jsonRes,
  sendRes,
  isValidToken,
  checkPermission,
} from '../utils/api-util.js';
import { ERROR } from '../constants/constants.js';
import _ from 'lodash';

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { method } = req;
  const { id } = req.params;

  if (!id) validationError.detail.push('MemberPoint id 를 입력해 주세요!');
  if (isNaN(id))
    validationError.detail.push('MemberPoint id 는 숫자를 입력해 주세요!');

  if (Object.keys(validationError.detail).length > 0)
    return jsonRes(req, res, validationError);
  else return next();
};

export default (app) => {
  app
    .route([
      '/member/point/all',
      '/member/point/list/:filter/:order/:desc/:limit',
    ])
    .get(isValidToken, MemberPoint.list_a_member_points);
  app
    .route('/member/point/place/:place_id')
    .get(isValidToken, MemberPoint.list_place_member_points);

  app
    .route('/member/point/:id/:user_id')
    .delete(
      valid,
      isValidToken,
      checkPermission,
      MemberPoint.delete_a_member_point,
    );

  app
    .route('/member/point/member/:member_id')
    .get(isValidToken, MemberPoint.read_member_member_point);

  app
    .route('/member/point/member/increase/:place_id/:member_id/:point') // ✅ 수정: 배열 제거
    .put(isValidToken, MemberPoint.increase_place_member_point_member_id);

  app
    .route('/member/point/member/decrease/:place_id/:member_id/:point') // ✅ 수정: // 제거
    .put(isValidToken, MemberPoint.decrease_place_member_point_member_id);

  app
    .route('/member/point/:id')
    .get(valid, isValidToken, MemberPoint.read_a_member_point)
    .put(
      valid,
      isValidToken,
      checkPermission,
      MemberPoint.update_a_member_point,
    )
    .delete(
      valid,
      isValidToken,
      checkPermission,
      MemberPoint.delete_a_member_point,
    );

  app.route('/member/point').post(MemberPoint.create_a_member_point);
};
