import snsLogin from "../controllers/sns-login.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";
import { ERROR } from "../constants/constants.js";
import _ from "lodash";

export default (app) => {
  // 구글 로그인 라우트
  app.route("/auth/google").post(snsLogin.google_sns_login);

  // 카카오 로그인 라우트
  app.route("/auth/kakao").post(snsLogin.kakao_sns_login);

  // 네이버 로그인 라우트
  app.route("/auth/naver").post(snsLogin.naver_sns_login);

  // 페이스북 로그인 라우트
  app.route("/auth/facebook").post(snsLogin.facebook_sns_login);
};
