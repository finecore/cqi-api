import As from "../controllers/as.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";

export default (app) => {
  app.route(["/as/list/:filter/:between/:begin/:end/:limit"]).get(isValidToken, As.list_a_ases);

  app.route("/as").post(isValidToken, checkPermission, As.create_a_as);

  app.route("/as/:id").get(isValidToken, As.read_a_as).put(isValidToken, checkPermission, As.update_a_as).delete(isValidToken, checkPermission, As.delete_a_as);
};
