import iscStateLog from "../controllers/isc-state-log.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";

export default (app) => {
  // room  state route.
  app.route("/isc/state/log/list/:filter/:order/:desc/:limit").get(isValidToken, iscStateLog.list_a_isc_state_logs);

  app.route("/isc/state/log/day/:place_id/:day").get(isValidToken, iscStateLog.read_a_isc_state_day_logs);
  app.route("/isc/state/log/last/:place_id/:last_id").get(isValidToken, iscStateLog.read_a_isc_state_last_logs);

  app
    .route("/isc/state/log/:id")
    .get(isValidToken, iscStateLog.read_a_isc_state_log)
    .put(isValidToken, iscStateLog.update_a_isc_state_log)
    .delete(isValidToken, checkPermission, iscStateLog.delete_a_isc_state_log);

  app.route("/isc/state/log").post(isValidToken, iscStateLog.create_a_isc_state_log);
};
