import placeImgs from "../controllers/place-imgs.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "../utils/api-util.js";

export default (app) => {
  // place  imgs route.
  app.route("/place/imgs/all/:place_id").get(isValidToken, placeImgs.list_all_place_imgs).delete(isValidToken, checkPermission, placeImgs.delete_all_place_imgs);

  app
    .route("/place/imgs/:id")
    .get(isValidToken, placeImgs.read_a_place_imgs)
    .put(isValidToken, checkPermission, placeImgs.update_a_place_imgs)
    .delete(isValidToken, checkPermission, placeImgs.delete_a_place_imgs);

  app.route("/place/imgs").post(isValidToken, checkPermission, placeImgs.create_a_place_imgs);
};
