import connection from "../db/connection.js";

// RoomView object constructor
const RoomView = {
  selectAllRoomViews: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT a.*, b.theme FROM room_view a LEFT JOIN room_theme b ON a.theme_id = b.id WHERE a.place_id = ? ORDER BY a.order ASC ", [place_id], result);
      }
    });
  },

  selectRoomView: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT a.*, b.theme FROM room_view a LEFT JOIN room_theme b ON a.theme_id = b.id WHERE a.id = ?  ORDER BY a.order ASC ", id, result);
      }
    });
  },

  insertRoomView: (newRoomView, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "UPDATE room_view SET `order` = `order` + 1 WHERE `place_id` = " + newRoomView.place_id + " AND `order` >= " + newRoomView.order + "; INSERT INTO room_view SET ? ON DUPLICATE KEY UPDATE ? ",
          [newRoomView, newRoomView],
          result
        );
      }
    });
  },

  updateRoomView: (id, roomView, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE room_view SET ? WHERE id = ?", [roomView, id], result);
      }
    });
  },

  deleteRoomView: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "UPDATE room_view SET `order` = `order` - 1 WHERE`order` > (SELECT A.order FROM (SELECT `order` FROM room_view  WHERE id = ? LIMIT 1) A); DELETE FROM room_view WHERE id = ?",
          [id, id],
          result
        );
      }
    });
  },

  deleteAllRoomViews: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM room_view WHERE place_id = ?", [place_id], result);
      }
    });
  },
};

export default RoomView;
