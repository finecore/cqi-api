import connection from "../db/connection.js";
import Room from "../model/room.js";
import format from "../utils/format-util.js";

// RoomStateLog object constructor
const RoomStateLog = {
  selectAllRoomStateLogs: (place_id, filter, between, begin, end, order, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name FROM room_state_log a INNER JOIN room b ON a.room_id = b.id
            WHERE b.place_id = ? AND ${filter} AND ${between} BETWEEN ? AND ?
            ORDER BY ${order} LIMIT ?`,
          [place_id, begin, end, limit],
          result
        );
      }
    });
  },

  selectRoomStateLogs: (place_id, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name, c.name type_name
             FROM room_state_log a
            INNER JOIN room b ON a.room_id = b.id
            INNER JOIN room_type c ON b.room_type_id = c.id
            WHERE b.place_id = ?
            ORDER BY a.id DESC LIMIT ?;`,
          [place_id, limit],
          result
        );
      }
    });
  },

  selectRoomStateDayLogs: (place_id, day, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name, c.name type_name
             FROM room_state_log a
            INNER JOIN room b ON a.room_id = b.id
            INNER JOIN room_type c ON b.room_type_id = c.id
            WHERE b.place_id = ? AND a.reg_date >= DATE_ADD(NOW(), INTERVAL ? DAY)
            ORDER BY a.id DESC LIMIT 300;`,
          [place_id, day],
          result
        );
      }
    });
  },

  selectRoomStateLastLogs: (place_id, last_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name
             FROM room_state_log a INNER JOIN room b ON a.room_id = b.id WHERE b.place_id = ?
              AND a.id > ?
            ORDER BY a.id DESC LIMIT 300 `,
          [place_id, last_id],
          result
        );
      }
    });
  },

  selectRoomStateLog: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT * FROM room_state_log WHERE id = ? ", id, result);
      }
    });
  },

  insertRoomStateLog: (newRoomStateLog, req, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        newRoomStateLog["reg_date"] = new Date();
        delete newRoomStateLog["mod_date"];

        _connection.sql("INSERT INTO room_state_log SET ? ", [newRoomStateLog], (err, info) => {
          result(err, info);

          if (!err && info) {
            let { room_id } = newRoomStateLog;

            room_id = Number(room_id);

            newRoomStateLog.room_id = room_id;
            newRoomStateLog.id = info.insertId;

            try {
              if (newRoomStateLog.data && typeof newRoomStateLog.data === "string") newRoomStateLog.data = JSON.parse(newRoomStateLog.data);
            } catch (e) {}

            let {
              headers: { channel, uuid },
              auth: { place_id },
            } = req;

            console.log("-----ADD_ROOM_STATE_LOG_LIST ", { channel, uuid, place_id });

            const json = {
              type: "ADD_ROOM_STATE_LOG_LIST", // 웹 reducer type, 어드민/모바일 store 타입은 socket server 에서 각각 변경.
              headers: {
                method: "put",
                url: "room/roomStateLog/" + room_id,
                token: "",
                channel: "api",
                uuid,
                req_channel: channel,
              },
              body: {
                room_state_log: newRoomStateLog,
                room_id,
                place_id,
              },
            };

            // 소켓 서버로 전송.
            global.dispatcher.dispatch(json);
          }
        });
      }
    });
  },

  updateRoomStateLog: (id, roomStateLog, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE roomStateLog SET ? WHERE id = ?", [roomStateLog, id], result);
      }
    });
  },

  deleteRoomStateAfterKeepDay: (place_id, date, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `DELETE a FROM room_state_log a INNER JOIN room b ON a.room_id = b.id INNER JOIN place c ON b.place_id = c.id
            WHERE b.place_id = ? AND a.reg_date < STR_TO_DATE(?, '%Y-%m-%d %H:%i'); `,
          [place_id, date],
          result
        );
      }
    });
  },

  deleteRoomStateLog: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM room_state_log WHERE id = ? ", [id], result);
      }
    });
  },
};

export default RoomStateLog;
