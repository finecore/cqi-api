import connection from "../db/connection.js";

// Device object constructor
const Device = {
  selectDeviceCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(DISTINCT a.id) count
             FROM device a INNER JOIN place b ON a.place_id = b.id INNER JOIN company c ON b.company_id = c.id LEFT JOIN user_place u ON a.place_id = u.place_id
            WHERE ${filter} `,
          [],
          result
        );
      }
    });
  },

  selectDevices: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT DISTINCT a.*, b.name as place_name, c.name as company_name, v.version, v.discription
           FROM device a  INNER JOIN place b ON a.place_id = b.id
                          INNER JOIN company c ON b.company_id = c.id
                          LEFT JOIN user_place u ON a.place_id = u.place_id
                          LEFT JOIN version v ON a.curr_version_id = v.id
           WHERE ${filter}
           ORDER BY ${order} ${desc}
           LIMIT ${limit} `,
          [],
          result
        );
      }
    });
  },

  selectPlaceDevices: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT DISTINCT a.*, a.expire_date >= CURRENT_TIMESTAMP expire, b.expire_date >= CURRENT_TIMESTAMP place_expire, v.version, v.discription
           FROM device a INNER JOIN place b ON a.place_id = b.id LEFT JOIN user_place u ON a.place_id = u.place_id LEFT JOIN version v ON a.curr_version_id = v.id
           WHERE a.place_id = ? `,
          [place_id],
          result
        );
      }
    });
  },

  selectDevice: (id, serialno, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT DISTINCT a.*, a.expire_date >= CURRENT_TIMESTAMP expire, b.expire_date >= CURRENT_TIMESTAMP place_expire, v.version, v.discription
             FROM device a INNER JOIN place b ON a.place_id = b.id  LEFT JOIN version v ON a.curr_version_id = v.id
            WHERE a.id = ? OR a.serialno = ?`,
          [id, serialno],
          result
        );
      }
    });
  },

  selectDeviceBySerialNo: (serialno, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "SELECT DISTINCT a.*, a.expire_date >= CURRENT_TIMESTAMP expire, b.expire_date >= CURRENT_TIMESTAMP place_expire  FROM device a INNER JOIN place b ON a.place_id = b.id WHERE a.serialno = ? ",
          [serialno],
          result
        );
      }
    });
  },

  insertDevice: (newDevice, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO device SET ? ON DUPLICATE KEY UPDATE ? ", [newDevice, newDevice], result);
      }
    });
  },

  updateDevice: (id, device, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE device SET ? WHERE id = ?", [device, id], result);
      }
    });
  },

  updateDeviceSerialno: (serialno, device, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE device SET ? WHERE serialno = ?", [device, serialno], result);
      }
    });
  },

  deleteDevice: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM device WHERE id = ?", [id], result);
      }
    });
  },

  deletePlaceDevices: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM device WHERE place_id = ?", [place_id], result);
      }
    });
  },
};

export default Device;
