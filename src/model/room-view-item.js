import connection from "../db/connection.js";

// RoomViewItem object constructor
const RoomViewItem = {
  selectAllRoomViewItems: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT a.* FROM room_view_item a INNER JOIN room_view b ON a.view_id = b.id WHERE b.place_id = ?  ORDER BY 'row' , 'col'  ", [place_id], result);
      }
    });
  },

  selectRoomViewItems: (view_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*
                , b.name  AS room_name
                , c.name  AS type_name
             FROM room_view_item a
                  LEFT JOIN room b  ON  a.room_id  =  b.id
                  LEFT JOIN room_type c  ON  b.room_type_id  =  c.id
            WHERE view_id = ?
            ORDER BY 'row', 'col'
            ;`,
          [view_id],
          result
        );
      }
    });
  },

  selectRoomViewItem: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT * FROM room_view_item WHERE id = ? ", [id], result);
      }
    });
  },

  insertRoomViewItem: (newRoomViewItem, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO room_view_item SET ? ON DUPLICATE KEY UPDATE ? ", [newRoomViewItem, newRoomViewItem], result);
      }
    });
  },

  putRoomViewItem: (id, roomViewItem, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE room_view_item SET ? WHERE id = ? ", [roomViewItem, id], result);
      }
    });
  },

  deleteRoomViewItem: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM room_view_item WHERE id = ? ", [id], result);
      }
    });
  },

  deleteAllRoomViewItems: (view_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM room_view_item WHERE view_id = ? ", [view_id], result);
      }
    });
  },
};

export default RoomViewItem;
