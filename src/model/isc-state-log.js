import connection from "../db/connection.js";
import IscState from "./isc-state.js";
import _ from "lodash";

// IscStateLog object constructor
const IscStateLog = {
  selectIscStateLogCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count
             FROM isc_state_log a INNER JOIN device b ON a.serialno = b.serialno
            WHERE ${filter} `,
          [],
          result
        );
      }
    });
  },

  selectIscStateLogs: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*,  b.place_id, b.type device_type, b.name device_name
           FROM isc_state_log a INNER JOIN device b ON a.serialno = b.serialno
           WHERE ${filter} ORDER BY ${order} ${desc} LIMIT ${limit} `,
          [],
          result
        );
      }
    });
  },
  // selectIscStateLogs: (filter, order, desc, limit, result) => {
  //   connection((_err, _connection) => {
  //     if (_err) result(_err, null);
  //     else {
  //       _connection.sql(
  //         `SELECT a.*,  b.place_id, b.type device_type, b.name device_name, c.user_id, u.level user_level
  //          FROM isc_state_log a INNER JOIN device b ON a.serialno = b.serialno LEFT JOIN user_place c ON b.place_id = c.place_id  INNER JOIN user u ON c.user_id = u.id
  //          WHERE ${filter} ORDER BY ${order} ${desc} LIMIT ${limit} `,
  //         [],
  //         result
  //       );
  //     }
  //   });
  // },

  selectIscStateLog: (id) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT * FROM isc_state_log WHERE id = ? "[id], result);
      }
    });
  },

  selectIscStateDayLogs: (place_id, day, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "  SELECT a.*, c.name" +
            " FROM isc_state_log a INNER JOIN device b ON a.serialno = b.serialno INNER JOIN device c ON c.serialno = b.serialno WHERE c.place_id = ? " +
            " AND a.reg_date >= DATE_ADD(NOW(), INTERVAL ? DAY)  " +
            " ORDER BY a.id DESC LIMIT 300 ",
          [place_id, day],
          result
        );
      }
    });
  },

  selectIscStateLastLogs: (place_id, last_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "  SELECT a.*, b.name" + " FROM isc_state_log a INNER JOIN device b ON a.serialno = b.serialno WHERE b.place_id = ? " + " AND a.id > ?  " + " ORDER BY a.id DESC LIMIT 300 ",
          [place_id, last_id],
          result
        );
      }
    });
  },

  selectIscStateLog: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT * FROM isc_state_log WHERE id = ? ", id, result);
      }
    });
  },

  insertIscStateLog: (newIscStateLog, result) => {
    let { place_id, serialno } = newIscStateLog;

    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO isc_state_log SET ? ", [newIscStateLog], (err, info) => {
          result(err, info);

          console.log("---> insertIscStateLog", { place_id, serialno });

          if (place_id && newIscStateLog.data) {
            newIscStateLog.id = info.insertId;

            try {
              if (typeof newIscStateLog.data === "string") newIscStateLog.data = JSON.parse(newIscStateLog.data);

              const json = {
                type: "SET_ISC_STATE_LOG", // 웹 reducer type, 어드민/모바일 store 타입은 socket server 에서 각각 변경.
                headers: {
                  method: "put",
                  url: "/",
                  token: "",
                  serialno,
                  channel: "api",
                  place_id,
                },
                body: {
                  isc_state_log: newIscStateLog,
                },
              };

              // 소켓 서버로 전송.
              global.dispatcher.dispatch(json);
            } catch (e) {}
          }
        });
      }
    });
  },

  updateIscStateLog: (id, isc_stateStateLog, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE isc_stateStateLog SET ? WHERE id = ?", [isc_stateStateLog, id], result);
      }
    });
  },

  deleteIscStateLog: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM isc_state_log WHERE id = ? ", [id], result);
      }
    });
  },

  deleteIscStateLogAfterKeepDay: (date, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `DELETE FROM isc_state_log
            WHERE reg_date < STR_TO_DATE(?, '%Y-%m-%d %H:%i'); `,
          [date],
          result
        );
      }
    });
  },
};

export default IscStateLog;
