import connection from "../db/connection.js";

// Preference object constructor
const Preference = {
  selectAllPreferences: (result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT a.*, b.name place_name FROM preference a INNER JOIN place b ON a.place_id = b.id ", [], result);
      }
    });
  },

  selectPreference: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT a.*, b.name place_name FROM preference a INNER JOIN place b ON a.place_id = b.id WHERE place_id = ? ", place_id, result);
      }
    });
  },

  insertPreference: (newPreference, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO preference SET ? ON DUPLICATE KEY UPDATE ? ", [newPreference, newPreference], result);
      }
    });
  },

  updatePreference: (id, preference, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE preference SET ? WHERE id = ?", [preference, id], result);
      }
    });
  },

  deletePreference: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM preference WHERE id = ?", [id], result);
      }
    });
  },
};

export default Preference;
