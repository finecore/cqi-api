import connection from "../db/connection.js";

// MemberLike object constructor
const MemberLike = {
  selectMemberLikesCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count
           FROM member_like a
              INNER JOIN place b ON a.place_id = b.id
              INNER JOIN member c ON a.member_id = c.id
          WHERE ${filter} `,
          [],
          result
        );
      }
    });
  },

  selectMemberLikes: (member_id, filter, between, begin, end, order, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name as place_name ,c.name as member_name, b.addr as place_addr, c.nic_name as nic_name
           FROM member_like a
              INNER JOIN place b ON a.place_id = b.id
              INNER JOIN member c ON a.member_id = c.id
            WHERE a.member_id = ?
              AND ${filter}
              AND ${between} BETWEEN ? AND ?
            ORDER BY ${order}
            LIMIT 1000;`,
          [member_id, begin, end],
          result
        );
      }
    });
  },

  selectByMemberLikes: (member_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name as place_name ,c.name as member_name, b.addr as place_addr, c.nic_name as nic_name
           FROM member_like a
              INNER JOIN place b ON a.place_id = b.id
              INNER JOIN member c ON a.member_id = c.id
           WHERE a.member_id = ?
           ORDER BY a.reg_date desc;`,
          [member_id],
          result
        );
      }
    });
  },

  selectMemberLike: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name as place_name ,c.name as member_name, b.addr as place_addr, c.nic_name as nic_name
           FROM member_like a
              INNER JOIN place b ON a.place_id = b.id
              INNER JOIN member c ON a.member_id = c.id
            WHERE a.id = ?
            ORDER BY a.reg_date desc;`,
          [id],
          result
        );
      }
    });
  },

  selectMemberLikeByPlaceId: (place_id, member_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name as place_name ,c.name as member_name, b.addr as place_addr, c.nic_name as nic_name
           FROM member_like a
              INNER JOIN place b ON a.place_id = b.id
              INNER JOIN member c ON a.member_id = c.id
            WHERE a.place_id = ? AND a.member_id = ?
            ORDER BY a.reg_date desc;`,
          [place_id, member_id],
          result
        );
      }
    });
  },

  insertMemberLike: (member_like, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO member_like SET ?   ", [member_like], result);
      }
    });
  },

  updateMemberLike: (id, member_like, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE member_like SET ? WHERE id = ?", [member_like, id], result);
      }
    });
  },

  deleteMemberLike: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM member_like WHERE id = ?", [id], result);
      }
    });
  },

  deleteAllMemberLike: (member_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`DELETE FROM member_like WHERE member_id =  ? ;`, [member_id], result);
      }
    });
  },
};

export default MemberLike;
