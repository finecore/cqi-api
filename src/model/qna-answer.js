import connection from "../db/connection.js";

// QnaAnswer object constructor
const QnaAnswer = {
  // 특정 QNA의 답변 개수 조회
  selectQnaAnswerCount: (qna_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT COUNT(*) AS count
             FROM qna_answers
            WHERE qna_id = ?`,
          [qna_id],
          result
        );
      }
    });
  },

  // 특정 QNA에 대한 모든 답변 조회 (작성자 이름 포함)
  selectQnaAnswers: (qna_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT q.*, u.name AS user_name
             FROM qna_answers q
             LEFT JOIN user u ON q.user_id = u.id
            WHERE q.qna_id = ?`,
          [qna_id],
          result
        );
      }
    });
  },

  // 특정 답변 조회 (작성자 이름 포함)
  selectQnaAnswer: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT q.*, u.name AS user_name
             FROM qna_answers q
             LEFT JOIN user u ON q.user_id = u.id
            WHERE q.id = ?`,
          [id],
          result
        );
      }
    });
  },

  // FAQ에 답변 삽입 (새 답변 추가)
  insertQnaAnswer: (newAnswer, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO qna_answers SET ? ", [newAnswer], result);
      }
    });
  },

  // FAQ 답변 수정 (업데이트)
  updateQnaAnswer: (id, qnaAnswer, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE qna_answers SET ? WHERE id = ?", [qnaAnswer, id], result);
      }
    });
  },

  // FAQ 답변 삭제
  deleteQnaAnswer: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM qna_answers WHERE id = ?", [id], result);
      }
    });
  },
};

export default QnaAnswer;
