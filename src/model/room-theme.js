import connection from "../db/connection.js";

// RoomTheme object constructor
const RoomTheme = {
  selectAllRoomThemes: (result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT * FROM room_theme ", [], result);
      }
    });
  },

  selectRoomTheme: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT * FROM room_theme WHERE id = ? ", id, result);
      }
    });
  },

  insertRoomTheme: (newRoomTheme, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO room_theme SET ? ON DUPLICATE KEY UPDATE ? ", [newRoomTheme, newRoomTheme], result);
      }
    });
  },

  updateRoomTheme: (id, roomTheme, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE room_theme SET ? WHERE id = ?", [roomTheme, id], result);
      }
    });
  },

  deleteRoomTheme: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM room_theme WHERE id = ? ", [id], result);
      }
    });
  },

  deleteAllRoomTheme: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM room_theme WHERE place_id = ?  ", [place_id], result);
      }
    });
  },
};

export default RoomTheme;
