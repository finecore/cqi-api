import connection from '../db/connection.js';
import _ from 'lodash';

// RoomReserv object constructor
const RoomReserv = {
  selectAllRoomReservs: (
    place_id,
    filter,
    between,
    begin,
    end,
    order,
    result,
  ) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name room_name, c.name room_type_name, c.default_fee_stay, c.default_fee_rent, c.reserv_discount_fee_stay, c.reserv_discount_fee_rent,e.longitude,e.latitude
             FROM room_reserv a
             LEFT JOIN room b ON a.room_id = b.id
             LEFT JOIN room_type c ON a.room_type_id = c.id
            INNER JOIN  place e ON a.place_id = e.id
            WHERE a.place_id = ? AND
            ${filter} AND ${between} BETWEEN ? AND ? ORDER BY ${order} LIMIT 1000;`,
          [place_id, begin, end],
          result,
        );
      }
    });
  },

  selectRoomReservs: (filter, between, begin, end, order, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name room_name, c.name room_type_name, c.default_fee_stay, c.default_fee_rent, c.reserv_discount_fee_stay, c.reserv_discount_fee_rent,e.longitude,e.latitude
             FROM room_reserv a
             INNER JOIN room b ON a.room_id = b.id
             INNER JOIN room_type c ON a.room_type_id = c.id
             INNER JOIN place e ON a.place_id = e.id
            WHERE ${filter} AND ${between} BETWEEN ? AND ? ORDER BY ${order} LIMIT 1000;`,
          [begin, end],
          result,
        );
      }
    });
  },

  selectRoomsCanReservByTypeId: (
    filter,
    between,
    begin,
    end,
    order,
    result,
  ) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT b.*, c.name AS room_type_name,
                c.default_fee_stay, c.default_fee_rent,
                c.reserv_discount_fee_stay, c.reserv_discount_fee_rent
                ,e.longitude,e.latitude
          FROM room b
          LEFT JOIN room_reserv a ON b.id = a.room_id
          INNER JOIN room_type c ON b.room_type_id = c.id
          INNER JOIN  place e ON a.place_id = e.id
          WHERE a.room_id IS NULL AND ${filter} AND ${between} BETWEEN ? AND ?
          ORDER BY ${order}
          LIMIT 1000;`,
          [begin, end],
          result,
        );
      }
    });
  },
  selectRoomsCanNotReservByTypeId: (
    filter,
    between,
    begin,
    end,
    order,
    result,
  ) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT DISTINCT b.*, c.name AS room_type_name,
                  c.default_fee_stay, c.default_fee_rent,
                  c.reserv_discount_fee_stay, c.reserv_discount_fee_rent
                  ,e.longitude,e.latitude
            FROM room b
            INNER JOIN room_reserv a ON b.id = a.room_id
                AND a.check_out_exp > ?  -- 체크아웃 시간이 시작 날짜 이후
                AND a.check_in_exp < ?  -- 체크인 시간이 종료 날짜 이전
            INNER JOIN room_type c ON b.room_type_id = c.id
            INNER JOIN  place e ON a.place_id = e.id
            WHERE ${filter}
            ORDER BY ${order}
            LIMIT 1000;`,
          [begin, end],
          result,
        );
      }
    });
  },

  selectMemberRoomReservs: (
    member_id,
    filter,
    between,
    begin,
    end,
    order,
    result,
  ) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*,
                  b.name room_name,
                  c.name room_type_name, c.default_fee_stay, c.default_fee_rent, c.reserv_discount_fee_stay, c.reserv_discount_fee_rent,
                  d.name, d.birth,
                  e.name as place_name,  e.addr  as place_addr, e.addr_detl as place_addr_detl, e.tel as place_tel,e.longitude,e.latitude
             FROM room_reserv a
                  INNER JOIN room b ON a.room_id = b.id
                  INNER JOIN room_type c ON a.room_type_id = c.id
                  INNER JOIN member d ON a.member_id = d.id
                  INNER JOIN  place e ON a.place_id = e.id
            WHERE a.member_id = ? AND ${filter} AND ${between} BETWEEN ? AND ? ORDER BY ${order} LIMIT 1000;`,
          [member_id, begin, end],
          result,
        );
      }
    });
  },

  selectRoomIdRoomReservs: (
    room_id,
    filter,
    between,
    begin,
    end,
    order,
    result,
  ) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*,
                  b.name room_name,
                  c.name room_type_name, c.default_fee_stay, c.default_fee_rent, c.reserv_discount_fee_stay, c.reserv_discount_fee_rent,
                  d.name, d.birth,
                  e.name as place_name,  e.addr  as place_addr, e.addr_detl as place_addr_detl, e.tel as place_tel
             FROM room_reserv a
                  INNER JOIN room b ON a.room_id = b.id
                  INNER JOIN room_type c ON a.room_type_id = c.id
                  INNER JOIN member d ON a.member_id = d.id
                  INNER JOIN  place e ON a.place_id = e.id
            WHERE a.room_id = ? AND ${filter} AND ${between} BETWEEN ? AND ? ORDER BY ${order} LIMIT 1;`,
          [room_id, begin, end],
          result,
        );
      }
    });
  },

  selectItemIdReservs: (id, filter, between, begin, end, order, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*,
                  b.name room_name,
                  c.name room_type_name, c.default_fee_stay, c.default_fee_rent, c.reserv_discount_fee_stay, c.reserv_discount_fee_rent,
                  d.name, d.birth,
                  e.name as place_name,  e.addr  as place_addr, e.addr_detl as place_addr_detl, e.tel as place_tel,e.longitude,e.latitude
             FROM room_reserv a
                  INNER JOIN room b ON a.room_id = b.id
                  INNER JOIN room_type c ON a.room_type_id = c.id
                  INNER JOIN member d ON a.member_id = d.id
                  INNER JOIN  place e ON a.place_id = e.id
            WHERE a.id = ? AND ${filter} AND ${between} BETWEEN ? AND ? ORDER BY ${order} LIMIT 1;`,
          [id, begin, end],
          result,
        );
      }
    });
  },

  selectPlaceIdRoomReservs: (
    place_id,
    filter,
    between,
    begin,
    end,
    order,
    result,
  ) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*,
                  b.name room_name,
                  c.name room_type_name, c.default_fee_stay, c.default_fee_rent, c.reserv_discount_fee_stay, c.reserv_discount_fee_rent,
                  d.name, d.birth,
                  e.name as place_name,  e.addr  as place_addr, e.addr_detl as place_addr_detl, e.tel as place_tel,e.longitude,e.latitude
             FROM room_reserv a
                  INNER JOIN room b ON a.room_id = b.id
                  INNER JOIN room_type c ON a.room_type_id = c.id
                  INNER JOIN member d ON a.member_id = d.id
                  INNER JOIN  place e ON a.place_id = e.id
            WHERE a.place_id = ? AND ${filter} AND ${between} BETWEEN ? AND ? ORDER BY ${order} LIMIT 100;`,
          [place_id, begin, end],
          result,
        );
      }
    });
  },

  selectPlaceEnableReservs: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, c.name room_type_name, d.name room_type_name,e.longitude,e.latitude
             FROM room_reserv a
             LEFT JOIN room_sale b ON a.id = b.room_reserv_id
             LEFT JOIN room c ON a.room_id = c.id
             LEFT JOIN room_type d ON a.room_type_id = d.id
            INNER JOIN  place e ON a.place_id = e.id
            WHERE a.place_id = ?
              AND a.state = 'A'
              AND date_format(NOW(), '%Y-%m-%d') <= date_format(a.check_out_exp, '%Y-%m-%d')
         ORDER BY check_in_exp; `,
          [place_id],
          result,
        );
      }
    });
  },

  selectRoomReserv: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*,
                  b.name room_name,
                  c.name room_type_name, c.default_fee_stay, c.default_fee_rent, c.reserv_discount_fee_stay, c.reserv_discount_fee_rent,
                  d.name, d.birth,
                  e.name as place_name,  e.addr  as place_addr, e.addr_detl as place_addr_detl,e.longitude,e.latitude
             FROM room_reserv a
                  INNER JOIN room b ON a.room_id = b.id
                  INNER JOIN room_type c ON a.room_type_id = c.id
                  INNER JOIN member d ON a.member_id = d.id
                  INNER JOIN  place e ON a.place_id = e.id
            WHERE a.id = ?  `,
          [id],
          result,
        );
      }
    });
  },

  selectRoomReservNum: (num, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          'SELECT a.*, b.name room_type_name, c.name place_name, c.tel place_tel,c.longitude,c.latitude FROM room_reserv a LEFT JOIN room_type b ON a.room_type_id = b.id LEFT JOIN place c ON a.place_id = c.id WHERE reserv_num = ? ',
          [num],
          result,
        );
      }
    });
  },

  selectRoomReservByMmsNo: (num, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          'SELECT a.*, b.name room_type_name, c.name place_name, c.tel place_tel,c.longitude,c.latitude FROM room_reserv a LEFT JOIN room_type b ON a.room_type_id = b.id LEFT JOIN place c ON a.place_id = c.id WHERE mms_mo_num = ? ',
          [num],
          result,
        );
      }
    });
  },

  selectMemberReserv: (member_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*,
                  b.name room_name,
                  c.name room_type_name, c.default_fee_stay, c.default_fee_rent, c.reserv_discount_fee_stay, c.reserv_discount_fee_rent,
                  d.name, d.birth,
                  e.name as place_name,  e.addr  as place_addr, e.addr_detl as place_addr_detl,e.longitude,e.latitude
             FROM room_reserv a
                  INNER JOIN room b ON a.room_id = b.id
                  INNER JOIN room_type c ON a.room_type_id = c.id
                  INNER JOIN member d ON a.member_id = d.id
                  INNER JOIN  place e ON a.place_id = e.id
            WHERE a.member_id = ?  `,
          [member_id],
          result,
        );
      }
    });
  },

  selectRoomReservByReservNum: (num, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          'SELECT a.*, b.name room_type_name, c.name place_name, c.tel place_tel,c.longitude,c.latitude FROM room_reserv a LEFT JOIN room_type b ON a.room_type_id = b.id LEFT JOIN place c ON a.place_id = c.id WHERE reserv_num = ? ',
          [num],
          result,
        );
      }
    });
  },

  selectRoomReservNow: (room_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "SELECT * FROM room_reserv WHERE room_id = ? AND state = 'A' AND check_out is not null AND check_out is null ORDER BY id DESC LIMIT 1 ",
          [room_id, filter, begin, end, order],
          result,
        );
      }
    });
  },

  insertRoomReserv: (roomReserv, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          'INSERT INTO room_reserv SET ? ON DUPLICATE KEY UPDATE ? ',
          [roomReserv, roomReserv],
          result,
        );
      }
    });
  },

  updateRoomReserv: (id, roomReserv, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          'UPDATE room_reserv SET ? WHERE id = ?',
          [roomReserv, id],
          result,
        );
      }
    });
  },

  deleteRoomReservs: (room_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          'DELETE FROM room_reserv WHERE room_id = ? ',
          [room_id],
          result,
        );
      }
    });
  },

  deleteMemberReservs: (member_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          'DELETE FROM room_reserv WHERE member_id = ? ',
          [member_id],
          result,
        );
      }
    });
  },

  deleteRoomReserv: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql('DELETE FROM room_reserv WHERE id = ? ', [id], result);
      }
    });
  },
};

export default RoomReserv;
