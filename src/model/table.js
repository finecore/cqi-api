import connection from "../db/connection.js";

// Table object constructor
const Table = {
  selectComment: (table_name, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT TABLE_NAME, COLUMN_NAME, COLUMN_COMMENT
                          FROM INFORMATION_SCHEMA.COLUMNS
                          WHERE TABLE_SCHEMA = 'checkqin'
                          AND TABLE_NAME = '${table_name}' ;`,
          [],
          result
        );
      }
    });
  },
};

export default Table;
