import connection from "../db/connection.js";

// IscKeyBox object constructor
const IscKeyBox = {
  selectAllIscKeyBoxs: (serialno, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT T.*, r.card_barcode, (CASE WHEN r.card_barcode IS NULL THEN '' ELSE r.name END) name FROM
            (
            SELECT a.*,c.place_id
            FROM isc_key_box a  INNER JOIN device c ON a.serialno = c.serialno
                        WHERE  a.serialno = ? ORDER BY 'gid', 'did'
            ) T
          LEFT JOIN room r ON T.place_id = r.place_id AND r.card_barcode = T.barcode;`,
          [serialno],
          result
        );
      }
    });
  },

  selectIscKeyBoxCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count
             FROM isc_key_box a LEFT JOIN room b ON a.barcode = b.card_barcode INNER JOIN device c ON a.serialno = c.serialno
            WHERE ${filter} `,
          [],
          result
        );
      }
    });
  },

  selectIscKeyBoxs: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name, b.place_id
           FROM isc_key_box a LEFT JOIN room b ON a.barcode = b.card_barcode INNER JOIN device c ON a.serialno = c.serialno
           WHERE ${filter} ORDER BY ${order} ${desc} LIMIT ${limit} `,
          [],
          result
        );
      }
    });
  },

  selectIscKeyBoxsUse: (result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT serialno, gid
           FROM isc_key_box GROUP BY serialno, gid;`,
          [],
          result
        );
      }
    });
  },

  selectIscKeyBox: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT * FROM isc_key_box WHERE id = ? ", [id], result);
      }
    });
  },

  insertIscKeyBox: (newIscKeyBox, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO isc_key_box SET ? ON DUPLICATE KEY UPDATE ? ", [newIscKeyBox, newIscKeyBox], result);
      }
    });
  },

  updateIscKeyBox: (iscKeyBox, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO isc_key_box SET ? ON DUPLICATE KEY UPDATE ? ", [iscKeyBox, iscKeyBox], result);
      }
    });
  },

  deleteIscKeyBox: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM isc_key_box WHERE id = ?", [id], result);
      }
    });
  },

  deleteAllIscKeyBoxs: (serialno, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM isc_key_box WHERE serialno = ?", [serialno], result);
      }
    });
  },
};

export default IscKeyBox;
