import connection from "../db/connection.js";

// MemberPointLog object constructor
const MemberPointLog = {
  selectAllMemberPointLogs: (place_id, filter, between, begin, end, order, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, c.name as place_name, d.id as member_id, d.name as member_name
             FROM member_point_log a
                  LEFT JOIN member_point b ON a.member_point_id = b.id
                  INNER JOIN place c ON a.place_id = c.id
                  LEFT JOIN cqi.member d ON a.place_id = d.place_id AND a.member_id = d.id
            WHERE a.change_point != 0 AND a.place_id = ? AND ${filter} AND ${between} BETWEEN ? AND ?
            ORDER BY ${order} `,
          [place_id, begin, end],
          result
        );
      }
    });
  },

  selectMemberPointLogs: (member_id, filter, between, begin, end, order, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, c.name as place_name, d.name as member_name
             FROM member_point_log a
                  LEFT JOIN member_point b ON a.member_point_id = b.id
                  INNER JOIN place c ON a.place_id = c.id
                  INNER JOIN member d ON a.member_id = d.id
            WHERE a.change_point != 0 AND a.member_id = ?
              AND ${filter}
              AND ${between} BETWEEN ? AND ?
            ORDER BY ${order}
            LIMIT 1000;`,
          [member_id, begin, end],
          result
        );
      }
    });
  },

  selectMemberPointLogsByPlace: (place_id, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, c.name as place_name, d.name as member_name
             FROM member_point_log a
                  LEFT JOIN member_point b ON a.member_point_id = b.id
                  INNER JOIN place c ON a.place_id = c.id
                  LEFT JOIN cqi.member d ON a.member_id = d.id
            WHERE a.change_point != 0 AND a.place_id = ?
            ORDER BY a.id DESC LIMIT ?;`,
          [place_id, limit],
          result
        );
      }
    });
  },

  selectMemberPointDayLogs: (place_id, day, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, c.name as place_name, d.name as member_name
             FROM member_point_log a
                  LEFT JOIN member_point b ON a.member_point_id = b.id
                  INNER JOIN place c ON a.place_id = c.id
                  LEFT JOIN cqi.member d ON a.member_id = d.id
            WHERE a.change_point != 0 AND a.place_id = ? AND a.reg_date >= DATE_ADD(NOW(), INTERVAL ? DAY)
            ORDER BY a.id DESC ;`,
          [place_id, day],
          result
        );
      }
    });
  },

  selectMemberPointLastLogs: (place_id, last_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, c.name as place_name, d.name as member_name
             FROM member_point_log a
                  LEFT JOIN member_point b ON a.member_point_id = b.id
                  INNER JOIN place c ON a.place_id = c.id
                  LEFT JOIN cqi.member d ON a.member_id = d.id
            WHERE a.change_point != 0 AND a.place_id = ?
              AND a.id > ?
            ORDER BY a.id DESC LIMIT 300 `,
          [place_id, last_id],
          result
        );
      }
    });
  },

  selectMemberPointLog: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT * FROM member_point_log WHERE id = ? ", id, result);
      }
    });
  },

  insertMemberPointLog: (member_point_log, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO member_point_log SET ? ", [member_point_log, member_point_log], result);
      }
    });
  },

  updateMemberPointLog: (id, member_point_log, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE member_point_log SET ? WHERE id = ?", [member_point_log, id], result);
      }
    });
  },

  deleteMemberPointAfterKeepDay: (place_id, date, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `DELETE a FROM member_point_log a INNER JOIN member_point b ON a.member_point_id = b.id INNER JOIN place c ON a.place_id = c.id
            WHERE a.place_id = ? AND a.reg_date < STR_TO_DATE(?, '%Y-%m-%d %H:%i'); `,
          [place_id, date],
          result
        );
      }
    });
  },

  deleteMemberPointLog: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM member_point_log WHERE id = ? ", [id], result);
      }
    });
  },
};

export default MemberPointLog;
