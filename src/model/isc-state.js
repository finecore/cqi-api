import connection from "../db/connection.js";
import IscStateLog from "./isc-state-log.js";
import _ from "lodash";

// IscState object constructor
const IscState = {
  selectIscStateCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count
             FROM isc_state a INNER JOIN device b ON a.serialno = b.serialno
                              INNER JOIN place c ON b.place_id = c.id
                              LEFT JOIN user_place d ON b.place_id = d.place_id
                              LEFT JOIN user u ON d.user_id = u.id
           WHERE ${filter} `,
          [],
          result
        );
      }
    });
  },

  selectIscStates: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.id as isc_id, b.place_id, b.name as isc_name, c.name as place_name
           FROM isc_state a INNER JOIN device b ON a.serialno = b.serialno
                            INNER JOIN place c ON b.place_id = c.id
                            LEFT JOIN user_place d ON b.place_id = d.place_id
                            LEFT JOIN user u ON d.user_id = u.id
           WHERE ${filter} ORDER BY ${order} ${desc} LIMIT ${limit} `,
          [],
          result
        );
      }
    });
  },

  selectPlaceIscStates: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.id as isc_id, b.place_id, b.name as isc_name, c.name as place_name
           FROM isc_state a INNER JOIN device b ON a.serialno = b.serialno
                            INNER JOIN place c ON b.place_id = c.id
           WHERE b.place_id = ? AND b.type = '02'`,
          [place_id],
          result
        );
      }
    });
  },

  selectIscState: (serialno, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.id as isc_id, b.place_id, b.name as isc_name, c.name as place_name
           FROM isc_state a INNER JOIN device b ON a.serialno = b.serialno INNER JOIN place c ON b.place_id = c.id WHERE a.serialno = ? `,
          serialno,
          result
        );
      }
    });
  },

  selectIscStateOnly: (serialno, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`SELECT * FROM isc_state WHERE serialno = ? `, serialno, result);
      }
    });
  },

  insertIscState: (newIscState, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO isc_state SET ? ON DUPLICATE KEY UPDATE ? ", [newIscState, newIscState], (err, info) => {
          result(err, info);
        });
      }
    });
  },

  updateIscState: (iscState, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        const { serialno, channel } = iscState;

        // 현재 상태 조회.
        IscState.selectIscState(serialno, (err, row) => {
          if (!err) {
            let _isc_state = row[0] || {};

            _connection.sql("INSERT INTO isc_state SET ? ON DUPLICATE KEY UPDATE ? ", [iscState, iscState], (err, info) => {
              result(err, info);

              if (!err) {
                let changed = {};

                // 변한 값 체크.
                _.map(iscState, (state, k) => {
                  // console.log("- is changed ", k, state, "/" + _isc_state[k]);
                  if (k !== "mod_date" && k !== "reg_date" && k !== "channel" && k !== "place_id") {
                    if (_isc_state[k] !== undefined && String(_isc_state[k]) !== String(state)) {
                      changed[k] = state;
                    }
                  }
                });

                console.log("- changed ", changed, _isc_state);

                // 변한 값이 없다면 로그 저장 하지 않음
                if (!_.isEmpty(changed) && _isc_state.place_id) {
                  IscState.add_isc_state_log(serialno, channel, _isc_state.place_id, changed);
                }
              }
            });
          }
        });
      }
    });
  },

  deleteIscState: (serialno, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM isc_state WHERE serialno = ? ", [serialno], result);
      }
    });
  },

  add_isc_state_log: (serialno, channel, place_id, isc_state) => {
    if (serialno) {
      const isc_state_log = {
        serialno,
        channel,
        data: JSON.stringify(isc_state),
        reg_date: new Date(),
        place_id,
      };

      console.log("---> add_isc_state_log", isc_state_log);

      IscStateLog.insertIscStateLog(isc_state_log, (err, info) => {
        if (err) console.log("---> IscStateLog failure inserted ", err);
        else {
          console.log("---> IscStateLog successfully inserted ");
        }
      });
    } else {
      console.log("---> IscStateLog failure inserted no isc_state_id ");
    }
  },
};

export default IscState;
