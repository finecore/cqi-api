import connection from "../db/connection.js";

// As object constructor
const As = {
  selectAsCount: (filter, between, begin, end, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count
             FROM \`as\` a
            WHERE ${filter} AND ${between} BETWEEN ? AND ? `,
          [begin, end],
          result
        );
      }
    });
  },

  selectAses: (filter, between, begin, end, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name place_name
             FROM \`as\` a INNER JOIN place b ON a.place_id = b.id
            WHERE ${filter} AND ${between} BETWEEN ? AND ?
            ORDER BY ${order} ${desc} LIMIT ${limit} `,
          [begin, end],
          result
        );
      }
    });
  },

  selectAs: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name place_name
             FROM \`as\` a  INNER JOIN place b ON a.place_id = b.id
            WHERE a.id = ? `,
          [id],
          result
        );
      }
    });
  },

  insertAs: (newAs, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO `as` SET ? ", [newAs], result);
      }
    });
  },

  updateAs: (id, as, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE `as` SET ? WHERE id = ?", [as, id], result);
      }
    });
  },

  deleteAs: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM `as` WHERE id = ?", [id], result);
      }
    });
  },
};

export default As;
