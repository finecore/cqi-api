import connection from "../db/connection.js";

// PlaceSubscribe object constructor
const PlaceSubscribe = {
  selectPlaceSubscribeCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count
             FROM place_subscribe a
            INNER JOIN subscribe b ON a.subscribe_id = b.id
            WHERE ${filter} `,
          [],
          result
        );
      }
    });
  },

  selectPlaceSubscribes: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name, b.pay_yn, b.pay_type, b.code, b.discription, b.default_fee, b.license_yn, c.name place_name, d.ota_place_name, d.ota_place_name_1, d.ota_place_name_2, d.ota_place_name_3
             FROM place_subscribe a
            INNER JOIN subscribe b ON a.subscribe_id = b.id
            INNER JOIN place c ON a.place_id = c.id
            INNER JOIN preference d ON a.place_id = d.place_id
            WHERE ${filter} ORDER BY ${order} ${desc} LIMIT ${limit} `,
          [],
          result
        );
      }
    });
  },

  selectPlaceUsedSubscribes: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name, b.pay_yn, b.pay_type, b.code, b.discription, b.default_fee, b.license_yn, c.name place_name, d.ota_place_name, d.ota_place_name_1, d.ota_place_name_2, d.ota_place_name_3
             FROM place_subscribe a
            INNER JOIN subscribe b ON a.subscribe_id = b.id
            INNER JOIN place c ON a.place_id = c.id
            INNER JOIN preference d ON a.place_id = d.place_id
            WHERE a.place_id = ?
              AND a.valid_yn > 0 `,
          [place_id],
          result
        );
      }
    });
  },

  selectPlaceSubscribesPrevNext: (id, place_id, filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `(
            SELECT 'DOWN' as tag
                  , a.id
                  , a.reg_date
                  , b.name as title
                  , b.discription as content
               FROM place_subscribe a
                    INNER JOIN subscribe b ON a.subscribe_id = b.id
                    INNER JOIN place c ON a.place_id = c.id
                    INNER JOIN preference d ON a.place_id = d.place_id
              WHERE a.id < ${id}
                AND a.place_Id = ${place_id}
                AND ${filter}
              ORDER BY a.id DESC LIMIT 1
            )
            union
            (
             SELECT 'UP' as tag
                  , a.id
                  , a.reg_date
                  , b.name as title
                  , b.discription as content
               FROM place_subscribe a
                    INNER JOIN subscribe b ON a.subscribe_id = b.id
                    INNER JOIN place c ON a.place_id = c.id
                    INNER JOIN preference d ON a.place_id = d.place_id
              WHERE a.id > ${id}
                AND a.place_Id = ${place_id}
                AND ${filter}
              ORDER BY a.id ASC LIMIT 1
          )`,
          [],
          result
        );
      }
    });
  },

  selectPlaceSubscribe: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name, b.pay_yn, b.pay_type, b.code, b.discription, b.default_fee, b.license_yn, c.name place_name
             FROM place_subscribe a INNER JOIN subscribe b ON a.subscribe_id = b.id INNER JOIN place c ON a.place_id = c.id
            WHERE a.id = ? `,
          [id],
          result
        );
      }
    });
  },

  insertPlaceSubscribe: (newPlaceSubscribe, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO place_subscribe SET ? ", [newPlaceSubscribe], result);
      }
    });
  },

  updatePlaceSubscribe: (id, place_subscribe, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE place_subscribe SET ? WHERE id = ?", [place_subscribe, id], result);
      }
    });
  },

  deletePlaceSubscribe: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM place_subscribe WHERE id = ?", [id], result);
      }
    });
  },
};

export default PlaceSubscribe;
