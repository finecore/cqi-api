import connection from "../db/connection.js";

// Member object constructor
const Member = {
  selectMemberCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count
             FROM member a
            WHERE ${filter} `,
          [],
          result
        );
      }
    });
  },

  selectMembers: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*
           FROM member a
           WHERE ${filter}
           ORDER BY ${order} ${desc}
           LIMIT ${limit} `,
          [],
          result
        );
      }
    });
  },

  selectPlaceMemberByPhone: (id, phone, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*
           FROM member a
            WHERE a.id = ? AND a.phone = ?`,
          [id, phone],
          result
        );
      }
    });
  },

  selectPlaceMembers: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*
           FROM member a
           WHERE a.id = ? `,
          [id],
          result
        );
      }
    });
  },

  selectMember: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*
           FROM member a
            WHERE a.id = ?  `,
          [id],
          result
        );
      }
    });
  },

  selectMemberById: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*
           FROM member a
            WHERE a.id = ? `,
          [id],
          result
        );
      }
    });
  },

  insertMember: (member, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO member SET ? ON DUPLICATE KEY UPDATE ? ", [member, member], result);
      }
    });
  },

  updateMember: (id, member, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE member SET ? WHERE id = ?", [member, id], result);
      }
    });
  },

  updatePlaceMemberByPhone: (id, phone, member, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE member SET ? WHERE id = ? AND phone = ?", [member, id, phone], result);
      }
    });
  },

  updateLastLoginDate: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE member SET last_login_date = NOW() WHERE id = ?", [id], result);
      }
    });
  },

  deleteMember: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM member WHERE id = ?", [id], result);
      }
    });
  },

  deletePlaceMembers: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM member WHERE id = ?", [id], result);
      }
    });
  },
};

export default Member;
