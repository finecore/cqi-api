import connection from "../db/connection.js";

// PlaceImgs object constructor
const PlaceImgs = {
  selectAllPlaceImgss: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT a.*, b.level FROM place_imgs a INNER JOIN user b ON a.user_id = b.id WHERE a.place_id = ? ", [place_id], result);
      }
    });
  },

  selectPlaceImgs: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT a.*, b.level FROM place_imgs a INNER JOIN user b ON a.user_id = b.id WHERE a.id = ? ", id, result);
      }
    });
  },

  insertPlaceImgs: (newPlaceImgs, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO place_imgs SET ? ON DUPLICATE KEY UPDATE ? ", [newPlaceImgs, newPlaceImgs], result);
      }
    });
  },

  updatePlaceImgs: (id, placeImgs, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE place_imgs SET ? WHERE id = ?", [placeImgs, id], result);
      }
    });
  },

  deletePlaceImgs: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM place_imgs WHERE id = ? ", [id], result);
      }
    });
  },

  deleteAllPlaceImgs: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM place_imgs WHERE place_id = ?  ", [place_id], result);
      }
    });
  },
};

export default PlaceImgs;
