import connection from "../db/connection.js";

// Room object constructor
const Room = {
  selectAllRooms: (result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT DISTINCT
                a.*,
                a.id as room_id
                f.name AS place_name,
                f.addr AS place_addr,
                f.addr_detl AS place_addr_detl,
                f.longitude,
                f.latitude,
                b.name as type_name,
                b.disc as type_disc,
                b.default_fee_stay,
                b.default_fee_rent,
                c.*,
                c.id as room_state_id
                d.state as room_state,
                e.id as sale_id,
                e.state as sale_state,
                e.stay_type,
                e.check_in_exp,
                e.check_out_exp,
                e.check_in,
                e.check_out
             FROM room a
             LEFT JOIN place f ON a.place_id = f.id
             LEFT JOIN room_type b ON a.room_type_id = b.id
             LEFT JOIN room_state c ON a.id = c.room_id
             LEFT JOIN room_sale d ON a.id = d.room_id
             LEFT JOIN room_sale e ON a.id = e.room_id AND e.state = 'A';`,
          [],
          result
        );
      }
    });
  },

  selectPlaceRooms: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*,
                a.id as room_id,
                f.name AS place_name,
                f.addr AS place_addr,
                f.addr_detl AS place_addr_detl,
                f.longitude,
                f.latitude,
                b.name AS type_name,
                b.disc AS type_disc,
                b.default_fee_stay,
                b.default_fee_rent,
                c.room_id, c.room_sale_id, c.channel, c.dnd, c.clean,
                c.clean_change_time, c.fire, c.emerg, c.sale, c.sale_change_time,
                c.key, c.key_change_time, c.outing, c.signal, c.theft, c.emlock,
                c.door, c.car_call, c.airrcon_relay, c.main_relay, c.use_auto_power_off,
                c.bath_on_delay, c.num_light, c.chb_led, c.car_ss1, c.car_ss2, c.car_ss3,
                c.shutter, c.toll_gate, c.entrance, c.air_sensor_no, c.air_set_temp,
                c.air_set_min, c.air_set_max, c.air_temp, c.air_preheat, c.air_heat_type,
                c.air_fan, c.air_power, c.air_power_type, c.main_power_type, c.light,
                c.dimmer, c.curtain, c.boiler_no, c.boiler_set_temp, c.boiler_temp,
                c.boiler_type, c.boiler_heating, c.boiler_thermo, c.boiler_power,
                c.notice, c.notice_opacity, c.notice_display, c.isc_sale_1, c.isc_sale_2,
                c.isc_sale_3, c.temp_key_1, c.temp_key_2, c.temp_key_3, c.temp_key_4,
                c.temp_key_5, c.inspect, c.qr_key_yn, c.keyless, c.reg_date, c.mod_date,
                d.channel, d.user_id, d.state,
                e.id as sale_id,
                e.state as sale_state,
                e.stay_type,
                e.check_in_exp,
                e.check_out_exp,
                e.check_in,
                e.check_out
          FROM room a
          LEFT JOIN place f ON a.place_id = f.id
          LEFT JOIN room_type b ON a.room_type_id = b.id
          LEFT JOIN room_state c ON a.id = c.room_id
          LEFT JOIN room_interrupt d ON a.id = d.room_id
          LEFT JOIN room_sale e ON a.id = e.room_id AND e.state = 'A'
          WHERE a.place_id = ?
          ORDER BY LENGTH(a.name), a.name`,
          [place_id],
          result
        );
      }
    });
  },

  selectRoomCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count
             FROM room a LEFT JOIN room_type b ON a.room_type_id = b.id LEFT JOIN room_state c ON a.id = c.room_id
            WHERE ${filter}`,
          [],
          result
        );
      }
    });
  },

  selectRooms: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*,
                f.name AS place_name,
                f.addr AS place_addr,
                f.addr_detl AS place_addr_detl,
                f.longitude,
                f.latitude,
                b.name AS type_name,
                b.disc AS type_disc,
                c.room_id, c.room_sale_id, c.channel, c.dnd, c.clean,
                c.clean_change_time, c.fire, c.emerg, c.sale, c.sale_change_time,
                c.key, c.key_change_time, c.outing, c.signal, c.theft, c.emlock,
                c.door, c.car_call, c.airrcon_relay, c.main_relay, c.use_auto_power_off,
                c.bath_on_delay, c.num_light, c.chb_led, c.car_ss1, c.car_ss2, c.car_ss3,
                c.shutter, c.toll_gate, c.entrance, c.air_sensor_no, c.air_set_temp,
                c.air_set_min, c.air_set_max, c.air_temp, c.air_preheat, c.air_heat_type,
                c.air_fan, c.air_power, c.air_power_type, c.main_power_type, c.light,
                c.dimmer, c.curtain, c.boiler_no, c.boiler_set_temp, c.boiler_temp,
                c.boiler_type, c.boiler_heating, c.boiler_thermo, c.boiler_power,
                c.notice, c.notice_opacity, c.notice_display, c.isc_sale_1, c.isc_sale_2,
                c.isc_sale_3, c.temp_key_1, c.temp_key_2, c.temp_key_3, c.temp_key_4,
                c.temp_key_5, c.inspect, c.qr_key_yn, c.keyless, c.reg_date, c.mod_date,
                d.channel, d.user_id, d.state,
                e.id as sale_id,
                e.state as sale_state,
                e.stay_type,
                e.check_in_exp,
                e.check_out_exp,
                e.check_in,
                e.check_out
          FROM room a
          LEFT JOIN place f ON a.place_id = f.id
          LEFT JOIN room_type b ON a.room_type_id = b.id
          LEFT JOIN room_state c ON a.id = c.room_id
          LEFT JOIN room_interrupt d ON a.id = d.room_id
          LEFT JOIN room_sale e ON a.id = e.room_id AND e.state = 'A'
          WHERE ${filter}
          ORDER BY ${order} ${desc}
          LIMIT ${limit}`,
          [],
          result
        );
      }
    });
  },

  selectRoomLikeCount: (filter, value1, value2, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count
             FROM room a LEFT JOIN room_type b ON a.room_type_id = b.id LEFT JOIN room_state c ON a.id = c.room_id
            WHERE ${filter} AND a.name LIKE CONCAT('%', ${value1}, '%')  ${value2 ? value2 : ""} ; `,
          [],
          result
        );
      }
    });
  },

  selectRoomsLike: (filter, value1, value2, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*,
                  b.name as type_name, b.disc as type_disc,
                  c.*
             FROM room a LEFT JOIN room_type b ON a.room_type_id = b.id LEFT JOIN room_state c ON a.id = c.room_id
             WHERE ${filter} AND a.name LIKE CONCAT('%', ${value1}, '%') ${value2 ? value2 : ""}
             ORDER BY ${order} ${desc} LIMIT ${limit}`,
          [],
          result
        );
      }
    });
  },

  selectRoomsByTypeId: (room_type_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name as type_name, b.disc as type_disc
          FROM room a
          LEFT JOIN room_type b ON a.room_type_id = b.id
          WHERE a.reserv_yn = 1 AND a.room_type_id = ?
          ORDER BY a.name asc; `,
          [room_type_id],
          result
        );
      }
    });
  },

  selectReservEnableRooms: (place_id, filter, order, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "SELECT a.*, b.name as type_name, b.disc as type_disc, c.sale, d.state, d.check_in, d.check_out, d.check_in_exp, d.check_out_exp " +
            "  FROM room a LEFT JOIN room_type b ON a.room_type_id = b.id LEFT JOIN room_state c ON a.id = c.room_id LEFT JOIN room_sale d ON c.room_sale_id = d.id " +
            " WHERE a.reserv_yn = 1 AND a.place_id = ? AND " +
            filter +
            " ORDER BY " +
            order,
          [place_id],
          result
        );
      }
    });
  },

  selectRoom: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*,
                a.id as room_id,
                f.name AS place_name,
                f.addr AS place_addr,
                f.addr_detl AS place_addr_detl,
                f.longitude,
                f.latitude,
                b.name AS type_name,
                b.disc AS type_disc,
                b.default_fee_stay,
                b.default_fee_rent,
                c.room_id, c.room_sale_id, c.channel, c.dnd, c.clean,
                c.clean_change_time, c.fire, c.emerg, c.sale, c.sale_change_time,
                c.key, c.key_change_time, c.outing, c.signal, c.theft, c.emlock,
                c.door, c.car_call, c.airrcon_relay, c.main_relay, c.use_auto_power_off,
                c.bath_on_delay, c.num_light, c.chb_led, c.car_ss1, c.car_ss2, c.car_ss3,
                c.shutter, c.toll_gate, c.entrance, c.air_sensor_no, c.air_set_temp,
                c.air_set_min, c.air_set_max, c.air_temp, c.air_preheat, c.air_heat_type,
                c.air_fan, c.air_power, c.air_power_type, c.main_power_type, c.light,
                c.dimmer, c.curtain, c.boiler_no, c.boiler_set_temp, c.boiler_temp,
                c.boiler_type, c.boiler_heating, c.boiler_thermo, c.boiler_power,
                c.notice, c.notice_opacity, c.notice_display, c.isc_sale_1, c.isc_sale_2,
                c.isc_sale_3, c.temp_key_1, c.temp_key_2, c.temp_key_3, c.temp_key_4,
                c.temp_key_5, c.inspect, c.qr_key_yn, c.keyless, c.reg_date, c.mod_date,
                d.channel, d.user_id, d.state,
                e.id as sale_id,
                e.state as sale_state,
                e.stay_type,
                e.check_in_exp,
                e.check_out_exp,
                e.check_in,
                e.check_out
          FROM room a
          LEFT JOIN place f ON a.place_id = f.id
          LEFT JOIN room_type b ON a.room_type_id = b.id
          LEFT JOIN room_state c ON a.id = c.room_id
          LEFT JOIN room_interrupt d ON a.id = d.room_id
          LEFT JOIN room_sale e ON a.id = e.room_id AND e.state = 'A'
          WHERE a.id = ?
          ORDER BY e.id DESC
          LIMIT 1;`,
          id,
          result
        );
      }
    });
  },

  selectRoomByName: (place_id, name, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name as type_name, b.disc as type_disc, c.sale, c.isc_sale_1, c.isc_sale_2, c.isc_sale_3, main_power_type
             FROM room a LEFT JOIN room_type b ON a.room_type_id = b.id LEFT JOIN room_state c ON a.id = c.room_id
            WHERE a.place_id = ? AND CAST(a.name AS UNSIGNED) = CAST(? AS UNSIGNED) ;`,
          [place_id, name],
          result
        );
      }
    });
  },

  insertRoom: (newRoom, result, conn) => {
    if (conn) {
      conn.sql("INSERT INTO room SET ? ON DUPLICATE KEY UPDATE ? ", [newRoom, newRoom], result, true);
    } else {
      connection((_err, _connection) => {
        if (_err) result(_err, null);
        else {
          _connection.sql("INSERT INTO room SET ? ON DUPLICATE KEY UPDATE ? ", [newRoom, newRoom], result);
        }
      });
    }
  },

  updateRoom: (id, room, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE room SET ? WHERE id = ?", [room, id], result);
      }
    });
  },

  deleteRoom: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM room WHERE id = ?", [id], result);
      }
    });
  },

  deletePlaceRooms: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM room WHERE place_id = ?", [place_id], result);
      }
    });
  },
};

export default Room;
