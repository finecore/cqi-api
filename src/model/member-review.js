import connection from "../db/connection.js";

// MemberReview object constructor
const MemberReview = {
  selectMemberReviewsCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count
           FROM member_review a
              INNER JOIN room_reserv d ON a.reserv_id = d.id
              INNER JOIN place b ON d.place_id = b.id
              INNER JOIN member c ON d.member_id = c.id
              INNER JOIN room e ON d.room_id = e.id
              INNER JOIN room_type f ON d.room_type_id = f.id
          WHERE ${filter} `,
          [],
          result
        );
      }
    });
  },

  selectMemberReviews: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, d.member_id, d.place_id, b.name as place_name ,c.name as member_name, b.addr as place_addr, c.nic_name as nic_name, e.name as room_name, f.name as room_type_name
           FROM member_review a
              INNER JOIN room_reserv d ON a.reserv_id = d.id
              INNER JOIN place b ON d.place_id = b.id
              INNER JOIN member c ON d.member_id = c.id
              INNER JOIN room e ON d.room_id = e.id
              INNER JOIN room_type f ON d.room_type_id = f.id
           WHERE ${filter}
           ORDER BY ${order} ${desc}
           LIMIT ${limit} `,
          [],
          result
        );
      }
    });
  },

  selectByMemberReviews: (member_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, d.member_id, d.place_id, b.name as place_name ,c.name as member_name, b.addr as place_addr, c.nic_name as nic_name, e.name as room_name, f.name as room_type_name
           FROM member_review a
              INNER JOIN room_reserv d ON a.reserv_id = d.id
              INNER JOIN place b ON d.place_id = b.id
              INNER JOIN member c ON d.member_id = c.id
              INNER JOIN room e ON d.room_id = e.id
              INNER JOIN room_type f ON d.room_type_id = f.id
           WHERE d.member_id = ?
           ORDER BY a.reg_date desc;`,
          [member_id],
          result
        );
      }
    });
  },

  selectByPlaceReviews: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, d.member_id, d.place_id, b.name as place_name ,c.name as member_name, b.addr as place_addr, c.nic_name as nic_name, e.name as room_name, f.name as room_type_name
           FROM member_review a
              INNER JOIN room_reserv d ON a.reserv_id = d.id
              INNER JOIN place b ON d.place_id = b.id
              INNER JOIN member c ON d.member_id = c.id
              INNER JOIN room e ON d.room_id = e.id
              INNER JOIN room_type f ON d.room_type_id = f.id
           WHERE d.place_id = ?
           ORDER BY a.reg_date desc;`,
          [place_id],
          result
        );
      }
    });
  },

  selectMemberReview: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, d.member_id, d.place_id, b.name as place_name ,c.name as member_name, b.addr as place_addr, c.nic_name as nic_name, e.name as room_name, f.name as room_type_name
           FROM member_review a
              INNER JOIN room_reserv d ON a.reserv_id = d.id
              INNER JOIN place b ON d.place_id = b.id
              INNER JOIN member c ON d.member_id = c.id
              INNER JOIN room e ON d.room_id = e.id
              INNER JOIN room_type f ON d.room_type_id = f.id
            WHERE a.id = ?
            ORDER BY a.reg_date desc;`,
          [id],
          result
        );
      }
    });
  },

  insertMemberReview: (member_review, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO member_review SET ?   ", [member_review], result);
      }
    });
  },

  updateMemberReview: (id, member_review, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE member_review SET ? WHERE id = ?", [member_review, id], result);
      }
    });
  },

  deleteMemberReview: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM member_review WHERE id = ?", [id], result);
      }
    });
  },

  deleteAllMemberReview: (member_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `DELETE a
           FROM member_review a
           INNER JOIN room_reserv b ON a.reserv_id = b.id
           WHERE b.member_id =  ? ;`,
          [member_id],
          result
        );
      }
    });
  },
};

export default MemberReview;
