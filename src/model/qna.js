import connection from "../db/connection.js";

// Qna object constructor
const Qna = {
  // QNA 목록 카운트 조회
  selectQnaCount: (filter, between, begin, end, all, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT COUNT(*) count
             FROM qna a
            WHERE ${filter} AND ${between} BETWEEN ? AND ? ` + (all === "0" ? ` AND NOW() BETWEEN a.begin_date AND a.end_date ` : ""),
          [begin, end],
          result
        );
      }
    });
  },

  // QNA 목록 조회 (작성자 이름 포함)
  selectQnas: (filter, between, begin, end, order, desc, limit, all, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, m.name AS member_name
             FROM qna a
             LEFT JOIN member m ON a.member_id = m.id
            WHERE ${filter} AND ${between} BETWEEN ? AND ? ` +
            (all === "0" ? ` AND NOW() BETWEEN a.begin_date AND a.end_date ` : "") +
            ` ORDER BY ${order} ${desc} LIMIT ${limit}`,
          [begin, end],
          result
        );
      }
    });
  },

  // QNA 이전/다음 항목 조회
  selectQnasPrevNext: (id, filter, between, begin, end, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `(
            SELECT 'DOWN' AS tag, a.id, a.reg_date, a.title, a.content, m.name AS member_name
              FROM qna a
             LEFT JOIN member m ON a.member_id = m.id
             WHERE a.id < ${id}
               AND ${filter}
               AND ${between} BETWEEN ? AND ?
             ORDER BY a.id DESC LIMIT 1
            )
            UNION
            (
            SELECT 'UP' AS tag, a.id, a.reg_date, a.title, a.content, m.name AS member_name
              FROM qna a
             LEFT JOIN member m ON a.member_id = m.id
             WHERE a.id > ${id}
               AND ${filter}
               AND ${between} BETWEEN ? AND ?
             ORDER BY a.id ASC LIMIT 1
          )`,
          [begin, end, begin, end],
          result
        );
      }
    });
  },

  // 특정 QNA 조회 (작성자 이름 포함)
  selectQna: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, m.name AS member_name
             FROM qna a
             LEFT JOIN member m ON a.member_id = m.id
            WHERE a.id = ?`,
          [id],
          result
        );
      }
    });
  },

  selectQnaWithAnswersCountCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT COUNT(*) count
             FROM qna q
            WHERE ${filter} `,
          [],
          result
        );
      }
    });
  },

  /// 특정 QNA 목록 조회 (답변 개수 포함)
  selectQnaWithAnswersCount: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT q.id ,
                q.title,
                q.content,
                q.reg_date,
                q.mod_date,
                q.status,
                q.member_id,
                m.name AS member_name,
                COUNT(qa.id) AS answer_count
          FROM qna q
          LEFT JOIN member m ON q.member_id = m.id
          LEFT JOIN qna_answers qa ON q.id = qa.qna_id
         WHERE ${filter}
         GROUP BY q.id
         ORDER BY ${order} ${desc}
         LIMIT ${limit} ;`,
          [],
          result
        );
      }
    });
  },

  // 특정 QNA + 답변 목록 조회 (질문 작성자 및 답변 작성자 이름 포함)
  selectQnaWithAnswers: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT q.id ,
                  q.title,
                  q.content,
                  q.reg_date,
                  q.mod_date,
                  q.status,
                  q.member_id,
                  m.name AS member_name,
                  qa.id AS answer_id,
                  qa.answer_content,
                  qa.answer_date,
                  qa.user_id,
                  u.name AS user_name
             FROM qna q
             LEFT JOIN member m ON q.member_id = m.id
             LEFT JOIN qna_answers qa ON q.id = qa.qna_id
             LEFT JOIN user u ON qa.user_id = u.id
            WHERE q.id = ?`,
          [id],
          result
        );
      }
    });
  },

  // 새로운 QNA 추가
  insertQna: (newQna, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO qna SET ? ", [newQna], result);
      }
    });
  },

  // 기존 QNA 수정
  updateQna: (id, qna, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE qna SET ? WHERE id = ?", [qna, id], result);
      }
    });
  },

  // 특정 QNA 삭제
  deleteQna: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM qna WHERE id = ?", [id], result);
      }
    });
  },
};

export default Qna;
