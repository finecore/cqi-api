import connection from "../db/connection.js";
import _ from "lodash";

// RoomSaleReport object constructor
const RoomSaleReportDay = {
  selectRoomSaleReportDays: (room_id, filter, between, begin, end, order, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT * FROM room_sale_report_day
          WHERE room_id = ${room_id} AND ${filter} AND ${between} BETWEEN ${begin} AND ${end}
          ORDER BY ${order}`,
          [],
          result
        );
      }
    });
  },

  selectRoomSaleReportDayLast: (result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT * FROM room_sale_report_day
          ORDER BY year DESC, month DESC, day DESC LIMIT 1;`,
          [],
          result
        );
      }
    });
  },

  selectRoomSaleReportDay: (room_id, year, month, day, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT * FROM room_sale_report_day
            WHERE room_id = ? AND DATE_FORMAT(CONCAT(year, '-', month, '-', day), '%Y%m%d') = DATE_FORMAT(CONCAT(?, '-', ?, '-', ?), '%Y%m%d');`,
          [room_id, year, month, day],
          result
        );
      }
    });
  },

  selectRoomSaleReportDaySum: (sdate, edate, order, filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT year, month, day,
                  SUM(ieg_long_cnt) ieg_long_cnt, SUM(ieg_stay_cnt) ieg_stay_cnt, SUM(ieg_rent_cnt) ieg_rent_cnt,
                  SUM(isg_long_cnt) isg_long_cnt, SUM(isg_stay_cnt) isg_stay_cnt, SUM(isg_rent_cnt) isg_rent_cnt,
                  SUM(prepay_ota_amt) prepay_ota_amt, SUM(prepay_cash_amt) prepay_cash_amt, SUM(prepay_card_amt) prepay_card_amt,
                  SUM(ieg_pay_cash_amt) ieg_pay_cash_amt, SUM(ieg_pay_card_amt) ieg_pay_card_amt,
                  SUM(isg_pay_cash_amt) isg_pay_cash_amt, SUM(isg_pay_card_amt) isg_pay_card_amt
             FROM room_sale_report_day
            WHERE DATE_FORMAT(CONCAT(year, '-', month, '-', day), '%Y%m%d') >= DATE_FORMAT(?, '%Y%m%d')
                AND DATE_FORMAT(CONCAT(year, '-', month, '-', day), '%Y%m%d') <= DATE_FORMAT(?, '%Y%m%d')
                AND ${filter || "1=1"}
             GROUP BY year, month, day
            ORDER BY ${order};`,
          [sdate, edate],
          result
        );
      }
    });
  },

  insertRoomSaleReportDay: (sday, eday, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `INSERT  INTO room_sale_report_day (year, month, day, room_id, ieg_stay_cnt, ieg_rent_cnt, isg_stay_cnt, isg_rent_cnt, prepay_ota_amt, prepay_cash_amt, prepay_card_amt, ieg_pay_cash_amt, ieg_pay_card_amt, isg_pay_cash_amt, isg_pay_card_amt)
            SELECT year, month, day, room_id, ieg_stay_cnt, ieg_rent_cnt, isg_stay_cnt, isg_rent_cnt, prepay_ota_amt, prepay_cash_amt, prepay_card_amt, ieg_pay_cash_amt, ieg_pay_card_amt, isg_pay_cash_amt, isg_pay_card_amt
            FROM (
              SELECT year, month, day, room_id,
                    SUM(ieg_long_cnt) ieg_long_cnt, SUM(ieg_stay_cnt) ieg_stay_cnt, SUM(ieg_rent_cnt) ieg_rent_cnt,
                    SUM(isg_long_cnt) isg_long_cnt, SUM(isg_stay_cnt) isg_stay_cnt, SUM(isg_rent_cnt) isg_rent_cnt,
                    SUM(prepay_ota_amt) prepay_ota_amt, SUM(prepay_cash_amt) prepay_cash_amt, SUM(prepay_card_amt) prepay_card_amt,
                    SUM(ieg_pay_cash_amt) ieg_pay_cash_amt, SUM(ieg_pay_card_amt) ieg_pay_card_amt,
                    SUM(isg_pay_cash_amt) isg_pay_cash_amt, SUM(isg_pay_card_amt) isg_pay_card_amt
                  FROM room_sale_report_hour
                WHERE DATE_FORMAT(CONCAT(year, '-', month, '-', day), '%Y%m%d') >= DATE_FORMAT(DATE_ADD(NOW(), INTERVAL ? DAY), '%Y%m%d')
                  AND DATE_FORMAT(CONCAT(year, '-', month, '-', day), '%Y%m%d') <= DATE_FORMAT(DATE_ADD(NOW(), INTERVAL ? DAY), '%Y%m%d')
                GROUP BY  year, month, day, room_id
            )  AS T
            ON DUPLICATE KEY UPDATE ieg_stay_cnt=T.ieg_stay_cnt, ieg_rent_cnt=T.ieg_rent_cnt, isg_stay_cnt=T.isg_stay_cnt, isg_rent_cnt=T.isg_rent_cnt,
            prepay_ota_amt=T.prepay_ota_amt, prepay_cash_amt=T.prepay_cash_amt, prepay_card_amt=T.prepay_card_amt, isg_pay_cash_amt=T.isg_pay_cash_amt, reg_date=now(); `,
          [sday, eday],
          (err, info) => {
            result(err, info);
          }
        );
      }
    });
  },

  updateRoomSaleReportDay: (id, deviceReport, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE room_sale_report_day SET ? WHERE id = ?", [deviceReport, id], result);
      }
    });
  },

  deleteRoomSaleReportDay: (date, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `DELETE FROM room_sale_report_day
            WHERE DATE_FORMAT(CONCAT(year, '-', month, '-', day), '%Y%m%d') = DATE_FORMAT(?, '%Y%m%d');`,
          [date],
          result
        );
      }
    });
  },

  deleteRoomSaleOldReportDay: (day, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `DELETE FROM room_sale_report_day
            WHERE DATE_FORMAT(CONCAT(year, '-', month, '-', day), '%Y%m%d') <= DATE_FORMAT(DATE_ADD(NOW(), INTERVAL ? DAY), '%Y%m%d');`,
          [day],
          result
        );
      }
    });
  },
};

export default RoomSaleReportDay;
