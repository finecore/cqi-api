import connection from "../db/connection.js";
import IscSetLog from "./isc-state-log.js";
import _ from "lodash";

// IscSet object constructor
const IscSet = {
  selectIscSetCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`SELECT count(*) count FROM isc_set WHERE ${filter} `, [], result);
      }
    });
  },

  selectIscSet: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT a.*, b.place_id FROM isc_set a INNER JOIN device b ON a.serialno = b.serialno WHERE id = ? ", [id], result);
      }
    });
  },

  selectIscSets: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.type device_type, b.name device_name, c.user_id, u.level user_level
           FROM isc_set a INNER JOIN device b ON a.serialno = b.serialno INNER JOIN user_place c ON b.place_id = c.place_id  INNER JOIN user u ON c.user_id = u.id
           WHERE ${filter} ORDER BY ${order} ${desc} LIMIT ${limit} `,
          [],
          result
        );
      }
    });
  },

  selectIscSetByPlaceId: (place_id, type, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.* , b.place_id
             FROM isc_set a INNER JOIN device b ON a.serialno = b.serialno INNER JOIN place c ON b.place_id = c.id
            WHERE c.id = ? AND b.type = ?;`,
          [place_id, type],
          result
        );
      }
    });
  },

  selectSerialnoTypeIscSet: (serialno, type, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT * FROM isc_set  WHERE serialno = ? AND type = ? ", [serialno, type], result);
      }
    });
  },

  selectSerialnoIscSet: (serialno, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT * FROM isc_set  WHERE serialno = ? ", [serialno], result);
      }
    });
  },

  insertIscSet: (newIscSet, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO isc_set SET ? ON DUPLICATE KEY UPDATE ? ", [newIscSet, newIscSet], result);
      }
    });
  },

  updateIscSet: (id, isc_set, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE isc_set SET ? WHERE id = ?", [isc_set, id], (err, info) => {
          result(err, info);
        });
      }
    });
  },

  updateIscSetBySerialnoType: (serialno, type, isc_set, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE isc_set SET ? WHERE serialno = ? AND type = ? ", [isc_set, serialno, type], result);
      }
    });
  },

  deleteIscSet: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM isc_set WHERE id = ?", [id], result);
      }
    });
  },

  deletePlaceIscSets: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM isc_set WHERE place_id = ?", [place_id], result);
      }
    });
  },
};

export default IscSet;
