import connection from "../db/connection.js";
import _ from "lodash";
import moment from "moment";
import { ERROR } from "../constants/constants.js";

// Server object constructor
const Server = {
  selectServerCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count
             FROM server a
            WHERE ${filter} `,
          [],
          result
        );
      }
    });
  },

  selectServers: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*
             FROM server a
            WHERE ${filter} `,
          [],
          result
        );
      }
    });
  },

  selectServer: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*
             FROM server a
            WHERE a.id = ? `,
          [id],
          result
        );
      }
    });
  },

  insertServer: (server, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO server SET ? ", [server], result);
      }
    });
  },

  updateServer: (id, server, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE server SET ? WHERE id = ?", [server, id], result);
      }
    });
  },

  deleteServer: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM server WHERE id = ?", [id], result);
      }
    });
  },
};

export default Server;
