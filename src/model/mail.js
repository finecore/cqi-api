import connection from "../db/connection.js";
import _ from "lodash";
import moment from "moment";
import { ERROR } from "../constants/constants.js";

let lastMail = [];

// Mail object constructor
const Mail = {
  selectMailCount: (filter, between, begin, end, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count
             FROM mail a
            WHERE ${filter} AND ${between} BETWEEN ? AND ? `,
          [begin, end],
          result
        );
      }
    });
  },

  selectMails: (filter, between, begin, end, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*
             FROM mail a
            WHERE ${filter} AND ${between} BETWEEN ? AND ?
            ORDER BY ${order} ${desc} LIMIT ${limit} `,
          [begin, end],
          result
        );
      }
    });
  },

  selectMail: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*
             FROM mail a
            WHERE a.id = ? `,
          [id],
          result
        );
      }
    });
  },

  insertMail: (mail, result) => {
    // 동일 메일 발송 검사
    let sameMail = 0;

    lastMail = _.filter(lastMail, (m) => {
      let near = moment(m.reg_date).add(1, "minute").isAfter(moment()); // 1 분이내 발송건만
      if (near && _.trim(m.to) === _.trim(mail.to) && _.trim(m.content) === _.trim(mail.content)) {
        sameMail++;
        console.log("- insertMail check ", m.to, mail.to, moment(m.reg_date).format("YYYY-MM-DD HH:mm"), moment().format("YYYY-MM-DD HH:mm"));
      }
      return near;
    });

    console.log("- insertMail sameMail", sameMail);

    // 최근 1분이내 발송 1건이상 동일 하면 발송 취소.
    if (sameMail < 1) {
      lastMail.push(mail);

      connection((_err, _connection) => {
        if (_err) result(_err, null);
        else {
          _connection.sql("INSERT INTO mail SET ? ", [mail], result);
        }
      });
    } else {
      result(ERROR.MAIL_DUPLICATE_ERROR, null);
    }
  },

  updateMail: (id, mail, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE mail SET ? WHERE id = ?", [mail, id], result);
      }
    });
  },

  deleteMail: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM mail WHERE id = ?", [id], result);
      }
    });
  },

  deleteOldMail: (date, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `DELETE  FROM mail
            WHERE send_date IS NOT NULL AND send_date < STR_TO_DATE(?, '%Y-%m-%d %H:%i'); `,
          [date],
          result
        );
      }
    });
  },
};

export default Mail;
