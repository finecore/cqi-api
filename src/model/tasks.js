import connection from "../db/connection.js";

//Task object constructor
const Task = {
  createTask: (newTask, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO tasks set ?", newTask, result);
      }
    });
  },

  getTaskById: (taskId, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("Select task from tasks WHERE id = ? ", taskId, result);
      }
    });
  },

  getAllTask: (result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT * FROM tasks", [], result);
      }
    });
  },

  updateById: (id, task, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE tasks SET task = ? WHERE id = ?", [task.task, id], result);
      }
    });
  },

  remove: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM tasks WHERE id = ?", [id], result);
      }
    });
  },
};

export default Task;
