import connection from "../db/connection.js";

// SyncLog object constructor
const SyncLog = {
  selectSyncLogCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count
             FROM sync_log
           WHERE ${filter} `,
          [],
          result
        );
      }
    });
  },

  selectSyncLogs: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT *
           FROM sync_log
           WHERE ${filter}
          ORDER BY ${order} ${desc}
          LIMIT ${limit}`,
          [],
          result
        );
      }
    });
  },

  selectSyncLog: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`SELECT * FROM sync_log WHERE id = ? `, id, result);
      }
    });
  },

  selectSyncLogPending: (result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`SELECT * FROM sync_log WHERE status = 'pending' LIMIT 30; `, result);
      }
    });
  },

  insertSyncLog: (sync_log, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO sync_log SET ? ", [sync_log], result);
      }
    });
  },

  updateSyncLog: (id, sync_log, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE sync_log SET ? WHERE id = ? ", [sync_log, id], result);
      }
    });
  },

  deleteSyncLogAfter24: (datetime, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM sync_log WHERE reg_date < ? ", [datetime], result);
      }
    });
  },

  deleteSyncLog: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM sync_log WHERE id = ? ", [id], result);
      }
    });
  },
};

export default SyncLog;
