import connection from "../db/connection.js";
import RoomStateLog from "./room-state-log.js";
import ApiPmsOut from "../controllers/api-pms-out.js";
import _ from "lodash";
import moment from "moment";
import format from "../utils/format-util.js";

// RoomState object constructor
const RoomState = {
  selectAllRoomStates: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT b.name, b.place_id, b.gid, b.lid, a.*,
                 c.name  AS type_name
             FROM room_state a
                  INNER JOIN room b       ON  a.room_id = b.id
                  LEFT  JOIN room_type c  ON  b.room_type_id = c.id
            WHERE b.place_id = ?
            ORDER BY b.gid, b.lid
          `,
          [place_id],
          result
        );
      }
    });
  },

  selectRoomStateCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*)
           FROM room_state a
                INNER JOIN room b       ON  a.room_id = b.id
                LEFT  JOIN room_type c  ON  b.room_type_id = c.id
           WHERE ${filter} `,
          [],
          result
        );
      }
    });
  },

  selectRoomStates: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT  b.name, b.gid, b.lid, b.place_id, a.*
           FROM room_state a
           INNER JOIN room b ON a.room_id = b.id
           WHERE ${filter}
           ORDER BY ${order} ${desc}
            LIMIT ${limit} `,
          [],
          result
        );
      }
    });
  },

  selectRoomStateSale: (room_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT a.*, b.name,  b.gid, b.lid, b.place_id FROM room_state a INNER JOIN room b ON a.room_id = b.id  WHERE a.room_id = ? ", room_id, result);
      }
    });
  },

  selectAllRoomStatesSale: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.place_id, b.room_type_id, b.name, b.gid, b.lid,  b.floor, c.state, c.channel sale_channel, c.stay_type, c.check_in, c.check_out, d.name type_name, e.id sale_pay_id, e.rollback
             FROM room_state a
             INNER JOIN room b ON a.room_id = b.id
             LEFT JOIN room_sale c ON a.room_sale_id = c.id
             INNER JOIN room_type d ON b.room_type_id = d.id
             LEFT JOIN room_sale_pay e ON e.id =  (select  max(id)  from room_sale_pay where sale_id =  c.id)
            WHERE b.place_id = ?
            ORDER BY b.id  `,
          [place_id],
          result
        );
      }
    });
  },

  selectRoomState: (room_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.place_id, b.room_type_id, b.name, b.gid, b.lid, b.floor, c.state, c.channel sale_channel, c.stay_type, c.check_in, c.check_out, d.name type_name, e.id sale_pay_id, e.rollback
             FROM room_state a
             INNER JOIN room b ON a.room_id = b.id
             LEFT JOIN room_sale c ON a.room_sale_id = c.id
             INNER JOIN room_type d ON b.room_type_id = d.id
             LEFT JOIN room_sale_pay e ON e.id =  (select  max(id)  from room_sale_pay where sale_id =  c.id)
            WHERE a.room_id = ?
            ORDER BY b.floor, b.gid, b.lid`,
          [room_id],
          result
        );
      }
    });
  },

  selectRoomStateAll: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name, b.gid, b.lid,  c.state, c.channel sale_channel, c.stay_type, c.check_in, c.check_out, d.name type_name
             FROM room_state a INNER JOIN room b ON a.room_id = b.id LEFT JOIN room_sale c ON a.room_sale_id = c.id INNER JOIN room_type d ON b.room_type_id = d.id
            WHERE b.place_id = ?
            ORDER BY b.gid, b.lid`,
          [place_id],
          result
        );
      }
    });
  },

  selectRoomStateByName: (place_id, name, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*,  b.name,  b.gid, b.lid, c.state, c.channel sale_channel, c.stay_type, c.check_in, c.check_out, d.name type_name,
            (select  max(id)  from room_sale_pay where sale_id =  c.id) as sale_pay_id
             FROM room_state a
              INNER JOIN room b ON a.room_id = b.id
              LEFT JOIN room_sale c ON a.room_sale_id = c.id
              INNER JOIN room_type d ON b.room_type_id = d.id
            WHERE b.place_id = ? AND CAST(b.name AS UNSIGNED) = CAST(? AS UNSIGNED)
            ORDER BY c.id`,
          [place_id, name],
          result
        );
      }
    });
  },

  selectRoomStateAndFee: (room_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "SELECT a.*, b.place_id, b.room_type_id,  b.gid, b.lid, c.default_fee_stay, c.default_fee_rent, d.check_in, d.check_out_exp, d.check_out " +
            "FROM room_state a LEFT JOIN room b ON a.room_id = b.id LEFT JOIN room_type c ON b.room_type_id = c.id LEFT JOIN room_sale d ON a.room_sale_id = d.id WHERE a.room_id = ? ",
          room_id,
          result
        );
      }
    });
  },

  insertRoomState: (newRoomState, result, conn) => {
    if (conn) {
      conn.sql("INSERT INTO room_state SET ? ON DUPLICATE KEY UPDATE ? ", [newRoomState, newRoomState], result, true);
    } else {
      newRoomState["reg_date"] = new Date();
      newRoomState["mod_date"] = new Date();

      connection((_err, _connection) => {
        if (_err) result(_err, null);
        else {
          _connection.sql("INSERT INTO room_state SET ? ON DUPLICATE KEY UPDATE ? ", [newRoomState, newRoomState], result);
        }
      });
    }
  },

  updateRoomStateSignal: (place_id, signal, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE room_state a INNER JOIN room b ON a.room_id = b.id SET a.signal = ? WHERE b.place_id = ?", [signal, place_id], result);
      }
    });
  },

  updateRoomStateAll: (place_id, changed, result) => {
    connection((_err, _connection) => {
      changed["mod_date"] = new Date();

      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE room_state a INNER JOIN room b ON a.room_id = b.id SET ? WHERE b.place_id = ?", [changed, place_id], result);
      }
    });
  },

  // id 대신에 room_id 를 키로 사용한다(device 에서 room_id 로 업데이트함)
  updateRoomState: (room_id, changed, req, pms, result) => {
    const changedLog = _.cloneDeep(changed); // 로그 정보를 보전 하기 위해 복제.

    let {
      auth: { place_id },
    } = req;

    if (changed["user_id"] !== undefined) delete changed["user_id"];

    if (changed["clean"] !== undefined) changed["clean_change_time"] = new Date();
    if (changed["key"] !== undefined) changed["key_change_time"] = new Date();
    if (changed["sale"] !== undefined) changed["sale_change_time"] = new Date(); // 입/퇴실/공실 전환 시간

    changed["mod_date"] = new Date();
    changed["reg_date"] = format.convertToUTC(changed["reg_date"]);

    if (changed["clean_change_time"] != undefined) changed["clean_change_time"] = format.convertToUTC(changed["clean_change_time"]);
    if (changed["key_change_time"] != undefined) changed["key_change_time"] = format.convertToUTC(changed["key_change_time"]);
    if (changed["sale_change_time"] != undefined) changed["sale_change_time"] = format.convertToUTC(changed["sale_change_time"]);

    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE room_state SET ? WHERE room_id = ? ", [changed, room_id], (err, info) => {
          result(err, info);

          if (!err) {
            delete changedLog["mod_date"];

            RoomState.add_room_state_log(room_id, changedLog, req);

            // 객실의 정비(청소) 시점에 해당 PMS api를 호출
            if (pms) {
              changedLog.pms = pms;
              changedLog.room_id = room_id;
              changedLog.place_id = place_id;

              ApiPmsOut.put_pms_room_state(changed);
            }
          }
        });
      }
    });
  },

  deleteRoomState: (room_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM room_state WHERE room_id = ? ", [room_id], result);
      }
    });
  },

  selectSaleDup: (result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.place_id, c.id state_id
             FROM cqi.room_sale a INNER JOIN room b ON a.room_id = b.id LEFT JOIN room_state c ON a.room_id = c.room_id
            WHERE a.state = 'A' AND c.room_sale_id IS NULL;`,
          [],
          result
        );
      }
    });
  },

  add_room_state_log: (room_id, room_state, req) => {
    if (room_id) {
      delete room_state["mod_date"];

      const room_state_log = {
        room_id,
        data: JSON.stringify(room_state),
        reg_date: new Date(),
        mod_date: new Date(),
      };

      // TODO user_id 저장 로직 구현!!! 필드 따로 생성 !!

      console.log("---> add_room_state_log", JSON.stringify(room_state_log));

      RoomStateLog.insertRoomStateLog(room_state_log, req, (err, info) => {
        if (err) console.log("---> RoomStateLog failure inserted ", err);
        else {
          console.log("---> RoomStateLog successfully inserted ");
        }
      });
    } else {
      console.log("---> RoomStateLog failure inserted no room_id ");
    }
  },
};

export default RoomState;
