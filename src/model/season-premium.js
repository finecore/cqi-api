import connection from "../db/connection.js";

// SeasonPremium object constructor
const SeasonPremium = {
  selectAllSeasonPremiums: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT * FROM season_premium WHERE place_id = ? ORDER BY begin, end ", [place_id], result);
      }
    });
  },

  selectBetweenSeasonPremiums: (place_id, begin, end, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT *
             FROM season_premium a
            WHERE place_id = ${place_id} AND begin >= ${begin} AND end <= ${end} `,
          [],
          result
        );
      }
    });
  },

  selectSeasonPremium: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT * FROM season_premium WHERE id = ? ", id, result);
      }
    });
  },

  insertSeasonPremium: (newSeasonPremium, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO season_premium SET ? ON DUPLICATE KEY UPDATE ? ", [newSeasonPremium, newSeasonPremium], result);
      }
    });
  },

  updateSeasonPremium: (id, roomReserv, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE season_premium SET ? WHERE id = ?", [roomReserv, id], result);
      }
    });
  },

  deleteAllSeasonPremiums: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM season_premium WHERE place_id = ? ", [place_id], result);
      }
    });
  },

  deleteSeasonPremium: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM season_premium WHERE id = ? ", [id], result);
      }
    });
  },
};

export default SeasonPremium;
