import connection from "../db/connection.js";

// Version object constructor
const Version = {
  selectVersionCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`SELECT count(*) count FROM version a WHERE ${filter} `, [], result);
      }
    });
  },

  selectVersions: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*
             FROM version a
            WHERE ${filter} ORDER BY ${order} ${desc} LIMIT ${limit} `,
          [],
          result
        );
      }
    });
  },

  selectVersion: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*
             FROM version a WHERE a.id = ? `,
          [id],
          result
        );
      }
    });
  },

  insertVersion: (newVersion, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO version SET ? ", [newVersion], result);
      }
    });
  },

  updateVersion: (id, version, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE version SET ? WHERE id = ?", [version, id], result);
      }
    });
  },

  deleteVersion: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM version WHERE id = ?", [id], result);
      }
    });
  },
};

export default Version;
