import connection from "../db/connection.js";

// Place object constructor
const Place = {
  selectPlaceCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`SELECT count(*) count FROM place a WHERE ${filter} `, [], result);
      }
    });
  },

  selectPlaces: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, a.id as place_id, a.name place_name, b.name as company_name,
            (select count(*) from room where place_id = a.id) as room_cnt
             FROM place a INNER JOIN company b ON a.company_id = b.id
            WHERE ${filter} ORDER BY ${order} ${desc} LIMIT ${limit} `,
          [],
          result
        );
      }
    });
  },

  selectPlacesSearch: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        const query = `
          SELECT
              a.*,
              a.id AS place_id,
              a.name AS place_name,
              b.name AS company_name,
              MAX(c.img_link) AS img_link,  -- place_imgs에서 첫 번째 이미지
              (SELECT COUNT(*) FROM room WHERE place_id = a.id) AS room_cnt, -- 객실 개수
              AVG(d.star_point) AS avg_star_point,  -- member_review에서 star_point 평균값
              rt.min_default_fee_stay,  -- ✅ 모든 객실 타입 중 가장 낮은 요금
              rt.max_default_fee_stay,  -- ✅ 모든 객실 타입 중 가장 높은 요금
              rt.min_default_fee_rent,  -- ✅ 모든 객실 타입 중 가장 낮은 대실 요금
              rt.max_default_fee_rent,  -- ✅ 모든 객실 타입 중 가장 높은 대실 요금
              rt.room_type_id,  -- ✅ 가장 저렴한 객실 타입 ID
              rm.room_id  -- ✅ 가장 저렴한 객실 ID
          FROM place a
          INNER JOIN company b ON a.company_id = b.id
          LEFT JOIN (
              SELECT place_id, MAX(img_link) AS img_link
              FROM place_imgs
              GROUP BY place_id
          ) c ON a.id = c.place_id
          LEFT JOIN (
              SELECT r.place_id, AVG(mr.star_point) AS star_point
              FROM room_reserv r
              INNER JOIN member_review mr ON r.id = mr.reserv_id
              GROUP BY r.place_id
          ) d ON a.id = d.place_id
          LEFT JOIN (
              SELECT place_id,
                    MIN(default_fee_stay) AS min_default_fee_stay,  -- ✅ 모든 객실 타입 중 가장 낮은 요금
                    MAX(default_fee_stay) AS max_default_fee_stay,  -- ✅ 모든 객실 타입 중 가장 높은 요금
                    MIN(default_fee_rent) AS min_default_fee_rent,  -- ✅ 모든 객실 타입 중 가장 낮은 대실 요금
                    MAX(default_fee_rent) AS max_default_fee_rent,  -- ✅ 모든 객실 타입 중 가장 높은 대실 요금
                    MIN(id) AS room_type_id  -- ✅ 가장 낮은 ID의 room_type을 대표 타입으로 설정
              FROM room_type
              GROUP BY place_id
          ) rt ON a.id = rt.place_id
          LEFT JOIN (
              -- ✅ 가장 저렴한 객실 ID 가져오기
              SELECT r.place_id, MIN(r.id) AS room_id
              FROM room r
              INNER JOIN room_type rt ON r.room_type_id = rt.id
              WHERE rt.default_fee_stay = (
                  SELECT MIN(default_fee_stay)
                  FROM room_type
                  WHERE place_id = r.place_id
              )
              GROUP BY r.place_id
          ) rm ON a.id = rm.place_id  -- ✅ 가장 저렴한 객실의 room_id
          WHERE ${filter}
          GROUP BY a.id, b.name, rt.room_type_id, rm.room_id
          ORDER BY ${order} ${desc}
          LIMIT ${limit};

      `;

        _connection.sql(query, [], result);
      }
    });
  },

  selectPlace: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT * FROM place WHERE id = ? ", id, result);
      }
    });
  },

  insertPlace: (newPlace, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO place SET ? ON DUPLICATE KEY UPDATE ? ", [newPlace, newPlace], result);
      }
    });
  },

  updatePlace: (id, place, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE place SET ? WHERE id = ?", [place, id], result);
      }
    });
  },

  deletePlace: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM place WHERE id = ?", [id], result);
      }
    });
  },
};

export default Place;
