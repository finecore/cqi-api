import connection from "../db/connection.js";

// MemberRead object constructor
const MemberRead = {
  // 회원 읽기 카운트 조회
  selectMemberReadsCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) AS count
           FROM member_read a
              INNER JOIN member c ON a.member_id = c.id
           WHERE ${filter}`,
          [],
          result
        );
      }
    });
  },

  // 회원 읽기 목록 조회
  selectMemberReads: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, c.name AS member_name, c.nic_name
           FROM member_read a
              INNER JOIN member c ON a.member_id = c.id
           WHERE ${filter}
           ORDER BY ${order} ${desc}
           LIMIT ${limit}`,
          [],
          result
        );
      }
    });
  },

  // 특정 회원의 읽기 목록 조회
  selectByMemberReads: (member_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, c.name AS member_name, c.nic_name
           FROM member_read a
              INNER JOIN member c ON a.member_id = c.id
           WHERE a.member_id = ?
           ORDER BY a.reg_date DESC`,
          [member_id],
          result
        );
      }
    });
  },

  // 특정 읽기 항목 조회
  selectMemberRead: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, c.name AS member_name, c.nic_name
           FROM member_read a
              INNER JOIN member c ON a.member_id = c.id
           WHERE a.id = ?
           ORDER BY a.reg_date DESC`,
          [id],
          result
        );
      }
    });
  },

  // 회원 읽기 기록 추가
  insertMemberRead: (member_read, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO member_read SET ?", [member_read], result);
      }
    });
  },

  // 회원 읽기 기록 수정
  updateMemberRead: (id, member_read, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE member_read SET ? WHERE id = ?", [member_read, id], result);
      }
    });
  },

  // 회원 읽기 기록 삭제
  deleteMemberRead: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM member_read WHERE id = ?", [id], result);
      }
    });
  },

  // 특정 회원의 모든 읽기 기록 삭제
  deleteAllMemberRead: (member_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `DELETE a
           FROM member_read a
           INNER JOIN room_read b ON a.read_id = b.id
           WHERE b.member_id = ?`,
          [member_id],
          result
        );
      }
    });
  },
};

export default MemberRead;
