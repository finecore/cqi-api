import connection from "../db/connection.js";

// DeviceRc object constructor
const DeviceRc = {
  selectDeviceRcCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(DISTINCT a.id) count
             FROM device_rc a INNER JOIN place b ON a.place_id = b.id INNER JOIN company c ON b.company_id = c.id LEFT JOIN user_place u ON a.place_id = u.place_id
            WHERE ${filter} `,
          [],
          result
        );
      }
    });
  },

  selectDeviceRcs: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT DISTINCT a.*, b.name as place_name, c.name as company_name
           FROM device_rc a  INNER JOIN place b ON a.place_id = b.id
                          INNER JOIN company c ON b.company_id = c.id
                          LEFT JOIN user_place u ON a.place_id = u.place_id
                          LEFT JOIN version v ON a.curr_version_id = v.id
           WHERE ${filter}
           ORDER BY ${order} ${desc}
           LIMIT ${limit} `,
          [],
          result
        );
      }
    });
  },

  selectPlaceDeviceRcs: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT DISTINCT a.*, b.name
           FROM device_rc a INNER JOIN place b ON a.place_id = b.id LEFT JOIN user_place u ON a.place_id = u.place_id
           WHERE a.place_id = ? `,
          [place_id],
          result
        );
      }
    });
  },

  selectDeviceRc: (id, uuid, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT DISTINCT a.*, b.name
             FROM device_rc a INNER JOIN place b ON a.place_id = b.id  LEFT JOIN version v ON a.curr_version_id = v.id
            WHERE a.id = ? OR a.uuid = ?`,
          [id, uuid],
          result
        );
      }
    });
  },

  selectDeviceRcByUuid: (uuid, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT DISTINCT a.*, b.name  FROM device_rc a INNER JOIN place b ON a.place_id = b.id WHERE a.uuid = ? ", [uuid], result);
      }
    });
  },

  insertDeviceRc: (newDeviceRc, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO device_rc SET ? ON DUPLICATE KEY UPDATE ? ", [newDeviceRc, newDeviceRc], result);
      }
    });
  },

  updateDeviceRc: (id, device_rc, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE device_rc SET ? WHERE id = ?", [device_rc, id], result);
      }
    });
  },

  updateDeviceRcUuid: (uuid, device_rc, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE device_rc SET ? WHERE uuid = ?", [device_rc, uuid], result);
      }
    });
  },

  deleteDeviceRc: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM device_rc WHERE id = ?", [id], result);
      }
    });
  },

  deletePlaceDeviceRcs: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM device_rc WHERE place_id = ?", [place_id], result);
      }
    });
  },
};

export default DeviceRc;
