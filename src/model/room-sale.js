import connection from "../db/connection.js";
import RoomState from "./room-state.js";
import RoomSalePay from "./room-sale-pay.js";
import ApiPmsOut from "../controllers/api-pms-out.js";
import _ from "lodash";
import moment from "moment";

// RoomSale object constructor
const RoomSale = {
  selectAllRoomSales: (place_id, filter, between, begin, end, order, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name room_name, b.room_type_id, c.sale, d.name user_name
             FROM room_sale a LEFT JOIN room b ON a.room_id = b.id LEFT JOIN room_state c ON a.room_id = c.room_id LEFT JOIN user d ON a.user_id = d.id
            WHERE b.place_id = ?
              AND ${filter} AND ${between} BETWEEN ? AND ? ORDER BY ${order} LIMIT 10000;`,
          [place_id, begin, end],
          result
        );
      }
    });
  },

  selectRoomSales: (room_id, filter, between, begin, end, order, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT * FROM room_sale a
                          WHERE room_id = ? AND ${filter} AND ${between} BETWEEN ? AND ?
                          ORDER BY ${order}
                          LIMIT ${limit};`,
          [room_id, begin, end],
          result
        );
      }
    });
  },

  selectNowRoomSales: (place_id, filter, order, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name room_name, b.room_type_id, c.sale, d.name user_name
             FROM room_sale a LEFT JOIN room b ON a.room_id = b.id INNER JOIN room_state c ON a.id = c.room_sale_id LEFT JOIN user d ON a.user_id = d.id
            WHERE b.place_id = ? AND ${filter}
            ORDER BY ${order};`,
          [place_id],
          result
        );
      }
    });
  },

  selectRoomSale: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name room_name, b.room_type_id, b.place_id
             FROM room_sale a LEFT JOIN room b ON a.room_id = b.id
            WHERE a.id = ? OR a.room_id = ? `,
          [id, id],
          result
        );
      }
    });
  },

  selectIsRoomSaleNow: (room_id, stay_type, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT count(*) count FROM room_sale WHERE room_id = ? AND stay_type = ? AND state = 'A' ", [room_id, stay_type], result);
      }
    });
  },

  selectPlaceRoomSale: (place_id, begin, end, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT place_id, stay_type, COUNT(*) count,
                  SUM(default_fee) default_fee, SUM(add_fee) add_fee,
                  SUM(pay_cash_amt) pay_cash_amt, SUM(pay_card_amt) pay_card_amt, SUM(pay_point_amt) pay_point_amt,
                  SUM(prepay_ota_amt) prepay_ota_amt, SUM(prepay_cash_amt) prepay_cash_amt, SUM(prepay_card_amt) prepay_card_amt, SUM(prepay_point_amt) prepay_point_amt
             FROM room_sale a INNER JOIN room b ON a.room_id = b.id
            WHERE b.place_id = ? AND state != 'B'
              AND date_format(check_in_exp, '%Y-%m-%d %H:%i') >= date_format(?, '%Y-%m-%d %H:%i')
              AND date_format(check_in_exp, '%Y-%m-%d %H:%i') <= date_format(?, '%Y-%m-%d %H:%i')
            GROUP BY place_id, stay_type WITH ROLLUP
            HAVING stay_type != ''  `,
          [place_id, begin, end],
          result
        );
      }
    });
  },

  selectSumRoomSale: (place_id, begin, end, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT room_id, stay_type, COUNT(*) count,
                  SUM(default_fee) default_fee, SUM(add_fee) add_fee,
                  SUM(pay_cash_amt) pay_cash_amt, SUM(pay_card_amt) pay_card_amt, SUM(pay_point_amt) pay_point_amt,
                  SUM(prepay_ota_amt) prepay_ota_amt, SUM(prepay_cash_amt) prepay_cash_amt, SUM(prepay_card_amt) prepay_card_amt, SUM(prepay_point_amt) prepay_point_amt
             FROM room_sale a INNER JOIN room b ON a.room_id = b.id
            WHERE b.place_id = ? AND state != 'B'
              AND date_format(check_in, '%Y-%m-%d %H:%i') >= date_format(?, '%Y-%m-%d %H:%i')
              AND date_format(check_in, '%Y-%m-%d %H:%i') <= date_format(?, '%Y-%m-%d %H:%i')
            GROUP BY room_id, stay_type WITH ROLLUP; `,
          [place_id, begin, end],
          result
        );
      }
    });
  },

  selectRoomSaleSum: (room_id, begin, end, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT room_id, stay_type, COUNT(*) count,
                  SUM(default_fee) default_fee, SUM(add_fee) add_fee,
                  SUM(pay_cash_amt) pay_cash_amt, SUM(pay_card_amt) pay_card_amt, SUM(pay_point_amt) pay_point_amt,
                  SUM(prepay_ota_amt) prepay_ota_amt, SUM(prepay_cash_amt) prepay_cash_amt, SUM(prepay_card_amt) prepay_card_amt, SUM(prepay_point_amt) prepay_point_amt
            FROM room_sale
           WHERE room_id = ? AND state != 'B'
             AND date_format(check_in, '%Y-%m-%d %H:%i') >= date_format(?, '%Y-%m-%d %H:%i')
             AND date_format(check_in, '%Y-%m-%d %H:%i') <= date_format(?, '%Y-%m-%d %H:%i')
           GROUP BY room_id, stay_type WITH ROLLUP `,
          [room_id, begin, end],
          result
        );
      }
    });
  },

  selectRoomSaleReport: (begin, end, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT DATE_FORMAT(reg_date, '%Y-%m-%d') sale_date, room_id, channel, stay_type,
                  SUM(stay_type) stay_type_cnt ,
                  SUM(default_fee) default_fee, SUM(add_fee) add_fee,
                  SUM(pay_cash_amt) pay_cash_amt, SUM(pay_card_amt) pay_card_amt, SUM(pay_point_amt) pay_point_amt,
                  SUM(prepay_ota_amt) prepay_ota_amt, SUM(prepay_cash_amt) prepay_cash_amt, SUM(prepay_card_amt) prepay_card_amt, SUM(prepay_point_amt) prepay_point_amt
                FROM room_sale
            	 WHERE DATE_FORMAT(reg_date, '%Y%m%d') >= DATE_FORMAT(DATE_ADD(NOW(),  INTERVAL ? DAY), '%Y%m%d')
                 AND DATE_FORMAT(reg_date, '%Y%m%d') <= DATE_FORMAT(DATE_ADD(NOW(),  INTERVAL ? DAY), '%Y%m%d')
                 AND state != 'B'
               GROUP BY DATE_FORMAT(reg_date, '%Y-%m-%d'), room_id, channel, stay_type;`,
          [begin, end],
          result
        );
      }
    });
  },

  insertRoomSale: (newRoomSale, req, result) => {
    const _newRoomSale = _.cloneDeep(newRoomSale);

    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        delete newRoomSale.season_fee;
        delete newRoomSale.person_fee;
        delete newRoomSale.discount_fee;
        delete newRoomSale.stay_days;

        console.log("===> insertRoomSale", newRoomSale);

        _connection.sql("INSERT INTO room_sale SET ? ;", [newRoomSale, newRoomSale], (err, row) => {
          result(err, row);

          if (!err) {
            const { room_id, channel } = _newRoomSale;

            // console.log("-> insertRoomSale", { room_id, channel });

            // 매출 로그 저장.
            if (room_id) {
              RoomState.add_room_state_log(room_id, _newRoomSale, req);

              if (channel !== "pms") {
                // 체크인 시 해당 PMS api를 호출
                ApiPmsOut.put_pms_room_sale(_newRoomSale);
              }
            }
          }
        });
      }
    });
  },

  updateCheckOut: (room_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `UPDATE room_sale a
              SET a.state = 'C', a.check_out = now()
            WHERE a.state = 'A' AND a.room_id = ? ;`,
          [room_id],
          result
        );
      }
    });
  },

  updateRoomSale: (id, roomSale, req, result) => {
    const _roomSale = _.cloneDeep(roomSale);

    const { room_id, state } = roomSale;

    delete roomSale.season_fee;
    delete roomSale.person_fee;
    delete roomSale.discount_fee;
    delete roomSale.stay_days;
    delete roomSale.id;

    console.log("---> roomSale", roomSale);

    // 현재 room_id 조회.
    RoomSale.selectRoomSale(id, (err, room_sale) => {
      if (!err) {
        const { id: sale_id, room_id: cur_room_id, stay_type } = room_sale[0];

        console.log("---> cur_room_id", cur_room_id, room_id);

        connection((_err, _connection) => {
          if (_err) result(_err, null);
          else {
            delete roomSale.rollback; // 입/퇴실 취소 정보 삭제.

            if (_roomSale.check_out_exp) {
              if (_roomSale.check_out_exp instanceof Date || /(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z)/.test(String(_roomSale.check_out_exp))) {
                _roomSale.check_out_exp = moment(_roomSale.check_out_exp).format("YYYY-MM-DD HH:mm"); // 날자 포멧.
                console.log("--- check_out_exp date format", _roomSale.check_out_exp);
              }
            }

            _connection.sql("UPDATE room_sale SET ? WHERE id = ?", [_roomSale, id], (err, row) => {
              result(err, row);

              if (!err) {
                if (cur_room_id && room_id && Number(cur_room_id) !== Number(room_id)) {
                  _roomSale.change_room_id = room_id; // 객실 이동 시.
                  delete _roomSale.room_id;
                }

                if (!_roomSale.stay_type) _roomSale.stay_type = stay_type; // 로그 정보를 위한 숙박 형태.

                // 판매 정보 업데이트.
                RoomSalePay.updateRoomSalePay(null, { sale_id, state }, (err, row) => {
                  if (!err) {
                    // 매출 로그 저장.
                    RoomState.add_room_state_log(room_id, _roomSale, req);

                    if (_roomSale.state && _roomSale.channel !== "pms") {
                      // 체크아웃 시 해당 PMS api를 호출
                      ApiPmsOut.put_pms_room_sale(_roomSale);
                    }
                  }
                });
              }
            });
          }
        });
      }
    });
  },

  deleteRoomSalesAfterKeepDay: (place_id, date, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `DELETE a FROM room_sale a INNER JOIN room b ON a.room_id = b.id INNER JOIN place c ON b.place_id = c.id
            WHERE b.place_id = ? AND a.check_in < STR_TO_DATE(?, '%Y-%m-%d %H:%i'); `,
          [place_id, date],
          result
        );
      }
    });
  },

  deleteRoomSales: (room_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM room_sale WHERE room_id = ? ", [room_id], result);
      }
    });
  },

  deleteRoomSale: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM room_sale WHERE id = ? ", [id], result);
      }
    });
  },
};

export default RoomSale;
