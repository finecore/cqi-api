import connection from "../db/connection.js";
import RoomState from "./room-state.js";
import _ from "lodash";

// RoomSalePay object constructor
const RoomSalePay = {
  selectAllRoomSalePays: (place_id, filter, between, begin, end, order, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*,
                b.state s_state, b.room_id,  b.check_in, b.check_out, b.check_out_exp, b.room_reserv_id,
                b.default_fee s_default_fee, b.add_fee s_add_fee,
                b.pay_cash_amt s_pay_cash_amt, b.pay_card_amt s_pay_card_amt, b.pay_point_amt s_pay_point_amt,
                b.prepay_ota_amt s_prepay_ota_amt, b.prepay_cash_amt s_prepay_cash_amt, b.prepay_card_amt s_prepay_card_amt, b.prepay_point_amt s_prepay_point_amt,
                b.memo s_memo, b.car_no s_car_no, b.comment s_comment,
                c.name room_name, d.sale, e.name user_name
             FROM room_sale_pay a
             INNER JOIN room_sale b ON a.sale_id = b.id
             INNER JOIN room c ON b.room_id = c.id
             INNER JOIN room_state d ON b.room_id = d.room_id
              LEFT JOIN user e ON a.user_id = e.id
            WHERE c.place_id = ?
            AND a.use_yn = 'Y'
            AND ${filter} AND ${between} BETWEEN ? AND ? ORDER BY ${order} LIMIT 10000;`,
          [place_id, begin, end],
          result
        );
      }
    });
  },

  selectRoomSalePayLast24Report: (result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT DATE_FORMAT(a.reg_date, '%Y-%m-%d %H') sale_date, b.room_id, b.channel, a.stay_type,
                  SUM(a.stay_type) stay_type_cnt ,
                  SUM(b.default_fee) default_fee, SUM(b.add_fee) add_fee,
                  SUM(a.pay_cash_amt) pay_cash_amt, SUM(a.pay_card_amt) pay_card_amt, SUM(a.pay_point_amt) pay_point_amt,
                  SUM(a.prepay_ota_amt) prepay_ota_amt, SUM(a.prepay_cash_amt) prepay_cash_amt, SUM(a.prepay_card_amt) prepay_card_amt, SUM(a.prepay_point_amt) prepay_point_amt
                FROM room_sale_pay a INNER JOIN room_sale b ON a.sale_id = b.id
            	 WHERE DATE_FORMAT(a.reg_date, '%Y%m%d%H') >= DATE_FORMAT(DATE_ADD(NOW(),  INTERVAL -1 DAY), '%Y%m%d%H')
                 AND b.state != 'B'
                 AND a.use_yn = 'Y'
               GROUP BY DATE_FORMAT(reg_date, '%Y-%m-%d %H'), b.room_id, b.channel, a.stay_type;`,
          [],
          result
        );
      }
    });
  },

  selectRoomSalePaysByRoomId: (room_id, filter, between, begin, end, order, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*,
                b.state s_state, b.room_id, b.check_in, b.check_out, b.check_out_exp, b.room_reserv_id,
                b.default_fee s_default_fee, b.add_fee s_add_fee,
                b.pay_cash_amt s_pay_cash_amt, b.pay_card_amt s_pay_card_amt, b.pay_point_amt s_pay_point_amt,
                b.prepay_ota_amt s_prepay_ota_amt, b.prepay_cash_amt s_prepay_cash_amt, b.prepay_card_amt s_prepay_card_amt, b.prepay_point_amt s_prepay_point_amt,
                b.memo s_memo, b.car_no s_car_no, b.comment s_comment,
                c.name room_name, d.sale, e.name user_name, p.name place_name, p.addr as place_addr, p.tel as place_tel, p.longitude, p.latitude
             FROM room_sale_pay a
             INNER JOIN room_sale b ON a.sale_id = b.id
             INNER JOIN room c ON b.room_id = c.id
             INNER JOIN place p ON c.place_id = p.id
             INNER JOIN room_state d ON b.room_id = d.room_id
              LEFT JOIN user e ON a.user_id = e.id
           WHERE c.room_id = ? AND a.use_yn = 'Y' AND ${filter} AND ${between} BETWEEN ? AND ? ORDER BY ${order} LIMIT 10000;`,
          [room_id, begin, end],
          result
        );
      }
    });
  },

  selectRoomSalePaysByMemberId: (member_id, filter, between, begin, end, order, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*,
                b.state s_state, b.room_id, b.check_in, b.check_out, b.check_out_exp, b.room_reserv_id,
                b.default_fee s_default_fee, b.add_fee s_add_fee,
                b.pay_cash_amt s_pay_cash_amt, b.pay_card_amt s_pay_card_amt, b.pay_point_amt s_pay_point_amt,
                b.prepay_ota_amt s_prepay_ota_amt, b.prepay_cash_amt s_prepay_cash_amt, b.prepay_card_amt s_prepay_card_amt, b.prepay_point_amt s_prepay_point_amt,
                b.memo s_memo, b.car_no s_car_no, b.comment s_comment,
                c.name room_name, d.sale, e.name member_name,p.name place_name, p.addr as place_addr, p.tel as place_tel, p.longitude, p.latitude
             FROM room_sale_pay a
             INNER JOIN room_sale b ON a.sale_id = b.id
             INNER JOIN room c ON b.room_id = c.id
             INNER JOIN place p ON c.place_id = p.id
             INNER JOIN room_state d ON b.room_id = d.room_id
              LEFT JOIN member e ON a.member_id = e.id
           WHERE a.member_id = ? AND a.use_yn = 'Y' AND ${filter} AND ${between} BETWEEN ? AND ? ORDER BY ${order} LIMIT 10000;`,
          [member_id, begin, end],
          result
        );
      }
    });
  },

  selectRoomSalePay: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*,
                b.state s_state, b.room_id, b.check_in, b.check_out, b.check_out_exp, b.room_reserv_id,
                b.default_fee s_default_fee, b.add_fee s_add_fee,
                b.pay_cash_amt s_pay_cash_amt, b.pay_card_amt s_pay_card_amt, b.pay_point_amt s_pay_point_amt,
                b.prepay_ota_amt s_prepay_ota_amt, b.prepay_cash_amt s_prepay_cash_amt, b.prepay_card_amt s_prepay_card_amt, b.prepay_point_amt s_prepay_point_amt,
                b.memo s_memo, b.car_no s_car_no, b.comment s_comment,
                c.name room_name, d.sale, e.name user_name, p.name place_name, p.addr as place_addr, p.tel as place_tel, p.longitude, p.latitude
             FROM room_sale_pay a
             INNER JOIN room_sale b ON a.sale_id = b.id
             INNER JOIN room c ON b.room_id = c.id
             INNER JOIN place p ON c.place_id = p.id
             INNER JOIN room_state d ON b.room_id = d.room_id
              LEFT JOIN user e ON a.user_id = e.id
            WHERE a.id = ? AND a.use_yn = 'Y';`,
          id,
          result
        );
      }
    });
  },

  selectIsRoomSalePayNow: (room_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, c.name room_name, d.sale, e.name user_name
             FROM room_sale_pay a
              INNER JOIN room_sale b ON a.sale_id = b.id
              INNER JOIN room c ON b.room_id = c.id
              INNER JOIN room_state d ON b.room_id = d.room_id
               LEFT JOIN user e ON a.user_id = e.id
            WHERE b.room_id = ? AND b.stay_type > 0 AND b.state = 'A' AND a.use_yn = 'Y' ;`,
          [room_id],
          result
        );
      }
    });
  },

  selectPlaceRoomSalePay: (place_id, begin, end, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT c.place_id, COUNT(*) count, SUM(a.default_fee) as default_fee, SUM(a.add_fee)  as add_fee,
                  SUM(a.pay_cash_amt) as pay_cash_amt, SUM(a.pay_card_amt) as pay_card_amt, SUM(a.pay_point_amt) as pay_point_amt
             FROM room_sale_pay a
              INNER JOIN room_sale b ON a.sale_id = b.id
              INNER JOIN room c ON b.room_id = c.id
            WHERE c.place_id = ? AND b.state != 'B'
              AND date_format(reg_date, '%Y-%m-%d %H:%i') >= date_format(?,  '%Y-%m-%d %H:%i')
              AND date_format(reg_date, '%Y-%m-%d %H:%i') <= date_format(?,  '%Y-%m-%d %H:%i')
              AND a.use_yn = 'Y'
            GROUP BY c.place_id, b.stay_type WITH ROLLUP; `,
          [place_id, begin, end],
          result
        );
      }
    });
  },

  selectRoomSalePaySumAll: (begin, end, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT c.place_id, COUNT(*) count, SUM(a.default_fee) a.default_fee, SUM(a.add_fee) a.add_fee,
                  SUM(a.pay_cash_amt) a.pay_cash_amt, SUM(a.pay_card_amt) a.pay_card_amt, SUM(a.pay_point_amt) a.pay_point_amt
             FROM room_sale_pay a
              INNER JOIN room_sale b ON a.sale_id = b.id
              INNER JOIN room c ON b.room_id = c.id
            WHERE b.state != 'B'
              AND date_format(reg_date, '%Y-%m-%d %H:%i') >= date_format(?,  '%Y-%m-%d %H:%i')
              AND date_format(reg_date, '%Y-%m-%d %H:%i') <= date_format(?,  '%Y-%m-%d %H:%i')
              AND a.use_yn = 'Y'
            GROUP BY c.place_id, b.stay_type WITH ROLLUP; `,
          [begin, end],
          result
        );
      }
    });
  },

  selectRoomSalePaySum: (room_id, begin, end, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT c.place_id, COUNT(*) count, SUM(a.default_fee) a.default_fee, SUM(a.add_fee) a.add_fee,
                  SUM(a.pay_cash_amt) a.pay_cash_amt, SUM(a.pay_card_amt) a.pay_card_amt, SUM(a.pay_point_amt) a.pay_point_amt
             FROM room_sale_pay a
              INNER JOIN room_sale b ON a.sale_id = b.id
              INNER JOIN room c ON b.room_id = c.id
            WHERE b.room_id = ? AND b.state != 'B'
              AND date_format(reg_date, '%Y-%m-%d %H:%i') >= date_format(?,  '%Y-%m-%d %H:%i')
              AND date_format(reg_date, '%Y-%m-%d %H:%i') <= date_format(?,  '%Y-%m-%d %H:%i')
              AND a.use_yn = 'Y'
            GROUP BY b.room_id, b.stay_type WITH ROLLUP; `,
          [room_id, begin, end],
          result
        );
      }
    });
  },

  insertRoomSalePay: (newRoomSalePay, result) => {
    const _newRoomSalePay = _.cloneDeep(newRoomSalePay);

    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`INSERT INTO room_sale_pay SET ? ;`, [newRoomSalePay, newRoomSalePay], result);
      }
    });
  },

  updateRoomSalePay: (id, roomSalePay, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`UPDATE room_sale_pay SET ? WHERE id = ? ;`, [roomSalePay, id], result);
      }
    });
  },

  updateRoomSalePayByMemberId: (member_id, roomSalePay, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`UPDATE room_sale_pay SET ? WHERE member_id = ? ;`, [roomSalePay, member_id], result);
      }
    });
  },

  deleteRoomSalesPayAfterKeepDay: (place_id, date, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `DELETE a FROM room_sale_pay a INNER JOIN room_sale s ON a.sale_id = s.id INNER JOIN room b ON s.room_id = b.id
            WHERE b.place_id = ? AND a.reg_date < STR_TO_DATE(?, '%Y-%m-%d %H:%i'); `,
          [place_id, date],
          result
        );
      }
    });
  },

  deleteRoomSalePay: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`DELETE FROM room_sale_pay WHERE id = ? ;`, [id], result);
      }
    });
  },
};

export default RoomSalePay;
