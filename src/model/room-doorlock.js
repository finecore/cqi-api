import connection from "../db/connection.js";

// RoomDoorLock object constructor
const RoomDoorLock = {
  selectAllRoomDoorLocks: (place_id, filter, order, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name room_name, c.name room_type_name, d.id place_id, d.name place_name, d.tel place_tel
           FROM room_doorlock a LEFT JOIN room b ON a.room_id = b.id LEFT JOIN room_type c ON b.room_type_id = c.id LEFT JOIN place d ON b.place_id = d.id
           WHERE b.place_id = ? AND ${filter} ORDER BY ${order} LIMIT 1000;`,
          [place_id],
          result
        );
      }
    });
  },

  selectRoomDoorLocks: (filter, between, begin, end, order, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name room_name, c.name room_type_name, d.id place_id, d.name place_name, d.tel place_tel
           FROM room_doorlock a LEFT JOIN room b ON a.room_id = b.id LEFT JOIN room_type c ON b.room_type_id = c.id LEFT JOIN place d ON b.place_id = d.id
           WHERE ${filter} AND ${between} BETWEEN ? AND ? ORDER BY ${order} LIMIT 1000;`,
          [begin, end],
          result
        );
      }
    });
  },

  selectRoomDoorLock: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name room_name, c.name room_type_name, d.id place_id, d.name place_name, d.tel place_tel
           FROM room_doorlock a LEFT JOIN room b ON a.room_id = b.id LEFT JOIN room_type c ON b.room_type_id = c.id LEFT JOIN place d ON b.place_id = d.id
           WHERE a.id = ? `,
          [id],
          result
        );
      }
    });
  },

  selectRoomDoorLockByRoomId: (room_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name room_name, c.name room_type_name, d.id place_id, d.name place_name, d.tel place_tel
           FROM room_doorlock a LEFT JOIN room b ON a.room_id = b.id LEFT JOIN room_type c ON b.room_type_id = c.id LEFT JOIN place d ON b.place_id = d.id
           WHERE a.room_id = ? `,
          [room_id],
          result
        );
      }
    });
  },

  insertRoomDoorLock: (roomDoorLock, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO room_doorlock SET ? ON DUPLICATE KEY UPDATE ? ", [roomDoorLock, roomDoorLock], result);
      }
    });
  },

  updateRoomDoorLock: (id, roomDoorLock, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE room_doorlock SET ? WHERE id = ?", [roomDoorLock, id], result);
      }
    });
  },

  updateRoomDoorLockByRoomId: (room_id, roomDoorLock, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE room_doorlock SET ? WHERE room_id = ?", [roomDoorLock, room_id], result);
      }
    });
  },

  deleteRoomDoorLock: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM room_doorlock WHERE id = ? ", [id], result);
      }
    });
  },

  deleteRoomDoorLockByRoomId: (room_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM room_doorlock WHERE room_id = ? ", [room_id], result);
      }
    });
  },
};

export default RoomDoorLock;
