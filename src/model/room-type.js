import connection from "../db/connection.js";

// RoomType object constructor
const RoomType = {
  selectAllRoomTypes: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT * FROM room_type WHERE place_id = ? ORDER BY default_fee_stay, name", [place_id], result);
      }
    });
  },

  selectRoomType: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT * FROM room_type WHERE id = ? ", id, result);
      }
    });
  },

  selectRoomTypeByName: (place_id, name, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT * FROM room_type WHERE place_id = ?
          AND (name = ? OR replace(name,' ','') = ? OR replace(ota_name_1,' ','') = ? OR replace(ota_name_2,' ','') = ? OR replace(ota_name_3,' ','') = ? OR replace(ota_name_4,' ','') = ? OR replace(ota_name_5,' ','') = ?) `,
          [place_id, name, name, name, name, name],
          result
        );
      }
    });
  },

  insertRoomType: (newRoomType, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO room_type SET ? ON DUPLICATE KEY UPDATE ? ", [newRoomType, newRoomType], result);
      }
    });
  },

  updateRoomType: (id, roomType, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE room_type SET ? WHERE id = ?", [roomType, id], result);
      }
    });
  },

  deleteRoomType: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM room_type WHERE id = ? ", [id], result);
      }
    });
  },

  deleteAllRoomTypes: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM room_type WHERE place_id = ? ", [place_id], result);
      }
    });
  },
};

export default RoomType;
