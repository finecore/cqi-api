import connection from "../db/connection.js";

// Company object constructor
const Company = {
  selectCompanyCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`SELECT count(*) count FROM company a WHERE ${filter} `, [], result);
      }
    });
  },

  selectCompanys: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`SELECT * FROM company a WHERE ${filter} ORDER BY ${order} ${desc} LIMIT ${limit} `, [], result);
      }
    });
  },

  selectCompany: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT * FROM company a WHERE id = ? ", id, result);
      }
    });
  },

  insertCompany: (newCompany, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO company SET ? ON DUPLICATE KEY UPDATE ? ", [newCompany, newCompany], result);
      }
    });
  },

  updateCompany: (id, company, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE company a SET ? WHERE id = ?", [company, id], result);
      }
    });
  },

  deleteCompany: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM company a WHERE id = ?", [id], result);
      }
    });
  },
};

export default Company;
