import connection from "../db/connection.js";
import _ from "lodash";

// AppInfo object constructor
const AppInfo = {
  selectAppInfoCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count
             FROM app_info a INNER JOIN user b ON a.user_id = b.id
           WHERE ${filter} `,
          [],
          result
        );
      }
    });
  },

  selectAppInfos: (filter, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name
           FROM app_info a INNER JOIN user b ON a.user_id = b.id
           WHERE ${filter} LIMIT ${limit} `,
          [],
          result
        );
      }
    });
  },

  selectPlaceAppInfos: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name
           FROM app_info a INNER JOIN user b ON a.user_id = b.id
           WHERE b.place_id = ? `,
          [place_id],
          result
        );
      }
    });
  },

  selectAppInfo: (token, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name
           FROM app_info a INNER JOIN user b ON a.user_id = b.id  WHERE a.token = ? `,
          token,
          result
        );
      }
    });
  },

  selectAppInfoOnly: (token, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`SELECT * FROM app_info WHERE token = ? `, token, result);
      }
    });
  },

  insertAppInfo: (app_info, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO app_info SET ?  ON DUPLICATE KEY UPDATE ? ", [app_info, app_info], (err, info) => {
          result(err, info);
        });
      }
    });
  },

  updateAppInfo: (token, app_info, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE app_info SET ? WHERE token = ? ", [app_info, token], (err, info) => {
          result(err, info);
        });
      }
    });
  },

  deleteAppInfo: (token, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM app_info WHERE token = ? ", [token], result);
      }
    });
  },
};

export default AppInfo;
