import connection from "../db/connection.js";

// MemberPreference object constructor
const MemberPreference = {
  selectMemberPreferencesCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count
             FROM member_preferences a
            WHERE ${filter} `,
          [],
          result
        );
      }
    });
  },

  selectMemberPreferences: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*
           FROM member_preferences a
           WHERE ${filter}
           ORDER BY ${order} ${desc}
           LIMIT ${limit} `,
          [],
          result
        );
      }
    });
  },

  selectMemberPreference: (member_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*
           FROM member_preferences a
            WHERE a.member_id = ?  `,
          [member_id],
          result
        );
      }
    });
  },

  insertMemberPreference: (member_preferences, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO member_preferences SET ? ON DUPLICATE KEY UPDATE ? ", [member_preferences, member_preferences], result);
      }
    });
  },

  updateMemberPreference: (member_id, member_preferences, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE member_preferences SET ? WHERE member_id = ?", [member_preferences, member_id], result);
      }
    });
  },

  deleteMemberPreference: (member_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM member_preferences WHERE member_id = ?", [member_id], result);
      }
    });
  },
};

export default MemberPreference;
