import connection from '../db/connection.js';

// MemberPoint object constructor
const MemberPoint = {
  selectMemberPointCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count
             FROM member_point a INNER JOIN place b ON a.place_id = b.id LEFT JOIN member c ON a.member_id = c.id
            WHERE ${filter} `,
          [],
          result,
        );
      }
    });
  },

  selectMemberPoints: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name as place_name, c.name as member_name
           FROM member_point a INNER JOIN place b ON a.place_id = b.id
                          LEFT JOIN member c ON a.member_id = c.id
           WHERE ${filter}
           ORDER BY ${order} ${desc}
           LIMIT ${limit} `,
          [],
          result,
        );
      }
    });
  },

  selectPlaceMemberPoints: (place_id, member_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name as place_name, c.name as member_name
           FROM member_point a INNER JOIN place b ON a.place_id = b.id
                          LEFT JOIN member c ON a.member_id = c.id
            WHERE a.place_id = ? AND a.member_id = ? `,
          [place_id, member_id],
          result,
        );
      }
    });
  },

  selectMemberMemberPoints: (member_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name as place_name, c.name as member_name
           FROM member_point a INNER JOIN place b ON a.place_id = b.id
                          LEFT JOIN member c ON a.member_id = c.id
           WHERE a.member_id = ? `,
          [member_id],
          result,
        );
      }
    });
  },

  selectMemberPoint: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name as place_name, c.name as member_name
           FROM member_point a INNER JOIN place b ON a.place_id = b.id
                          LEFT JOIN member c ON a.member_id = c.id
            WHERE a.id = ?  `,
          [id],
          result,
        );
      }
    });
  },

  insertMemberPoint: (member_point, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          'INSERT INTO member_point SET ? ',
          [member_point],
          result,
        );
      }
    });
  },

  updateMemberPoint: (id, member_point, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          'UPDATE member_point SET ? WHERE id = ?',
          [member_point, id],
          result,
        );
      }
    });
  },

  updatePlaceMemberPointByPhone: (place_id, member_point, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          'UPDATE member_point SET ? WHERE place_id = ? ?',
          [member_point, place_id],
          result,
        );
      }
    });
  },

  increaseMemberPoint: (id, point, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `UPDATE member_point SET point = ${point} WHERE id = ? `,
          [id],
          result,
        );
      }
    });
  },

  decreaseMemberPoint: (id, point, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `UPDATE member_point SET point =  ${point} WHERE id = ? `,
          [id],
          result,
        );
      }
    });
  },

  deleteMemberPoint: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql('DELETE FROM member_point WHERE id = ?', [id], result);
      }
    });
  },

  deletePlaceMemberPoints: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          'DELETE FROM member_point WHERE place_id = ?',
          [place_id],
          result,
        );
      }
    });
  },
};

export default MemberPoint;
