import moment from "moment";
//import _ from "lodash";

class ExcelUtil {
  constructor(id, name) {
    this.id = id;
    this.name = name || id;

    console.log("- ExcelUtil", id, name);
  }

  tableToExcel(table) {
    console.log("- ExcelUtil tableToExcel", this.id, this.name);

    var data_type = "data:application/vnd.ms-excel;charset=utf-8";
    var table_html = encodeURIComponent(table);

    var aId = "excel_a_" + this.id;

    var a = `<a `;
    a += ` id="excel_a_${this.id}"`;
    a += ` href="${data_type + ",%EF%BB%BF" + table_html}"`;
    a += ` download="${this.name}"`;
    a += `>${this.name}</a>`;
    return a;
  }
}

export default ExcelUtil;
