import _ from "lodash";
import moment from "moment";
import axios from "axios";
import jquery from "jquery";

class YYDoorLock {
  constructor() {
    console.log("-> YYDoorLock constructor");

    this.URL = "https://yylock.eeun.cn/dms/app";
    this.APPKEY = "0A615734763B4D1D8F8C82E7B5861CDF"; // 앱 개발자 키(DMS 홈페이지에 로그인 해서 체크)
  }

  orderedQuery(params) {
    console.log("- YYDoorLock orderedQuery");

    // 파라메터 명 정렬.
    const ordered = Object.keys(params)
      .sort()
      .reduce((obj, key) => {
        obj[key] = String(params[key]);
        return obj;
      }, {});

    console.log("- YYDoorLock call ordered", ordered);

    // return $.param(ordered).toUpperCase();

    var str = [];

    for (var p in ordered) {
      if (ordered.hasOwnProperty(p)) str.push(encodeURIComponent(p) + "=" + encodeURIComponent(ordered[p]));
    }

    return str.join("&").toUpperCase();
  }

  async call(path, query) {
    console.log("- YYDoorLock orderedQuery");
    // console.log("-> YYDoorLock call", { path, query });

    let url = `${this.URL}/${path}?${query}`;

    console.log("-> YYDoorLock call url", url);

    // POST 전송 한다.
    let promise = await axios
      .post(url)
      .then((res) => res.data || [])
      .then((data) => {
        console.log("-> YYDoorLock call res", data);
        return data;
      })
      .catch((error) => {
        console.info("- YYDoorLock call error", error);
        return {};
      });

    return promise;
  }

  async login(query) {
    console.log("- YYDoorLock login ", query);

    return await this.call("dmsLogin", query, false).then((data) => {
      return data || {};
    });
  }

  // QR 코드 발급 / 취소(취소는 STARTDATE 를 - 시간으로 설정하고 ENDDATE 를 1분으로 설정 함)
  async getLockQRCode(query) {
    console.log("- YYDoorLock getLockQRCode ", query);

    return await this.call("getLockQRCode", query, true).then((data) => {
      return data || {};
    });
  }

  // checkout.(사용 못함!)
  async sysLockTime(query) {
    console.log("- YYDoorLock sysLockTime ", query);

    return await this.call("sysLockTime", query, true).then((data) => {
      return data || {};
    });
  }
}

export { YYDoorLock };
