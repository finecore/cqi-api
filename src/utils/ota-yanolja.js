import moment from "moment";
import _ from "lodash";
import chalk from "chalk";
import format from "./format-util.js";

import RoomReserv from "../model/room-reserv.js";
import RoomType from "../model/room-type.js";

import { ERROR, PREFERENCES } from "../constants/constants.js";
import { keyToValue, keyCodes } from "../constants/key-map.js";

export const yanolja_today = (ota, reserv, line, place, callback) => {
  console.log(chalk.yellow("- yanolja_today"), line);

  // 0 야놀자 당일예약
  // 1 <숙박>
  // 2 천안 호텔 감
  // 3 2005271916683041
  // 4 디럭스(넷플릭스,웨이브온)
  // 5 50,000원
  // 6 안성민 / 0503-5079-9875
  // 7 2020-05-27(수) 15:00~
  // 8 2020-05-28(목) 14:00 (1박)
  // 9 도보방문

  let stay_type = line[1] ? line[1].match(/(대실|숙박)/) : ["숙박"];

  if (stay_type) {
    console.log(chalk.yellow("- stay_type"), stay_type[0]);
    reserv.stay_type = stay_type[0] === "대실" ? 2 : 1;
  }

  let reserv_num = line[3] ? line[3].match(/([0-9]{16,20})/) : [0];

  if (reserv_num[1]) {
    console.log(chalk.yellow("- reserv_num"), reserv_num);
    reserv.reserv_num = Number(reserv_num[0]);
  }

  let prepay_ota_amt = line[5] ? line[5].match(/([0-9,?]+)(원|만원)+/) : [0];

  if (prepay_ota_amt) {
    console.log(chalk.yellow("- prepay_ota_amt"), prepay_ota_amt[0]);
    reserv.prepay_ota_amt = Number(prepay_ota_amt[0].replace(/[^\d]/g, ""));
  }

  // 고객명 / 전화번호
  let customer = line[6] ? line[6].split(/\//) : null;
  if (customer) {
    console.log(chalk.yellow("- customer"), customer);
    reserv.name = _.trim(customer[0]);
    reserv.hp = _.trim(customer[1]);
  }

  let date = line[7] ? line[7].match(/(\d{4})-?(\d{1,2})-?(\d{1,2})\((?:.){1}\)(\d{1,2}):(\d{1,2})/) : null;
  if (date) {
    console.log(chalk.yellow("- check_in_exp"), date);
    reserv.check_in_exp = `${date[1]}-${date[2]}-${date[3]} ${date[4]}:${date[5]}`;
  }

  date = line[8] ? line[8].match(/(\d{4})-?(\d{1,2})-?(\d{1,2})\((?:.){1}\)(\d{1,2}):(\d{1,2})/) : null;
  if (date) {
    console.log(chalk.yellow("- check_out_exp"), date);
    reserv.check_out_exp = `${date[1]}-${date[2]}-${date[3]} ${date[4]}:${date[5]}`;
  }

  let visit_type = line[9] ? line[9].match(/(도보|차량)/) : ["도보"];
  if (visit_type) {
    console.log(chalk.yellow("- visit_type"), visit_type[0]);
    reserv.visit_type = visit_type[0] === "도보" ? 1 : 2;
  }

  if (reserv.place_id && line[4]) {
    let typeName = line[4];

    // 한글,영문,숫자만
    var regexp = new RegExp(/[^ㄱ-ㅎ|가-힣|a-z|0-9|]/gi);

    typeName = typeName.replace(regexp, "");

    console.log(chalk.yellow("- typeName"), typeName);

    ota.getRoomTypes(reserv.place_id, (roomTypes) => {
      let roomType = _.findLast(roomTypes, (v) => {
        let name = v.name.replace(regexp, "");
        let ota_name_1 = v.ota_name_1 ? v.ota_name_1.replace(regexp, "") : "";
        let ota_name_2 = v.ota_name_2 ? v.ota_name_2.replace(regexp, "") : "";
        let ota_name_3 = v.ota_name_3 ? v.ota_name_3.replace(regexp, "") : "";
        let ota_name_4 = v.ota_name_4 ? v.ota_name_4.replace(regexp, "") : "";
        let ota_name_5 = v.ota_name_5 ? v.ota_name_5.replace(regexp, "") : "";

        return name === typeName || ota_name_1 === typeName || ota_name_2 === typeName || ota_name_3 === typeName || ota_name_4 === typeName || ota_name_5 === typeName;
      });

      if (!roomType && process.env.MODE !== "PROD") {
        typeName = roomTypes[Math.floor(Math.random() * roomTypes.length)];
        console.log(chalk.red("- TEST 객실 타입 전환"), typeName);
      }

      console.log(chalk.yellow("- roomType"), roomType ? roomType.name : "no room type");

      if (roomType) {
        let { id, name, default_fee_stay, default_fee_rent, reserv_discount_fee_stay, reserv_discount_fee_rent } = roomType;

        reserv.room_type_id = id;
        reserv.room_type_name = name;

        ota.setRoomFee(reserv, (add_fee) => {
          let room_fee = reserv.stay_type === 2 ? default_fee_rent : default_fee_stay; //  기본 요금
          let reserv_fee = reserv.stay_type === 2 ? default_fee_rent - reserv_discount_fee_rent : default_fee_stay - reserv_discount_fee_stay; // 예약 할인 요금

          // 요일/시간별 추가/할인 요금 적용.
          room_fee += add_fee;

          if (reserv.prepay_ota_amt > 0) {
            reserv_fee = reserv.prepay_ota_amt; // OTA 결제 시 결제 요금을 예약 가로 설정 한다.(잔금 발생 방지)
          } else {
            reserv_fee += add_fee; // OTA 결제 시 옵션 추가 요금은 적용 하지 않는다.
          }

          reserv.room_fee = room_fee;
          reserv.reserv_fee = reserv_fee;
          if (reserv.stat === "A") reserv.memo = line[4]; // 객실 타입 정보 상세 추가.

          callback({ reserv });
        });
      } else {
        callback({ reserv });
      }
    });
  } else {
    callback({ reserv });
  }
};

export const yanolja_fix = (ota, reserv, line, place, callback) => {
  console.log(chalk.yellow("- yanolja_fix"), line);

  // 2022.07.27
  // 0 야놀자<오늘입실할미리예약>
  // 1 월곶더휴식이로호텔
  // 2 오늘07-27(수)입실할미리예약
  // 3 총4건(대실2건,숙박2건,연박0건)
  // 4 ▶대실1/2
  // 5 2207261831170145
  // 6 도보특가(주차불가65"UHD스마트...
  // 7 18,000원
  // 8 신민상/050350787153
  // 9 2022-07-27(수)12:0020:00(8시간)
  // 10 도보방문
  //  ▶대실2/2
  //  2207262257010158
  //  DELUXE펫룸(애견동반65"UH...
  //  30,000원
  //  안성민/050373383685
  //  2022-07-27(수)13:0020:00(7시간)
  //  차량방문
  //  ▶숙박1/2
  //  2207261917020011
  //  도보특가(주차불가65"UHD스마트...
  //  33,000원
  //  곽주영/050351097853
  //  2022-07-27(수)16:00
  //  2022-07-28(목)13:00(1박)
  //  도보방문
  //  ▶숙박2/2
  //  2207270133590058
  //  도보특가(주차불가65"UHD스마트...
  //  33,000원
  //  김덕한/050373304716
  //  2022-07-27(수)16:00
  //  2022-07-28(목)13:00(1박)
  //  도보방문
  //  총4건목록끝
  //  상세내용확인:https://yaapp.page.link/6XLQ
  //  야놀자고객센터:1644-1346

  let name = line[1];
  let stays = [];
  let idx = 0;
  let type = 0;

  _.each(line, (v, k) => {
    if (/(▶숙박)/.test(v)) {
      stays[idx] = ["야놀자 미리예약", "<숙박>", name];
      stays[idx] = stays[idx].concat(line.slice(k + 1, k + 8));
      idx++;
    } else if (/(▶대실)/.test(v)) {
      stays[idx] = ["야놀자 미리예약", "<대실>", name];
      stays[idx] = stays[idx].concat(line.slice(k + 1, k + 5));

      let date = line[k + 5].match(/(\d{4}-?\d{1,2}-?\d{1,2}\(.{1}\)\d{2,2}:\d{2,2})(~?)(\d{2,2}:\d{2,2}.+)/);

      console.log(chalk.yellow("- date"), date);

      stays[idx].push(date[1]);
      stays[idx].push(date[1].replace(/(\d{4}-?\d{1,2}-?\d{1,2}\(.{1}\))/, "$1" + date[2]));
      idx++;
    } else if (/(▶연박)/.test(v)) {
      stays[idx] = ["야놀자 미리예약", "<숙박>", name];
      stays[idx] = stays[idx].concat(line.slice(k + 1, k + 8));
      idx++;
    }
  });

  console.log(chalk.yellow("- stays"), stays);

  let reservs = [];

  console.log(chalk.blue("- stays length"), stays.length);

  _.each(stays, (line, k) => {
    // reserv clone 해서 각각의 정보를 담는다.
    yanolja_today(ota, _.cloneDeep(reserv), line, place, ({ reserv }) => {
      console.log(chalk.yellow("- reserv"), k, reserv);

      reservs.push({ reserv });

      if (reservs.length === stays.length) {
        console.log(chalk.blue("- reservs length"), reservs.length);
        callback(reservs);
      }
    });
  });
};
