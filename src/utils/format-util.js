import _ from "lodash";
import hangul from "hangul-js"; // default export로 가져오기
import moment from "moment";
const { Hangul } = hangul; // Hangul을 추출

const format = {
  /**
   * 기본 포멧.
   *
   * @param type (rrno, phone, post)
   * @param text
   * @return
   */
  do: (type, text, delimiter) => {
    var formatTxt = text;
    delimiter = delimiter || "-";

    if (type === "rrno") formatTxt = format.toRrno(text, delimiter);
    else if (type === "phone") formatTxt = format.toPhone(text, delimiter);
    else if (type === "post") formatTxt = format.toPost(text, delimiter);
    else if (type === "date") formatTxt = format.toDate(text, delimiter);
    else if (type === "datetime") formatTxt = format.toDateTime(text, delimiter);
    else if (type === "acct") formatTxt = format.toAccount(text, delimiter);
    else if (type === "card") formatTxt = format.toCard(text, delimiter);
    else if (type === "money") formatTxt = format.toMoney(text);
    else {
      console.log("- format : " + type + " delimiter : " + delimiter);

      var ts = type.match(/\(.*?\)/g);

      // console.log('- ts : ' + ts);

      var pattern = "";
      var replace = "";

      var txt = formatTxt.replace(new RegExp("[" + delimiter + "]", "g"), "");

      for (var n in ts) {
        pattern += ts[n];
        replace += "$" + (Number(n) + 1) + (Number(n) < ts.length - 1 ? delimiter : "");

        var re = new RegExp(pattern, "g");

        if (txt.match(re) && txt.match(re).length === 1) {
          // console.log('- txt : ' + txt + ' re : ' + re + ' replace : ' + replace + ' test : ' + txt.match(re));
          formatTxt = txt.replace(re, replace); // RegExp.$1
          // console.log('- formatTxt : ' + formatTxt);
        }
      }
    }

    return formatTxt;
  },
  /**
   * Null to Void
   *
   * @param data
   * @returns
   */
  nullToVoid: (data, name) => {
    if (data) {
      try {
        if (!data[name] || data[name] === "null" || data[name] === "undefined") data[name] = "";
      } catch (e) {
        data[name] = "";
      }
      return data[name];
    }
    return "";
  },
  /**
   * json object -> array
   *
   * @param object
   * @return
   */
  JSONtoArray: (object) => {
    var results = [];
    for (var property in object) {
      var value = object[property];
      if (value) results.push(property.toString() + ":" + value);
    }
    return results;
  },
  /**
   * array 에서 name 값 반환.
   *
   * @param array ({name=value,name=value...} 형식의 1차원 배열.)
   * @param dilimiter ±¸ºÐÀÚ
   * @return
   */
  ArrayToJSON: (array, dilimiter) => {
    if (dilimiter === undefined) dilimiter = "=";
    var sub = {};
    for (var inx in array) {
      var data = array[inx].split(dilimiter);
      if (data.length > 1) {
        sub[_.trim(data[0])] = _.trim(data[1]);
      }
    }
    return sub;
  },
  /**
   * 배열 정렬
   */
  sorted: (list, column, order, name) => {
    console.log("-- sorted", { list, column, order, name });

    if (!list) return [];

    let sorted = column.slice();

    delete sorted[name];
    sorted.splice(0, 0, name);

    order[name] = !order[name];

    console.log("-", { sorted, order });

    list = _.orderBy(list, sorted, order);

    return list;
  },
  /**
   * html 제거.
   */
  stripTags: (str) => {
    var RegExpTag = /[<][^>]*[>]/gi;
    str = str.replace(RegExpTag, "");

    var RegExpJS = "<script[^>]*>(.*?)</script>";
    str = str.replace(RegExpJS, "");

    var RegExpCSS = "<style[^>]*>(.*?)";
    str = str.replace(RegExpCSS, "");

    var RegExpDS = /<!--[^>](.*?)-->/gi;
    str = str.replace(RegExpDS, "");

    var RegExpPh = /document.|object|cookie|&/gi;
    str = str.replace(RegExpPh, "");

    return str;
  },
  /**
   * 숫자 컴마 추가.
   *
   * @param numString
   */
  formatCommas: (numString) => {
    if (!numString) return "";

    numString = numString + "";

    var re = /,|\s+/g;
    var retNumString = numString.replace(re, "");

    re = /(-?\d+)(\d{3})/;
    while (re.test(retNumString)) retNumString = retNumString.replace(re, "$1,$2");

    return retNumString;
  },
  /**
   * 컴마 제거.
   *
   * @param {String} numString
   */
  stripCommas: (numString) => {
    return numString ? format.replaceAll(numString, ",", "") : "";
  },
  /**
   * 숫자형으로 변환.
   *
   * @param {String} numStr
   */
  toNumber: (numStr) => {
    if (!numStr) return "";

    numStr = numStr + "";

    var m = numStr.substring(0, 1) === "-";
    var ret = numStr.replace(/[^\d]/g, "");

    return Number(m ? "-" + ret : ret);
  },
  /**
   * 실수형으로 변환.
   *
   * @param {String} numStr
   * @param {int} point 소숫점 자릿수.
   */
  toFloat: (numStr, point) => {
    if (numStr === undefined) numStr = "";
    numStr = numStr + "";
    if (numStr === "") return 0;
    if (point === undefined || point === "") point = 2;

    var data = numStr.split(".");
    data[0] = parseInt(format.stripCommas(data[0]), 10);
    data[1] = format.stripCommas(data[1]) + "000000000000";
    data[1] = data[1].substring(0, point);
    return parseFloat(data[0] + "." + data[1]);
  },
  /**
   * 날자 형식으로 변환.
   *
   * @param dateStr
   * @param se
   * @returns
   */
  toDateTime: (dateStr, se = "-") => {
    var dateStrNoDash = format.toNumber(dateStr);
    var date = format.toDate(dateStrNoDash.substring(0, 8), se);
    var time = format.toTime(dateStrNoDash.substring(8), ":");
    return date + " " + time;
  },
  /**
   * 주민번호 형식으로 변환.
   *
   * @param num
   * @param se
   * @returns
   */
  toRrno: (num, se = "-") => {
    if (num) return num.replace(/[^\d]/g, "").replace(/(\d{6})(\d+)/g, "$1-$2");
    else return "";
  },
  /**
   * 전화번호 형식으로 변환.
   *
   * @param num
   * @param se
   * @returns
   */
  toPhone: (num, se = "-") => {
    if (num) return num.replace(/[^\d]/g, "").replace(/(^02.{0}|^01.{1}|[0-9]{3})([0-9]+)([0-9]{4})/, "$1" + se + "$2" + se + "$3");
    else return "";
  },
  /**
   * 계좌번호 형식으로 변환.
   *
   * @param num
   * @param se
   * @returns
   */
  toAccount: (num, se = "-") => {
    if (num && num !== "") {
      num = num.replace(/[^\d]/g, "");
      if (num.length > 11) {
        num = num.replace(/([0-9]{3})([0-9]{5})([0-9]{3})([0-9]+)/, "$1" + se + "$2" + se + "$3" + se + "$4");
      } else if (num.length > 8) {
        num = num.replace(/([0-9]{3})([0-9]{5})([0-9]+)/, "$1" + se + "$2" + se + "$3");
      } else if (num.length > 3) {
        num = num.replace(/([0-9]{3})([0-9]+)/, "$1" + se + "$2");
      }
      return num;
    } else return "";
  },
  /**
   * 카드번호 형식으로 변환.
   *
   * @param num
   * @param se
   * @returns
   */
  toCard: (num, se = "-") => {
    let card = String(num || "");

    card = format.toNumber(card).replace(/([0-9]{4})([0-9]{4})([0-9]{4})([0-9]+)/, "$1" + se + "$2" + se + "$3" + se + "$4");

    console.log("- toCard", num, card, se);

    return card;
  },
  /**
   * 날자형식으로 변환.
   *
   * @param dateStr
   * @param se
   * @returns
   */
  toDate: (dateStr, se = "-") => {
    var re = "";
    var replace = "";

    var dateStrNoDash = format.toNumber(dateStr);

    if (dateStrNoDash.length === 4) {
      re = /(\d{4})/;
      replace = "$1";
    } else if (dateStrNoDash.length === 5) {
      re = /(\d{4})/;
      replace = "$1" + se;
    } else if (dateStrNoDash.length === 6) {
      re = /(\d{4})(\d{2})/;
      replace = "$1" + se + "$2";
    } else if (dateStrNoDash.length === 7) {
      re = /(\d{4})(\d{2})/;
      replace = "$1" + se + "$2" + se;
    } else {
      re = /(\d{4})(\d{2})(\d{2})/;
      replace = "$1" + se + "$2" + se + "$3";
    }

    return dateStrNoDash.replace(re, replace);
  },
  /**
   * 시분초 현식으로 변환.
   *
   * @param time
   * @param se
   * @returns
   */
  toTime: (time, se = "-") => {
    if (time)
      if (time.length === 6) return format.toNumber(time).replace(/(\d{2})(\d{2})(\d{2})/, "$1" + se + "$2" + se + "$3");
      else return format.toNumber(time).replace(/(\d{2})(\d{2})(\d{2})(\d{3})/, "$1" + se + "$2" + se + "$3.$4");
    else return "";
  },
  /**
   * 금액 형식으로 변환.
   *
   * @param numStr
   */
  toMoney: (numStr, min, max) => {
    if (!numStr) return "0";

    numStr = numStr.toString();

    var m = numStr.substring(0, 1) === "-";

    numStr = Number(numStr.replace(/[^\d]/g, ""));

    if (min !== undefined && numStr < min) numStr = min;
    if (max !== undefined && numStr > max) numStr = max;

    numStr = format.formatCommas(numStr) || 0;

    return m ? "-" + numStr : numStr;
  },

  /**
   * 휴대폰 번호 유효성 체크
   *
   * @param phone
   */
  isPhone: (phone) => {
    phone = phone.replace(/[^\d]/g, "");

    // 01로 시작하는 핸드폰 및 지역번호와 050, 070 검증함. 그리고 -(하이픈)은 넣어도 되고 생략해도 되나 넣을 때에는 정확한 위치에 넣어야 함.
    var regExp = /^(01[0156789]{1}|05[0-9]{2}|02|0[3-9]{1}[0-9]{1})-?[0-9]{3,5}-?[0-9]{4}$/;
    return regExp.test(phone);
  },

  /**
   * 이메일 주소 유효성 체크
   *
   * @param email
   */
  isEmail: (email) => {
    email = email.replace(/[^\d]/g, "");

    var regExp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return regExp.test(email);
  },

  /**
   * 문자 배열 검식.
   *
   * @param data
   * @param text
   * @param key
   */
  textFilter: (data, text, key) => {
    let re = /[-,\s]/gi;

    text = String(text || "").replace(re, "");

    let list = text
      ? _.filter(data, (item) => {
          let isFind = false;

          _.each(item, (v) => {
            let value = String((key ? v[key] : v) || "").replace(re, "");
            if (value) {
              if (Hangul.search(value, text) > -1) {
                isFind = true;
                return false;
              }
            }
          });
          return isFind;
        })
      : data;

    return list;
  },

  /**
   * 종성 여부 체크.
   *
   * @param {String} wd
   */
  isJongsong: (wd) => {
    var INDETERMINATE = 0;
    var NOJONGSONG = 1;
    var JONGSONG = 2;

    var word = String(wd); /* 숫자 대비해서 문자열로 변환. */
    var numStr1 = "013678lmnLMN";
    var numStr2 = "2459aefhijkoqrsuvwxyzAEFHIJKOQRSUVWXYZ";

    if (word === null || word.length < 1) return INDETERMINATE;

    var lastChar = word.charAt(word.length - 1);
    var lastCharCode = word.charCodeAt(word.length - 1);

    if (numStr1.indexOf(lastChar) > -1) return JONGSONG;
    else if (numStr2.indexOf(lastChar) > -1) return NOJONGSONG;

    if (lastCharCode < 0xac00 || lastCharCode > 0xda0c) {
      return INDETERMINATE;
    } else {
      var lastjongseong = ((lastCharCode - 0xac00) % (21 * 28)) % 28;
      if (lastjongseong === 0) return NOJONGSONG;
      else return JONGSONG;
    }
  },
  /* 내부함수 (을/를) */
  ul: (s) => {
    if (!format.isHangul(s)) return s;
    var ul0 = ["(을)를", "를", "을"];
    return s + ul0[format.isJongsong(s)];
  },
  /* 내부함수 (이/가) */
  ka: (s) => {
    if (!format.isHangul(s)) return s;
    var ka0 = ["(이)가", "가", "이"];
    return s + ka0[format.isJongsong(s)];
  },
  /* 내부함수 (은/는) */
  un: (s) => {
    if (!format.isHangul(s)) return s;
    var un0 = ["(은)는", "는", "은"];
    return s + un0[format.isJongsong(s)];
  },
  /* 내부함수 (와/과) */
  wa: (s) => {
    if (!format.isHangul(s)) return s;
    var arr = ["(와)과", "와", "과"];
    return s + arr[format.isJongsong(s)];
  },
  /**
   * 전각문자로 변환.
   *
   * @param : is 변환할 문자열.
   * @param : isAllNum 모든 문자가 숫자인지 여부.
   * @return
   * @see
   */
  toFullChar: (is, isAllNum) => {
    if (!is) return;

    if (isAllNum && !format.isHangul(is)) return is;

    var os = "";
    for (var i = 0; i < is.length; i++) {
      var c = is.charCodeAt(i);
      if (c >= 32 && c <= 126) {
        // 전각으로 변환 될 수 있는 문자의 범위.
        if (c === 32)
          // 스페이스인 경우 ascii 코드 32
          os = os + unescape("%u" + (12288).toString(16));
        else os = os + unescape("%u" + (c + 65248).toString(16));
      } else {
        os = os + is.charAt(i);
      }
    }
    return os;
  },
  /**
   * 전각을 반각으로 변환.
   *
   * @param is
   * @returns
   */
  toHalfChar: (is) => {
    var os = String();
    var len = is.length;
    for (var i = 0; i < len; i++) {
      var c = is.charCodeAt(i);
      if (c >= 65281 && c <= 65374 && c !== 65340) {
        os += String.fromCharCode(c - 65248);
      } else if (c === 8217) {
        os += String.fromCharCode(39);
      } else if (c === 8221) {
        os += String.fromCharCode(34);
      } else if (c === 12288) {
        os += String.fromCharCode(32);
      } else if (c === 65507) {
        os += String.fromCharCode(126);
      } else if (c === 65509) {
        os += String.fromCharCode(92);
      } else {
        os += is.charAt(i);
      }
    }
    return os;
  },

  toUtf8: (s) => {
    var c;
    var d = "";
    for (var i = 0; i < s.length; i++) {
      c = s.charCodeAt(i);
      if (c <= 0x7f) {
        d += s.charAt(i);
      } else if (c >= 0x80 && c <= 0x7ff) {
        d += String.fromCharCode(((c >> 6) & 0x1f) | 0xc0);
        d += String.fromCharCode((c & 0x3f) | 0x80);
      } else {
        d += String.fromCharCode((c >> 12) | 0xe0);
        d += String.fromCharCode(((c >> 6) & 0x3f) | 0x80);
        d += String.fromCharCode((c & 0x3f) | 0x80);
      }
    }
    return d;
  },
  fromUtf8: (s) => {
    var c;
    var d = "";
    var flag = 0;
    var tmp = null;
    for (var i = 0; i < s.length; i++) {
      c = s.charCodeAt(i);
      if (flag === 0) {
        if ((c & 0xe0) === 0xe0) {
          flag = 2;
          tmp = (c & 0x0f) << 12;
        } else if ((c & 0xc0) === 0xc0) {
          flag = 1;
          tmp = (c & 0x1f) << 6;
        } else if ((c & 0x80) === 0) {
          d += s.charAt(i);
        } else {
          flag = 0;
        }
      } else if (flag === 1) {
        flag = 0;
        d += String.fromCharCode(tmp | (c & 0x3f));
      } else if (flag === 2) {
        flag = 3;
        tmp |= (c & 0x3f) << 6;
      } else if (flag === 3) {
        flag = 0;
        d += String.fromCharCode(tmp | (c & 0x3f));
      } else {
        flag = 0;
      }
    }
    return d;
  },
  hexToStr: (hex) => {
    hex = hex.toString(); // force conversion
    var str = "";
    for (var i = 0; i < hex.length; i += 2) str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
    return str;
  },
  convertToUTC: (utcDateString) => {
    // moment 객체를 생성하고 +12시간을 더한 후 원하는 포맷으로 변환
    const localDate = moment.utc(utcDateString).add(12, "hours");

    // 날짜 문자열을 반환 (YYYY-MM-DD HH:mm:ss 형식)
    return localDate.format("YYYY-MM-DD HH:mm:ss");
  },
};

export default format;
