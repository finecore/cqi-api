import chalk from "chalk";
import mysql from "mysql2/promise";
import axios from "axios";
import SyncLog from "../model/sync_log.js";
import ip from "ip";
import { REMOTE_API_HOST_URL } from "../api/config.js";
import { api, ajax } from "../utils/cloud-api-util.js";
import { list, item, post, put, del } from "../api/index.js";
import flatted from "flatted";

class DataSyncManager {
  constructor() {
    console.log(chalk.yellow("----------- DataSyncManager Scheduler Create ----------"));

    this.timer = null;
    this.timer2 = null;
    this.API_ENDPOINT = REMOTE_API_HOST_URL;

    this.ipaddr = ip.address();

    console.log("---- ipaddr", this.ipaddr, process.env.REMOTE_API_HOST);

    // 2초 후 초기화
    setTimeout(() => {
      if (this.ipaddr != process.env.REMOTE_API_HOST) this.init();
    }, 2000);
  }

  // 초기화
  async init() {
    console.log("-----------------------------------");
    console.log("---- DataSyncManager init!");
    console.log("- API_ENDPOINT:", this.API_ENDPOINT);
    console.log("-----------------------------------");

    if (this.timer) clearInterval(this.timer);
    if (this.timer2) clearInterval(this.timer2);

    // 주기적으로 로그 전송
    this.timer = setInterval(() => {
      this.syncPendingLogs();
    }, 3000); // 10초마다 실행

    // 오래된 로그 삭제
    this.timer2 = setInterval(() => {
      this.cleanupOldLogs();
    }, 3600000); // 1시간마다 실행
  }

  // 로그 데이터 추가
  async addLog(data) {
    console.log("- addLog", data);

    const logData = { log_data: JSON.stringify(data) };

    try {
      await SyncLog.insertSyncLog(logData, (err, row) => {
        if (!err) {
        }
      });

      console.log("Log added to database.");
    } catch (err) {
      console.error("Error adding log:", err.message);
    }
  }

  // 인터넷 연결 확인
  async checkInternetConnection() {
    try {
      await axios.get(this.API_ENDPOINT);
      console.log("인터넷에 연결되어 있습니다.", this.API_ENDPOINT);
      return true;
    } catch (err) {
      console.log("인터넷 연결이 없습니다.", this.API_ENDPOINT);
      return false;
    }
  }

  // 전송 대기 중인 로그 동기화
  async syncPendingLogs() {
    const isConnected = await this.checkInternetConnection();

    if (!isConnected) {
      console.log("인터넷 연결이 없어 동기화를 건너뜁니다.");
      return;
    }

    try {
      // 대기 중인 로그 가져오기
      await SyncLog.selectSyncLogPending(async (err, rows) => {
        if (err) {
          console.log("조회중 오류가 발생했습니다", err);
          return;
        }

        if (rows.length === 0) {
          console.log("전송 대기 중인 로그가 없습니다.");
          return;
        }

        console.log(`전송 대기 중인 로그 ${rows.length}개.`);

        for (const log of rows) {
          // console.log(`로그 ${JSON.stringify(log)} 처리 시작...`);
          try {
            const logData = JSON.parse(log.log_data);

            // console.log(`전송 대기 중인 logData ${logData}`);

            await this.sendCloud(logData).then((success) => {
              console.log(`--> success : ${success}`, "\n\n");

              // 전송 성공 시 로그 상태 업데이트
              if (success) {
                SyncLog.updateSyncLog(log.id, { status: "sent" }, (err, rows) => {
                  if (!err) {
                    console.log(`Log ID ${log.id} marked as sent.`);
                  }
                });
              }
            });
          } catch (error) {
            console.error(`Error sending log ID ${log.id}:`, error.message);
          }
        }
      });
    } catch (err) {
      console.error("Error syncing logs:", err.message);
    }
  }

  // 서버로 로그 전송 (from app.js 에서 모든 요청 인터셉트 한다.)
  async sendCloud(req) {
    if (this.ipaddr == process.env.REMOTE_API_HOST) return;

    if (req.method.toUpperCase() !== "GET") {
      try {
        let { url, method, headers, body, auth } = req;

        const baseURL = this.API_ENDPOINT + url; // 올바른 baseURL
        console.log("--> sendCloud Start", baseURL);
        //console.log("--> sendCloud req",req.headers);\

        // JSON 값인 키만 필터링
        const jsonKeys = Object.keys(body).filter((key) => {
          const value = body[key];
          return typeof value === "object" && !Array.isArray(value) && value !== null;
        });

        body[jsonKeys].channel = "device";

        let data = {
          url,
          method,
          headers,
          body,
          auth,
        };

        //console.log("--> data",data);

        const isConnected = await this.checkInternetConnection();

        if (!isConnected) {
          console.log("인터넷 연결 없음: 데이터를 저장합니다.");
          await this.addLog(data); // 인터넷 연결이 없으면 데이터베이스에 저장
        } else {
          const config = {
            headers: {
              ...headers,
              serialno: "00000001",
              Authorization: headers["x-access-token"],
            },
          };
          try {
            // console.log("Fetching req ", { baseURL, data });

            if (method.toUpperCase() === "POST") {
              await post(baseURL, { body }, config).then((res) => {
                console.log("=> res ", res);
                return res;
              });
            } //response = await axiosInstance.post(baseURL, body, config);
            else if (method.toUpperCase() === "PUT") {
              try {
                return await axios.put(baseURL, body, config).then((res) => {
                  console.log("-- res.data", res.data);
                  let {
                    common: { success, error },
                    body: { info },
                  } = res.data;

                  return success;
                });
              } catch (error) {
                console.error("Error:", stringify(error));
              }
            } else if (method.toUpperCase() === "DELETE") {
              await del(url, body, config).then(({ common: { success }, body: { info } }) => {
                return success;
              });
            } // response = await axiosInstance.delete(baseURL,  body,  ...config);
          } catch (error) {
            console.error("Error while fetching req:", error);
          }
        }
      } catch (e) {
        console.error(e);
      }
    }

    console.log("--> sendCloud End", req?.url);

    return null;
  }

  // 오래된 로그 삭제
  async cleanupOldLogs() {
    const cutoffDate = new Date();
    cutoffDate.setHours(cutoffDate.getHours() - 24); // 24시간 이전 로그 삭제

    try {
      await SyncLog.deleteSyncLogAfter24(cutoffDate, (err, rows) => {
        if (!err) {
          console.log(`Deleted ${rows.affectedRows} old logs.`);
        }
      });
    } catch (err) {
      console.error("Error deleting old logs:", err.message);
    }
  }
}

export default DataSyncManager;
