import moment from "moment";
import _ from "lodash";
import chalk from "chalk";
import format from "../utils/format-util.js";

import RoomReserv from "../model/room-reserv.js";
import RoomType from "../model/room-type.js";

import { ERROR, PREFERENCES } from "../constants/constants.js";
import { keyToValue, keyCodes } from "../constants/key-map.js";

export const goodchoice_ready = (ota, reserv, line, place, callback) => {
  console.log(chalk.yellow("- goodchoice_ready"), line);

  // 2022.02.09
  //  0 "[여기어때]미리예약-숙박",
  //  1 "예약번호:22120611002F26YE1",
  //  2 "객실정보:평택역호텔감/디럭스(커플게임룸)(UHD65TV공기청정기욕조노하드게이밍PC2대)",
  //  3 "판매금액:80,000원",
  //  4 "입실일시:2/12(토)18:00~",
  //  5 "퇴실일시:2/13(일)12:00(1박)",
  //  6 "고객정보",
  //  7 "예약자명:한상학",
  //  8 "안심번호:050440663974",
  //  9 "*개인정보보호를위해고객의연락처는실제전화번호대신안심번호로전달됩니다.",
  //  10 "*안심번호는일회성전화번호로고객퇴실후7일간유효합니다.",
  //  11 "방문방법:차량방문",
  //  12 "※예약상세내역은마케팅센터에서확인해주세요.",
  //  13 "※오늘하루도행복하세요~항상응원합니다!-여기어때",

  let stay_type = line[0] ? line[0].split(/-/) : null;

  if (stay_type) {
    console.log(chalk.yellow("- stay_type"), stay_type[1]);
    reserv.stay_type = stay_type[1] === "숙박" ? 1 : 2;
  }

  let reserv_num = line[1] ? line[1].split(/:/) : null;

  if (reserv_num[1]) {
    console.log(chalk.yellow("- reserv_num"), reserv_num[1]);
    reserv.reserv_num = Number(reserv_num[1]);
  }

  // 예약정보
  let name = line[7] ? line[7].split(/:/) : null;
  if (name) {
    console.log(chalk.yellow("- name"), name[1]);
    reserv.name = name[1];
  }

  let hp = line[8] ? line[8].split(/:/) : null;
  if (hp) {
    console.log(chalk.yellow("- hp"), hp[1]);
    reserv.hp = hp[1];
  }

  // 입실시간/퇴실시간
  let date = line[4] ? line[4].match(/(?:.+):(\d{1,2})\/(\d{1,2})\((?:.+)\)(\d{1,2}):(\d{1,2})(~?)/) : null;
  if (date) {
    reserv.check_in_exp = `${moment().year()}-${date[1]}-${date[2]} ${date[3]}:${date[4]}`;
    console.log(chalk.yellow("- check_in_exp"), reserv.check_in_exp);
  }

  date = line[5] ? line[5].match(/(?:.+):(\d{1,2})\/(\d{1,2})\((?:.+)\)(\d{1,2}):(\d{1,2})(~?)/) : null;
  if (date) {
    reserv.check_out_exp = `${moment().year()}-${date[1]}-${date[2]} ${date[3]}:${date[4]}`;

    if (moment(reserv.check_out_exp).isBefore(moment(reserv.check_in_exp), "day")) {
      reserv.check_out_exp = `${moment().year() + 1}-${date[1]}-${date[2]} ${date[3]}:${date[4]}`;
    }
    console.log(chalk.yellow("- check_out_exp"), reserv.check_out_exp);
  }

  let visit_type = line[11] ? line[11].split(/:/) : null;

  if (visit_type) {
    let vt = visit_type[1] ? visit_type[1].match(/(도보|차량|지정안함)/) : ["도보"];

    console.log(chalk.yellow("- visit_type"), vt[0]);
    reserv.visit_type = vt[0] === "도보" ? 1 : 2;
  }

  // 입금 대기 상태는 결제 금액 없음.
  let prepay_ota_amt = line[3] ? line[3].split(/:/) : null;

  if (prepay_ota_amt) {
    console.log(chalk.yellow("- prepay_ota_amt"), prepay_ota_amt[1]);
    reserv.prepay_ota_amt = Number(prepay_ota_amt[1].replace(/[^\d]/g, ""));
  }

  if (place && place.place_id && line[4]) {
    let typeName = line[2].split(/:/)[1];

    console.log(chalk.yellow("- typeName"), typeName);

    if (typeName && process.env.MODE !== "PROD") {
      typeName = ["일반 !+ , @#실", "VIP룸+&", "스위트룸+& @!@"][Math.floor(Math.random() * 3)];
      console.log(chalk.red("- TEST 객실 타입 전환"), typeName);
    }

    // 특문 공백 문자.
    var regexp = new RegExp(/[/[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"\s]/gi);

    typeName = typeName.replace(regexp, "");
    console.log(chalk.red("- 객실 타입 정제"), typeName);

    ota.getRoomTypes(reserv.place_id, (roomTypes) => {
      let roomType = _.findLast(roomTypes, (v) => {
        let name = v.name.replace(regexp, "");
        let ota_name_1 = v.ota_name_1 ? v.ota_name_1.replace(regexp, "") : "";
        let ota_name_2 = v.ota_name_2 ? v.ota_name_2.replace(regexp, "") : "";
        let ota_name_3 = v.ota_name_3 ? v.ota_name_3.replace(regexp, "") : "";
        let ota_name_4 = v.ota_name_4 ? v.ota_name_4.replace(regexp, "") : "";
        let ota_name_5 = v.ota_name_5 ? v.ota_name_5.replace(regexp, "") : "";

        return name === typeName || ota_name_1 === typeName || ota_name_2 === typeName || ota_name_3 === typeName || ota_name_4 === typeName || ota_name_5 === typeName;
      });

      console.log(chalk.yellow("- roomType"), roomType ? roomType.name : "no room type");

      if (roomType) {
        let { id, name, default_fee_stay, default_fee_rent, reserv_discount_fee_stay, reserv_discount_fee_rent } = roomType;

        reserv.room_type_id = id;
        reserv.room_type_name = name;

        ota.setRoomFee(reserv, (add_fee) => {
          let room_fee = reserv.stay_type === 2 ? default_fee_rent : default_fee_stay; //  기본 요금
          let reserv_fee = reserv.stay_type === 2 ? default_fee_rent - reserv_discount_fee_rent : default_fee_stay - reserv_discount_fee_stay; // 예약 할인 요금

          // 요일/시간별 추가/할인 요금 적용.
          room_fee += add_fee;

          if (reserv.prepay_ota_amt > 0) {
            reserv_fee = reserv.prepay_ota_amt; // OTA 결제 시 결제 요금을 예약 가로 설정 한다.(잔금 발생 방지)
          } else {
            reserv_fee += add_fee; // OTA 결제 시 옵션 추가 요금은 적용 하지 않는다.
          }

          reserv.room_fee = room_fee;
          reserv.reserv_fee = reserv_fee;
          if (reserv.stat === "A") reserv.memo = line[4]; // 객실 타입 정보 상세 추가.

          callback({ reserv });
        });
      }

      callback({ reserv });
    });
  } else {
    callback({ reserv });
  }
};

export const goodchoice_today = (ota, reserv, line, place, callback) => {
  console.log(chalk.yellow("- goodchoice_today"), line);

  // 0 [여기어때] 당일예약/미리예약 - 대실
  // 1 예약번호: 45421356
  // 2 객실정보: 천안 호텔 감 / 디럭스(넷플릭스,웨이브온)
  // 3 판매금액: 25,000원
  // 4 입실일시: 5/27 (수) 17:00 ~
  // 5 퇴실일시: 5/27 (수) 22:00 (5시간)
  // 6 + 고객정보 +
  // 7 예약자명 : 지봉환
  // 8 안심번호: 050440126738
  // 9 개인정보보호를위해고객의연락처는실제전화번호대신안심번호로전달됩니다.
  // 10 안심번호는일회성전화번호로고객퇴실후7일간유효합니다.
  // 11 방문방법:차량

  let stay_type = line[0] ? line[0].match(/(대실|숙박)/) : ["숙박"];

  if (stay_type) {
    console.log(chalk.yellow("- stay_type"), stay_type[1]);
    reserv.stay_type = stay_type[1] === "대실" ? 2 : 1;
  }

  let reserv_num = line[1] ? line[1].split(/:/) : null;

  if (reserv_num[1]) {
    console.log(chalk.yellow("- reserv_num"), reserv_num[1]);
    // reserv.reserv_num = Number(reserv_num[1]);
    reserv.reserv_num = String(reserv_num[1]);
  }

  // 판매 금액
  let prepay_ota_amt = line[3] ? line[3].match(/([0-9,?]+)(원|만원)+/) : null;

  if (prepay_ota_amt[1]) {
    console.log(chalk.yellow("- prepay_ota_amt"), prepay_ota_amt[1]);
    reserv.prepay_ota_amt = Number(prepay_ota_amt[1].replace(/[^\d]/g, ""));
  }

  // 고객명
  let name = line[7] ? line[7].split(/:/) : null;
  if (name) {
    console.log(chalk.yellow("- name"), name[1]);
    reserv.name = name[1];
  }

  let hp = line[8] ? line[8].split(/:/) : null;
  if (hp) {
    console.log(chalk.yellow("- hp"), hp[1]);
    reserv.hp = hp[1];
  }

  let date = line[4] ? line[4].match(/(?:.+):(\d{1,2})\/(\d{1,2})\((?:.+)\)(\d{1,2}):(\d{1,2})(~?)/) : null;
  if (date) {
    reserv.check_in_exp = `${moment().year()}-${date[1]}-${date[2]} ${date[3]}:${date[4]}`;
    console.log(chalk.yellow("- check_in_exp"), reserv.check_in_exp);
  }

  date = line[5] ? line[5].match(/(?:.+):(\d{1,2})\/(\d{1,2})\((?:.+)\)(\d{1,2}):(\d{1,2})(~?)/) : null;
  if (date) {
    reserv.check_out_exp = `${moment().year()}-${date[1]}-${date[2]} ${date[3]}:${date[4]}`;

    if (moment(reserv.check_out_exp).isBefore(moment(reserv.check_in_exp), "day")) {
      reserv.check_out_exp = `${moment().year() + 1}-${date[1]}-${date[2]} ${date[3]}:${date[4]}`;
    }

    console.log(chalk.yellow("- check_out_exp"), reserv.check_out_exp);
  }

  let visit_type = line[11] ? line[11].split(/:/) : null;

  if (visit_type) {
    let vt = visit_type[1] ? visit_type[1].match(/(도보|차량)/) : ["도보"];

    console.log(chalk.yellow("- visit_type"), vt[0]);
    reserv.visit_type = vt[0] === "도보" ? 1 : 2;
  } else {
    visit_type = 1;
  }

  let roomInfo = line[2] ? line[2].match(/(?::)(.+)/) : null;

  console.log("- roomInfo", roomInfo);

  if (reserv.place_id && roomInfo) {
    let typeName = roomInfo[1];

    // 한글,영문,숫자만
    var regexp = new RegExp(/[^ㄱ-ㅎ|가-힣|a-z|0-9|]/gi);

    typeName = typeName.replace(regexp, "");

    console.log(chalk.yellow("- typeName"), typeName);

    ota.getRoomTypes(reserv.place_id, (roomTypes) => {
      let roomType = _.findLast(roomTypes, (v) => {
        let name = v.name.replace(regexp, "");
        let ota_name_1 = v.ota_name_1 ? v.ota_name_1.replace(regexp, "") : "";
        let ota_name_2 = v.ota_name_2 ? v.ota_name_2.replace(regexp, "") : "";
        let ota_name_3 = v.ota_name_3 ? v.ota_name_3.replace(regexp, "") : "";
        let ota_name_4 = v.ota_name_4 ? v.ota_name_4.replace(regexp, "") : "";
        let ota_name_5 = v.ota_name_5 ? v.ota_name_5.replace(regexp, "") : "";

        return name === typeName || ota_name_1 === typeName || ota_name_2 === typeName || ota_name_3 === typeName || ota_name_4 === typeName || ota_name_5 === typeName;
      });

      if (!roomType && process.env.MODE !== "PROD") {
        typeName = roomTypes[Math.floor(Math.random() * roomTypes.length)];
        console.log(chalk.red("- TEST 객실 타입 전환"), typeName);
      }

      console.log(chalk.yellow("- roomType"), roomType ? roomType.name : "no room type");

      if (roomType) {
        let { id, name, default_fee_stay, default_fee_rent, reserv_discount_fee_stay, reserv_discount_fee_rent } = roomType;

        reserv.room_type_id = id;
        reserv.room_type_name = name;

        ota.setRoomFee(reserv, (add_fee) => {
          let room_fee = reserv.stay_type === 2 ? default_fee_rent : default_fee_stay; //  기본 요금
          let reserv_fee = reserv.stay_type === 2 ? default_fee_rent - reserv_discount_fee_rent : default_fee_stay - reserv_discount_fee_stay; // 예약 할인 요금

          // 요일/시간별 추가/할인 요금 적용.
          room_fee += add_fee;

          if (reserv.prepay_ota_amt > 0) {
            reserv_fee = reserv.prepay_ota_amt; // OTA 결제 시 결제 요금을 예약 가로 설정 한다.(잔금 발생 방지)
          } else {
            reserv_fee += add_fee; // OTA 결제 시 옵션 추가 요금은 적용 하지 않는다.
          }

          reserv.room_fee = room_fee;
          reserv.reserv_fee = reserv_fee;
          if (reserv.stat === "A") reserv.memo = line[4]; // 객실 타입 정보 상세 추가.

          callback({ reserv });
        });
      } else {
        callback({ reserv });
      }
    });
  } else {
    callback({ reserv });
  }
};

export const goodchoice_fix = (ota, reserv, line, place, callback) => {
  console.log(chalk.yellow("- goodchoice_fix"), line);

  // 2022.07.27
  // 0 [여기어때]오늘2건의미리예약고객이입실예정
  // 1 ★미리예약건으로입실일당일보내드리는메시지입니다.
  // 2 숙소이름:월곶더휴식이로호텔-도보특가65UHDSMARTTV넷플릭스유튜브디즈니고이고이침구세트공기청정기
  // 3 예약번호:99987404
  // 4 객실타입:도보특가(65UHDSMARTTV,넷플릭스,유튜브,디즈니고이고이침구세트,공기청정기)
  // 5 입실구분:숙박
  // 6 예약일시:2022.07.25(월)18:31
  // 7 판매금액:33,000원
  // 8 정산금액:29,700원
  // 9 예약정보:이용욱/0504-4048-2360
  // 10 입실시간:2022.07.27(수)15:00
  // 11 퇴실시간:2022.07.28(목)13:00
  // 12 방문방법:도보방문

  // => 변환 정보
  // 0 "[여기어때]미리예약-숙박",
  // 1 "예약번호:85151267",
  // 2 "객실정보:평택역호텔감/디럭스(커플게임룸)(UHD65TV공기청정기욕조노하드게이밍PC2대)",
  // 3 "판매금액:80,000원",
  // 4 "입실일시:2/12(토)18:00~",
  // 5 "퇴실시간:2/13(일)12:00(1박)",
  // 6 "고객정보",
  // 7 "예약자명:한상학",
  // 8 "안심번호:050440663974",

  let name = line[3];
  let stays = [];
  let idx = 0;
  let type = 0;

  _.each(line, (v, k) => {
    // console.log(chalk.yellow("- line"), { v, k });

    if (/(:)/.test(v)) {
      let d = v.split(":");
      let d2 = d[1].split("/");
      let d3 = v.match(/(\d{4}.?\d{1,2}.?\d{1,2}\(.{1}\)\d{2,2}:\d{2,2})(~?)/);

      // if (d3) console.log(chalk.yellow("- data"), { v, d3 });

      if (!stays[idx]) stays[idx] = [];

      if (/(입실구분)/.test(v)) stays[idx][0] = "[여기어때]미리예약-" + d[1];
      if (/(예약번호)/.test(v)) stays[idx][1] = "예약번호:" + d[1];
      if (/(객실타입)/.test(v)) stays[idx][2] = "객실정보:" + d[1];
      if (/(판매금액)/.test(v)) stays[idx][3] = "판매금액:" + d[1];
      if (/(입실시간)/.test(v)) stays[idx][4] = "입실일시:" + d3[0].replace(/(\d{4}).?(\d{1,2}).?(\d{1,2})(\(.{1}\))(\d{2,2}:\d{2,2})(~?)/, "$2/$3$4$5");
      if (/(퇴실시간)/.test(v)) stays[idx][5] = "퇴실시간:" + d3[1].replace(/(\d{4}).?(\d{1,2}).?(\d{1,2})(\(.{1}\))(\d{2,2}:\d{2,2})(~?)/, "$2/$3$4$5");
      if (/(예약정보)/.test(v)) {
        stays[idx][6] = "고객정보";
        stays[idx][7] = "예약자명:" + d2[0];
        stays[idx][8] = "안심번호:" + d2[1].replace(/\-/gi, "");
      }
      if (/(방문방법)/.test(v)) {
        stays[idx][9] = "*미리예약 입실예정";
        stays[idx][10] = "";
        stays[idx][11] = "방문방법:" + d[1];
        idx++;
      }
    }
  });

  stays = _.filter(stays, (v) => v.length >= 11);

  console.log(chalk.yellow("- stays"), stays);

  let reservs = [];

  console.log(chalk.blue("- stays length"), stays.length);

  _.each(stays, (line, k) => {
    // reserv clone 해서 각각의 정보를 담는다.
    goodchoice_ready(ota, _.cloneDeep(reserv), line, place, ({ reserv }) => {
      console.log(chalk.yellow("- reserv"), k, reserv);

      reservs.push({ reserv });

      if (stays.length === k + 1) {
        console.log(chalk.blue("- reservs length"), reservs.length);
        callback(reservs);
      }
    });
  });
};

export const goodchoice_cancel = (ota, reserv, line, place, callback) => {
  console.log(chalk.yellow("- goodchoice_cancel"), line);

  // 0 [여기어때] 미리예약 - 숙박취소
  // 1 예약번호: 46008895
  // 2 객실정보: 천안 호텔 감 / 디럭스(넷플릭스,웨이브온)
  // 3 판매금액: 50,000원
  // 4 입실일시: 6/9 (화) 15:00 ~
  // 5 퇴실일시: 6/10 (수) 14:00 (1박)

  let reserv_num = line[1] ? line[1].split(/:/) : null;

  if (reserv_num[1]) {
    console.log(chalk.yellow("- reserv_num"), reserv_num[1]);
    reserv.reserv_num = Number(reserv_num[1]);

    ota.getReservByReservNum(reserv.reserv_num, (room_reserv) => {
      if (room_reserv) {
        reserv = _.merge({}, reserv, room_reserv);
      }
      reserv.state = "B";

      callback({ reserv });
    });
  } else {
    callback({ reserv });
  }
};
