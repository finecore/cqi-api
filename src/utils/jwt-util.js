import jsonwebtoken from "jsonwebtoken";
import { ERROR } from "../constants/constants.js";
import chalk from "chalk";

// JWT Secreet key.
const JWT_SECRET = "i_Mo@n-iCO_SeC_K_e_Y_001";
const JWT_EXPIRE = 60 * 60 * 24 * 365 * 10; // 10년.

/**
 * JWT Utils.
 */
const verify = (token, callback) => {
  let log = "-----------  JWT VERIFY  -----------\n";
  log += `token: [${token}],  JWT_SECRET:[${JWT_SECRET}] \n`;

  if (!token) callback(ERROR.NO_CERTIFICATION, null);
  else {
    // jsonwebtoken을 직접 사용
    jsonwebtoken.verify(token, JWT_SECRET, (err, payload) => {
      if (err) console.log(log, "- jwt verify error ", { err, payload });
      else console.log(log, "- jwt verify ok! ", payload, "----------------------------------");
      callback(err, payload);
    });
  }
};

const sign = (payload, callback, expires) => {
  // 유효 기간.
  var options = {
    expiresIn: expires || JWT_EXPIRE,
  };

  let log = "-----------  JWT SIGN  -----------\n";

  const { channel, place_id, birth } = payload;

  if (channel !== "admin" && channel !== "app") {
    if (place_id === undefined) {
      console.log(log, "---- 필수 입력 정보 place_id 누락!! ", payload);
      callback(ERROR.REQUIRE_CERTIFICATION, null);
      return;
    }
  }

  // jsonwebtoken을 직접 사용
  jsonwebtoken.sign(payload, JWT_SECRET, options, (err, token) => {
    log += `token: [${token}],  JWT_SECRET:[${JWT_SECRET}] \n`;

    if (err) console.log(log, "- jwt sign error ", err);
    else console.log(log, "- jwt sign ok! ", { payload }, "----------------------------------");
    callback(err, token);
  });
};

export { verify, sign };
