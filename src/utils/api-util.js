import { ERROR } from "../constants/constants.js";
import { verify, sign } from "./jwt-util.js";
import User from "../model/user.js";
import Member from "../model/member.js";
import Device from "../model/device.js";
import chalk from "chalk";
import _ from "lodash";
const env = process.env.NODE_ENV || "development";

const api = {
  // error request.
  errorRequest: (err) => {
    if (!err?.response) return;

    console.error("- errorRequest", err, "\n err.response", err.response);

    let error = {
      code: "Network Error",
      message: "HttpRequest ERR_CONNECTION_REFUSED",
      detail: "",
    };

    if (err.response) {
      const comerr = err.response.data.common ? err.response.data.common.error : error;

      error = {
        code: err.response.status,
        message: err.response.statusText,
        detail: comerr.message,
      };
    } else if (err) {
      const errs = String(err).split(":");

      error = {
        type: SET_API_ERR,
        code: errs[0] === "TypeError" ? 401 : 400,
        message: errs[1] ? errs[1] : err,
      };
    }

    fetching = false;

    return err;
  },

  // check response.
  checkResponse: (config, res) => {
    // console.log("- checkResponse", res);

    if (res.status !== 200) {
      const error = {
        type: SET_API_ERR,
        code: res.statue,
        message: res?.data?.message || "",
        detail: res.statusText,
      };
    } else {
      const { common, body } = res.data || {
        common: { success: false, error: {} },
        body: {},
      };

      let { success, error } = common;

      if (!success) {
        // to array.
        if (error.detail && !(error.detail instanceof Array)) error.detail = [error.detail];
        if (res.status !== 200 && !error.detail) error.detail.push(res.statusText);
      }

      fetching = false;
      return { common, body }; // return object.
    }
  },
};

/**
 * API Utils.
 */
const _success = (data, message = "ok", req, auth = {}) => {
  const textlen = 500;

  // client ip
  const ip = req.headers["x-forwarded-for"] || req.remoteAddress;

  let res = {
    common: {
      ip,
      success: true,
      message,
      error: null,
    },
    body: data,
    auth, // 서버용 데이터.
  };

  console.log("--> success ", JSON.stringify(res.common));

  if (res.body) {
    let logdate = JSON.stringify(res.body);

    logdate =
      logdate && logdate.length > textlen
        ? logdate.substring(0, textlen) + "\n    .............................. skip ...............................  \n" + logdate.substring(logdate.length - textlen / 2, logdate.length - 1)
        : logdate;

    if (res.body) console.log(chalk.bold("result: "), chalk.hex("#c66b27")(logdate));
  }

  return res;
};

const _failure = (err = ERROR.DEFAULT, _message = "ERROR", auth = {}) => {
  console.log("--> api error", chalk.red(JSON.stringify(err), JSON.stringify(_message)));

  let { code, message, sqlMessage, sql, detail } = err;

  // 오류 상세는 3 줄만 내려보낸다.
  if (detail && detail instanceof String) {
    detail = detail
      .split("\n")
      .filter((v, k) => v.trim().length && k < 3)
      .join("<br/>");
  }

  if (env === "development" && sqlMessage) detail = [sqlMessage, sql];

  let res = {
    common: {
      success: false,
      message: _message,
      error: { code, message, detail },
    },
    body: { info: {} },
    auth, // 서버용 데이터.
  };

  console.log("--> failure ", err);

  return res;
};

/** reponse json */
const jsonRes = (_req, _res, _err, _data, _message) => {
  let {
    url,
    method,
    headers: { channel, "x-access-token": token, uuid = "" },
    body,
    auth,
  } = _req;

  if (channel === "sync") {
    // 동기화 정보가 없다면 pass
    _err = null;
  }

  console.log("");
  console.log(chalk.green(">>>>>>>>>>>>>>>>>>>>>>>> api jsonRes >>>>>>>>>>>>>>>>>>>>>>>>"));
  console.log(chalk.bold(JSON.stringify({ channel, method, url })));
  console.log(chalk.bold("err"), _err);

  if (_err) {
    _err = _err instanceof Object ? _err : { message: _err };

    console.error(_err);

    if (_data) _err.detail = _data instanceof Object ? JSON.stringify(_data) : _data;

    if (_req) {
      if (_req.app.get("env") === "development" && _err.message && _err.message.indexOf("[" + url + "]") === -1) _err.message = "[" + url + "] " + _err.message;
    } else _err.message = "[API Error] " + _err.message;

    // Don't response error "Cannot set headers after they are sent to the client"
    if (_err.code !== "ERR_HTTP_HEADERS_SENT" && _err.code !== "PROTOCOL_CONNECTION_LOST") {
      return _res.json(_failure(_err, "ERROR", auth));
    }
  } else {
    // Http 로 들어온 데이터를 DB 반영 후 각 단말(WEB, MOBILE, DEVICE) 로 내려준다.
    if (method.toLowerCase() !== "get" && channel !== "device" && auth) {
      const { place_id = 1 } = auth;

      let message = {
        uuid, // 웹소켓에 접속시 발급한 고유 번호를 전송 해서 2 중 업데이트 방지 한다.
        headers: { method, url: url.substring(1), token, channel: "api", place_id },
        body: method.toUpperCase() === "DELETE" ? _data : body, // 삭제는 삭제된 정보를 담는다(intterupt 용)
      };

      // 소켓 서버로 전송.(앱 정보 관련은 제외)
      if (url.indexOf("app/info") === -1) {
        // console.log(chalk.bold("- to socket dispatch message"), message);
        global.dispatcher.dispatch(message);
      }
    }

    return _res.json(_success({ ..._data }, _message, _req, auth));
  }

  console.log(chalk.green(">>>>>>>>>>>>>>>>>>>>>>>>=========="));
  console.log("\n\n\n");
};

/** reponse send */
const sendRes = (_res, _err, _data) => {
  return _err ? _res.send(_failure(_err)) : _res.send(_success(_data));
};

// middlewares
const isValidToken = (_req, _res, _next) => {
  let {
    url,
    headers: { channel, "x-access-token": xtoken, token },
  } = _req;

  // 브라우저에서 직접 URL 호출 테스트 시 사용.
  const test = _req.query.test;

  token = token || xtoken;

  // Device channel 'test' 는 token 체크를 하지 않는다.
  console.log("");
  console.log(">>>>>>>>>>>>>>>>>>>>>>>> isValidToken ====================");
  console.log({ channel, url, token });

  if (!channel) {
    //return jsonRes(_req, _res.status(401), ERROR.NO_CHANNEL);
    console.error(ERROR.NO_CHANNEL, "채널을 Device 로 설정합니다.");
    channel = "device";
  }

  if (!token) return jsonRes(_req, _res.status(401), ERROR.NO_TOKEN);

  if (test === "imonaico") {
    if (_req.app.get("env") !== "development") console.log("- check 인증 정보를 우회 하여 접근! 관리자에게 보고 바랍니다! \n");
    return _next();
  } else if (channel === "sync" || channel === "front" || channel === "admin" || channel === "pms" || channel === "pad" || channel === "rc") {
    // jwt 에서 인증 정보 추출.
    verify(token, (err, user) => {
      if (err) {
        return jsonRes(_req, _res.status(401), err);
      } else {
        _req.auth = user; // request 에 auth 정보 설정.
        return _next();
      }
    });
  } else if (channel === "app") {
    // jwt 에서 인증 정보 추출.
    verify(token, (err, member) => {
      if (err) {
        return jsonRes(_req, _res.status(401), err);
      } else {
        _req.auth = member; // request 에 auth 정보 설정.
        return _next();
      }
    });
  } else if (channel === "device") {
    // jwt 에서 인증 정보 추출.
    verify(token, (err, device) => {
      if (err) {
        return jsonRes(_req, _res.status(401), err);
      } else {
        _req.auth = device; // request 에 auth 정보 설정.
        return _next();
      }
    });
  } else if (channel === "socket") {
    // 소켓 서버에서 전송된 요청.
    verify(token, (err, socket) => {
      if (err) {
        return jsonRes(_req, _res.status(401), err);
      } else {
        _req.auth = socket; // request 에 auth 정보 설정.
        return _next();
      }
    });
  } else {
    let error = Object.assign({}, ERROR.INVALID_CERTIFICATION);
    error.detail = "채널 정보가 없습니다.";
    return jsonRes(_req, _res.status(401), error);
  }
};

// 장비 일련번호 체크.
const checkSerialno = (_req, _res, _next) => {
  let {
    url,
    headers: { channel, "x-access-token": xtoken, token },
    params: { serialno },
  } = _req;

  token = token || xtoken;

  if (channel === "device") {
    if (serialno) {
      const device = _req.auth || {};

      console.log("");
      console.log("-> checkSerialno param serialno :", serialno, " token serialno :", device.serialno);

      /// 일련번호 체크
      if (device.serialno !== serialno) {
        console.log(chalk.red("-> INVALID SERIALNO "), ERROR.INVALID_SERIALNO);
        return jsonRes(_req, _res.status(401), ERROR.INVALID_SERIALNO);
      }
    }
  }

  return _next();
};

// private functions
const checkPermission = (_req, _res, _next) => {
  let {
    url,
    headers: { serialno, channel },
    body: { user = {} },
    auth, // token 에서 추출 한 정보.
  } = _req;

  console.log("");
  console.log("-> checkPermission ", { channel, auth, user });

  if (!auth) {
    return jsonRes(_req, _res.status(401), ERROR.INVALID_AUTHORITY);
  }

  if (channel === "sync" || channel === "front" || channel === "admin" || channel === "pms" || channel === "pad" || channel === "rc" || channel === "app" || channel === "watch") {
    const id = user.id || auth.id; // login || jwt

    if (!id) return jsonRes(_req, _res.status(401), ERROR.INVALID_AUTHORITY);

    // 이모나이코 관리자면 토큰 체크 안함. 권한 (0: 슈퍼관리자(ICT), 1: 업주(책임자), 2: 매니저(관리자), 3: 카운터(사용자), 4: 메이드, 5: PMS 9:뷰어)
    if (auth.type === 1 && auth.level < 3) {
      console.log("-  슈퍼 관리자는 권한 인증 패스!");
      return _next();
    } else if (url === "/user" && user.place_id === auth.place_id) {
      console.log("- 동일 업소의 사용자 등록 시 권한 인증 패스!");
      return _next();
    } else {
      if (channel !== "app") {
        User.selectUser(id, (_err, _row) => {
          const _user = _row ? _row[0] : {};
          if (_err || !_user) {
            return jsonRes(_req, _res, _err);
          }
          // jwt 인증 정보 비교.
          else {
            if (!_user) {
              return jsonRes(_req, _res.status(401), ERROR.INVALID_AUTHORITY);
            } else if (auth.id !== _user.id) {
              if (auth.place_id === _user.place_id && auth.level < _user.level) {
                console.log("- 동일 업소일때 토큰값 달라도 권한이 높다면 패스!");
                return _next();
              } else {
                return jsonRes(_req, _res.status(401), ERROR.INVALID_USER_INFO);
              }
            } else {
              return _next();
            }
          }
        });
      } else {
        Member.selectMember(id, (_err, _row) => {
          const _mamber = _row ? _row[0] : {};
          if (_err || !_mamber) {
            return jsonRes(_req, _res, _err);
          }
          // jwt 인증 정보 비교.
          else {
            if (!_mamber) {
              return jsonRes(_req, _res.status(401), ERROR.INVALID_AUTHORITY);
            } else if (auth.id !== _mamber.id) {
              return jsonRes(_req, _res.status(401), ERROR.INVALID_USER_INFO);
            } else {
              return _next();
            }
          }
        });
      }
    }
  } else if (channel === "device") {
    serialno = serialno || auth.serialno;

    console.log("-> device serialno", serialno);

    if (!serialno) return jsonRes(_req, _res.status(401), ERROR.INVALID_SERIALNO);

    Device.selectDeviceBySerialNo(serialno, (_err, _row) => {
      const _device = _row ? _row[0] : null;
      if (_err) return jsonRes(_req, _res, _err);
      else if (!_device) return jsonRes(_req, _res.status(401), ERROR.INVALID_DEVICE);
      else if (serialno !== _device.serialno) return jsonRes(_req, _res.status(401), ERROR.INVALID_AUTHORITY);
      else return _next();
    });
  } else if (channel === "socket") {
    let mode = auth.mode;

    console.log("-> socket server mode", mode);

    if (!mode) return jsonRes(_req, _res.status(401), ERROR.NO_SERVER_MODE);
    else return _next();
  } else {
    return _next();
  }
};

export { api, jsonRes, sendRes, isValidToken, checkSerialno, checkPermission };
