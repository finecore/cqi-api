import axios from "axios";
import moment from "moment";
import _ from "lodash";
import { SET_API_ERR } from "../store/mutation_types.js";
import { Consts } from "../constants/constants.js";

let fetching = false;

const api = {
  // error request.
  errorRequest: (err) => {
    if (!err?.response) return;

    console.error("- errorRequest", err, "\n err.response", err.response);

    let error = {
      code: "Network Error",
      message: "HttpRequest ERR_CONNECTION_REFUSED",
      detail: "",
    };

    if (err.response) {
      const comerr = err.response.data.common ? err.response.data.common.error : error;

      error = {
        code: err.response.status,
        message: err.response.statusText,
        detail: comerr.message,
      };
    } else if (err) {
      const errs = String(err).split(":");

      error = {
        type: SET_API_ERR,
        code: errs[0] === "TypeError" ? 401 : 400,
        message: errs[1] ? errs[1] : err,
      };
    }

    fetching = false;

    return err;
  },

  // check response.
  checkResponse: (res) => {
    //console.log("- checkResponse", res);

    if (res.status !== 200) {
      const error = {
        type: SET_API_ERR,
        code: res.statue,
        message: res?.data?.message || "",
        detail: res.statusText,
      };
    } else {
      const { common, body } = res.data || {
        common: { success: false, error: {} },
        body: {},
      };

      let { success, error } = common;

      if (!success) {
        // to array.
        if (error.detail && !(error.detail instanceof Array)) error.detail = [error.detail];
        if (res.status !== 200 && !error.detail) error.detail.push(res.statusText);
      }

      fetching = false;
      return { common, body }; // return object.
    }
  },
};

const getFormData = (json) => {
  console.log("----------------- form data -----------------");
  const formData = new FormData();
  for (const key in json) {
    // console.log(key, json[key]);
    formData.append(key, json[key]);
  }
  for (const key of formData.entries()) console.log("- ", key.join(":"));
  console.log("----------------- form data -----------------");
  return formData;
};

// Ajax As Axios
const ajax = axios.create({
  baseURL: "/",
  headers: {
    "Content-type": "application/json",
  },
});

// Add a request interceptor
ajax.interceptors.request.use(
  (config) => {
    const { headers, url, method, dispatch, data, loading } = config;

    // 여러 api 호출시 처음 하나만 정상처리되면서 나머지는 오류 처리되어서 주석 처리함. (2023.08.16 ej)
    // if (fetching) {
    //   return Promise.reject(config);
    // }

    // 로드 완료.
    fetching = true;

    // 서버 모드 전송
    headers.server_mode = "DEV";

    const token = store.getters["token"]; // store 사용.
    // console.log(`*** token: ${token} ***`);

    if (token) headers["x-access-token"] = token;

    //사용안함. (2023.08.17 ej)
    // const uuid = sessionStorage.getItem('uuid');
    // if (uuid) headers.uuid = uuid;

    // 채널 정보 전송
    if (config.headers.channel) {
      headers.channel = config.headers.channel;
    } else {
      headers.channel = Consts.Channel.FRONT;
    }
    // console.log(`*** headers.channel ::`, headers.channel);
    // console.log('- ajax request config ', { ...config });

    config.url = url;
    config.time = new Date();

    // file POST 는 form data 로 해야 파라메터 전송됨!(multipart/form-data 일때만 파라메터 전송)
    if (headers["Content-Type"] === "multipart/form-data") {
      config.data = getFormData(data);
    }

    console.log(">>>>>> axios request >>>>>>\n %c" + method.toUpperCase(), "background: #222; color: #bada55", url, moment(config.time).format("mm:ss:SSS"));
    if (data) console.log("data: %c" + JSON.stringify(data, null, 2), "background: #ececb8; color: blue");

    if (dispatch) {
      // 로딩바 보이기.
      if (loading !== false) dispatch("showLoader", null, { root: true });
    }

    return config;
  },
  function (error) {
    fetching = false;
    return Promise.reject(error);
  }
);

// Add a response interceptor
ajax.interceptors.response.use(
  (response) => {
    let diff = moment.utc(moment(new Date(), "DD/MM/YYYY HH:mm:ss:SSS").diff(moment(response.config.time, "DD/MM/YYYY HH:mm:ss:SSS"))).format("ss:SSS");

    console.log(
      "<<<<<< axios response <<<<<<\n %c" + response.config.method.toUpperCase() + "%c  %c " + diff + " ",
      "background: #f66; color: #fff",
      "background: #fff; color: #fff",
      "background: #4b49d7; color: #fff",
      response.config.url,
      "(" + moment(response.config.time).format("mm:ss:SSS") + " ~ " + moment(new Date()).format("mm:ss:SSS") + ")",
      "\ndata:",
      response.data
    );

    if (!response.data?.common.success) {
      const comerr = response.data.common ? response.data.common?.error : "";

      console.error("!!!! api response fail !!!!\n", comerr);
    }

    // 로딩바 숨기기
    if (response.config.dispatch) response.config.dispatch("hideLoader", null, { root: true });

    // 로드 완료.
    fetching = false;

    return response;
  },
  function (error) {
    fetching = false;
    return Promise.reject(error);
  }
);

export { api, ajax, fetching };
