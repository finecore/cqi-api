export const deepEqual = (obj1, obj2) => {
  // 두 값이 동일한지 확인 (기본 자료형 비교)
  if (obj1 === obj2) {
    return true;
  }

  // 객체가 아니거나 둘 중 하나가 null이면 false
  if (typeof obj1 !== "object" || obj1 === null || typeof obj2 !== "object" || obj2 === null) {
    return false;
  }

  // 객체의 키 배열 가져오기
  const keys1 = Object.keys(obj1);
  const keys2 = Object.keys(obj2);

  // 키의 개수가 다르면 다름
  if (keys1.length !== keys2.length) {
    return false;
  }

  // 키와 값 비교
  for (const key of keys1) {
    // 키가 obj2에 없거나 값이 다르면 false
    if (!keys2.includes(key) || !deepEqual(obj1[key], obj2[key])) {
      return false;
    }
  }

  // 모든 키와 값이 동일
  return true;
};
