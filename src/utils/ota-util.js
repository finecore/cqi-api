import moment from "moment";
import _ from "lodash";
import chalk from "chalk";
import format from "./format-util.js";

import RoomReserv from "../model/room-reserv.js";
import RoomType from "../model/room-type.js";
import RoomFee from "../model/room-fee.js";

import { ERROR, PREFERENCES } from "../constants/constants.js";
import { keyToValue, keyCodes } from "../constants/key-map.js";

import { yanolja_today, yanolja_fix } from "./ota-yanolja.js";
import { naver_ready, naver_fix, naver_cancel } from "./ota-naver.js";
import { goodchoice_ready, goodchoice_today, goodchoice_fix, goodchoice_cancel } from "./ota-goodchoice.js";
import { airbnb_fix, airbnb_cancel } from "./ota-airbnb.js";
import { hotelnow_fix, hotelnow_cancel } from "./ota-hotelnow.js";

const ota = {
  // 예약 MmsNo 로 조회.
  getReservByMmsNo: (mms_mo_num, callback) => {
    RoomReserv.selectRoomReservByMmsNo(mms_mo_num, (err, room_reservs) => {
      if (err) console.log("- getReservByMmsNo", { err, room_reservs });
      // room_reserv = !err && room_reserv && room_reserv[0] ? room_reserv[0] : null;
      callback(room_reservs);
    });
  },

  // 예약 번호로 조회.
  getReservByReservNum: (reserv_num, callback) => {
    RoomReserv.selectRoomReservByReservNum(reserv_num, (err, room_reserv) => {
      if (err) console.log("- getReservByReservNum", { err, room_reserv });
      room_reserv = !err && room_reserv && room_reserv[0] ? room_reserv[0] : null;
      callback(room_reserv);
    });
  },

  // 객실 타입 목록 조회.
  getRoomTypes: (place_id, callback) => {
    RoomType.selectAllRoomTypes(place_id, (err, result) => {
      callback(result);
    });
  },

  // 객실 타입 조회.
  getRoomType: (place_id, room_type_name, callback) => {
    RoomType.selectRoomTypeByName(place_id, room_type_name, (err, result) => {
      callback(result ? result[0] : null);
    });
  },

  // 객실 요금 조회.
  getRoomFee: (room_type_id, callback) => {
    let channel = 0; // 적용 채널 (0: 카운터, 1:자판기)
    RoomFee.selectRoomFees(room_type_id, channel, (err, result) => {
      callback(result);
    });
  },

  setRoomFee: (reserv, callback) => {
    let { room_type_id, check_in_exp, stay_type } = reserv;

    console.log("- setRoomFee", { room_type_id, check_in_exp, stay_type });

    let date = moment(check_in_exp, "YYYY-MM-DD HH:mm:ss");

    let day = date.isoWeekday(); // 입실 예정일의 요일

    ota.getRoomFee(room_type_id, (room_fees) => {
      // 해당 룸타입의 요일 요금 목록
      let roomFeeeList = _.filter(room_fees, (v) => v.day === day && v.stay_type === stay_type);

      let hhmm = date.format("HHmm");

      let add_fee = 0;

      // 해당 요일의 해당 시간대의 추가 요금 조회.
      roomFeeeList.forEach(function (v) {
        let st = Number(v.begin);
        let et = Number(v.end);
        let nt = Number(hhmm);

        // console.log("- room_fee ", v, st, "<=", nt, "~", nt, "<", et);

        // 이전일 ~ 다음일 시간 이라면.
        if (st > et) {
          if (st <= nt || et > nt) add_fee = v.add_fee;
        } else {
          if (st <= nt && et > nt) add_fee = v.add_fee;
        }
      });

      console.log("- add_fee ", add_fee);

      callback(add_fee);
    });
  },

  setCheckInOutTime: (reserv, callback) => {
    console.log("- setCheckInOutTime", reserv.place_id);

    PREFERENCES.Get(reserv.place_id, (preference) => {
      console.log(chalk.yellow("- check_in_exp"), reserv.check_in_exp);
      console.log(chalk.yellow("- check_out_exp"), reserv.check_out_exp);

      let { place_id, stay_time, stay_time_type, stay_time_weekend, use_rent_begin, use_rent_end, rent_time_am_weekend, rent_time_am, rent_time_pm_weekend, rent_time_pm } = preference;

      // console.log("- preference", { place_id, stay_time, stay_time_type, stay_time_weekend, use_rent_begin, use_rent_end, rent_time_am_weekend, rent_time_am, rent_time_pm_weekend, rent_time_pm });

      if (place_id) {
        const day = moment(reserv.check_in_exp, "YYYY-MM-DD HH:mm:ss").isoWeekday();

        // 기본 숙박 시간
        let stayTime =
          day > 5 // 주말
            ? stay_time_weekend
            : stay_time;

        // 기본 대실 시간
        let rentTime =
          day > 5 // 주말
            ? Number(use_rent_begin) < 12
              ? rent_time_am_weekend
              : rent_time_pm_weekend
            : Number(use_rent_begin) < 12
            ? rent_time_am
            : rent_time_pm;

        let diff = moment(reserv.check_out_exp, "YYYY-MM-DD HH:mm:ss").diff(moment(reserv.check_in_exp, "YYYY-MM-DD HH:mm:ss"), "days");

        console.log("- stay_time", {
          stay_time_type,
          stayTime,
          rentTime,
          diff,
        });

        // 입실 시간 설정.
        reserv.check_in_exp = moment(reserv.check_in_exp, "YYYY-MM-DD HH:mm:ss")
          .hour(reserv.sale === 2 ? Number(use_rent_begin) : Number(use_rent_end))
          .format("YYYY-MM-DD HH:mm");

        // 퇴실 시간 설정.
        reserv.check_out_exp =
          reserv.sale === 2
            ? moment(reserv.check_in_exp, "YYYY-MM-DD HH:mm:ss").add(rentTime, "hour").format("YYYY-MM-DD HH:mm")
            : stay_time_type === 0 && diff === 1 // 숙박 시간 타입 (0: 이용 시간 기준, 1:퇴실 시간 기준) && 1박 2일 일때만 이용 시간 기준으로..
            ? moment(reserv.check_in_exp, "YYYY-MM-DD HH:mm:ss").add(stayTime, "hour").format("YYYY-MM-DD HH:mm")
            : moment(reserv.check_out_exp, "YYYY-MM-DD HH:mm:ss").hour(stayTime).format("YYYY-MM-DD HH:mm");
      }

      callback({ reserv });
    });
  },

  parse: (otaSubscribePlace, mms_no, callback) => {
    console.log(chalk.yellow("- ota parse"), { mms_no });

    let { MO_KEY, SUBJECT, CONTENT } = mms_no;

    let reserv = {
      place_id: null,
      room_type_id: null,
      room_id: null,
      reserv_num: null,
      ota_code: 0,
      ota_type: 1, // OTA 예약 형태 (1:OTA 자동예약, 2: OTA 수동 예약)
      mms_mo_num: MO_KEY, // MMS_NO 참조 num
      stay_type: 1, // 숙박
      state: "A",
      name: null,
      hp: null,
      check_in_exp: null,
      check_out_exp: null,
      reserv_date: moment().format("YYYY-MM-DD HH:mm"),
      room_fee: 0, // 숙박 기본 요금
      reserv_fee: 0, // 예약 할인 요금
      prepay_ota_amt: 0,
      prepay_cash_amt: 0,
      prepay_card_amt: 0,
      visit_type: 1,
      memo: "",
      type: 0, // 0: 등록, 1: 변경, 2: 취소
    };

    let standby = null; // 예약 대기:I, 기타:N

    CONTENT = CONTENT.replace(/\t/gi, "").replace(/\n/gi, "\r\n").replace(/\r/gi, "\r\n").replace(/(\n+)/g, "\n");
    SUBJECT = SUBJECT ? SUBJECT.replace(/\t/gi, "") : "";

    let line = CONTENT.split(/\r\n/gi) || [];

    // console.log(chalk.yellow("- CONTENT"), "\n", line);

    line = _.map(
      _.filter(line, (v, k) => _.trim(v)),
      (v) => v.replace(/\n/gi, "").replace(/\s/gi, "") // 공백 제거.
    );

    let webIdx = -1;

    _.each(line, (v, k) => {
      if (v.indexOf("Web발신") > -1) {
        webIdx = k;
        return false;
      }
    });

    if (webIdx > -1) line.splice(0, webIdx + 1); // 업소명/Web발신 삭제.

    // 문자자동전달 앱 문구 삭제.
    if (line.length > 3 && /(문자자동전달)/.test(line[line.length - 3])) {
      line.splice(line.length - 4, line.length - 1);
    }

    // console.log(chalk.yellow("- parse line"), line);
    console.log(chalk.yellow("- parse line"), line.length, "\n", line.join("\n"));

    // 문자 형식이 잘못 되었다면..
    if (line.length < 7) {
      console.log(chalk.red("- line length"), line.length);
      callback([{ reserv, standby: "N" }]);
      return false;
    }

    let ota_code = 0;

    if (/(야놀자)/.test(line[0])) ota_code = 1;
    else if (/(여기어때)/.test(line[0])) ota_code = 2;
    else if (/(예약이)/.test(line[0])) ota_code = 3;
    else if (/(에어비앤비)/.test(line[0])) ota_code = 4;
    else if (/(호텔나우)/.test(line[0])) ota_code = 5;

    console.log(chalk.yellow("- ota_code"), ota_code, { SUBJECT });

    let place = null;

    if (ota_code) {
      reserv.ota_code = ota_code;

      let place_name = "";

      if (ota_code === 1) place_name = /(입실할)/.test(line[0]) ? line[1] : line[2];
      else if (ota_code === 2) place_name = line[2].replace(/(?:.+):(.*?)\/(?:.+)/, "$1");
      else if (ota_code === 3) place_name = line[0].replace(/(.*?),(?:.+)/, "$1");
      else if (ota_code === 4) place_name = line[0].replace(/\[(.*?)\](?:.+)/, "$1");
      else if (ota_code === 5) place_name = line[1].replace(/(.*?)예약(?:.+)/, "$1");

      console.log(chalk.yellow("- ota_place_name"), place_name);

      // 예약 문자에 표시된 업소명으로 등록된 ota_place_name 찾는다.
      place = _.find(otaSubscribePlace, function (sub) {
        // TODO 원격 관제 시 이모나이코 법인폰으로 수신 시  사용 (설정은 이글 관리자 로그인 후 설정 > 예약 설정 에 한다.)
        let ota_place_name = sub["ota_place_name_" + ota_code] || sub.place_name;

        ota_place_name = ota_place_name.replace(/\s/g, ""); // 공백 제거

        if (sub["ota_place_name_" + ota_code]) {
          // console.log("- place set ota_place_name", ota_place_name);
        }

        // 문자 전달 시 url 에 업소명을 넣어 보낸다. 없으면 등록한 ota 업소명 조회.
        return (SUBJECT && SUBJECT.replace(/\s/gi, "") === sub.place_name.replace(/\s/gi, "")) || place_name === ota_place_name;
      });

      console.log(chalk.yellow("- matching place"), place);

      if (!place && process.env.MODE !== "PROD") {
        place = _.find(otaSubscribePlace, { place_id: 1 });
        console.log(chalk.red("- TEST 고정 업소 전환"));
      }
    } else {
      callback([{ reserv, standby }]);
      return;
    }

    console.log(chalk.yellow("- place"), place);

    if (place && place.place_id) {
      reserv.place_id = place.place_id;

      console.log(chalk.yellow("- real place_name"), place.place_name);

      // 예약 있는지 조회.
      ota.getReservByMmsNo(MO_KEY, (room_reservs) => {
        try {
          // 있다면 예약 정보 셋팅.(예약 확정은 여러건이므로 무시한다.)
          if (room_reservs && room_reservs.length === 1) {
            reserv = room_reservs[0];
            reserv.type = 1; // 변경.
            console.log("- room_reserv", reserv);
          }

          let state = /(취소)/.test(line[0]) || /(취소)/.test(line[1]) ? "B" : "A"; // 예약 상태 (A: 정상, B: 취소, C: 완료)

          console.log(chalk.yellow("- state"), state);

          reserv.state = state;

          if (state === "B") reserv.type = 2; // 취소.

          // 야놀자
          if (ota_code === 1) {
            console.log(chalk.yellow("- 야놀자"));

            let type = line[0].match(/(당일|미리|입실할)/gi);

            console.log(chalk.yellow("- type"), type);

            if (type.length < 2) type[1] = type[0];

            type[0] = type[0].replace(/\s/gi, "");
            type[1] = type[1].replace(/\s/gi, "");

            if (type[0] === "입실할" || type[1] === "입실할") {
              yanolja_fix(ota, reserv, line, place, (reservs) => {
                callback(reservs);
              });
            } else if (type[0] === "미리") {
              callback([{ reserv, standby: "I" }]);
            } else if (type[0] === "당일") {
              yanolja_today(ota, reserv, line, place, ({ reserv }) => {
                callback([{ reserv, standby }]);
              });
            } else {
              callback([{ reserv, standby }]);
            }
          }
          // 여기어때
          else if (ota_code === 2) {
            console.log(chalk.yellow("- 여기어때"));

            let type = line[0].match(/(미리예약|예정|당일예약|취소)/gi); // 당일/미리예약 형식이 동일 하며 예약번호 있음.

            console.log(chalk.yellow("- type"), type);

            if (type.length < 2) type[1] = type[0];

            type[0] = type[0].replace(/\s/gi, "");
            type[1] = type[1].replace(/\s/gi, "");

            if (type[0] === "미리예약" && type[1] === "예정") {
              goodchoice_fix(ota, reserv, line, place, (reservs) => {
                callback(reservs);
              });
            } else if (type[0] === "미리예약") {
              goodchoice_ready(ota, reserv, line, place, ({ reserv }) => {
                callback([{ reserv, standby }]);
              });
            } else if (type[0] === "당일예약") {
              goodchoice_today(ota, reserv, line, place, ({ reserv }) => {
                callback([{ reserv, standby }]);
              });
            } else if ((type[0] === "취소" || type[1] === "취소") && reserv.reserv_num) {
              goodchoice_cancel(ota, reserv, line, place, ({ reserv }) => {
                callback([{ reserv, standby }]);
              });
            } else {
              callback([{ reserv, standby }]);
            }
          }
          // 네이버
          else if (ota_code === 3) {
            console.log(chalk.yellow("- 네이버"));

            let type = line[0].match(/(확정|미리|취소|입금대기)/gi);

            console.log(chalk.yellow("- type"), type);

            if (type.length < 2) type[1] = type[0];

            type[0] = type[0].replace(/\s/gi, "");
            type[1] = type[1].replace(/\s/gi, "");

            if (state === "B" && /(취소사유)/.test(line[1])) {
              reserv.memo = `[취소사유] ${line[2]}`;
              line.splice(1, 2); // 취소 사유 삭제.
              console.log(chalk.yellow("- 취소사유"), reserv.memo);
            }

            if (type[0] === "입금대기" || type[1] === "입금대기") {
              naver_ready(ota, reserv, line, place, ({ reserv }) => {
                callback([{ reserv, standby }]);
              });
            } else if (type[0] === "확정" || type[1] === "확정") {
              naver_fix(ota, reserv, line, place, ({ reserv }) => {
                callback([{ reserv, standby }]);
              });
            } else if ((type[0] === "취소" || type[1] === "취소") && reserv.reserv_num) {
              naver_cancel(ota, reserv, line, place, ({ reserv }) => {
                callback([{ reserv, standby }]);
              });
            } else {
              callback([{ reserv, standby }]);
            }
          }
          // 에어비앤비 (현재 사용 안함 에어비앤비 계약 후 인증 코드 받아야함!!!)
          else if (ota_code === 40000) {
            console.log(chalk.yellow("- 에어비앤비"));

            let type = line[0].match(/(예약알림|예약취소알림)/gi);

            console.log(chalk.yellow("- type"), type);

            type[0] = type[0].replace(/\s/gi, "");

            console.log(chalk.yellow("- type[0]"), type[0]);

            if (type[0] === "예약알림") {
              airbnb_fix(ota, reserv, line, place, ({ reserv }) => {
                callback([{ reserv, standby }]);
              });
            } else if (type[0] === "예약취소알림" && reserv.reserv_num) {
              airbnb_cancel(ota, reserv, line, place, ({ reserv }) => {
                callback([{ reserv, standby }]);
              });
            } else {
              callback([{ reserv, standby }]);
            }
          }
          // 호텔나우
          else if (ota_code === 5) {
            console.log(chalk.yellow("- 호텔나우"));

            let type = line[0].match(/(예약알림|예약취소알림)/gi);

            console.log(chalk.yellow("- type"), type);

            type[0] = type[0].replace(/\s/gi, "");

            console.log(chalk.yellow("- type[0]"), type[0]);

            if (type[0] === "예약알림") {
              hotelnow_fix(ota, reserv, line, place, ({ reserv }) => {
                callback([{ reserv, standby }]);
              });
            } else if (type[0] === "예약취소알림" && reserv.reserv_num) {
              hotelnow_cancel(ota, reserv, line, place, ({ reserv }) => {
                callback([{ reserv, standby }]);
              });
            } else {
              callback([{ reserv, standby }]);
            }
          } else {
            callback([{ reserv, standby }]);
          }
        } catch (e) {
          console.info("- ota parer error", e, reserv, standby);
          callback([{ reserv, standby }]);
        }
      });
    } else {
      if (/(미리예약)/.test(line[0])) standby = "I";
      else standby = "N";

      console.log("- standby", standby);

      callback([{ reserv, standby }]);
    }
  },
};

export default ota;
