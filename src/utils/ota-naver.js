import moment from "moment";
import _ from "lodash";
import chalk from "chalk";
import format from "../utils/format-util.js";

import RoomReserv from "../model/room-reserv.js";
import RoomType from "../model/room-type.js";

import { ERROR, PREFERENCES } from "../constants/constants.js";
import { keyToValue, keyCodes } from "../constants/key-map.js";

export const naver_ready = (ota, reserv, line, place, callback) => {
  console.log(chalk.yellow("- naver_ready"), line);

  // 0  호텔감, 새로운 예약이 접수되어 입금대기 중입니다.
  // 1 - 예약번호: 87714768
  // 2 - 예약자명: 강지혜
  // 3 - 전화번호: 01034668658
  // 4 - 예약상품: STANDARD
  // 5 - 이용기간: 2020.06.26.(토)~2020.06.27.(일)(1박 2일)
  // 6 - 네이버페이 결제상태: 입금대기
  // 7 - 결제수단: 신용카드
  // 8 - 결제금액: STANDARD(1) 80, 000원
  let stay_type = line[5] ? line[5].match(/(\d{1,2})박(\d{1,2})일/) : null;

  if (stay_type) {
    console.log(chalk.yellow("- stay_type"), stay_type[1]);
    reserv.stay_type = stay_type[1] > 0 ? 1 : 2;
  }

  let reserv_num = line[1] ? line[1].split(/:/) : null;

  if (reserv_num[1]) {
    console.log(chalk.yellow("- reserv_num"), reserv_num[1]);
    reserv.reserv_num = Number(reserv_num[1]);
  }

  // 고객명
  let name = line[2] ? line[2].split(/:/) : null;
  if (name) {
    console.log(chalk.yellow("- name"), name[1]);
    reserv.name = name[1];
  }

  let hp = line[3] ? line[3].split(/:/) : null;
  if (hp) {
    console.log(chalk.yellow("- hp"), hp[1]);
    reserv.hp = hp[1];
  }

  // 이용기간: 2020.06.02.(화)~2020.06.03.(수)(1박 2일)
  let date = line[5] ? line[5].match(/(?::)(\d{4}).?(\d{2}).?(\d{2})(?:.+)(~?)(\d{4}).?(\d{2}).?(\d{2})/) : null;

  if (date) {
    reserv.check_in_exp = `${date[1]}-${date[2]}-${date[3]}`;
    reserv.check_out_exp = `${date[4]}-${date[5]}-${date[6]}`;

    ota.setCheckInOutTime(reserv, () => {
      console.log(chalk.yellow("- check_in_exp"), reserv.check_in_exp);
      console.log(chalk.yellow("- check_out_exp"), reserv.check_out_exp);
    });
  }

  let visit_type = 1;
  if (visit_type) {
    console.log(chalk.yellow("- visit_type"), visit_type);
    reserv.visit_type = visit_type; // visit_type[0] === "도보" ? 1 : 2;
  }

  // 입금 대기 상태는 결제 금액 없음.
  let prepay_ota_amt = 0; // line[8] ? line[8].match(/([0-9,?]+)(원|만원)+/) : null;

  if (prepay_ota_amt) {
    console.log(chalk.yellow("- prepay_ota_amt"), prepay_ota_amt[1]);
    reserv.prepay_ota_amt = Number(prepay_ota_amt[1].replace(/[^\d]/g, ""));
  }

  if (place && place.place_id && line[4]) {
    let name = line[4].split(/:/)[1];

    let typeName = name;

    console.log(chalk.yellow("- typeName"), typeName);

    if (typeName && process.env.MODE !== "PROD") {
      typeName = ["일반 !+ , @#실", "VIP룸+&", "스위트룸+& @!@"][Math.floor(Math.random() * 3)];
      console.log(chalk.red("- TEST 객실 타입 전환"), typeName);
    }

    // 특문 공백 문자.
    var regexp = new RegExp(/[/[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"\s]/gi);

    typeName = typeName.replace(regexp, "");

    ota.getRoomTypes(reserv.place_id, (roomTypes) => {
      let roomType = _.findLast(roomTypes, (v) => {
        let name = v.name.replace(regexp, "");
        let ota_name_1 = v.ota_name_1 ? v.ota_name_1.replace(regexp, "") : "";
        let ota_name_2 = v.ota_name_2 ? v.ota_name_2.replace(regexp, "") : "";
        let ota_name_3 = v.ota_name_3 ? v.ota_name_3.replace(regexp, "") : "";
        let ota_name_4 = v.ota_name_4 ? v.ota_name_4.replace(regexp, "") : "";
        let ota_name_5 = v.ota_name_5 ? v.ota_name_5.replace(regexp, "") : "";

        return name === typeName || ota_name_1 === typeName || ota_name_2 === typeName || ota_name_3 === typeName || ota_name_4 === typeName || ota_name_5 === typeName;
      });

      console.log(chalk.yellow("- roomType"), roomType ? roomType.name : "no room type");

      if (roomType) {
        let { id, name, default_fee_stay, default_fee_rent, reserv_discount_fee_stay, reserv_discount_fee_rent } = roomType;

        reserv.room_type_id = id;
        reserv.room_type_name = name;

        ota.setRoomFee(reserv, (add_fee) => {
          let room_fee = reserv.stay_type === 2 ? default_fee_rent : default_fee_stay; //  기본 요금
          let reserv_fee = reserv.stay_type === 2 ? default_fee_rent - reserv_discount_fee_rent : default_fee_stay - reserv_discount_fee_stay; // 예약 할인 요금

          // 요일/시간별 추가/할인 요금 적용.
          room_fee += add_fee;

          if (reserv.prepay_ota_amt > 0) {
            reserv_fee = reserv.prepay_ota_amt; // OTA 결제 시 결제 요금을 예약 가로 설정 한다.(잔금 발생 방지)
          } else {
            reserv_fee += add_fee; // OTA 결제 시 옵션 추가 요금은 적용 하지 않는다.
          }

          reserv.room_fee = room_fee;
          reserv.reserv_fee = reserv_fee;
          if (reserv.stat === "A") reserv.memo = line[4]; // 객실 타입 정보 상세 추가.

          callback({ reserv });
        });
      }

      callback({ reserv });
    });
  } else {
    callback({ reserv });
  }
};

export const naver_fix = (ota, reserv, line, place, callback) => {
  console.log(chalk.yellow("- naver_fix"), line);

  // 0  딜라이트 호텔, 새로운 예약이 확정되었습니다. 예약 내역을 확인해 보세요.
  // 1 - 예약번호: 87714768
  // 2 - 예약자명: 강지혜
  // 3 - 전화번호: 01034668658
  // 4 - 예약상품: STANDARD
  // 5 - 이용기간: 2020.06.26.(토)~2020.06.27.(일)(1박 2일)
  // 6 - 네이버페이 결제상태: 결제완료
  // 7 - 결제수단: 신용카드
  // 8 - 결제금액: STANDARD(1) 80, 000원
  let stay_type = line[5] ? line[5].match(/(\d{1,2})박(\d{1,2})일/) : null;

  if (stay_type) {
    console.log(chalk.yellow("- stay_type"), stay_type[1]);
    reserv.stay_type = stay_type[1] > 0 ? 1 : 2;
  }

  let reserv_num = line[1] ? line[1].split(/:/) : null;

  if (reserv_num[1]) {
    console.log(chalk.yellow("- reserv_num"), reserv_num[1]);
    reserv.reserv_num = Number(reserv_num[1]);
  }

  // 고객명
  let name = line[2] ? line[2].split(/:/) : null;
  if (name) {
    console.log(chalk.yellow("- name"), name[1]);
    reserv.name = name[1];
  }

  let hp = line[3] ? line[3].split(/:/) : null;
  if (hp) {
    console.log(chalk.yellow("- hp"), hp[1]);
    reserv.hp = hp[1];
  }

  // 이용기간: 2020.06.02.(화)~2020.06.03.(수)(1박 2일)
  let date = line[5] ? line[5].match(/(?::)(\d{4}).?(\d{2}).?(\d{2})(?:.+)(~?)(\d{4}).?(\d{2}).?(\d{2})/) : null;

  if (date) {
    reserv.check_in_exp = `${date[1]}-${date[2]}-${date[3]}`;
    reserv.check_out_exp = `${date[4]}-${date[5]}-${date[6]}`;

    ota.setCheckInOutTime(reserv, () => {
      console.log(chalk.yellow("- check_in_exp"), reserv.check_in_exp);
      console.log(chalk.yellow("- check_out_exp"), reserv.check_out_exp);
    });
  }

  let visit_type = 1;
  if (visit_type) {
    console.log(chalk.yellow("- visit_type"), visit_type);
    reserv.visit_type = visit_type; // visit_type[0] === "도보" ? 1 : 2;
  }

  let prepay_ota_amt = line[8] ? line[8].match(/([0-9,?]+)(원|만원)+/) : null;

  if (prepay_ota_amt) {
    console.log(chalk.yellow("- prepay_ota_amt"), prepay_ota_amt[1]);
    reserv.prepay_ota_amt = Number(prepay_ota_amt[1].replace(/[^\d]/g, ""));
  }

  if (place && place.place_id && line[4]) {
    let name = line[4].split(/:/)[1];
    let typeName = name.replace(/\([^)]*\)?/g, "");

    console.log(chalk.yellow("- typeName"), typeName);

    // 한글,영문,숫자만
    var regexp = new RegExp(/[^ㄱ-ㅎ|가-힣|a-z|0-9|]/gi);

    typeName = typeName.replace(regexp, "");

    ota.getRoomTypes(reserv.place_id, (roomTypes) => {
      let roomType = _.findLast(roomTypes, (v) => {
        let name = v.name.replace(regexp, "");
        let ota_name_1 = v.ota_name_1 ? v.ota_name_1.replace(regexp, "") : "";
        let ota_name_2 = v.ota_name_2 ? v.ota_name_2.replace(regexp, "") : "";
        let ota_name_3 = v.ota_name_3 ? v.ota_name_3.replace(regexp, "") : "";
        let ota_name_4 = v.ota_name_4 ? v.ota_name_4.replace(regexp, "") : "";
        let ota_name_5 = v.ota_name_5 ? v.ota_name_5.replace(regexp, "") : "";

        return name === typeName || ota_name_1 === typeName || ota_name_2 === typeName || ota_name_3 === typeName || ota_name_4 === typeName || ota_name_5 === typeName;
      });

      if (!roomType && process.env.MODE !== "PROD") {
        typeName = roomTypes[Math.floor(Math.random() * roomTypes.length)];
        console.log(chalk.red("- TEST 객실 타입 전환"), typeName);
      }

      console.log(chalk.yellow("- roomType"), roomType ? roomType.name : "no room type");

      if (roomType) {
        let { id, name, default_fee_stay, default_fee_rent, reserv_discount_fee_stay, reserv_discount_fee_rent } = roomType;

        reserv.room_type_id = id;
        reserv.room_type_name = name;

        ota.setRoomFee(reserv, (add_fee) => {
          let room_fee = reserv.stay_type === 2 ? default_fee_rent : default_fee_stay; //  기본 요금
          let reserv_fee = reserv.stay_type === 2 ? default_fee_rent - reserv_discount_fee_rent : default_fee_stay - reserv_discount_fee_stay; // 예약 할인 요금

          // 요일/시간별 추가/할인 요금 적용.
          room_fee += add_fee;

          if (reserv.prepay_ota_amt > 0) {
            reserv_fee = reserv.prepay_ota_amt; // OTA 결제 시 결제 요금을 예약 가로 설정 한다.(잔금 발생 방지)
          } else {
            reserv_fee += add_fee; // OTA 결제 시 옵션 추가 요금은 적용 하지 않는다.
          }

          reserv.room_fee = room_fee;
          reserv.reserv_fee = reserv_fee;
          if (reserv.stat === "A") reserv.memo = line[4]; // 객실 타입 정보 상세 추가.

          callback({ reserv });
        });
      } else {
        callback({ reserv });
      }
    });
  } else {
    callback({ reserv });
  }
};

export const naver_cancel = (ota, reserv, line, place, callback) => {
  console.log(chalk.yellow("- naver_cancel"), line);

  // 0 호텔감,고객님이예약을취소하셨습니다.취소내역을확인해보세요.
  // 1 -예약번호:87976991
  // 2 -예약자명:박종현
  // 3 -전화번호:01045930822
  // 4 -예약상품:Deluxe
  // 5 -이용기간:2020.06.05.(금)~2020.06.06.(토)1박2일
  // 6 -결제금액:Deluxe(1)50,000원

  let reserv_num = line[1] ? line[1].split(/:/) : null;

  if (reserv_num[1]) {
    console.log(chalk.yellow("- reserv_num"), reserv_num[1]);
    reserv.reserv_num = Number(reserv_num[1]);

    ota.getReservByReservNum(reserv.reserv_num, (room_reserv) => {
      if (room_reserv) {
        reserv = _.merge({}, reserv, room_reserv);
      }
      reserv.state = "B";

      callback({ reserv });
    });
  } else {
    callback({ reserv });
  }
};
