import { jsonRes, sendRes, isValidToken, checkPermission } from "../../utils/api-util.js";
import { ERROR } from "../../constants/constants.js";
import RoomModel from "./room-model-v1.js";

const RoomController = {
  /**
   * 층별, 객실정보 (2023.09.24 ej)
   */
  list_rooms_floors: (req, res) => {
    const { place_id } = req.params;
    if (!place_id) {
      jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room place_id for select");
    } else {
      RoomModel.selectRoomsFloors(place_id, (err, floors) => {
        jsonRes(req, res, err, { floors });
      });
    }
  },

  /**
   * 객실화면 표시되는 객실 아이템
   */
  list_room_items: (req, res) => {
    const { place_id } = req.params;
    if (!place_id) {
      jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room place_id for select");
    } else {
      RoomModel.selectRoomItems(place_id, (err, room_items) => {
        jsonRes(req, res, err, { room_items });
      });
    }
  },
};

export default RoomController;
