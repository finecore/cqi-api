import connection from "../../db/connection.js";

// Room object constructor
const RoomModel = {
  /**
   * 층별, 객실정보 (2023.09.24 ej)
   */
  selectRoomsFloors: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT floor AS no
                , concat(floor, '층') AS name
                , COUNT(*) AS room_count
                , GROUP_CONCAT(id, '/', name ORDER BY name SEPARATOR '^') AS room_infos
              FROM room
            WHERE place_id = ?
            GROUP BY floor;`,
          [place_id],
          result
        );
      }
    });
  },

  /**
   * 객실화면 표시되는 객실 뷰 아이템
   * type: @/types/sale.d.ts => RoomItem
   */
  selectRoomItems: (view_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT v.id     AS view_item_id
                , v.view_id
                , v.room_id
                , v.row
                , v.col
                , 1        AS active
                , a.name   AS room_name
                , a.count
                , a.floor
                , a.card_barcode
                , a.reserv_yn
                , a.doorlock_id
                , b.id    AS room_type_id
                , b.name  AS room_type_name
                , b.person
                , b.default_fee_stay
                , b.default_fee_rent
                , b.person_add_fee
                , b.reserv_discount_fee_stay
                , b.reserv_discount_fee_rent
                , b.card_add_fee
                , b.over_time_fee_stay
                , b.over_time_fee_rent
                , c.id    AS room_state_id
                , c.dnd
                , c.clean
                , c.clean_change_time
                , c.fire
                , c.emerg
                , c.sale
                , c.key
                , c.key_change_time
                , c.outing           	  -- 외출여부 (0:없음, 1:외출)
                , c.signal   			      -- 통신상태 (0:정상, 1:이상)
                , c.theft        		    -- 도난센서 (0:없음, 1:감지)
                , c.emlock     		      -- EM LOCK (0:잠김, 1:열림)
                , c.door                -- 도어상태 (0:닫힘, 1:열림)
                , c.car_call            -- 차량호출 (0:없음, 1:호출)
                , c.airrcon_relay       -- 에어컨릴레이 (0:OFF, 1:ON)
                , c.main_relay          -- 메인릴레이 (0:OFF, 1:ON)
                , c.use_auto_power_off	-- 자동 전원 차단 사용 여부(0:안함, 1:사용)
                , c.entrance            -- 입실경로 (0:매니저, 1:정산기)
                , c.air_temp            -- 현재온도 (0~63)
                , c.notice         	    -- 객실 표시
                , c.notice_opacity      -- 표시 투명도
                , c.notice_display      -- 객실 표시 여부 (0:표시안함, 1:표시)
                , c.temp_key_1          -- 키에 연동 시 사용중 온도
                , c.temp_key_2          -- 키에 연동 시 외출중
                , c.temp_key_3          -- 키에 연동 시 공실
                , c.temp_key_4          -- 키에 연동 시 청소중
                , c.temp_key_5          -- 키에 연동 시 청소대기
                , c.inspect             -- 인스펙트 상태 (0:없음, 1:점검대기, 2:점검중, 3:점검완료)
                , d.id    AS room_sale_id  -- room_sale d 은 미판매시 모두 null 임.
                , d.state   			      -- 판매 상태 (A:정상, B:취소, C:완료), null 이면 미판매 상태.
                , d.stay_type
                , d.check_in_exp
                , d.check_out_exp
                , d.check_in
                , d.check_out
                , d.memo
            FROM room_view_item v
                  LEFT  JOIN room a       ON  v.room_id       =  a.id
                  INNER JOIN room_type b  ON  a.room_type_id  =  b.id
                  INNER JOIN room_state c ON  a.id            =  c.room_id
                  LEFT  JOIN room_sale d  ON  c.room_sale_id  =  d.id
            WHERE v.view_id  =  ?
            ORDER BY v.row, v.col
          ;`,
          [view_id],
          result
        );
      }
    });
  },
};

export default RoomModel;
