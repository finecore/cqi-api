import { isValidToken, checkPermission } from "../../utils/api-util.js";
import RoomController from "./room-controller-v1.js";

export default (app) => {
  /**
   * 층별, 객실정보 (2023.09.24 ej)
   */
  app.route("/v1/room/floors/:place_id").get(isValidToken, RoomController.list_rooms_floors);

  /**
   * 객실화면 표시되는 객실 아이템
   */
  app.route("/v1/room/items/:place_id").get(isValidToken, RoomController.list_room_items);
};
