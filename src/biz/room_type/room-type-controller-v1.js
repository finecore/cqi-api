import { jsonRes, sendRes, isValidToken, checkPermission } from "../../utils/api-util.js";
import { ERROR } from "../../constants/constants.js";
import RoomTypeModel from "./room-type-model-v1.js";

const RoomTypeController = {
  /**
   * 객실ID로 객실유형 조회.
   * type: @/types/index.d.ts => RoomType
   */
  list_room_type: (req, res) => {
    const { room_id } = req.params;
    if (!room_id) {
      jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room place_id for select");
    } else {
      RoomTypeModel.selectRoomTypeByRoomId(room_id, (err, room_type) => {
        jsonRes(req, res, err, { room_type });
      });
    }
  },
};

export default RoomTypeController;
