import { isValidToken, checkPermission } from "../../utils/api-util.js";
import RoomTypeController from "./room-type-controller-v1.js";

export default (app) => {
  /**
   * 객실ID로 객실유형 조회.
   * type: @/types/index.d.ts => RoomType
   */
  app.route("/v1/room_type/room_id/:room_id").get(isValidToken, RoomTypeController.list_room_type);
};
