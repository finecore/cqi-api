import connection from "../../db/connection.js";

const KioskModel = {
  /**
   * 키오스크 조회
   */
  selectKioskDevices: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT *
             FROM device
            WHERE place_id  =  ?
              AND type      =  '02'
          ;`,
          [place_id],
          result
        );
      }
    });
  },

  /**
   * ISC State 조회
   */
  selectKioskStates: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*
                , b.id    AS isc_id
                , b.place_id
                , b.name  AS isc_name
                , c.name  AS place_name
             FROM isc_state a
                  INNER JOIN device b  ON  a.serialno = b.serialno
                  INNER JOIN place  c  ON  b.place_id = c.id
            WHERE b.place_id  =  ?
          ;`,
          [place_id],
          result
        );
      }
    });
  },

  /**
   * 키오스크 판매 조회
   */
  selectKioskSalePaySum: (place_id, begin, end, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.id    AS sale_id
                , a.serialno
                , a.room_id
                , b.name  AS room_name
                , c.name  AS type_name
                , b.floor
                , a.state
                , a.stay_type
                , CASE WHEN a.check_in  IS NOT NULL THEN a.check_in  ELSE a.check_in_exp  END  AS check_in
                , CASE WHEN a.check_out IS NOT NULL THEN a.check_out ELSE a.check_out_exp END  AS check_out
                , a.reg_date
                , a.car_no
                , a.default_fee
                , a.add_fee
                , a.option_fee
                , (a.default_fee + a.add_fee + a.option_fee)  AS total_fee
                , (a.prepay_ota_amt + a.prepay_cash_amt + a.prepay_card_amt + a.prepay_point_amt + a.pay_cash_amt + a.pay_card_amt + a.pay_point_amt)  AS total_amount
                , (a.default_fee + a.add_fee + a.option_fee)
                  - (a.prepay_ota_amt + a.prepay_cash_amt + a.prepay_card_amt + a.prepay_point_amt + a.pay_cash_amt + a.pay_card_amt + a.pay_point_amt)  AS unpaid_amount
                , a.prepay_ota_amt
                , a.prepay_cash_amt
                , a.prepay_card_amt
                , a.prepay_point_amt
                , a.pay_cash_amt
                , a.pay_card_amt
                , a.pay_point_amt
                , d.*
            FROM room_sale a
                  INNER JOIN room b      ON  a.room_id  =  b.id
                  LEFT JOIN room_type c  ON  b.room_type_id  =  c.id
                  LEFT JOIN (
                     SELECT sale_id
                          , count(*)  AS total_count
                          , count(case when a.prepay_ota_amt > 0 then 1 end)    AS prepay_ota_cnt
                          , count(case when a.prepay_cash_amt > 0 then 1 end)   AS prepay_cash_cnt
                          , count(case when a.prepay_card_amt > 0 then 1 end)   AS prepay_card_cnt
                          , count(case when a.prepay_point_amt > 0 then 1 end)  AS prepay_point_cnt
                          , count(case when a.pay_cash_amt > 0 then 1 end)   AS pay_cash_cnt
                          , count(case when a.pay_card_amt > 0 then 1 end)   AS pay_card_cnt
                          , count(case when a.pay_point_amt > 0 then 1 end)  AS pay_point_cnt
                          , sum(a.prepay_ota_amt)    AS total_prepay_ota_amt
                          , sum(a.prepay_cash_amt)   AS total_prepay_cash_amt
                          , sum(a.prepay_card_amt)   AS total_prepay_card_amt
                          , sum(a.prepay_point_amt)  AS total_prepay_point_amt
                          , sum(a.pay_cash_amt)   AS total_pay_cash_amt
                          , sum(a.pay_card_amt)   AS total_pay_card_amt
                          , sum(a.pay_point_amt)  AS total_pay_point_amt
                       FROM room_sale_pay a
                            INNER JOIN device b  ON  b.serialno  =  a.serialno
                      WHERE b.place_id  =  ?
                        AND a.serialno  IS NOT NULL
                        AND a.state     != 'B'
                        AND a.reg_date  >= date_format(?, '%Y-%m-%d')
                        AND a.reg_date  <  date_add(date_format(?, '%Y-%m-%d'), INTERVAL 1 DAY)
                      GROUP BY sale_id
                            ) d  ON  d.sale_id  =  a.id
            WHERE b.place_id      =  ?
              AND a.serialno      IS NOT NULL
              AND a.state         != 'B'
              AND a.check_in_exp  >= date_format(?, '%Y-%m-%d')
              AND a.reg_date      <  date_add(date_format(?, '%Y-%m-%d'), INTERVAL 1 DAY)
            ORDER BY a.reg_date
          ;`,
          [place_id, begin, end, place_id, begin, end],
          result
        );
      }
    });
  },
};

export default KioskModel;
