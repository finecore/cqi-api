import { isValidToken, checkPermission } from "../../utils/api-util.js";
import KioskController from "./kiosk-controller-v1.js";

export default (app) => {
  /**
   * 키오스크 디바이스 조회
   */
  app.route("/v1/kiosk/devices/:place_id").get(isValidToken, KioskController.list_kiosk_device);

  /**
   * ISC State 조회
   */
  app.route("/v1/kiosk/states/:place_id").get(isValidToken, KioskController.list_kiosk_state);

  /**
   * 키오스크 판매 조회
   */
  app.route("/v1/kiosk/sale_pay_sum/:place_id/:begin/:end").get(isValidToken, KioskController.list_kiosk_sale_pay_sum);
};
