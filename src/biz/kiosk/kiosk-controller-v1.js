import { jsonRes, sendRes, isValidToken, checkPermission } from "../../utils/api-util.js";
import { ERROR } from "../../constants/constants.js";
import KioskModel from "./kiosk-model-v1.js";

const KioskController = {
  /**
   * 키오스크 디바이스 조회
   */
  list_kiosk_device: (req, res) => {
    const { place_id } = req.params;

    if (!place_id) {
      jsonRes(req, res, ERROR.INVALID_ARGUMENT, `Please provide place_id(${place_id}) for select`);
    } else {
      KioskModel.selectKioskDevices(place_id, (err, kiosk_devices) => {
        jsonRes(req, res, err, { kiosk_devices });
      });
    }
  },

  /**
   * ISC State 조회
   */
  list_kiosk_state: (req, res) => {
    const { place_id } = req.params;

    if (!place_id) {
      jsonRes(req, res, ERROR.INVALID_ARGUMENT, `Please provide place_id(${place_id}) for select`);
    } else {
      KioskModel.selectKioskStates(place_id, (err, kiosk_states) => {
        jsonRes(req, res, err, { kiosk_states });
      });
    }
  },

  /**
   * 키오스크 판매 조회
   */
  list_kiosk_sale_pay_sum: (req, res) => {
    const { place_id, begin, end } = req.params;

    if (!place_id || !begin || !end) {
      jsonRes(req, res, ERROR.INVALID_ARGUMENT, `Please provide place_id(${place_id}), begin(${begin}), end(${end}) for select`);
    } else {
      KioskModel.selectKioskSalePaySum(place_id, begin, end, (err, kiosk_sale_pay_sums) => {
        jsonRes(req, res, err, { kiosk_sale_pay_sums });
      });
    }
  },
};

export default KioskController;
