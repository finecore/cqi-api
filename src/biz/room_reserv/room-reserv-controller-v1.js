import { jsonRes, sendRes, isValidToken, checkPermission } from "../../utils/api-util.js";
import { ERROR } from "../../constants/constants.js";
import RoomReservModel from "./room-reserv-model-v1.js";

const RoomReservController = {
  /**
   * [업소] 예약 조회 (2023.09.27 ej)
   */
  list_room_reservs: (req, res) => {
    const { place_id, begin, end } = req.params;

    if (!place_id || !begin || !end) {
      jsonRes(req, res, ERROR.INVALID_ARGUMENT, `Please provide place_id(${place_id}), begin(${begin}), end(${end}) for select`);
    } else {
      RoomReservModel.selectRoomReservs(place_id, begin, end, (err, room_reservs) => {
        jsonRes(req, res, err, { room_reservs });
      });
    }
  },

  /**
   * [일] 예약 건수 조회.
   */
  list_reserv_count_day: (req, res) => {
    const { place_id, begin, end } = req.params;

    if (!place_id || !begin || !end) {
      jsonRes(req, res, ERROR.INVALID_ARGUMENT, `Please provide place_id(${place_id}), begin(${begin}), end(${end}) for select`);
    } else {
      RoomReservModel.selectReservCountByDay(place_id, begin, end, (err, reserv_counts) => {
        jsonRes(req, res, err, { reserv_counts });
      });
    }
  },

  /**
   * [주] 예약 건수 조회.
   */
  list_reserv_count_week: (req, res) => {
    const { place_id, begin, end } = req.params;

    if (!place_id || !begin || !end) {
      jsonRes(req, res, ERROR.INVALID_ARGUMENT, `Please provide place_id(${place_id}), begin(${begin}), end(${end}) for select`);
    } else {
      RoomReservModel.selectReservCountByWeek(place_id, begin, end, (err, reserv_counts) => {
        jsonRes(req, res, err, { reserv_counts });
      });
    }
  },

  /**
   * [월] 예약 건수 조회.
   */
  list_reserv_count_month: (req, res) => {
    const { place_id, begin, end } = req.params;

    if (!place_id || !begin || !end) {
      jsonRes(req, res, ERROR.INVALID_ARGUMENT, `Please provide place_id(${place_id}), begin(${begin}), end(${end}) for select`);
    } else {
      RoomReservModel.selectReservCountByMonth(place_id, begin, end, (err, reserv_counts) => {
        jsonRes(req, res, err, { reserv_counts });
      });
    }
  },

  /**
   * [기간별] APP 예약 건수 조회.
   */
  list_ota_reserv_count: (req, res) => {
    const { place_id, begin, end, unit } = req.params;

    if (!place_id || !begin || !end) {
      jsonRes(req, res, ERROR.INVALID_ARGUMENT, `Please provide place_id(${place_id}), begin(${begin}), end(${end}), unit(${unit}) for select`);
    } else {
      RoomReservModel.selectOtaReservCount(place_id, begin, end, unit, (err, ota_reserv_counts) => {
        jsonRes(req, res, err, { ota_reserv_counts });
      });
    }
  },

  /**
   * [기간별] 숙발형태별 예약 건수 조회.
   */
  list_stay_reserv_count: (req, res) => {
    const { place_id, begin, end, unit } = req.params;

    if (!place_id || !begin || !end) {
      jsonRes(req, res, ERROR.INVALID_ARGUMENT, `Please provide place_id(${place_id}), begin(${begin}), end(${end}), unit(${unit}) for select`);
    } else {
      RoomReservModel.selectStayReservCount(place_id, begin, end, unit, (err, stay_reserv_counts) => {
        jsonRes(req, res, err, { stay_reserv_counts });
      });
    }
  },
};

export default RoomReservController;
