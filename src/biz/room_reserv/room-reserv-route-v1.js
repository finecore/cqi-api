import { isValidToken, checkPermission } from "../../utils/api-util.js";
import RoomReservController from "./room-reserv-controller-v1.js";

export default (app) => {
  /**
   * [업소] 예약 조회 (2023.09.27 ej)
   */
  app.route("/v1/room_reserv/:place_id/:begin/:end").get(isValidToken, RoomReservController.list_room_reservs);

  /**
   * [일] 예약 건수 조회.
   */
  app.route("/v1/room_reserv/countday/:place_id/:begin/:end").get(isValidToken, RoomReservController.list_reserv_count_day);

  /**
   * [주] 예약 건수 조회.
   */
  app.route("/v1/room_reserv/countweek/:place_id/:begin/:end").get(isValidToken, RoomReservController.list_reserv_count_week);

  /**
   * [월] 예약 건수 조회.
   */
  app.route("/v1/room_reserv/countmonth/:place_id/:begin/:end").get(isValidToken, RoomReservController.list_reserv_count_month);

  /**
   * [기간별] APP 예약 건수 조회.
   */
  app.route("/v1/room_reserv/otacount/:place_id/:begin/:end/:unit").get(isValidToken, RoomReservController.list_ota_reserv_count);

  /**
   * [기간별] APP 예약 건수 조회.
   */
  app.route("/v1/room_reserv/staycount/:place_id/:begin/:end/:unit").get(isValidToken, RoomReservController.list_stay_reserv_count);
};
