import connection from "../../db/connection.js";

const RoomReservModel = {
  /**
   * [업소] 예약 조회 (2023.09.27 ej)
   */
  selectRoomReservs: (place_id, begin, end, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*
                , b.name room_name
                , b.floor
                , c.name room_type_name
                , c.default_fee_stay
                , c.default_fee_rent
                , c.reserv_discount_fee_stay
                , c.reserv_discount_fee_rent
            FROM room_reserv a
                  LEFT JOIN room b       ON a.room_id = b.id
                  LEFT JOIN room_type c  ON a.room_type_id = c.id
            WHERE a.place_id      =  ?
              AND a.check_in_exp  >= date_format(?, '%Y-%m-%d')
              AND a.check_in_exp  <  date_add(date_format(?, '%Y-%m-%d'), INTERVAL 1 DAY)
            ORDER BY a.check_in_exp
          ;`,
          [place_id, begin, end],
          result
        );
      }
    });
  },

  /**
   * [일] 예약 건수 조회.
   */
  selectReservCountByDay: (place_id, begin, end, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.place_id
                , DATE_FORMAT(a.check_in_exp, '%e')  AS group1  -- day
                , DATE_FORMAT(a.check_in_exp, '%k')  AS group2  -- 24Hour
                , COUNT(*)  AS count
            FROM room_reserv a
            WHERE a.place_id      =  ?
              AND a.state         != 'B'
              AND a.check_in_exp  >= DATE_FORMAT(?, '%Y-%m-%d')
              AND a.check_in_exp  <  DATE_ADD(DATE_FORMAT(?, '%Y-%m-%d'), INTERVAL 1 DAY)
            GROUP BY a.place_id, DATE_FORMAT(a.check_in_exp, '%e'), DATE_FORMAT(a.check_in_exp, '%k')
            ORDER BY 1 , 2 , 3
          ;`,
          [place_id, begin, end],
          result
        );
      }
    });
  },

  /**
   * [주] 예약 건수 조회.
   */
  selectReservCountByWeek: (place_id, begin, end, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.place_id
                , date_format(a.check_in_exp, '%v')  AS group1  -- year_week_num
                , date_format(a.check_in_exp, '%w')  AS group2  -- week_num (0=Sunday..6=Saturday)
                , count(*)  AS count
            FROM room_reserv a
            WHERE a.place_id      =  ?
              AND a.state         != 'B'
              AND a.check_in_exp  >= date_format(?, '%Y-%m-%d')
              AND a.check_in_exp  <  date_add(date_format(?, '%Y-%m-%d'), INTERVAL 1 DAY)
            GROUP BY a.place_id, date_format(a.check_in_exp, '%v'), date_format(a.check_in_exp, '%w')
            ORDER BY 1, 2, 3
          ;`,
          [place_id, begin, end],
          result
        );
      }
    });
  },

  /**
   * [월] 예약 건수 조회.
   */
  selectReservCountByMonth: (place_id, begin, end, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.place_id
                , date_format(a.check_in_exp, '%c')  AS group1  -- month(0..12)
                , date_format(a.check_in_exp, '%e')  AS group2  -- day(0..31)
                , count(*)  AS count
            FROM room_reserv a
            WHERE a.place_id      =  ?
              AND a.state         != 'B'
              AND a.check_in_exp  >= date_format(?, '%Y-%m-%d')
              AND a.check_in_exp  <  date_add(date_format(?, '%Y-%m-%d'), INTERVAL 1 DAY)
            GROUP BY a.place_id, date_format(a.check_in_exp, '%c'), date_format(a.check_in_exp, '%e')
            ORDER BY 1, 2, 3
          ;`,
          [place_id, begin, end],
          result
        );
      }
    });
  },

  /**
   * [기간별] APP 예약 건수 조회.
   */
  selectOtaReservCount: (place_id, begin, end, unit, result) => {
    let unitFormat = "%j";
    if (unit === "j") unitFormat = "%j"; //DAY_OF_YEAR (1..366)
    else if (unit === "v") unitFormat = "%v"; //WEEK_OF_YEAR (1..53)
    else if (unit === "Ym") unitFormat = "%Y%m"; //MONTH_OF_YEAR (..202301..202312..)

    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.place_id
                , CAST(MAX(DATE_FORMAT(a.check_in_exp, '${unitFormat}')) AS UNSIGNED)  AS group_num
                , COUNT(*)  AS total_count
                , COUNT(case when a.ota_code =  1 then 1 end)  AS ota_1_count
                , COUNT(case when a.ota_code =  2 then 1 end)  AS ota_2_count
                , COUNT(case when a.ota_code =  3 then 1 end)  AS ota_3_count
                , COUNT(case when a.ota_code >= 4 then 1 end)  AS ota_etc_count
             FROM room_reserv a
            WHERE a.place_id      =  ?
              AND a.state         != 'B'
              AND a.check_in_exp  >= DATE_FORMAT(?, '%Y-%m-%d')
              AND a.check_in_exp  <  DATE_ADD(DATE_FORMAT(?, '%Y-%m-%d'), INTERVAL 1 DAY)
            GROUP BY a.place_id, DATE_FORMAT(a.check_in_exp, '${unitFormat}')
            ORDER BY 1, 2
          ;`,
          [place_id, begin, end],
          result
        );
      }
    });
  },

  /**
   * [기간별] 숙박형태별 예약 건수 조회.
   */
  selectStayReservCount: (place_id, begin, end, unit, result) => {
    let unitFormat = "%j";

    if (unit === "j") unitFormat = "%j"; //DAY_OF_YEAR (1..366)
    else if (unit === "v") unitFormat = "%v"; //WEEK_OF_YEAR (1..53)
    else if (unit === "Ym") unitFormat = "%Y%m"; //MONTH_OF_YEAR (..202301..202312..)

    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.place_id
                , CAST(MAX(DATE_FORMAT(a.check_in_exp, '${unitFormat}')) AS UNSIGNED)  AS group_num
                , COUNT(*)  AS total_count
                , COUNT(case when a.stay_type =  1 then 1 end)  AS stay_1_count
                , COUNT(case when a.stay_type =  2 then 1 end)  AS stay_2_count
                , COUNT(case when a.stay_type =  3 then 1 end)  AS stay_3_count
            FROM room_reserv a
            WHERE a.place_id      =  ?
              AND a.state         != 'B'
              AND a.check_in_exp  >= DATE_FORMAT(?, '%Y-%m-%d')
              AND a.check_in_exp  <  DATE_ADD(DATE_FORMAT(?, '%Y-%m-%d'), INTERVAL 1 DAY)
            GROUP BY a.place_id, DATE_FORMAT(a.check_in_exp, '${unitFormat}')
          ;`,
          [place_id, begin, end],
          result
        );
      }
    });
  },
};

export default RoomReservModel;
