import { isValidToken, checkPermission } from "../../utils/api-util.js";
import RoomSaleController from "./room-sale-controller-v1.js";

export default (app) => {
  /**
   * 업소 기간별 매출
   */
  app.route("/v1/place_sale/range_sums/:place_id/:begin/:end/:unit").get(isValidToken, RoomSaleController.list_sale_range_sums);

  /**
   * [업소] 기간별 처리 현황 (2023.09.27 ej)
   */
  app.route("/v1/place_sale/state_sums/:place_id/:begin/:end").get(isValidToken, RoomSaleController.list_place_sale_status_sums);

  /**
   * [업소] 기간별 매출 (2023.09.27 ej)
   */
  app.route("/v1/place_sale/sums/:place_id/:begin/:end").get(isValidToken, RoomSaleController.list_place_sale_sums);

  /**
   * [객실] 기간별 매출 조회 (2023.09.25 ej)
   */
  app.route("/v1/room_sale/sums/:place_id/:begin/:end").get(isValidToken, RoomSaleController.list_room_sale_sums);

  /**
   * [객실,숙박형태] 기간별 매출 조회 (2023.09.25 ej)
   */
  app.route("/v1/room_sale/stay_sums/:place_id/:begin/:end").get(isValidToken, RoomSaleController.list_room_sale_stay_sums);

  app.route("/v1/room_sale/:room_id");
};
