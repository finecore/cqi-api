import { jsonRes, sendRes, isValidToken, checkPermission } from "../../utils/api-util.js";
import { ERROR } from "../../constants/constants.js";
import RoomSaleModel from "./room-sale-model-v1.js";

const RoomSaleController = {
  /**
   * 업소 기간별 매출
   */
  list_sale_range_sums: (req, res) => {
    const { place_id, begin, end, unit } = req.params;

    if (!place_id || !begin || !end) {
      jsonRes(req, res, ERROR.INVALID_ARGUMENT, `Please provide place_id(${place_id}), begin(${begin}), end(${end}), unit(${unit}) for select`);
    } else {
      RoomSaleModel.selectSaleRangeSums(place_id, begin, end, unit, (err, sale_range_sums) => {
        jsonRes(req, res, err, { sale_range_sums });
      });
    }
  },

  /**
   * [업소] 기간별 처리 현황 (2023.09.27 ej)
   */
  list_place_sale_status_sums: (req, res) => {
    const { place_id, begin, end } = req.params;

    if (!place_id || !begin || !end) {
      jsonRes(req, res, ERROR.INVALID_ARGUMENT, `Please provide place_id(${place_id}), begin(${begin}), end(${end}) for select`);
    } else {
      RoomSaleModel.selectPlaceSaleStateSums(place_id, begin, end, (err, place_state_sums) => {
        jsonRes(req, res, err, { place_state_sums });
      });
    }
  },

  /**
   * [업소] 기간별 매출 (2023.09.27 ej)
   */
  list_place_sale_sums: (req, res) => {
    const { place_id, begin, end } = req.params;

    if (!place_id || !begin || !end) {
      jsonRes(req, res, ERROR.INVALID_ARGUMENT, `Please provide place_id(${place_id}), begin(${begin}), end(${end}) for select`);
    } else {
      RoomSaleModel.selectPlaceSaleSums(place_id, begin, end, (err, place_sums) => {
        jsonRes(req, res, err, { place_sums });
      });
    }
  },

  /**
   * [객실] 기간별 매출 조회 (2023.09.25 ej)
   */
  list_room_sale_sums: (req, res) => {
    const { place_id, begin, end } = req.params;

    if (!place_id || !begin || !end) {
      jsonRes(req, res, ERROR.INVALID_ARGUMENT, `Please provide place_id(${place_id}), begin(${begin}), end(${end}) for select`);
    } else {
      RoomSaleModel.selectRoomSaleSums(place_id, begin, end, (err, room_sums) => {
        jsonRes(req, res, err, { room_sums });
      });
    }
  },

  /**
   * [객실,숙박형태] 기간별 매출 조회 (2023.09.25 ej)
   */
  list_room_sale_stay_sums: (req, res) => {
    const { place_id, begin, end } = req.params;

    if (!place_id || !begin || !end) {
      jsonRes(req, res, ERROR.INVALID_ARGUMENT, `Please provide place_id(${place_id}), begin(${begin}), end(${end}) for select`);
    } else {
      RoomSaleModel.selectRoomSaleStaySums(place_id, begin, end, (err, room_sums) => {
        jsonRes(req, res, err, { room_sums });
      });
    }
  },
};

export default RoomSaleController;
