import connection from "../../db/connection.js";

const RoomSaleModel = {
  /**
   * 업소 기간별 매출
   * type: @/types/sale.d.ts => SaleRangeSum
   */
  selectSaleRangeSums: (place_id, begin, end, unit, result) => {
    let group = "%j";
    let gropuSub = "%e";
    let groupDisplay = "%c/%e";

    if (unit === "j") {
      group = "%j"; //DAY_OF_YEAR (001..366)
      gropuSub = "%e"; //0..31
      groupDisplay = "%c/%e"; //{0..12/0..31}
    } else if (unit === "v") {
      group = "%V"; //WEEK_OF_YEAR (01..53)
      gropuSub = "%V"; //01..53
      groupDisplay = "%V주(%c/%e)"; //{3주(0..12/0..31)}
    } else if (unit === "Ym") {
      group = "%c"; //MONTH_OF_YEAR (0..12)
      gropuSub = "%c"; //0..12
      groupDisplay = "%y/%c월"; //{2023/0..12월}
    } else if (unit === "Y") {
      group = "%Y"; //YEAR (..2023..2023..)
      gropuSub = "%Y"; //2023..
      groupDisplay = "%Y년"; //2023..
    }

    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT b.place_id
                , cast(date_format(max(a.reg_date), '${group}') as unsigned)  AS group_num
                , cast(date_format(max(a.reg_date), '%Y') as unsigned)  AS group_year
                , cast(date_format(max(a.reg_date), '${gropuSub}') as unsigned)  AS group_sub
                , date_format(max(a.reg_date), '${groupDisplay}')  AS group_display
                , count(*)  AS count
                , sum(a.default_fee) + sum(a.add_fee) + sum(a.option_fee)  AS total_fee
                , sum(a.prepay_ota_amt)  AS prepay_ota_amt
                , sum(a.prepay_cash_amt)  AS prepay_cash_amt
                , sum(a.prepay_card_amt)  AS prepay_card_amt
                , sum(a.prepay_point_amt)  AS prepay_point_amt
                , sum(a.pay_cash_amt)  AS pay_cash_amt
                , sum(a.pay_card_amt)  AS pay_card_amt
                , sum(a.pay_point_amt)  AS pay_point_amt
                , sum(a.prepay_ota_amt)
                  + sum(a.prepay_cash_amt) + sum(a.prepay_card_amt) + sum(a.prepay_point_amt)
                      + sum(a.pay_cash_amt) + sum(a.pay_card_amt) + sum(a.pay_point_amt)  AS total_amt
              , (sum(a.default_fee) + sum(a.add_fee) + sum(a.option_fee)) -
                  (sum(a.prepay_ota_amt)
                    + sum(a.prepay_cash_amt) + sum(a.prepay_card_amt) + sum(a.prepay_point_amt)
                    + sum(a.pay_cash_amt) + sum(a.pay_card_amt) + sum(a.pay_point_amt))  AS unpaid_amt
            FROM room_sale a
                  INNER JOIN room b  ON a.room_id = b.id
            WHERE b.place_id  =  ?
              AND a.state     != 'B'
              AND a.reg_date  >= date_format(?, '%Y-%m-%d')
              AND a.reg_date  <  date_add(date_format(?, '%Y-%m-%d'), INTERVAL 1 DAY)
            GROUP BY b.place_id, date_format(a.reg_date, '${group}')
            ORDER BY 1, 2
          ;`,
          [place_id, begin, end],
          result
        );
      }
    });
  },

  /**
   * [업소] 기간별 처리 현황 (2023.09.27 ej)
   * type: @/types/sale.d.ts => PlaceSaleStateSum
   */
  selectPlaceSaleStateSums: (place_id, begin, end, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT b.place_id
                , date_format(a.reg_date, '%Y-%m-%d')  AS reg_date
                , count(*)  AS count
                , sum(CASE WHEN a.state = 'A' THEN 1 ELSE 0 END)  AS a_count
                , sum(CASE WHEN a.state = 'B' THEN 1 ELSE 0 END)  AS b_count
                , sum(CASE WHEN a.state = 'C' THEN 1 ELSE 0 END)  AS c_count
            FROM room_sale a
                  INNER JOIN room b  ON a.room_id = b.id
            WHERE b.place_id  =  ?
            AND a.reg_date    >= date_format(?, '%Y-%m-%d')
            AND a.reg_date    <  date_add(date_format(?, '%Y-%m-%d'), INTERVAL 1 DAY)
            GROUP BY b.place_id, date_format(a.reg_date, '%Y-%m-%d')
            ORDER BY 1, 2
          ;`,
          [place_id, begin, end],
          result
        );
      }
    });
  },

  /**
   * [업소] 기간별 매출 (2023.09.27 ej)
   * type: @/types/sale.d.ts => PlaceSaleSum
   */
  selectPlaceSaleSums: (place_id, begin, end, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT b.place_id
                , date_format(a.reg_date, '%Y-%m-%d')  AS reg_date
                , count(*)  AS count
                , sum(a.default_fee) + sum(a.add_fee) + sum(a.option_fee)  AS total_fee
                , sum(a.prepay_ota_amt)  AS prepay_ota_amt
                , sum(a.prepay_cash_amt)  AS prepay_cash_amt
                , sum(a.prepay_card_amt)  AS prepay_card_amt
                , sum(a.prepay_point_amt)  AS prepay_point_amt
                , sum(a.pay_cash_amt)  AS pay_cash_amt
                , sum(a.pay_card_amt)  AS pay_card_amt
                , sum(a.pay_point_amt)  AS pay_point_amt
                , sum(a.prepay_ota_amt)
              + sum(a.prepay_cash_amt) + sum(a.prepay_card_amt) + sum(a.prepay_point_amt)
                  + sum(a.pay_cash_amt) + sum(a.pay_card_amt) + sum(a.pay_point_amt)  AS total_amt
              , (sum(a.default_fee) + sum(a.add_fee) + sum(a.option_fee) -
              (sum(a.prepay_ota_amt)
                + sum(a.prepay_cash_amt) + sum(a.prepay_card_amt) + sum(a.prepay_point_amt)
                + sum(a.pay_cash_amt) + sum(a.pay_card_amt) + sum(a.pay_point_amt))  AS unpaid_amt
             FROM room_sale a
                  INNER JOIN room b  ON a.room_id = b.id
            WHERE b.place_id  =  ?
              AND a.state     != 'B'
              AND a.reg_date  >= date_format(?, '%Y-%m-%d')
              AND a.reg_date  <  date_add(date_format(?, '%Y-%m-%d'), INTERVAL 1 DAY)
            GROUP BY b.place_id, date_format(a.reg_date, '%Y-%m-%d')
            ORDER BY 1, 2
          ;`,
          [place_id, begin, end],
          result
        );
      }
    });
  },

  /**
   * [객실] 기간 매출 (2023.09.24 ej)
   * type: @/types/sale.d.ts => RoomSaleSum
   */
  selectRoomSaleSums: (place_id, begin, end, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT room_id
                , b.name  AS room_name
                , COUNT(*) count
                , SUM(case when prepay_ota_amt > 0 then 1 else 0 end)  AS prepay_ota_cnt
                , SUM(case when prepay_cash_amt > 0 then 1 else 0 end)  AS prepay_cash_cnt
                , SUM(case when prepay_card_amt > 0 then 1 else 0 end)  AS prepay_card_cnt
                , SUM(case when prepay_point_amt > 0 then 1 else 0 end)  AS prepay_point_cnt
                , SUM(case when pay_cash_amt > 0 then 1 else 0 end)  AS pay_cash_cnt
                , SUM(case when pay_card_amt > 0 then 1 else 0 end)  AS pay_card_cnt
                , SUM(case when pay_point_amt > 0 then 1 else 0 end)  AS pay_point_cnt
                , SUM(default_fee)  AS default_fee
                , SUM(add_fee)  AS add_fee
                , sum(a.option_fee)  AS option_fee
                , SUM(prepay_ota_amt)  AS prepay_ota_amt
                , SUM(prepay_cash_amt)  AS prepay_cash_amt
                , SUM(prepay_card_amt)  AS prepay_card_amt
                , SUM(prepay_point_amt)  AS prepay_point_amt
                , SUM(pay_cash_amt)  AS pay_cash_amt
                , SUM(pay_card_amt)  AS pay_card_amt
                , SUM(pay_point_amt)  AS pay_point_amt
                , SUM(default_fee) + SUM(add_fee) + sum(a.option_fee)  AS total_fee
                , SUM(prepay_ota_amt) + SUM(prepay_cash_amt) + SUM(prepay_card_amt) + SUM(prepay_point_amt) + SUM(pay_cash_amt) + SUM(pay_card_amt) + SUM(pay_point_amt)  AS total_amt
                , (SUM(default_fee) + SUM(add_fee) + sum(a.option_fee))
                  - (SUM(prepay_ota_amt) + SUM(prepay_cash_amt) + SUM(prepay_card_amt) + SUM(prepay_point_amt) + SUM(pay_cash_amt) + SUM(pay_card_amt) + SUM(pay_point_amt))  AS unpaid_amt
            FROM room_sale a
                  INNER JOIN room b ON a.room_id = b.id
            WHERE b.place_id  = ?
              AND state      != 'B'		-- 판매 상태 (A: 정상, B: 취소, C: 완료)
              AND a.check_in >= date_format(?, '%Y-%m-%d')
              AND a.check_in <  date_add(date_format(?, '%Y-%m-%d'), interval 1 day)
            GROUP BY room_id
            ORDER BY length(b.name), b.name
            ;`,
          [place_id, begin, end],
          result
        );
      }
    });
  },

  /**
   * [객실,투숙형태] 기간 매출 (2023.09.24 ej)
   * type: @/types/sale.d.ts => RoomSaleSum
   */
  selectRoomSaleStaySums: (place_id, begin, end, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT room_id
                , b.name  AS room_name
                , stay_type  -- 투숙형태 (1:숙박, 2:대실, 3:장기)
                , COUNT(*)  AS count
                , SUM(case when prepay_ota_amt > 0 then 1 else 0 end)  AS prepay_ota_cnt
                , SUM(case when prepay_cash_amt > 0 then 1 else 0 end)  AS prepay_cash_cnt
                , SUM(case when prepay_card_amt > 0 then 1 else 0 end)  AS prepay_card_cnt
                , SUM(case when prepay_point_amt > 0 then 1 else 0 end)  AS prepay_point_cnt
                , SUM(case when pay_cash_amt > 0 then 1 else 0 end)  AS pay_cash_cnt
                , SUM(case when pay_card_amt > 0 then 1 else 0 end)  AS pay_card_cnt
                , SUM(case when pay_point_amt > 0 then 1 else 0 end)  AS pay_point_cnt
                , SUM(default_fee)  AS default_fee
                , SUM(add_fee)  AS add_fee
                , sum(a.option_fee)  AS option_fee
                , SUM(prepay_ota_amt)    AS prepay_ota_amt
                , SUM(prepay_cash_amt)   AS prepay_cash_amt
                , SUM(prepay_card_amt)   AS prepay_card_amt
                , SUM(prepay_point_amt)  AS prepay_point_amt
                , SUM(pay_cash_amt)   AS pay_cash_amt
                , SUM(pay_card_amt)   AS pay_card_amt
                , SUM(pay_point_amt)  AS pay_point_amt
                , SUM(default_fee) + SUM(add_fee) + SUM(option_fee)  AS total_fee
                , SUM(prepay_ota_amt) + SUM(prepay_cash_amt) + SUM(prepay_card_amt) + SUM(prepay_point_amt) + SUM(pay_cash_amt) + SUM(pay_card_amt) + SUM(pay_point_amt)  AS total_amt
            FROM room_sale a
                  INNER JOIN room b ON a.room_id = b.id
            WHERE b.place_id  = ?
              AND state      != 'B'		-- 판매 상태 (A: 정상, B: 취소, C: 완료)
              AND a.check_in >= date_format(?, '%Y-%m-%d')
              AND a.check_in <  date_add(date_format(?, '%Y-%m-%d'), interval 1 day)
            GROUP BY room_id, stay_type
          ;`,
          [place_id, begin, end],
          result
        );
      }
    });
  },

  /**
   * 판매 지불 조회
   * type: @/types/sale.d.ts => SalePaySum
   */
  selectSalePaySum: (place_id, room_id = null, begin, end, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.id    AS sale_id
                , a.serialno
                , a.room_id
                , b.name  AS room_name
                , c.name  AS type_name
                , b.floor
                , a.state
                , a.stay_type
                , CASE WHEN a.check_in  IS NOT NULL THEN a.check_in  ELSE a.check_in_exp  END  AS check_in
                , CASE WHEN a.check_out IS NOT NULL THEN a.check_out ELSE a.check_out_exp END  AS check_out
                , a.reg_date
                , a.car_no
                , a.default_fee
                , a.add_fee
                , a.option_fee
                , 1  AS count  -- room_sale 기준이므로 판매수는 1임.
                , (a.default_fee + a.add_fee + a.option_fee)  AS total_fee
                , (a.prepay_ota_amt + a.prepay_cash_amt + a.prepay_card_amt + a.prepay_point_amt + a.pay_cash_amt + a.pay_card_amt + a.pay_point_amt)  AS total_amount
                , (a.default_fee + a.add_fee + a.option_fee)
                  - (a.prepay_ota_amt + a.prepay_cash_amt + a.prepay_card_amt + a.prepay_point_amt + a.pay_cash_amt + a.pay_card_amt + a.pay_point_amt)  AS unpaid_amount
                , a.prepay_ota_amt
                , a.prepay_cash_amt
                , a.prepay_card_amt
                , a.prepay_point_amt
                , a.pay_cash_amt
                , a.pay_card_amt
                , a.pay_point_amt
                , d.*
            FROM room_sale a
                  INNER JOIN room b      ON  a.room_id  =  b.id
                  LEFT JOIN room_type c  ON  b.room_type_id  =  c.id
                  LEFT JOIN (
                     SELECT sale_id
                          , count(*)  AS paid_count
                          , count(case when a.prepay_ota_amt > 0 then 1 end)    AS prepay_ota_cnt
                          , count(case when a.prepay_cash_amt > 0 then 1 end)   AS prepay_cash_cnt
                          , count(case when a.prepay_card_amt > 0 then 1 end)   AS prepay_card_cnt
                          , count(case when a.prepay_point_amt > 0 then 1 end)  AS prepay_point_cnt
                          , count(case when a.pay_cash_amt > 0 then 1 end)   AS pay_cash_cnt
                          , count(case when a.pay_card_amt > 0 then 1 end)   AS pay_card_cnt
                          , count(case when a.pay_point_amt > 0 then 1 end)  AS pay_point_cnt
                          , sum(a.prepay_ota_amt)    AS total_prepay_ota_amt
                          , sum(a.prepay_cash_amt)   AS total_prepay_cash_amt
                          , sum(a.prepay_card_amt)   AS total_prepay_card_amt
                          , sum(a.prepay_point_amt)  AS total_prepay_point_amt
                          , sum(a.pay_cash_amt)   AS total_pay_cash_amt
                          , sum(a.pay_card_amt)   AS total_pay_card_amt
                          , sum(a.pay_point_amt)  AS total_pay_point_amt
                       FROM room_sale_pay a
                            INNER JOIN device b  ON  b.serialno  =  a.serialno
                      WHERE b.place_id  =  ?
                        AND a.serialno  IS NOT NULL
                        AND a.state     != 'B'
                        AND a.reg_date  >= date_format(?, '%Y-%m-%d')
                        AND a.reg_date  <  date_add(date_format(?, '%Y-%m-%d'), INTERVAL 1 DAY)
                      GROUP BY sale_id
                            ) d  ON  d.sale_id  =  a.id
            WHERE b.place_id      =  ?
              ${room_id !== null ? " AND a.room_id = ? " : ""}
              AND a.serialno      IS NOT NULL
              AND a.state         != 'B'
              AND a.check_in_exp  >= date_format(?, '%Y-%m-%d')
              AND a.reg_date      <  date_add(date_format(?, '%Y-%m-%d'), INTERVAL 1 DAY)
            ORDER BY a.reg_date
          ;`,
          [place_id, begin, end, place_id, begin, end],
          result
        );
      }
    });
  },

  selectRoomSale: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*
                , b.name room_name
                , b.room_type_id
                , b.place_id
             FROM room_sale a
                  LEFT JOIN room b  ON  a.room_id  =  b.id
            WHERE a.id  =  ?
            ;`,
          id,
          result
        );
      }
    });
  },
};

export default RoomSaleModel;
