module.exports = {
  apps: [
    {
      name: 'cqi-api',
      script: 'build/index.js',
      error_file: '/dev/null',
      node_args: '--experimental-modules',
      exec_mode: 'cluster',
      instances: '2', // 클러스터 모드에서 2개의 인스턴스 실행
      env: {
        NODE_ENV: 'production',
      },
    },
  ],
};
