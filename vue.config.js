// vue.config.js
import path from 'path';

export default {
  configureWebpack: {
    resolve: {
      alias: {
        '@': path.resolve(__dirname, 'src'), // '@'를 'src' 폴더로 설정
      },
    },
    devServer: {},
    server: {
      host: 'localhost',
      port: 4001,
    },
    preview: {
      host: 'localhost',
      post: 4001,
    },
    publicDir: 'public', // 이 부분이 "src/public"이면 "public"으로 변경 필요
    base: '/',
    build: {
      outDir: path.resolve(__dirname, 'dist'),
    },
  },
};
