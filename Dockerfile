# Node.js 이미지 선택
FROM node:20

# 작업 디렉토리 설정
WORKDIR /Users/friendkj/src/app/api

# package.json과 yarn.lock 파일을 먼저 복사하고 의존성 설치
COPY package.json yarn.lock ./
RUN yarn install

# 현재 디렉토리의 모든 파일을 컨테이너의 /app 디렉토리로 복사
COPY . .

# 빌드 실행
RUN yarn build

# 포트 설정 - 4001 포트를 컨테이너에서 노출
EXPOSE 4001

# wait-for-it.sh 스크립트를 프로젝트 폴더에 복사
COPY wait-for-it.sh /wait-for-it.sh
RUN chmod +x /wait-for-it.sh

# 애플리케이션 시작 명령 (오타 수정 및 경로 확인)
CMD ["bash", "/wait-for-it.sh", "mysql_container:3306", "--", "node", "build/index.js"]
