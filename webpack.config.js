import path from 'path';

import 'babel-polyfill';

export default {
  context: __dirname,
  devtool: 'source-map',
  entry: ['babel-polyfill', './src/index.js'],
  output: {
    filename: './build/build.js',
    path: path.resolve(__dirname),
  },
  serve: {},
  rules: [
    {
      test: /\.js$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env'],
        },
      },
    },
  ],
};
